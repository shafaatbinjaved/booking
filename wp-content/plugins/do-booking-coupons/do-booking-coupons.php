<?php
/*
Plugin Name: Do Booking Coupons
Plugin URI: https://sjm.com/
Description: Do Booking plugin addon coupons.
Version: 1.0
Author: Shafaat Javed
Author URI: https://shafaat.com/booking-plugin/
License: GPLv2 or later
Text Domain: rbdobooking
Domain Path: /languages
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Don't allow multiple versions to be active
if ( class_exists( 'RBDoBooking_Coupons_Addon' ) ) {

} else {
    class RBDoBooking_Coupons_Addon {
        /**
         * One is the loneliest number that you'll ever do.
         *
         * @since 1.0.0
         *
         * @var object
         */
        private static $instance;

        /**
         * Addon version for enqueueing, etc.
         *
         * @since 1.0.0
         *
         * @var string
         */
        public $addon_version = '1.0.0';

        /**
         * Main RBDoBooking Coupons addon Instance.
         *
         * Insures that only one instance of RBDoBooking Coupons addon exists in memory at any one
         * time. Also prevents needing to define globals all over the place.
         *
         * @since 1.0.0
         *
         * @return RBDoBooking
         */

        public static function instance() {

            if (!isset( self::$instance ) && !(self::$instance instanceof RBDoBooking_Coupons_Addon)) {

                self::$instance = new RBDoBooking_Coupons_Addon();
                self::$instance->constants();
                self::$instance->load_textdomain();
                self::$instance->includes();

                add_action( 'plugins_loaded', array( self::$instance, 'objects' ), 10 );
            }
            return self::$instance;
        }

        /**
         * Setup plugin constants.
         *
         * @since 1.0.0
         * */
        private function constants() {
            // Plugin version.
            if ( ! defined( 'RBDOBOOKING_COUPONS_ADDON_VERSION' ) ) {
                define( 'RBDOBOOKING_COUPONS_ADDON_VERSION', $this->addon_version );
            }

            // Plugin Folder Path.
            if ( ! defined( 'RBDOBOOKING_COUPONS_ADDON_DIR' ) ) {
                define( 'RBDOBOOKING_COUPONS_ADDON_DIR', plugin_dir_path( __FILE__ ) );
            }

            // Plugin Folder URL.
            if ( ! defined( 'RBDOBOOKING_COUPONS_ADDON_URL' ) ) {
                define( 'RBDOBOOKING_COUPONS_ADDON_URL', plugin_dir_url( __FILE__ ) );
            }

            // Plugin Root File.
            if ( ! defined( 'RBDOBOOKING_COUPONS_ADDON_FILE' ) ) {
                define( 'RBDOBOOKING_COUPONS_ADDON_FILE', __FILE__ );
            }

            define( 'RBDOBOOKING_COUPONS_ADDON_SLUG', 'do-booking-coupons' );
        }

        /**
         * Loads the plugin language files.
         *
         * @since 1.0.0
         */
        public function load_textdomain() {

            load_plugin_textdomain( 'rbdobooking', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
        }

        /**
         * Include files.
         *
         * @since 1.0.0
         */
        private function includes() {
            require_once RBDOBOOKING_COUPONS_ADDON_DIR . 'class-coupons.php';
        }

        /**
         * @since 1.0.0
         */
        public function objects() {}
    }

    /**
     * The function which returns the one RBDoBooking coupons addon instance.
     *
     * @since 1.0.0
     * @return object
     */
    function rbdobooking_coupons() {

        return RBDoBooking_Coupons_Addon::instance();
    }

    function coupons_addon_init() {
        if (class_exists('RBDoBooking')) {
            rbdobooking_coupons();
        }
    }

    add_action('plugins_loaded', 'coupons_addon_init');
}
var RBDoBooking_Form_addon_coupon;
jQuery(document).ready(function() {
    RBDoBooking_Form_addon_coupon = new RBDoBooking_Form_Addon_Coupon();
});

function RBDoBooking_Form_Addon_Coupon() {

    var self = this;

    this.component = jQuery('#rbdobooking-booking-form-addon-coupon');

    this.coupon_error_placeholder = this.component.find('.coupon-error');

    this.btn_redeem_coupon = this.component.find('.btn-redeem-coupon');
    this.btn_redeem_coupon.off().on('click',function() {
        var coupon_code_btn = self.btn_redeem_coupon.closest('#rbdobooking-booking-form-addon-coupon').find('.coupon-code');
        var coupon_code = coupon_code_btn.val();
        if (coupon_code != "") {
            ajax_request_front(
                {
                    action: 'rbdobooking_form_addon_coupon',
                    security: RBDoBookingAjax.security_rbdobooking_form,
                    type: 'click_coupon_redeem',
                    vendor_id: RBDoBookingHelper.vendor_id,
                    form_id: RBDoBookingHelper.form_id,
                    coupon_code: coupon_code
                },
                function (response) {
                    if (response.success) {
                        var payment_column = self.component.closest('.payment-column');
                        payment_column.next().remove();
                        payment_column.after(response.data.html);
                        coupon_code_btn.val("");
                        self.coupon_error_placeholder.html("");
                    }
                    else {
                        if (response && response.data && response.data.msg) {
                            self.coupon_error_placeholder.html(response.data.msg);
                        }
                    }
                }
            );
        }
    });

}
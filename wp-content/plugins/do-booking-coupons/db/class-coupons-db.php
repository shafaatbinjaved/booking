<?php

class RBDoBooking_DB_Coupons {

	/**
	 * One is the loneliest number that you'll ever do.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * plugin prefix
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $plugin_prefix = 'rbdobooking_';

	public function __construct() {
	}

	/**
	 * Insures that only one instance of RBDoBooking DB Customers class exists in memory
	 *
	 * @since 1.0.0
	 *
	 * @return RBDoBooking_DB_Coupons
	 */
	public static function instance() {

		if ( !isset( self::$instance ) && !( self::$instance instanceof RBDoBooking_DB_Customers ) ) {

			global $wpdb;
			self::$instance = new RBDoBooking_DB_Coupons();
			self::$instance->plugin_prefix = $wpdb->prefix . self::$instance->plugin_prefix;

		} else {
			self::$instance;
		}
		return self::$instance;
	}

	public function getAllCoupons( $vendor_id ) {
		global $wpdb;

		$sql = 'SELECT a.*, GROUP_CONCAT( c.service_id ) as service_ids, GROUP_CONCAT( c.name ) as service_names
				FROM '.$this->plugin_prefix.'coupons a
				LEFT JOIN '.$this->plugin_prefix.'coupons_service b
				ON a.vendor_id = '.$vendor_id.'
				AND a.coupon_id = b.coupon_id
				LEFT JOIN '.$this->plugin_prefix.'services c
				ON c.service_id = b.service_id
				GROUP BY a.coupon_id';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

	public function getCouponDetails( $coupon_id ) {
		global $wpdb;

		$sql = 'SELECT a.*, GROUP_CONCAT( c.service_id ) as service_ids, GROUP_CONCAT( c.name ) as service_names
				FROM '.$this->plugin_prefix.'coupons a
				LEFT JOIN '.$this->plugin_prefix.'coupons_service b
				ON a.coupon_id = '.$coupon_id.'
				AND a.coupon_id = b.coupon_id
				LEFT JOIN '.$this->plugin_prefix.'services c
				ON c.service_id = b.service_id
				WHERE a.coupon_id = '.$coupon_id.'
				GROUP BY a.coupon_id';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

    /**
     * Get coupon details using coupon code and total coupon redeem
     *
     * @param $couponCode
     * @return array|null
     * @since 1.0.0
     */
	public function getCouponByCouponCodeAndTotalCouponRedeem($couponCode, $vendor_id) {
	    global $wpdb;

	    $sql = 'SELECT 
                    a.* , COUNT(b.coupon_id) as total_coupon_redeem
                FROM 
                    '.$this->plugin_prefix.'coupons a
                LEFT JOIN
                    '.$this->plugin_prefix.'orders_coupons b
                ON
                    a.coupon_id = b.coupon_id
                WHERE 
                    a.code = "'.$wpdb->_real_escape($couponCode).'"
                AND
                    a.vendor_id = ' . $vendor_id;

	    $result = $wpdb->get_results($sql, 'ARRAY_A');

	    foreach ($result as $row) {
	        return $row;
        }

	    return null;
    }

    /**
     * @param $coupon_id
     * @param $customer_id
     * @return bool
     */
    function isCouponAlreadyRedeemByCustomer($coupon_id, $customer_id) {
	    global $wpdb;

	    $sql = 'SELECT
	                *
	            FROM
	                '.$this->plugin_prefix.'orders a
	            INNER JOIN 
	                '.$this->plugin_prefix.'orders_coupons b
	            ON
	                a.customer_id = ' . $customer_id . '
	            AND 
	                a.order_id = b.order_id
	            AND
	                b.coupon_id = ' . $coupon_id;

        $result = $wpdb->get_results($sql, 'ARRAY_A');

        foreach ($result as $row) {
            return true;
        }

	    return false;
    }

    /**
     * Get coupon service ids
     *
     * @var int $coupon_id
     * @return array
     * @since 1.0.0
     */
	public function getCouponServiceIds($coupon_id) {
	    global $wpdb;

	    $sql = 'SELECT 
	                a.*
	            FROM 
	                '.$this->plugin_prefix.'coupons_service a
	            WHERE
	                a.coupon_id = '.$coupon_id;

	    $result = $wpdb->get_results($sql, 'ARRAY_A');

	    return $result;
    }

	/**
	 * Delete coupons with coupon_ids
	 *
	 * @var int $coupon_id
	 * @return bool|int
	 */
	function deleteCouponByCouponID( $coupon_id ) {
		global $wpdb;

		$query = null;

		if ( strpos($coupon_id,',') == false ) {
			$query = 'DELETE
						FROM '.$this->plugin_prefix.'coupons
						WHERE coupon_id = '.$coupon_id;
		}
		else {
			$query = 'DELETE
						FROM '.$this->plugin_prefix.'coupons 
						WHERE coupon_id IN ('.$coupon_id.')';
		}

		$this->deleteCouponServices( $coupon_id );

		return $wpdb->query($query);
	}

	/*
	 * Delete coupon attached with services
	 *
	 * @var int $coupon_id
	 * */
	function deleteCouponServices( $coupon_id ) {
		global $wpdb;

		$query = null;

		if ( strpos($coupon_id,',') == false ) {
			$query = 'DELETE
						FROM '.$this->plugin_prefix.'coupons_service
						WHERE coupon_id = '.$coupon_id;
		}
		else {
			$query = 'DELETE
						FROM '.$this->plugin_prefix.'coupons_service
						WHERE coupon_id IN ('.$coupon_id .')';
		}

		return $wpdb->query( $query );
	}

	function getOrderRedeemCoupons($order_id) {
	    global $wpdb;

	    $sql = '
	        SELECT
	            *
	        FROM 
	            '.$this->plugin_prefix.'orders_coupons a
	        INNER JOIN 
	            '.$this->plugin_prefix.'coupons b
	        ON
	            a.order_id = '.$order_id.'
	        AND
	            a.coupon_id = b.coupon_id 
	    ';

        return $wpdb->get_results($sql, 'ARRAY_A');
    }

}
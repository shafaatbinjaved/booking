<?php
/**
 * User: shafaatbinjaved
 * Date: 11/17/2019
 * Time: 2:49 PM
 */
?>
<div class="form-group" id="rbdobooking-booking-form-addon-coupon">
    <div class="row">
        <div class="col-6">
            <input type="text" class="form-control coupon-code" />
        </div>
        <div class="col-6 px-0">
            <button class="btn btn-sm btn-default btn-redeem-coupon theme-bg-color" type="button"
            ><?php _e('Redeem coupon','rbdobooking') ?></button>
        </div>
        <div class="col-12 coupon-error rbdobooking-error"></div>
    </div>
</div>
<script type="application/javascript">
    jQuery(document).ready(function() {
        var coupon_script_url = '<?php echo RBDOBOOKING_COUPONS_ADDON_URL . 'assets/js/do_booking_default_coupon.js' ?>';
        jQuery.getScript(coupon_script_url);
        //var coupon_script = jQuery("script[src$='do_booking_default_addon_coupon.js']");
        //console.log( coupon_script );
        //if (coupon_script.length == 0) {}
    });
</script>

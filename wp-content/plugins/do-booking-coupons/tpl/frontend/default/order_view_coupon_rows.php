<?php
/**
 * @var $subtotal
 * @var $only_tax
 * @var array $coupons
 * @var $currency
 */
$coupon_deduction = 0.0;
$percent_without_tax = $subtotal / ($subtotal+$only_tax);
foreach ($coupons as $coupon) {
    $coupon_deduction = $coupon_deduction + (float)$coupon['deduction'];
    $amt = $percent_without_tax*$coupon['deduction'];
    $coupons_deduction_without_tax = $amt;
    $coupons_deduction_only_tax = $coupon['deduction']-$amt;
    $subtotal -= $coupons_deduction_without_tax;
    $only_tax -= $coupons_deduction_only_tax;
    ?>
    <div class="order-subtotal row">
        <div class="col-6 left-div">
            <?php
            _e('Coupon: ','rbdobooking');
            echo $coupon['code']
            ?>
        </div>
        <div class="col-2 amount right-div">
            <?php
            echo '-'.$currency . $coupon['deduction']
            ?>
        </div>
        <div class="col-2"></div>
        <div class="col-2 amount right-div">
            <?php
            echo $currency . ($subtotal+$only_tax);
            ?>
        </div>
    </div>
    <?php
}
?>
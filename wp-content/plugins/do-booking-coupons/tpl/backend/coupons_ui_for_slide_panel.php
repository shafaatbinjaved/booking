<div class="coupons-ui-slidepanel slidepanel">
	<div class="row">
		<div class="col-12">
			<div class="row header">
				<div class="col-12">
					<div class="float-left">
						<h3 class="heading"
                            data-add_heading="<?php echo __('Add New Coupon','rbdobooking'); ?>"
                            data-edit_heading="<?php echo __('Edit Coupn'); ?>"
                        ></h3>
					</div>
					<div class="float-right">
						<i class="far fa-save btn-save" title="<?php echo __('Save','rbdbooking'); ?>"></i>
						<i class="fas fa-times btn-close" title="<?php echo __('Close','rbdbooking'); ?>"></i>
						<i class="fas fa-trash circle btn-delete" title="<?php echo __('Delete','rbdbooking'); ?>"></i>
					</div>
				</div>
			</div>
			<div class="row detail-part">
				<div class="col-1"></div>
				<div class="col-10">
					<div class="form-horizontal">

						<div class="form-group">
							<div class="row">
								<div class="col-12">
									<label class="control-label" for=""><?php echo __('Code','rbdobooking'); ?></label>
									<input type="text" class="form-control code"
									       data-name="code" value="" />
								</div>
							</div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label" for=""><?php echo __('Discount','rbdobooking'); ?> %</label>
                                    <input type="text" class="form-control discount"
                                           data-name="discount" value="" />
                                </div>
                                <div class="col-6">
                                    <label class="control-label" for=""><?php echo __('Deduction','rbdobooking'); ?></label>
                                    <input type="text" class="form-control deduction"
                                           data-name="deduction" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label" for=""><?php echo __('Service','rbdobooking'); ?></label>
                                    <select data-name="service_id" class="form-control service_id" multiple data-live-search="true">
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label class="control-label" for="" ><?php echo __('Usage limit','rbdobooking'); ?></label>
                                    <input type="text" class="form-control usage_limit"
                                           data-name="usage_limit" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label" for=""><?php echo __('Start time','rbdobooking'); ?></label>
                                    <input type="text" class="form-control start_time"
                                           data-name="start_time" value="" />
                                </div>
                                <div class="col-6">
                                    <label class="control-label" for=""><?php echo __('End time','rbdobooking'); ?></label>
                                    <input type="text" class="form-control end_time"
                                           data-name="end_time" value="" />
                                </div>
                            </div>
						</div>

					</div>
				</div>
				<div class="col-1"></div>
			</div>
		</div>
	</div>
</div>
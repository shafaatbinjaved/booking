<?php
/*
 * This tpl is used in 2 places.
 * i)  In order detail view in backend
 * ii) In download invoices addon
 */
$coupon_deduction_with_tax = $subtotal_with_tax;
foreach ($order_coupons as $coupon) {
    ?>
    <tr>
        <th colspan="4">
            <?php
                _e('Coupon','rbdobooking');
                echo ': ' . $coupon['code'];
            ?>
        </th>
        <th><?php echo '-'.$currency . $coupon['deduction'] ?></th>
        <th></th>
        <th>
            <?php
            echo $subtotal_with_tax - $coupon['deduction'];
            $coupon_deduction_with_tax -= $coupon['deduction'];
            ?>
        </th>
    </tr>
    <?php
}
?>
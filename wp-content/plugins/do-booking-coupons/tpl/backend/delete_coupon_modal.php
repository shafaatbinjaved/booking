<div class="modal fade del-coupon-confirm-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php _e('Delete coupons','rbdobooking'); ?></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">

				<div class="form-horizontal">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="coupon-1"><?php _e('Do you really want to delete this coupon ?','rbdobooking'); ?></div>
								<div class="coupon-multi" style="display: none;"><?php echo __('Do you really want to delete these coupons ?','rbdobooking'); ?></div>
								<div class="float-right">
									<button type="button" class="delete-coupon-confirm btn">
										<?php _e('Delete','rbdobooking'); ?>
									</button>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="rbdobooking-coupons" class="wrap rbdobooking-admin-wrap">

	<div class="container-fluid">

		<div class="rbdobooking-admin-content rbdobooking-admin-coupons">

			<div class="row rbdobooking-coupons-container">
				<div class="col-12">
					<div class="header">
						<div class="float-left">
							<h4><?php _e('Coupons','rbdobooking'); ?></h4>
						</div>
						<div class="clear"></div>
					</div>
					<div class="table-area">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>
                                    <input class="coupon-head-row-checkbox" type="checkbox" />
                                </th>
                                <th><?php _e('Edit','rbdobooking') ?></th>
                                <th><?php _e('Sr. No','rbdobooking') ?></th>
                                <th><?php _e('Code','rbdobooking') ?></th>
                                <th><?php _e('Discount','rbdobooking') ?></th>
                                <th><?php _e('Deduction','rbdobooking') ?></th>
                                <th><?php _e('Service','rbdobooking') ?></th>
                                <th><?php _e('Usage Limit','rbdobooking') ?></th>
                                <th><?php _e('Start time','rbdobooking') ?></th>
                                <th><?php _e('End time','rbdobooking') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php echo $coupon_rows; ?>
                            </tbody>
                        </table>
					</div>

                    <i class="fa fa-plus btn-add"
                       type="button"
                       title="<?php __("Add new coupon","rbdobooking") ?>"></i>

                    <i class="fas fa-trash btn-delete-multi"
                       type="button"
                       title="<?php __("Delete marked coupons","rbdobooking") ?>"></i>

				</div>
			</div>

		</div>

	</div>

    <?php echo $coupons_slidepanel; ?>
    <?php echo $coupon_del_modal; ?>

</div>
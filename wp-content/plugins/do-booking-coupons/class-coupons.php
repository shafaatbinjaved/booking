<?php

/**
 * Coupons backend class
 * @since 1.0.0
 **/

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';
require_once RBDOBOOKING_COUPONS_ADDON_DIR . 'db/class-coupons-db.php';

class RBDoBooking_Coupons {//} extends View {

    /**
     * @var View $view
     */
    private $view;

    /**
     * @var string $tplPath
     */
    private $tplPath;

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	/*
	 * Primary class constructor
	 * @since 1.0.0
	 * */
	public function __construct() {

		$this->tplPath = RBDOBOOKING_COUPONS_ADDON_DIR . 'tpl/';
		$this->view = new View($this->tplPath);

        // Let's make some menus.
        add_action( 'admin_menu', array( $this, 'register_menus' ), 9 );
        // Plugins page settings link.
        /*add_filter(
            'plugin_action_links_' . plugin_basename(RBDOBOOKING_PLUGIN_DIR . 'do_booking.php'),
            array( $this, 'settings_link' )
        );*/

		//Load coupons page
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_coupons',array($this,'coupons_ajax_handler'));

        add_action('wp_ajax_nopriv_rbdobooking_form_addon_coupon',array($this,'form_coupon_ajax_call'));
        add_action('wp_ajax_rbdobooking_form_addon_coupon',array($this,'form_coupon_ajax_call'));

		add_action('rbdobooking_coupons_view', array($this,'load_coupon_view'));
		//add_action('rbdobooking_enqueue_addons_script', array($this,'enqueue_shortcode_scripts'));

        add_action('rbdobooking_order_view_after_coupon_redeem', array($this, 'order_view_after_coupon_redeem'), 10, 5);

		add_filter('rbdobooking_deduct_order_coupons',array($this, 'deduct_order_coupons'), 10, 2);
		add_filter('rbdobooking_store_order_coupons',array($this, 'store_order_coupons'), 10, 3);

		add_action('rbdobooking_backend_order_coupon_rows', array($this,'backend_order_coupon_rows'), 10, 3);

		$this->db = RBDoBooking_DB::instance();
		$this->db->coupons = RBDoBooking_DB_Coupons::instance();
	}

	public function order_view_after_coupon_redeem($subtotal, $only_tax, $coupons, $currency, $tpl_theme = 'default') {
        $this->view->setViewLocation($this->tplPath);
	    echo $this->view->get_view(
	        'frontend/'.$tpl_theme.'/order_view_coupon_rows',
            false,
            [
                'subtotal'  => $subtotal,
                'only_tax'  => $only_tax,
                'coupons'   => $coupons,
                'currency'  => $currency
            ]
        );
    }

	public function backend_order_coupon_rows($orderId, $vendor_id, $subtotal_with_tax) {
        $order_coupons = [
            'order_coupons'     => $this->db->coupons->getOrderRedeemCoupons($orderId),
            'currency'          => $this->db->getVendorSpecificSettingValueByName('currency', $vendor_id),
            'subtotal_with_tax' => $subtotal_with_tax
        ];
        echo $this->view->get_view('backend/order/coupon.row', false, $order_coupons);
    }

	public function deduct_order_coupons($totalWithoutTaxAndOnlyTax, $userDataCoupons) {
        $totalWithoutTax = $totalWithoutTaxAndOnlyTax['total_without_tax'];
        $totalOnlyTax = $totalWithoutTaxAndOnlyTax['total_only_tax'];
        $percentWithoutTax = $totalWithoutTax/($totalWithoutTax+$totalOnlyTax);
        $couponsDeductionWithoutTax = 0.0;
        $couponsDeductionOnlyTax = 0.0;
        $couponIds = [];
        foreach ($userDataCoupons as $coupon) {
            $coupon['deduction'] = floatval($coupon['deduction']);
            $amt = $percentWithoutTax*$coupon['deduction'];
            $couponsDeductionWithoutTax += $amt;
            $couponsDeductionOnlyTax += $coupon['deduction']-$amt;
            $couponIds[] = $coupon['coupon_id'];
        }
        $totalWithoutTax -= $couponsDeductionWithoutTax;
        $totalOnlyTax -= $couponsDeductionOnlyTax;
        return [
            'total_without_tax' => $totalWithoutTax,
            'total_only_tax'    => $totalOnlyTax,
            'coupon_ids'        => $couponIds
        ];
    }

    public function store_order_coupons($totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds, $orderId, $creationTime) {
	    if (isset($totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds['coupon_ids'])) {
	        foreach ($totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds['coupon_ids'] as $couponId) {
                $orderCoupons = array(
                    'order_id'      => $orderId,
                    'coupon_id'     => $couponId,
                    'created_at'    => $creationTime
                );
                $this->db->insert('orders_coupons', $orderCoupons);
            }
        }
    }

    /**
     * load coupon view
     * @var string $tplname
     */
	public function load_coupon_view($tpl_theme = 'default') {
	    echo $this->view->get_view('frontend/'.$tpl_theme.'/redeem_coupon');
    }

    /**
     * Register menu for addon
     * @since 1.0.0
     */
    public function register_menus() {
        $menu_cap = rbdobooking_get_capability_manage_options();

        // Coupons sub menu item
        add_submenu_page(
            'rbdobooking-calendar',
            esc_html__( 'RB Do Boooking - Coupons', 'rbdobooking' ),
            esc_html__( 'Coupons', 'rbdobooking' ),
            $menu_cap,
            'rbdobooking-coupons',
            array( $this, 'admin_page' )
        );
    }

    /**
     * Wrapper for the hook to render our custom settings pages.
     *
     * @since 1.0.0
     */
    public function admin_page() {
        do_action('rbdobooking_admin_page');
    }

	/*
	 * Determine if the user is viewing the coupons page
	 * @since 1.0.0
	 * */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the customers page.
		if ( 'rbdobooking-coupons' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_coupons_init' );
		}
	}

    /**
     * Include files
     *
     * @since 1.0
     */
    private function includes() {
        //Admin dashboard only includes
        /*if (is_admin()) {
            require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';
        }
        //require_once RB
        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/db/class-coupons-db.php';*/
    }

    public function form_coupon_ajax_call() {//coupons ajax handler for frontend form only

        check_ajax_referer('rbdobooking-front-form-nonce','security');

        $vendor_id = isset($_POST['vendor_id']) ? $_POST['vendor_id'] : 1;
        $returned_data = null;
        $success = false;
        $msg = '';

        if ($_POST['type'] === 'click_coupon_redeem') {
            $coupon_code = $_POST['coupon_code'];
            $coupon_details = $this->db->coupons->getCouponByCouponCodeAndTotalCouponRedeem($coupon_code, $vendor_id);

            if ($coupon_details !== null) {
                $time = time();
                //checking coupon should be in duration and should be in usage limit
                if ($time >= strtotime($coupon_details['start_time']) &&
                    $time <= strtotime($coupon_details['end_time'])) {

                    if ($coupon_details['total_coupon_redeem'] <= $coupon_details['usage_limit']) {

                        $coupon_services = $this->db->coupons->getCouponServiceIds($coupon_details['coupon_id']);
                        $form_id = $_POST["form_id"];
                        $email = '';
                        $customer_id = 0;
                        $userData =  $this->db->getTempFormData($form_id);
                        if (is_null($userData)) {
                            return;
                        }
                        $userData = unserialize($userData['data']);
                        if (isset($userData['details']['details-email'])) {
                            $email = $userData['details']['details-email'];
                            $customer_id = $this->db->customer->checkCustomerExists($email);
                        }
                        if ($customer_id == 0 || !$this->db->coupons->isCouponAlreadyRedeemByCustomer($coupon_details['coupon_id'], $customer_id)) {
                            $service_ids = [];
                            $coupon_valid_for_service = false;
                            foreach ($userData['service'] as $service) {
                                $service_ids[] = $service['service_id'];
                            }
                            foreach ($coupon_services as $coupon_service) {
                                if (in_array($coupon_service['service_id'],$service_ids)) {
                                    $coupon_valid_for_service = true;
                                }
                            }
                            if ($coupon_valid_for_service) {
                                $userData['payment']['coupons'][] = $coupon_details;
                                $this->db->updateTempFormData($form_id, serialize($userData));
                                //...................
                                //redo order view
                                $cart_enable_disabled = $this->db->getVendorSpecificSettingValueByName( "cart_enable", $vendor_id );
                                $vendor_steps_configuration_with_template_name = $this->db->getVendorStepConfigurationWithTemplateName( $cart_enable_disabled, $vendor_id );
                                $template = $vendor_steps_configuration_with_template_name["template"];
                                if ($template == 'default') {
                                    $success = true;
                                    $start_time_of_first_item = null;
                                    $order_items = array();
                                    $cartItems = count($userData['time']);
                                    for ($iteration=0; $iteration <= $cartItems; $iteration++) {
                                        if ( isset($userData["time"][$iteration]) && isset($userData["service"][$iteration]) ) {
                                            if ($iteration == 0) {
                                                $start_time_of_first_item = $userData["time"][$iteration]["start"];
                                            }
                                            $service_name_member_price_name = $this->db->getServiceMemberPriceNameAndServiceName(
                                                $userData["time"][$iteration]["selected_time_member_id"],
                                                $userData["service"][$iteration]["service_id"]
                                            );
                                            if ( $service_name_member_price_name["member_price"] == "0" ) {
                                                $service_name_member_price_name["member_price"] = $service_name_member_price_name["service_price"];
                                            }
                                            array_push($order_items,$service_name_member_price_name);
                                        }
                                    }
                                    $order_view = $this->get_order_view_html_after_applying_coupon(
                                        $order_items,
                                        $userData,
                                        $start_time_of_first_item,
                                        $template,
                                        $vendor_id
                                    );
                                    $returned_data = array(
                                        'html'  =>  $order_view
                                    );
                                }
                                //.......................
                            }
                            else {
                                $msg = __('Coupon is not valid for service.','rbdobooking');
                            }
                        }
                        else {
                            $msg = __('You have already consumed this coupon.','rbdobooking');
                        }
                    }
                    else {
                        $msg = __('Coupon usage limit is finished.','rbdobooking');
                    }
                }
                else {
                    $msg = __('Coupon validity period is finished.','rbdobooking');
                }
            }
            else {
                $msg = __('Coupon is not valid.','rbdobooking');
            }
        }

        //$returned_data['success'] = $success;
        if ($success) {
            die(wp_send_json_success($returned_data));
        }
        $returned_data = [
            'msg' => $msg
        ];
        wp_send_json_error($returned_data);
    }

    private function get_order_view_html_after_applying_coupon($order_items, $userData, $start_time_of_first_item, $template, $vendor_id) {
        $this->view->setViewLocation(
            RBDOBOOKING_PLUGIN_DIR . 'includes/frontend/tpl/'.$template.'/'
        );
        $order_view = $this->view->get_view(
            "order_view",
            false,
            array(
                "time"          =>  date("j M, g:i a",strtotime($start_time_of_first_item)),
                "order_items"   =>  $order_items,
                "include_tax"   =>  ($this->db->getVendorSpecificSettingValueByName("include_tax",$vendor_id) == "Enabled" ? true : false ),
                "currency"      =>  $this->db->getVendorSpecificSettingValueByName( "currency", $vendor_id ),
                'coupons'       =>  isset($userData['payment']['coupons']) ? $userData['payment']['coupons'] : []
            )
        );
        $this->view->setViewLocation($this->tplPath);
        return $order_view;
    }

	public function coupons_ajax_handler() {//coupons_ajax_handler

		check_ajax_referer('rbdobooking-coupons-nonce','security');

		$vendor_id = 1;

		if ( $_POST["type"] == "get_coupon_details" ) {

			$coupon_id = $_POST["coupon_id"];

			$coupon_details = $this->db->coupons->getCouponDetails( $coupon_id );
			$services = $this->db->getAllServices(
				'*',
				false,
				$vendor_id
			);

			$return_data = null;

			if ( count($coupon_details) > 0 ) {

				$return_data = array(
					"coupon"            =>  $coupon_details[0],
					"services"          =>  $services
				);
				wp_send_json_success( $return_data );
			}
			else {
				$msg = __("No coupon is found","rbdobooking");
				$data = array(
					'notification' => array(
						'type'  =>  'error',
						'title' =>  __('Failed!','rbdobooking'),
						'text'  =>  $msg
					)
				);
				wp_send_json_error( $data );
			}

		}
		else if ( $_POST["type"] == "get_coupon_form_data" ) {
			$services = $this->db->getAllServices(
				'*',
				false,
				$vendor_id
			);
			$return_data = array(
				"services"  =>  $services
			);
			wp_send_json_success( $return_data );
		}
		else if ( $_POST['type'] == "delete_coupons" ) {

			$ids = $_POST["ids"];

			$result = $this->db->coupons->deleteCouponByCouponID( $ids );

			$return_data = null;

			if ( $result !== false ) {
				$msg = __("Successfully deleted coupons","rbdobooking");
				$return_data = array(
					'notification'  => array(
						'type'  =>  'success',
						'title' =>  __('Success!','rbdobooking'),
						'text'  =>  $msg
					)
				);
				wp_send_json_success( $return_data );
			}
			else {
				$msg = __("Unable to delete coupons","rbdobooking");
				$return_data = array(
					'notification'  => array(
						'type'  =>  'error',
						'title' =>  __('Failed!','rbdobooking'),
						'text'  =>  $msg
					)
				);
				wp_send_json_error( $return_data );
			}

		}
		else if ( $_POST["type"] == "save_coupon_form_data" ) {

			$coupon_data = array(
				"code"          =>  $_POST["data"]["code"],
				"discount"      =>  $_POST["data"]["discount"],
				"deduction"     =>  $_POST["data"]["deduction"],
				"usage_limit"   =>  $_POST["data"]["usage_limit"],
				"start_time"    =>  $_POST["data"]["start_time"],
				"end_time"      =>  $_POST["data"]["end_time"]
			);

			if ( isset( $_POST["data"]["coupon_id"] ) ) {// edit case
				$coupon_id = $_POST["data"]["coupon_id"];
				$where = array(
					"coupon_id"    =>  $coupon_id
				);
				$result = $this->db->update("coupons",$coupon_data,$where);

				if ( $result !== false ) {

					$coupon_services = null;

					$this->db->coupons->deleteCouponServices( $coupon_id );

					foreach ( $_POST["data"]["service_ids"] as $service_id ) {
						$this->db->insert(
							"coupons_service",
							array(
								"coupon_id"     =>  $coupon_id,
								"service_id"    =>  $service_id,
								"created_at"    =>  date("Y-m-d H:i:s")
							)
						);
					}

					$coupon_details = $this->db->coupons->getCouponDetails( $coupon_id );
					$coupon_row_html = $this->get_coupon_row_html( $coupon_details[0], 0 );

					$msg = __("Coupon updated successfully","rbdobooking");
					$return_data = array(
						"html"  =>  $coupon_row_html,
						'notification' => array(
							'type'  =>  'success',
							'title' =>  __('Success!','rbdobooking'),
							'text'  =>  $msg
						)
					);
					wp_send_json_success( $return_data );
				}
				else {
					$msg = __("Coupon can not be updated.","rbdobooking");
					$return_data = array(
						'notification' => array(
							'type'  =>  'error',
							'title' =>  __('Failed!','rbdobooking'),
							'text'  =>  $msg
						)
					);
					wp_send_json_error( $return_data );
				}
			}
			else {// insert case

				$date = date("Y-m-d H:i:s");

				$coupon_data["vendor_id"] = $vendor_id;
				$coupon_data["created_at"] = $date;
				$coupon_data["updated_at"] = $date;
				$coupon_data["vendor_id"] = $vendor_id;
				$insert_coupon_id = $this->db->insert( "coupons", $coupon_data );

				if ( $insert_coupon_id ) {

					foreach ( $_POST["data"]["service_ids"] as $service_id ) {
						$this->db->insert(
							"coupons_service",
							array(
								"coupon_id"     =>  $insert_coupon_id,
								"service_id"    =>  $service_id,
								"created_at"    =>  date("Y-m-d H:i:s")
							)
						);
					}

					$coupon_details = $this->db->coupons->getCouponDetails( $insert_coupon_id );
					$coupon_row_html = $this->get_coupon_row_html( $coupon_details[0], 0 );

					$msg = __("New Coupon inserted successfully","rbdobooking");
					$return_data = array(
						"html"          =>  $coupon_row_html,
						'notification'  => array(
							'type'  =>  'success',
							'title' =>  __('Success!','rbdobooking'),
							'text'  =>  $msg
						)
					);
					wp_send_json_success( $return_data );

				}
				else {
					$msg = __("Failed to insert new coupon","rbdobooking");
					$return_data = array(
						'notification'  => array(
							'type'  =>  'error',
							'title' =>  __('Failed!','rbdobooking'),
							'text'  =>  $msg
						)
					);
					wp_send_json_error( $return_data );
				}
			}
		}
		else {
			$msg = __("You are at wrong place","rbdobooking");
			$return_data = array(
				'notification'  => array(
					'type'  =>  'error',
					'title' =>  __('Failed!','rbdobooking'),
					'text'  =>  $msg
				)
			);
			wp_send_json_error( $return_data );
		}

	}

	/**
	 * Enqueue assets for the coupons page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
		// Coupons admin script.
		wp_enqueue_script(
			'rbdobooking-bootstrap-select-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/js/bootstrap-select.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style(
			"jquery-ui-theme-rbdobooking",
			RBDOBOOKING_PLUGIN_URL . "assets/css/jquery-ui/jquery-ui.min.css",
			array(),
			"1.0"
		);
		wp_enqueue_script(
			'rbdobooking-datetimepicker-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/timepicker/timepicker.js",
			array( 'jquery'),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-datetimepicker-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/timepicker/timepicker.css"
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-select-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/css/bootstrap-select.css"
		);
		wp_enqueue_script(
			'rbdobooking-coupons',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/coupons.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
        wp_enqueue_script(
            'rbdobooking-booking-form-addon-coupon',
            plugin_dir_path( __FILE__ ) . '',//RBDOBOOKING_PLUGIN_URL . "assets/js/frontend/do_booking_default.js",
            array('rbdobooking-booking-form'),
            RBDOBOOKING_VERSION,
            true
        );

		do_action( 'rbdobooking_coupons_enqueue' );
	}

	/**
	 * Build the output for plugin customers page.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		$vendor_id = 1;

		$coupons = $this->db->coupons->getAllCoupons( $vendor_id );
		$coupon_rows = '';

		foreach ( $coupons as $index => $coupon ) {

			$coupon_rows .= $this->get_coupon_row_html( $coupon, $index );

		}

		$coupons_slidepanel = $this->view->get_view("backend/coupons_ui_for_slide_panel");

		$data = array(
			'coupon_rows'           =>  $coupon_rows,
			'coupons_slidepanel'    =>  $coupons_slidepanel,
			'coupon_del_modal'      =>  $this->view->get_view("backend/delete_coupon_modal")
		);

		echo $this->view->get_view("backend/output",false,$data);
	}

	private function get_coupon_row_html( $coupon, $index ) {
		$coupon_rows = '';
		$service_ids_arr = explode( ",",$coupon["service_ids"] );
		$service_names_arr = explode( ",",$coupon["service_names"] );
		$services_html = '';

		foreach ( $service_ids_arr as $index2 => $service ) {
			$services_html .= $this->view->get_view(
				"backend/coupons.row.services",
				false,
				array(
					"service_id"    =>  $service_ids_arr[$index2],
					"service_name"          =>  $service_names_arr[$index2],
				)
			);
		}

		$coupon_rows .= $this->view->get_view(
			"backend/coupons.row",
			false,
			array(
				"number"        =>  $index+1,
				"coupon_id"     =>  $coupon["coupon_id"],
				"code"          =>  $coupon["code"],
				"discount"      =>  $coupon["discount"],
				"deduction"     =>  $coupon["deduction"],
				"services"      =>  $services_html,
				"usage_limit"   =>  $coupon["usage_limit"],
				"start_time"    =>  $coupon["start_time"],
				"end_time"      =>  $coupon["end_time"]
			)
		);
		return $coupon_rows;
	}

}
new RBDoBooking_Coupons();
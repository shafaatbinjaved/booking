<?php
/**
 * @var $all_template_options
 * @var $all_service_options
 * @var $custom_fields
 * @var $buttons
 * @var $script_tpl
 * @var $active_template_id
**/
?>
<div id="rbdobooking-custom-fields" class="wrap rbdobooking-admin-wrap">

	<div class="container-fluid">

		<h1 class="rbdobooking-h1-placeholder"><?php _e('Custom Fields','rbdobooking'); ?></h1>

		<div class="rbdobooking-admin-content rbdobooking-admin-custom-fields">

			<div class="row rbdobooking-custom-fields-container">
				<div class="col-12">
					<div class="header">
						<div class="float-left">
							<h4><?php _e('Custom fields','rbdobooking'); ?></h4>
						</div>
						<div class="clear"></div>
					</div>
                    <div class="form-horizontal templates-btn-and-fields-container">
                        <div class="row">
                            <div class="col-12 templates-container">
                                <div class="row">
                                    <div class="col-3">
                                        <input type="hidden" class="active_template_id" name="active_template_id" value="<?php echo $active_template_id ?>" />
                                        <div class="radio">
                                            <label>
                                                <input class="template-option" type="radio" name="template-option" />
                                                <span class="label"><?php _e('Select template','rbdobooking'); ?></span>
                                            </label>
                                        </div>
                                        <div class="option-view">
                                            <select data-opposite="new-template-name"
                                                    class="form-control existing-template-name"
                                                    disabled>
                                                <option value=""></option>
                                                <?php echo $all_template_options; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="radio">
                                            <label>
                                                <input class="template-option" type="radio" name="template-option" />
                                                <span class="label"><?php _e('New Template','rbdobooking'); ?></span>
                                            </label>
                                        </div>
                                        <div class="option-view">
                                            <input disabled type="text" class="form-control new-template-name"
                                                   data-opposite="existing-template-name"
                                                   placeholder="<?php _e('Enter name here for new template','rbdobooking') ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="row last-row"></div>
                                <div class="row custom-fields-service-bind-container">
                                    <div class="col-3">
                                        <p class="bind-custom-fields-msg"><?php _e('Bind custom fields template to services') ?></p>
                                        <select class="form-control bind-service-option">
                                            <option value="disabled"><?php _e('Disabled','rbdobooking') ?></option>
                                            <option value="enabled"><?php _e('Enabled','rbdobooking') ?></option>
                                        </select>
                                    </div>
                                    <div class="col-3">
                                        <p class="bind-custom-fields-msg-2"><?php _e('Choose service to bind it to template','rbdobooking'); ?></p>
                                        <select class="form-control bind-service-template">
                                            <option value=""></option>
                                            <?php echo $all_service_options; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row btn-and-fields-container">
                                    <div class="col-9 custom-fields-container form-group">
                                        <?php echo $custom_fields; ?>
                                    </div>
                                    <div class="col-3 buttons-container">
                                        <?php echo $buttons; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row last-row">
                            <div class="col-12">
                                <div class="float-right">
                                    <button class="btn btn-default btn-save" type="button">
					                    <?php _e("Save","rbdobooking"); ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>

		</div>
	</div>

    <?php echo $script_tpl; ?>

</div>
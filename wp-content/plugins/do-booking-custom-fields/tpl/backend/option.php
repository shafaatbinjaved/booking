<div class="row option-row" data-option_id="<?php echo $item_id; ?>">
    <div class="col-1 text-center del-right-gutter">
        <i class="fas fa-align-justify option-move"></i>
    </div>
    <div class="col-10 del-left-gutter del-right-gutter">
        <input type="text" class="form-control option-input" value="<?php echo $item; ?>" />
    </div>
    <div class="col-1 text-center del-left-gutter">
        <i class="fas fa-trash del-btn-option"></i>
    </div>
</div>
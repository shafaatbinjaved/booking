<div class="input-group">
	<input type="text" class="form-control input" value="<?php echo $value; ?>" />
	<div class="input-group-append">
		<span class="input-group-text">
			<input type="checkbox" class="checkbox" <?php echo $checked; ?> />
			<span class="require-option"><?php _e('Required field','rbdobooking'); ?></span>
		</span>
	</div>
</div>
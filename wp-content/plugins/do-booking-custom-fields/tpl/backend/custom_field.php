<div class="custom-field" data-key="<?php echo $key; ?>"
     data-field_id="<?php echo $template_field_id; ?>"
     data-list_id="<?php echo $list_id; ?>">
    <div class="name-part">
        <i class="fas fa-align-justify move"></i>
        <span><?php echo $field_name; ?></span>
        <i class="fas fa-trash del-btn"></i>
    </div>
    <div class="input-part">
        <?php echo $custom_field_input_html; ?>
        <div class="all-options">
            <?php echo $options; ?>
        </div>
        <?php echo $option_button; ?>
    </div>
</div>
<?php

/*
 * Services class.
 * @since 1.0.0
 * */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

class RBDoBooking_CustomFields {

    /**
     * @var View $view
     */
    private $view;

    /**
     * @var string $tplPath
     */
    private $tplPath;

    /**
     * Holds instance of plugin db class
     *
     * @since 1.0.0
     *
     * @var RBDoBooking_DB
    */
    private $db;

	/*
	 * Primary class constructor
	 * @since 1.0.0
	 * */
	public function __construct() {

	    $this->tplPath = RBDOBOOKING_CUSTOM_FIELDS_ADDON_DIR . 'tpl/backend/';
		$this->view = new View($this->tplPath);

		//Load Custom Fields page
		add_action('init',array($this,'save_cookie'));
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_custom_fields',array($this,'custom_fields_ajax_handler'));

        add_action( 'admin_menu', array( $this, 'register_menus' ), 9 );

		$this->db = RBDoBooking_DB::instance();
	}

	function register_menus() {
        $menu_cap = rbdobooking_get_capability_manage_options();

        // Custom Fields sub menu item
        add_submenu_page(
            'rbdobooking-calendar',
            esc_html__( 'RB Do Boooking - Custom Fields', 'rbdobooking' ),
            esc_html__( 'Custom Fields', 'rbdobooking' ),
            $menu_cap,
            'rbdobooking-custom-fields',
            array( $this, 'admin_page' )
        );
    }

    /**
     * Wrapper for the hook to render our custom settings pages.
     *
     * @since 1.0.0
     */
    public function admin_page() {
        do_action('rbdobooking_admin_page');
    }

	/**
     * Ajax handler for all requests originating from custom fields page
     *
     * @since 1.0.0
	*/
	public function custom_fields_ajax_handler() {

	    check_ajax_referer('rbdobooking-custom-fields-nonce','security');
		$msg = 'Trying to access wrong url';

	    if ( $_POST["type"] == "save_custom_fields" ) {
	        $this->save_custom_fields( $_POST );
        }
        else if ( $_POST["type"] == "get_template_data" ) {
	    	$this->get_template_data( $_POST );
        }
        else {
	        $msg = __('Wrong place','rbdobooking' );
	        wp_send_json_error(
		        array(
			        'notification' => array(
				        'type'  =>  'error',
				        'title' =>  __('Failed!','rbdobooking'),
				        'text'  =>  $msg
			        )
		        )
	        );
        }
    }

    function save_cookie() {
	    //setting cookie
	    if ( !isset($_COOKIE[RBDOBOOKING_VENDOR_COOKIE]) ) {
		    $vendor = array(
			    "vendor_id" =>  1,
			    "testing"   =>  "yes",
			    "frau"      =>  "Zainab"
		    );
		    $cookie_encrypt_value = $this->view->encrypt(
			    serialize($vendor),
			    RBDOBOOKING_PLUGIN_PREFIX
		    );
		    setcookie(
			    RBDOBOOKING_VENDOR_COOKIE,
			    $cookie_encrypt_value,
			    time()+3*86400,//86400 is seconds in a day
			    COOKIEPATH,
			    COOKIE_DOMAIN
		    );
	    }
	    else {
	    }
	    //done with setting cookie
    }

    private function arrange_template_html( $custom_fields ) {

		$keys_val = array(
			"text_field"            =>  __("Text Field","rbdobooking"),
			"text_area"             =>  __("Text Area","rbdobooking"),
			"text_content"          =>  __("Text Content","rbdobooking"),
			"checkbox_group"        =>  __("Checkbox Group","rbdobooking"),
			"radio_button_group"    =>  __("Radio Button Group","rbdobooking"),
			"dropdown"              =>  __("Dropdown","rbdobooking")
		);

	    $fields_html = '';
	    foreach ( $custom_fields as $index => $custom_field ) {
		    $type = $custom_field["type"];
		    $custom_field["key"] = $custom_field["type"];
		    if ( $type == "text_content" ) {
			    $custom_field["custom_field_input_html"] = $this->view->get_view(
				    "custom_field_textarea",
				    false,
				    array("value"=>$custom_field["value"])
			    );
		    }
		    else {
			    $checked = '';
			    if ( $custom_field["is_required"] == "1" ) {
				    $checked = 'checked';
			    }
			    $custom_field["custom_field_input_html"] = $this->view->get_view(
				    "custom_field_input",
				    false,
				    array(
					    "value"     =>  $custom_field["value"],
					    "checked"   =>  $checked
				    )
			    );
		    }

		    $custom_field["options"] = "";
		    $custom_field["option_button"] = "";
		    if ( $custom_field["is_list"] == "1" ) {
			    $list_items = $this->db->getListItems( $custom_field["list_id"] );
			    foreach ( $list_items as $list_index => $list_item ) {
				    $custom_field["options"] .= $this->view->get_view(
					    "option",
					    false,
					    $list_item
				    );
			    }
			    $custom_field["option_button"] = $this->view->get_view("option_button");
		    }
		    else {
			    $custom_field["list_id"] = "";
		    }
		    $custom_field["field_name"] = $keys_val[$custom_field["type"]];
		    $fields_html .= $this->view->get_view(
			    "custom_field",
			    false,
			    $custom_field
		    );
	    }
	    return $fields_html;
    }

    public function get_template_data( $data ) {
		$success = false;
		$msg = __('Failed to load template data','rbdobooking');
		$fields_html = '';
		$service_id = null;

		$template_id = '';
		if ( isset($data["template_id"]) ) {
			$template_id = $data["template_id"];
			$custom_fields = $this->db->getCustomFieldTemplateData( $template_id );
			$fields_html = $this->arrange_template_html( $custom_fields );
			if ( isset($custom_fields[0]["service_id"]) ) {
				$service_id = $custom_fields[0]["service_id"];
			}
			$success = true;
			$msg = __('Successfully loaded template data.','rbdobooking');
		}
		else {
			$msg = __('Trying to access wrong custom field template.','rbdobooking');
		}

		if ( $success ) {
			wp_send_json_success(
				array(
					'notification' => array(
						'type'  =>  'success',
						'title' =>  __('Success!','rbdobooking'),
						'text'  =>  $msg
					),
					"fields_html"    =>  $fields_html,
					"service_id"     => $service_id
				)
			);
		}
		else {
			wp_send_json_error(
				array(
					'notification' => array(
						'type'  =>  'error',
						'title' =>  __('Failed!','rbdobooking'),
						'text'  =>  $msg
					)
				)
			);
		}
    }

    public function save_custom_fields( $data, $vendor_id = 1 ) {
		$success = false;
		$msg = 'Failed to store data';
	    $template_id = '';
	    $template_name = '';

		if ( isset($data["data"]) ) {

			$this->db->startTransaction();

			$template_id_status = false;
			$list_id_status = false;
			$item_id_status = [];
			$field_id_status = [];

			$template = array(
				"name"                  =>  $data["data"]["template_name"],
				"vendor_id"             =>  1,
				"is_service_specific"   =>  $data["data"]["is_service_specific"],
				"is_active"             =>  1,
				"created_at"            =>  date("Y-m-d H:i:s")
			);
			$template_name = $data["data"]["template_name"];
			if ( $data["data"]["is_service_specific"] == "0" ) {
				$template["service_id"] = null;
			}
			if ( isset($data["data"]["service_id"]) ) {
				$template["service_id"] = $data["data"]["service_id"];
			}
			if ( isset($data["data"]["template_id"]) ) {// update case
				$template_id = $data["data"]["template_id"];
				$template_id_status = $this->db->update(
					"templates",
					$template,
					array("template_id"=>$template_id)
				);
				if ( $template_id_status == 0 ) {
					$template_id_status = true;
				}
				$msg = __('Custom field template updated successfully','rbdobooking');
                do_action( 'rbdobooking_cache_refresh', $vendor_id, "custom_fields", $template_id );
			}
			else {// insert case
				$template_id_status = $this->db->insert("templates",$template);
				$template_id = $template_id_status;
				$msg = __('Custom field template inserted successfully.','rbdobooking');
			}

			$this->db->disableAllOtherVendorTemplatesExceptTemplateID( $template_id, $vendor_id );

			$all_custom_fields_update = array();

			foreach ( $data["data"]["custom_fields"] as $index => $custom_field ) {
				$list_id = '';
				if ( $custom_field["is_list"] == "1" ) {
					$list = array(
						"vendor_id"     =>  $template["vendor_id"],
						"list_name"     =>  $template["vendor_id"] . "_" . $template["name"]."_".$custom_field["type"]."_list",
						"created_at"    =>  date("Y-m-d H:i:s")
					);
					if ( isset($custom_field["list_id"]) ) {//update case
						$list_id = $custom_field["list_id"];
						$list_id_status = true;
					}
					else {// insert case
						$list_id_status = $this->db->insert(
							"lists",
							$list
						);
						$list_id = $list_id_status;
					}

					$list_items_process = array();
					foreach ( $custom_field["list"] as $list_item_index => $list_item  ) {
						$list_item_arr = array(
							"list_id"       =>  $list_id,
							"item"          =>  $list_item["item"],
							"sort"          =>  $list_item["sort"],
							"created_at"    =>  date("Y-m-d H:i:s")
						);
						if ( isset($list_item["item_id"]) ) {//update case
							$item_val = $this->db->update(
								"list_items",
								$list_item_arr,
								array("item_id"=>$list_item["item_id"])
							);
							if ( $item_val == 0 ) {
								$item_id_status = true;
							}
							$item_id_status[] = $item_val;
							array_push($list_items_process,$list_item["item_id"]);
						}
						else {//insert case
							$item_id = $this->db->insert("list_items",$list_item_arr);
							$item_id_status[] = $item_id;
							array_push( $list_items_process, $item_id );
						}
					}
					$this->db->deleteListItems(
						$list_id,
						implode(",",$list_items_process)
					);
					$count = $this->db->countListItems($list_id);
					if ( $count == 0 ) {
						$this->db->deleteList($list_id);
					}
				}
				else {
					$list_id_status = true;
					$item_id_status[] = true;
				}
				$field = array(
					"template_id"   =>  $template_id,
					"type"          =>  $custom_field["type"],
					"value"         =>  $custom_field["value"],
					"sort"          =>  $custom_field["sort"],
					"is_list"       =>  $custom_field["is_list"],
					"is_required"   =>  $custom_field["is_required"]
				);
				if ( $list_id != "" ) {
					$field["list_id"] = $list_id;
				}
				$process_template_field_id = '';
				if ( isset($custom_field["template_field_id"]) ) {//update case
					$val = $this->db->update(
						"template_fields",
						$field,
						array("template_field_id"=>$custom_field["template_field_id"] )
					);
					if ( $val == 0 ) {
						$val = true;
					}
					$field_id_status[] = $val;
					$process_template_field_id = $custom_field["template_field_id"];
				}
				else {//insert case
					$field["created_at"] = date("Y-m-d H:i:s");
					$process_template_field_id = $this->db->insert("template_fields",$field);
					$field_id_status[] = $process_template_field_id;
				}
				array_push($all_custom_fields_update,$process_template_field_id);
			}
			$this->db->deleteCustomFieldTemplateFieldIds(
				$template_id,
				implode(",",$all_custom_fields_update)
			);
			if ( $template_id_status != false &&
					$list_id_status != false &&
					in_array(false,$item_id_status) != true &&
					in_array(false,$field_id_status) != true ) {

				$this->db->commitTransaction();
				$success = true;
			}
			else {
				$msg = __('Something went wrong while saving data','rbdobooking');
				$this->db->rollbackTransaction();
			}
		}

		if ( $success ) {
			$custom_fields = $this->db->getCustomFieldTemplateData( $template_id );
			$service_id = null;
			if ( isset($custom_fields[0]["service_id"]) ) {
				$service_id = $custom_fields[0]["service_id"];
			}
			$custom_fields_html = $this->arrange_template_html( $custom_fields );
			wp_send_json_success(
				array(
					'notification' => array(
						'type'  =>  'success',
						'title' =>  __('Success!','rbdobooking'),
						'text'  =>  $msg
					),
					'custom_fields_html'    =>  $custom_fields_html,
					'service_id'            =>  $service_id,
					'template_id'           =>  $template_id,
					'template_name'         =>  $template_name
				)
			);
		}
		else {
			wp_send_json_error(
				array(
					'notification' => array(
						'type'  =>  'error',
						'title' =>  __('Failed!','rbdobooking'),
						'text'  =>  $msg
					)
				)
			);
		}
    }

	/*
	 * Determine if the user is viewing the custom fields page
	 * @since 1.0.0
	 * */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the services page.
		if ( 'rbdobooking-custom-fields' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_custom_fields_init' );
		}
	}

	/**
	 * Enqueue assets for the custom fields page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
		// Services admin script.
		wp_enqueue_script(
			'rbdobooking-custom-fields',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/custom_fields.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script('jquery-ui-sortable');
		do_action( 'rbdobooking_custom_fields_enqueue' );
	}

	/**
	 * Build the output for the plugin services page.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		$buttons = array(
			array(
				"key"           =>  "text_field",
				"field_name"    =>  __("Text Field","rbdobooking"),
				"type"          =>  "input",
				"has_options"   =>  false
			),
			array(
				"key"           =>  "text_area",
				"field_name"    =>  __("Text Area","rbdobooking"),
				"type"          =>  "input",
				"has_options"   =>  false
			),
			array(
				"key"           =>  "text_content",
				"field_name"    =>  __("Text Content","rbdobooking"),
				"type"          =>  "textarea",
				"has_options"   =>  false
			),
			array(
				"key"           =>  "checkbox_group",
				"field_name"    =>  __("Checkbox Group","rbdobooking"),
				"type"          =>  "input",
				"has_options"   =>  true
			),
			array(
				"key"           =>  "radio_button_group",
				"field_name"    =>  __("Radio Button Group","rbdobooking"),
				"type"          =>  "input",
				"has_options"   =>  true
			),
			array(
				"key"           =>  "dropdown",
				"field_name"    =>  __("Dropdown","rbdobooking"),
				"type"          =>  "input",
				"has_options"   =>  true
			)/*,
			array(
				"name"  =>  __("Captcha","rbdobooking")
			)*/
		);

		$buttons_html = '';
		$custom_fields_html = '';
		$script_tpl_html = '';
		foreach ( $buttons as $button ) {
			$buttons_html .= $this->view->get_view(
				"button",
				false,
				$button
			);

			$html = $this->view->get_view(
				"custom_field_".$button["type"],
				false,
				array("value"=>"","checked"=>"")
			);
			$button["custom_field_input_html"] = $html;

			$button["options"] = '';
			$button["option_button"] = '';
			if ( $button["has_options"] ) {
				$button["options"] .= $this->view->get_view(
					"option",
					false,
					array("item_id"=>"","item"=>"")
				);
				$button["option_button"] .= $this->view->get_view("option_button");
			}

			$button["template_field_id"] = "";
			$button["list_id"] = "";
			/*$custom_fields_html .= $this->view->get_view(
				"custom_field",
				false,
				$button
			);*/

			$button["template_field_id"] = "";
			$button["list_id"] = "";
			$script_tpl_html .= $this->view->get_view(
				"custom_field",
				true,
				$button,
				"tpl_".$button["key"]
			);
		}

		$script_tpl_html .= $this->view->get_view(
			'option',
			true,
			array("item"=>"","item_id"=>""),
			"tpl_option"
		);

        $active_template_id = 0;
		$all_template_options_html = '';
		$all_custom_field_templates = $this->db->getVendorAllTemplates( 1 );
		foreach ( $all_custom_field_templates as $index => $template ) {
		    if ( $template["is_active"] == 1 ) {
                $active_template_id = $template["template_id"];
            }
			$all_template_options_html .= $this->view->get_view(
				"template_option",
				false,
				$template
			);
		}

		$all_service_options_html = '';
		$all_service_options = $this->db->getAllVendorServices( 1 );
		foreach ( $all_service_options as $service_option ) {
			$all_service_options_html .= $this->view->get_view(
				"service_option",
				false,
				$service_option
			);
		}

		$data = array(
		    'active_template_id'    =>  $active_template_id,
			'buttons'               =>  $buttons_html,
			'custom_fields'         =>  $custom_fields_html,
			'script_tpl'            =>  $script_tpl_html,
			'all_template_options'  =>  $all_template_options_html,
			'all_service_options'   =>  $all_service_options_html
        );
		echo $this->view->get_view("output",false,$data);
	}
}
new RBDoBooking_CustomFields();
<?php
/*
Plugin Name: Do Booking Custom Fields
Plugin URI: https://sjm.com/
Description: Do Booking plugin addon custom fields.
Version: 1.0
Author: Shafaat Javed
Author URI: https://shafaat.com/booking-plugin/
License: GPLv2 or later
Text Domain: rbdobooking
Domain Path: /languages
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Don't allow multiple versions to be active
if ( class_exists( 'RBDoBooking_Custom_Fields_Addon' ) ) {

} else {
    class RBDoBooking_Custom_Fields_Addon {
        /**
         * One is the loneliest number that you'll ever do.
         *
         * @since 1.0.0
         *
         * @var object
         */
        private static $instance;

        /**
         * Addon version for enqueueing, etc.
         *
         * @since 1.0.0
         *
         * @var string
         */
        public $addon_version = '1.0.0';

        /**
         * Main RBDoBooking Custom fields addon Instance.
         *
         * Insures that only one instance of RBDoBooking Custom Fields addon exists in memory at any one
         * time. Also prevents needing to define globals all over the place.
         *
         * @since 1.0.0
         *
         * @return RBDoBooking_Custom_Fields_Addon
         */

        public static function instance() {

            if (!isset( self::$instance ) && !(self::$instance instanceof RBDoBooking_Custom_Fields_Addon)) {

                self::$instance = new RBDoBooking_Custom_Fields_Addon();
                self::$instance->constants();
                self::$instance->load_textdomain();
                self::$instance->includes();

                add_action( 'plugins_loaded', array( self::$instance, 'objects' ), 10 );
            }
            return self::$instance;
        }

        /**
         * Setup plugin constants.
         *
         * @since 1.0.0
         * */
        private function constants() {
            // Plugin version.
            if ( ! defined( 'RBDOBOOKING_CUSTOM_FIELDS_ADDON_VERSION' ) ) {
                define( 'RBDOBOOKING_CUSTOM_FIELDS_ADDON_VERSION', $this->addon_version );
            }

            // Plugin Folder Path.
            if ( ! defined( 'RBDOBOOKING_CUSTOM_FIELDS_ADDON_DIR' ) ) {
                define( 'RBDOBOOKING_CUSTOM_FIELDS_ADDON_DIR', plugin_dir_path( __FILE__ ) );
            }

            // Plugin Folder URL.
            if ( ! defined( 'RBDOBOOKING_CUSTOM_FIELDS_ADDON_URL' ) ) {
                define( 'RBDOBOOKING_CUSTOM_FIELDS_ADDON_URL', plugin_dir_url( __FILE__ ) );
            }

            // Plugin Root File.
            if ( ! defined( 'RBDOBOOKING_CUSTOM_FIELDS_ADDON_FILE' ) ) {
                define( 'RBDOBOOKING_CUSTOM_FIELDS_ADDON_FILE', __FILE__ );
            }

            define( 'RBDOBOOKING_CUSTOM_FIELDS_ADDON_SLUG', 'do-booking-custom-fields' );
        }

        /**
         * Loads the plugin language files.
         *
         * @since 1.0.0
         */
        public function load_textdomain() {

            load_plugin_textdomain( 'rbdobooking', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
        }

        /**
         * Include files.
         *
         * @since 1.0.0
         */
        private function includes() {
            require_once RBDOBOOKING_CUSTOM_FIELDS_ADDON_DIR . 'class-custom-fields.php';
        }

        /**
         * @since 1.0.0
         */
        public function objects() {}
    }

    /**
     * The function which returns the one RBDoBooking custom fields addon instance.
     *
     * @since 1.0.0
     * @return object
     */
    function rbdobooking_custom_fields() {

        return RBDoBooking_Custom_Fields_Addon::instance();
    }

    function custom_fields_addon_init() {
        if (class_exists('RBDoBooking')) {
            rbdobooking_custom_fields();
        }
    }

    add_action('plugins_loaded', 'custom_fields_addon_init');
}
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $bootstrap_css ?>">
</head>
<body>
<div class="page">
	<table class="table-cust-and-invoice">
		<tr>
			<td class="first">
				<div>
					<div><strong><?php echo $company_name ?></strong></div>
					<div><?php echo nl2br($company_address) ?></div>
				</div>
			</td>
		</tr>
	</table>
	<?php
	echo $order_html;
	?>
</div>
</body>
</html>
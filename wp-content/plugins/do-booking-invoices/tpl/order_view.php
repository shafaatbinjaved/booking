<?php
/**
 * User: shafaatbinjaved
 * Date: 4/7/2019
 * Time: 4:26 PM
 */
?>

<br>
<br>

<table class="table-cust-and-invoice">
    <tr>
        <td class="first">
            <div class="">
                <div><strong><?php _e("Csutomer","rbdobooking") ?></strong></div>
                <div>
                    <span><?php echo $order_data[0]["customer_name"] ?></span><br>
                    <span><?php echo $order_data[0]["extra_address"] ?></span><br>
                    <span><?php echo $order_data[0]["street"] ?></span><br>
                    <span><?php echo $order_data[0]["zip"] . " " . $order_data[0]["city"] ?></span><br>
                    <span><?php echo ucfirst($order_data[0]["country"]) ?></span>
                </div>
            </div>
        </td>
        <td class="second">
            <div class="">
                <div><strong><?php
						if ( $order_type == "invoice" ) {
							_e("Invoice","rbdobooking");
						}
						?></strong></div>
                <div class="text-left">
					<?php
					if ( $order_type == "invoice" ) {
						?>
                        <div>
                            <span><?php _e('Invoice no','rbdobooking') ?>:</span>
                            <span><?php echo $order_data[0]["order_id"] ?></span>
                        </div>
						<?php
					}
					?>
                    <div>
                        <span><?php _e('Date','rbdobooking') ?>:</span>
                        <span><?php echo date("d.m.Y g:i a",strtotime($order_data[0]["created_at"])) ?></span>
                    </div>
                    <div>
                        <span><?php _e('Payment method','rbdobooking') ?>:</span>
                        <span><?php echo $order_data[0]["payment_method_name"] ?></span>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>

<br>
<br>

<table class="table-order">
	<thead>
	<tr>
		<th><?php _e("Service","rbdobooking"); ?></th>
		<th><?php _e("Date","rbdobooking"); ?></th>
		<th><?php _e("Provider","rbdobooking"); ?></th>
		<?php
		if ( (int)$order_data[0]["total_with_tax"] != (int)$order_data[0]["total_without_tax"] ) {//taxable order
			?>
			<th><?php _e("Quantity","rbdobooking"); ?></th>
			<th><?php _e("Price","rbdobooking"); ?></th>
			<th><?php _e("Tax %","rbdobooking"); ?></th>
			<th><?php _e("Taxable Price","rbdobooking"); ?></th>
			<?php
		}
		else {
			?>
			<th colspan="3"><?php _e("Price","rbdobooking"); ?></th>
			<?php
		}
		?>
	</tr>
	</thead>
	<tbody>
	<?php
    $subtotal_without_tax = 0.0;
    $subtotal_with_tax = 0.0;
	foreach ( $order_data as $index => $item ) {
        $subtotal_without_tax += $item['price_without_tax'];
        $subtotal_with_tax += $item['price_with_tax'];
		?>
		<tr>
			<td><?php echo $item["service_name"] ?></td>
			<td><?php echo date("d.m.Y g:i a",strtotime($item["slot_start_date"])) ?></td>
			<td><?php echo (!is_null($item["first_name"]) ? $item["first_name"] . " " . $item["last_name"] : "") ?></td>
			<td><?php echo $item["quantity"] ?></td>
			<?php
			if ( intval($item["price_with_tax"]) == 0 ) {
				?>
				<td colspan="3">
					<?php echo $currency.$item["price_with_tax"] ?>
				</td>
				<?php
			}
			else {
				?>
				<td>
					<?php echo $currency.$item["price_without_tax"] ?>
				</td>
				<td>
					<?php echo $item["tax_percent"]."%" ?>
				</td>
				<td>
					<?php echo $currency.$item["price_with_tax"] ?>
				</td>
				<?php
			}
			?>
		</tr>
		<?php
	}
	?>
	</tbody>
	<tfoot>
	<tr>
		<th colspan="4">
			<?php _e("Subtotal","rbdobooking") ?>
		</th>
		<?php
		if ( (int)$order_data[0]["total_with_tax"] != (int)$order_data[0]["total_without_tax"] ) {//taxable case
			?>
			<th><?php echo $currency.$subtotal_without_tax ?></th>
			<th></th>
			<th><?php echo $currency.$subtotal_with_tax ?></th>
			<?php
		}
		else {//not taxable case
			?>
			<th colspan="3"><?php echo $currency.$order_data[0]["total_with_tax"] ?></th>
			<?php
		}
		?>
	</tr>
    <?php
    do_action(
        'rbdobooking_backend_order_coupon_rows',
        $order_data[0]['order_id'],
        $order_data[0]['vendor_id'],
        $subtotal_with_tax
    );
    ?>
	<tr>
		<th colspan="4">
			<?php _e("Total","rbdobooking") ?>
		</th>
		<?php
		if ( (int)$order_data[0]["total_with_tax"] != (int)$order_data[0]["total_without_tax"] ) {//taxable case
			?>
			<th><?php echo $currency.$order_data[0]["total_without_tax"] ?></th>
			<th></th>
			<th><?php echo $currency.$order_data[0]["total_with_tax"] ?></th>
			<?php
		}
		else {//not taxable case
			?>
			<th colspan="3"><?php echo $currency.$order_data[0]["total_with_tax"] ?></th>
			<?php
		}
		?>
	</tr>
	</tfoot>
</table>
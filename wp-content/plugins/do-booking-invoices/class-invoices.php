<?php

/**
 * Cache class.
 *
 * @package    RB Do Booking
 * @author     RB Do Booking
 * @since      1.0.0
 */
class RBDoBooking_Invoices {

	private $mpdf;
	private $view;
	private $db;

	/*
     * Primary class constructor
     *
     * @since 1.0
     * */
	public function __construct() {
		$this->includes();
		$this->view = new View(plugin_dir_path( __FILE__ ) . 'tpl/');
		$this->db = RBDoBooking_DB::instance();
		add_action('wp_ajax_rbdobooking_invoices',array($this,'payments_ajax_handler'));
	}

	/**
	 * Ajax handler for invoices
	 *
	 * @since 1.0.0
	 */
	public function payments_ajax_handler() {
		check_ajax_referer('rbdobooking-invoices-nonce','security');

		$type = $_POST["type"];

		switch ($type) {
			case "download_invoice":
				$order_id = $_POST["order_id"];
				$vendor_id = $_POST["vendor_id"];
				$order_data = $this->db->orders->getOrderData( $order_id, $vendor_id );
				$currency = $this->db->getVendorSpecificSettingValueByName( "currency", $vendor_id );
				$company_name = $this->db->getVendorSpecificSettingValueByName( "company_name", $vendor_id );
				$company_address = $this->db->getVendorSpecificSettingValueByName( "company_address", $vendor_id );
				$order_html = $this->view->get_view(
					"order_view",
					false,
					array(
						"order_data"        =>  $order_data,
						"currency"          =>  $currency,
						"order_type"        =>  "invoice"
					)
				);
				$invoice_print = $this->view->get_view(
					"invoice_print",
					false,
					array(
						"order_html"        =>  $order_html,
						"bootstrap_css"     =>  plugin_dir_path( __FILE__ ) . 'tpl/stylesheet.css',
						"company_name"      =>  $company_name,
						"company_address"   =>  $company_address
					)
				);
				$mpdf = new \Mpdf\Mpdf([
					'mode' => 'utf-8',
					'format' => [190, 236],
					'orientation' => 'L'
				]);

				$stylesheet = file_get_contents(
					plugin_dir_path( __FILE__ ) . 'tpl/stylesheet.css'
				);
				/*$stylesheet = file_get_contents(
					RBDOBOOKING_PLUGIN_URL . 'assets/css/bootstrap/bootstrap.css'
				);*/

				//$mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
				//$mpdf->WriteHTML($invoice_print,\Mpdf\HTMLParserMode::HTML_BODY);

				// Write some HTML code:
				$mpdf->WriteHTML($invoice_print);
				// Output a PDF file directly to the browser
				$mpdf->Output();
				die();
				break;
		}
	}

	/**
	 * Include files.
	 *
	 * @since 1.0
	 */
	private function includes() {

		// Admin/Dashboard only includes
		if ( is_admin() ) {
			require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';
			require_once plugin_dir_path( __FILE__ ) . 'lib/vendor/autoload.php';
			add_action('rbdobooking_generate_invoice',array($this,'generate_invoice'), 10, 2);
			add_action('rbdobooking_get_invoice',array($this,'get_invoice'), 10, 2);
			add_action('rbdobooking_show_invoice_btn',array($this,'show_invoice_btn'), 10, 2);
		}
		require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';

	}

	/**
	 * Show invoice btn
	 *
	 * @since 1.0.0
	 *
	 * @var int $order_id
	 * @return String $button_html
	*/
	public function show_invoice_btn( $order_id, $vendor_id ) {
		echo $this->view->get_view(
			"download_invoice_btn",
			false,
			array(
				"order_id"  =>  $order_id,
				"vendor_id" =>  $vendor_id
			)
		);
	}

	/**
	 * get invoice.
	 *
	 * @since 1.0.0
	 */
	private function get_invoice( $payment_id ) {
	}

}
new RBDoBooking_Invoices();
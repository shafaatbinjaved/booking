<?php
/*
Plugin Name: Do Booking Invoices
Plugin URI: https://sjm.com/
Description: Do Booking plugin addon invoices.
Version: 1.0
Author: Shafaat Javed
Author URI: https://shafaat.com/booking-plugin/
License: GPLv2 or later
Text Domain: rbdobooking
Domain Path: /languages
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Don't allow multiple versions to be active
if ( class_exists( 'RBDoBooking_Invoices_Addon' ) ) {

} else {
    class RBDoBooking_Invoices_Addon {
        /**
         * One is the loneliest number that you'll ever do.
         *
         * @since 1.0.0
         *
         * @var object
         */
        private static $instance;

        /**
         * Addon version for enqueueing, etc.
         *
         * @since 1.0.0
         *
         * @var string
         */
        public $addon_version = '1.0.0';

        /**
         * Main RBDoBooking Invoices addon Instance.
         *
         * Insures that only one instance of RBDoBooking Invoices addon exists in memory at any one
         * time. Also prevents needing to define globals all over the place.
         *
         * @since 1.0.0
         *
         * @return RBDoBooking
         */

        public static function instance() {

            if (!isset( self::$instance ) && !(self::$instance instanceof RBDoBooking_Invoices_Addon)) {

                self::$instance = new RBDoBooking_Invoices_Addon();
                self::$instance->constants();
                self::$instance->load_textdomain();
                self::$instance->includes();

                add_action( 'plugins_loaded', array( self::$instance, 'objects' ), 10 );
            }
            return self::$instance;
        }

        /**
         * Setup plugin constants.
         *
         * @since 1.0.0
         * */
        private function constants() {
            // Plugin version.
            if ( ! defined( 'RBDOBOOKING_INVOICES_ADDON_VERSION' ) ) {
                define( 'RBDOBOOKING_INVOICES_ADDON_VERSION', $this->addon_version );
            }

            // Plugin Folder Path.
            if ( ! defined( 'RBDOBOOKING_INVOICES_ADDON_DIR' ) ) {
                define( 'RBDOBOOKING_INVOICES_ADDON_DIR', plugin_dir_path( __FILE__ ) );
            }

            // Plugin Folder URL.
            if ( ! defined( 'RBDOBOOKING_INVOICES_ADDON_URL' ) ) {
                define( 'RBDOBOOKING_INVOICES_ADDON_URL', plugin_dir_url( __FILE__ ) );
            }

            // Plugin Root File.
            if ( ! defined( 'RBDOBOOKING_INVOICES_ADDON_FILE' ) ) {
                define( 'RBDOBOOKING_INVOICES_ADDON_FILE', __FILE__ );
            }

            define( 'RBDOBOOKING_INVOICES_ADDON_SLUG', 'do-booking-invoices' );
        }

        /**
         * Loads the plugin language files.
         *
         * @since 1.0.0
         */
        public function load_textdomain() {

            load_plugin_textdomain( 'rbdobooking', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
        }

        /**
         * Include files.
         *
         * @since 1.0.0
         */
        private function includes() {
            require_once RBDOBOOKING_INVOICES_ADDON_DIR . 'class-invoices.php';
        }

        /**
         * @since 1.0.0
         */
        public function objects() {}
    }

    /**
     * The function which returns the one RBDoBooking invoices addon instance.
     *
     * @since 1.0.0
     * @return object
     */
    function rbdobooking_invoices() {

        return RBDoBooking_Invoices_Addon::instance();
    }

    function invoices_addon_init() {
        if (class_exists('RBDoBooking') && class_exists('RBDoBooking_Coupons_Addon')) {
            rbdobooking_invoices();
        }
    }

    add_action('plugins_loaded', 'invoices_addon_init');
}
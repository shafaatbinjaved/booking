var rbdobookingForm;
jQuery(document).ready(function() {
    rbdobookingForm = new RBDoBooking_Form();
    rbdobookingForm.pre_compute();
});

function RBDoBooking_Form() {

    var self = this;
    var session = null;
    var dateToday = new Date();

    this.current_step = null;
    this.first_step = null;
    this.last_step = null;
    this.form = null;
    this.cart_number = 0;

    this.pre_compute = function() {
        console.log(self.component.find(".category-select").attr("data-category_services"));

        self.first_step = RBDoBookingHelper.step;

        //service-step is first
        self.service_step_all();

        self.back_btn.hide();
    }

    this.component = jQuery("#rbdobooking-booking-form");

    this.form = this.component.find("form");
    this.template = this.component.find(".template-form");

    this.steps_container = this.template.find(".steps-container");
    this.step_html = this.template.find(".step-content");

    this.category_select = null;
    this.service_select = null;
    this.member_select = null;
    this.datepicker = null;
    this.day_select = null;
    this.time_start = null;
    this.time_end = null;
    this.category_services = null;
    this.service_categories = null;
    this.service_members = null;
    this.category_members = null;
    this.categories = [];
    this.services = [];
    this.members = [];

    this.service_step_all = function () {

        self.category_services = JSON.parse(
            self.component.find(".category-select").attr("data-category_services")
        );
        self.service_members = JSON.parse(
            self.component.find(".service-select").attr("data-service_members")
        );
        self.category_members = JSON.parse(
            self.component.find(".category-select").attr("data-category_members")
        );
        self.service_categories = JSON.parse(
            self.component.find(".service-select").attr("data-service_categories")
        );
        self.categories = self.component.find(".category-select option");
        self.services = self.component.find(".service-select option");
        self.members = self.component.find(".member-select option");

        var pre_filled_date = dateToday.getDate()+"-"+(dateToday.getMonth()+1)+"-"+dateToday.getFullYear()
        self.template.find(".date-picker").val( pre_filled_date );

        self.category_select = self.step_html.find(".category-select");
        self.category_select.off();
        self.category_select.on("change",function() {
            self.category_select_trigger(this);
        });

        self.service_select = self.step_html.find(".service-select");
        self.service_select.off();
        self.service_select.on("change",function() {
            self.service_select_trigger( this );
        });

        self.member_select = self.step_html.find(".member-select");
        self.member_select.off();
        self.member_select.on("click",function() {
            console.log("in member select");
        });

        self.datepicker = self.template.find(".date-picker").datepicker({
            dateFormat: "dd-mm-yy",
            minDate: dateToday,
            beforeShow: function() {
                jQuery(this).datepicker("widget").addClass("dobooking-date");
            },
            onClose: function() {
                jQuery(this).datepicker("widget").removeClass("dobooking-date");
            }
        });

        self.day_select = self.template.find(".day-select");
        self.day_select.on("click",function() {
            self.day_select_trigger( this );
        });

        self.time_start = self.template.find("select[name='time-start']");
        self.time_end = self.template.find("select[name='time-end']");

        self.time_start.find("option:first").prop("selected",true);
        self.time_end.find("option:last").prop("selected",true);
    }

    this.time_step_all = function () {
        self.step_html.find(".time").on("click",function() {
            var prev_time_slot = self.step_html.find(".time");
            prev_time_slot.removeClass("slot-selected");
            prev_time_slot.find(".spin-icon").addClass("spin-hide");
            jQuery(this).addClass("slot-selected");
            jQuery(this).find(".spin-icon").removeClass("spin-hide");
            self.next_btn.trigger("click");
        });
    }

    this.category_select_trigger = function (ptr) {
        var cat_val = jQuery(ptr).val();
        //.........for services select..................
        var services = self.services.clone();
        var service_select = self.step_html.find(".service-select");
        service_select.empty();
        if ( cat_val != "" ) {
            var options = '<option>';
            options += service_select.attr("data-select_default");
            options += '</option>';
            jQuery.each(services,function(index,item){
                var condition = self.category_services["category_"+cat_val].indexOf(
                    jQuery(item).attr("value")
                );
                if ( condition != -1 ) {
                    options += '<option value="'+jQuery(item).attr("value")+'">';
                    options += jQuery(item).text();
                    options += '</option>';
                }
            });
            service_select.html( options );
        }
        else {
            service_select.html( services );
        }
        //.......end services select...............
        //......for members select.................
        var members = self.members.clone();
        var member_select = self.step_html.find(".member-select");
        member_select.empty();
        if ( cat_val != member_select.attr("data-select_default_value") ) {
            var options = '<option value="'+member_select.attr("data-select_default_value")+'">';
            options += member_select.attr("data-select_default");
            options += '</option>';
            jQuery.each(members,function(index, item) {
                var condition = self.category_members["category_"+cat_val].indexOf(
                    jQuery(item).attr("value")
                );
                if ( condition != -1 ) {
                    options += '<option value="'+jQuery(item).attr("value")+'">';
                    options += jQuery(item).text();
                    options += '</option>';
                }
            });
            member_select.html( options );
        }
        else {
            member_select.html( members );
        }
        //......end members select.................
    }

    this.service_select_trigger = function ( ptr ) {
        if ( jQuery(ptr).hasClass('error') ) {
            jQuery(ptr).removeClass('error');
            jQuery(ptr).next().html("");
        }
        console.log("in service select");
        var ser_val = jQuery(ptr).val();
        var category_select = self.step_html.find(".category-select");
        //............for category select..................
        console.log( self.service_categories );
        var condition = self.service_categories["service_"+ser_val].indexOf(
            category_select.val()
        )
        if ( condition == -1 ) {
            console.log( "Not selected" );
            var category_id = self.service_categories["service_"+ser_val][0];
            category_select.val( category_id );
        }
        else {
            console.log( "Already selected" );
        }
        //............end category select..................
        //............for member select..................
        var members = self.members.clone();
        var member_select = self.step_html.find(".member-select");
        member_select.empty();
        if ( ser_val != member_select.attr("data-select_default_value") ) {
            var options = '<option value="'+member_select.attr("data-select_default_value")+'">';
            options += member_select.attr("data-select_default");
            options += '</option>';
            jQuery.each(members,function(index, item) {
                var condition = self.service_members["service_"+ser_val].indexOf(
                    jQuery(item).attr("value")
                );
                if ( condition != -1 ) {
                    options += '<option value="'+jQuery(item).attr("value")+'">';
                    options += jQuery(item).text();
                    options += '</option>';
                }
            });
            member_select.html( options );
        }
        else {
            member_select.html( members );
        }
        //............end member select..................
    }

    this.day_select_trigger = function ( ptr ) {
        var element = jQuery(ptr).find(".day-include-uninclude");
        if ( element.hasClass("day-include") ) {
            element.removeClass("day-include");
            element.addClass("day-uninclude");
        }
        else {
            element.addClass("day-include");
            element.removeClass("day-uninclude");
        }
    }

    this.back_btn = this.template.find(".btn-previous");
    this.back_btn.off();
    this.back_btn.on("click",function(){
        if ( self.current_step == self.first_step || self.current_step == self.last_step ) {
            self.back_btn.hide();
        }

        if ( self.current_step == "time" ) {}

        var spin_icon = self.back_btn.find(".spin-icon");
        spin_icon.removeClass("spin-hide");

        ajax_request_front(
            {
                action: "rbdobooking_form",
                security: RBDoBookingAjax.security_rbdobooking_form,
                //step_data: all_data,
                type: "back-step",
                vendor_id: RBDoBookingHelper.vendor_id,
                current_step: self.current_step,
                form_id: RBDoBookingHelper.form_id,
                cart_number: self.cart_number,
                action_type: "render"
            },
            function ( response ) {
                console.log( response );
                spin_icon.addClass("spin-hide");
                self.back_step_success( response.data );
            }
        );
    });

    this.back_step_success = function ( data ) {
        var element = self.step_html;
        element.empty();
        element.html( data.html );
        var all_steps = self.steps_container.find("li");
        all_steps.removeClass("active");
        var found = false;
        jQuery.each(all_steps,function(index,item) {
            if ( jQuery(item).hasClass(data.prev_step+"-step") ) {
                jQuery(item).addClass("active");
                return false;
            }
            else {
                jQuery(item).addClass("active");
            }
        });
        self.current_step = data.prev_step;
        if ( data.prev_step == "service" ) {
            self.service_step_all();
            if (typeof data.step_data[self.cart_number] !== 'undefined') {
                self.service_select.val(data.step_data[self.cart_number].service_id);
                self.category_select.val(data.step_data[self.cart_number].category_id);
                self.time_start.val(data.step_data[self.cart_number].start_time);
                self.time_end.val(data.step_data[self.cart_number].end_time);
                jQuery.each(self.day_select,function(index,item){
                    var iterated_day = jQuery(item).attr("data-day");
                    if ( data.step_data[self.cart_number].day.indexOf(iterated_day) == -1 ) {//not founf
                        var obj = jQuery(item).find(".day-include-uninclude");
                        obj.removeClass("day-include");
                        obj.addClass("day-uninclude");
                    }
                    if ( data.step_data[self.cart_number].member_id.length == 1 ) {
                        self.member_select.val(data.step_data[self.cart_number].member_id[0]);
                    }
                });
            }
        }
        else if ( data.prev_step == "time" ) {
            var timing_obj = self.step_html.find(".time[data-start='"+data.step_data[self.cart_number].start+"'][data-end='"+data.step_data[self.cart_number].end+"'][data-timing_ids='"+data.step_data[self.cart_number].timing_ids+"']");
            timing_obj.addClass("time-selected");
            timing_obj.addClass("slot-selected");
            self.time_step_all();
            console.log( timing_obj );
        }
        else if ( data.prev_step == "cart" ) {
            self.cart_step_all_btns_event_handlers( element );
        }
        else if ( data.prev_step == "details" ) {
            self.step_html.find(".details-email").val( data.step_data["details-email"] );
            self.step_html.find(".details-name").val( data.step_data["details-name"] );
            self.step_html.find(".details-phone").val( data.step_data["details-phone"] );
            self.populate_tpl_fields(data.step_data);
        }
    }

    this.populate_tpl_fields = function (step_data) {
        jQuery.each(step_data,function(index, item) {
            console.log(index,item);
            if ( index.search("text_field") != -1 ) {//for text field

                self.step_html.find("input[name='"+index+"']").val( item );

            } else if ( index.search("dropdown") != -1 ) {//for dropdown

                self.step_html.find("select[name='"+index+"']").val( item );

            } else if ( index.search("checkbox_group") != -1 ) {//for checkboxgroup

                var temp_vals = item.split(";");
                jQuery.each(temp_vals,function(index2,item2) {
                    self.step_html.find("input[name='"+index+"'][value='"+item2+"']").prop("checked",true);
                });

            } else if ( index.search("radio_button_group") != -1 ) {//for radio button group
                self.step_html.find("input[name='"+index+"'][value='"+item+"']").prop("checked",true);
            }
        });
    }

    this.next_btn = this.template.find(".btn-next");
    this.next_btn.off();
    this.next_btn.on("click",function() {

        if ( self.current_step == null ) {
            self.current_step = RBDoBookingHelper.step;
        }

        var all_data = null;

        if ( self.current_step == "service" ) {
            var service_select = self.template.find(".service-select");
            if ( service_select.val() == "" ) {
                service_select.addClass("error");
                var error_element = service_select.next();
                error_element.html( error_element.attr("data-error_msg") );
                return false;
            }

            all_data = [];
            var readable_options = self.template.find(".read-option");
            jQuery.each(readable_options,function(index,item){
                var obj = new Object();
                obj.name = jQuery(item).attr("name");

                if ( obj.name == "date_available" ) {
                    var date = jQuery(this).datepicker("getDate");
                    var date_month = parseInt(date.getMonth())+1;
                    if ( date_month < 10 ) {
                        date_month = '0'+date_month;
                    }
                    var d = date.getDate();
                    if ( d < 10 ) {
                        d = "0"+d;
                    }
                    obj.value = date.getFullYear()+"-"+date_month+"-"+d;
                }
                else {
                    obj.value = jQuery(this).val();
                }
                all_data.push( obj );
            });
            var day_select = new Object();
            day_select.name = "selected-days";
            day_select.value = [];
            self.template.find(".day-select").each(function(index,item) {
                if ( jQuery(item).find(".day-include-uninclude").hasClass("day-include") ) {
                    day_select.value.push( jQuery(this).attr("data-day") );
                }
            });
            day_select.value = day_select.value.join(",");
            all_data.push( day_select );
        }
        else if ( self.current_step == "time" ) {
            var selected_slot = self.template.find(".slot-selected");
            all_data = new Object();
            all_data.start = selected_slot.attr("data-start");
            all_data.end = selected_slot.attr("data-end");
            all_data.timing_ids = selected_slot.attr("data-timing_ids");
            all_data.member_ids = selected_slot.attr("data-member_ids");
        }
        else if ( self.current_step == "details" ) {
            all_data = new Object();
            var data_type = null;
            jQuery.each(self.template.find(".details-input"),function(index,item) {
                data_type = jQuery(item).attr("data-type");
                if ( data_type == "input" ) {
                    all_data[jQuery(item).attr("name")] = jQuery(item).val();
                }
            });
            var search_value_with_name = self.search_for_checkbox_or_radio_values(
                self.template.find(".field_checkbox")
            );
            if ( search_value_with_name != null ) {
                all_data[ search_value_with_name[0] ] = search_value_with_name[1];
            }
            var search_value_with_name = self.search_for_checkbox_or_radio_values(
                self.template.find(".field_radio")
            );
            if ( search_value_with_name != null ) {
                all_data[ search_value_with_name[0] ] = search_value_with_name[1];
            }
            console.log( all_data );
        }
        else if ( self.current_step == "payment" ) {
            var is_selected = false;
            all_data = new Object();
            jQuery.each(self.template.find(".payment-method"),function(index,item){
                if ( jQuery(item).prop("checked") ) {
                    is_selected = true;
                    all_data.payment_method = jQuery(item).val();
                }
            });
            if ( is_selected == false ) {
                return;
            }
        }

        jQuery(this).find(".spin-icon").removeClass("spin-hide");

        ajax_request_front(
            {
                action: "rbdobooking_form",
                security: RBDoBookingAjax.security_rbdobooking_form,
                step_data: all_data,
                type: "next-step",
                vendor_id: RBDoBookingHelper.vendor_id,
                current_step: self.current_step,
                form_id: RBDoBookingHelper.form_id,
                cart_number: self.cart_number,
                action_type: "submit"
            },
            function( response ) {
                console.log( response );
                if ( response.success ) {

                    self.back_btn.show();

                    self.next_step_success( response.data );
                }
                self.next_btn.find(".spin-icon").addClass("spin-hide");
            }
        );
    });

    this.search_for_checkbox_or_radio_values = function( element ) {
        var vals = [];
        var name = null;
        jQuery.each(element,function(index,item){
            jQuery(item).find("input").each(function(j,subitem) {
                if ( jQuery(subitem).prop("checked") ) {
                    vals.push( jQuery(subitem).val() );
                    name = jQuery(subitem).attr("name");
                }
            });
        });
        if ( vals.length != 0 ) {
            var arr = [ name, vals.join(";") ];
            return arr;
        }
        else {
            return null;
        }
    }

    this.hasProp = function(obj, prop) {
        return Object.prototype.hasOwnProperty.call(obj, prop);
    }

    this.next_step_success = function ( data ) {
        var element = self.step_html;
        element.empty();
        element.html( data.html );
        if ( data.next_step == "time" ) {
            self.current_step = data.next_step;
            self.steps_container.find("li.time-step").addClass("active");
            self.time_step_all();
            if ( data.step_data_already_exists ) {
                if (typeof data.step_data[self.cart_number] !== 'undefined') {
                    var timing_obj = self.step_html.find(".time[data-start='"+data.step_data[self.cart_number].start+"'][data-end='"+data.step_data[self.cart_number].end+"'][data-timing_ids='"+data.step_data[self.cart_number].timing_ids+"']");
                    timing_obj.addClass("time-selected");
                    timing_obj.addClass("slot-selected");
                }
            }
        }
        else if ( data.next_step == "cart" ) {
            self.current_step = data.next_step;
            if (typeof data.cart_count !== 'undefined') {
                self.cart_number = parseInt( data.cart_count );
            }
            self.steps_container.find("li.cart-step").addClass("active");

            self.cart_step_all_btns_event_handlers( element );

        }
        else if ( data.next_step == "details" ) {
            self.current_step = data.next_step;
            self.steps_container.find("li.details-step").addClass("active");
            if ( data.step_data_already_exists ) {
                self.step_html.find(".details-email").val(data.step_data["details-email"]);
                self.step_html.find(".details-phone").val(data.step_data["details-phone"]);
                self.step_html.find(".details-name").val(data.step_data["details-name"]);
                self.populate_tpl_fields(data.step_data);
            }
        }
        else if ( data.next_step == "payment" ) {
            self.current_step = data.next_step;
            self.steps_container.find("li.payment-step").addClass("active");
        }
        else if ( data.next_step == "done" ) {
            self.current_step = data.next_step;
            self.steps_container.find("li.done-step").addClass("active");
            self.next_btn.hide();
            self.back_btn.hide();
        }
    }

    this.cart_step_all_btns_event_handlers = function ( element ) {
        element.find(".btn-book-more").off("click").on("click",function() {
            self.book_more_btn_click( this );
        });
        element.find(".btn-edit-booking").off("click").on("click",function() {
            self.btn_edit_booking_click( this );
        });
        element.find(".btn-del-booking").off("click").on("click",function() {
            self.btn_del_booking_click( this );
        });
    }

    this.book_more_btn_click = function( ptr ) {
        console.log( self.cart_number );
        self.current_step = "time";
        self.cart_number++;
        self.back_btn.trigger("click");
    }

    this.btn_edit_booking_click = function ( ptr ) {//
        console.log( self.cart_number );
        self.current_step = "time";
        self.cart_number = parseInt( jQuery(ptr).closest("tr").attr("data-iteration") );
        self.back_btn.trigger("click");
    }

    this.btn_del_booking_click = function ( ptr ) {
        console.log( self.cart_number );
    }

}

ajax_request_front = function (data, success_callback,other_data) {
    if ( typeof(other_data) === undefined ) {
        other_data = null;
    }
    //-------------------------- send request using the core $.ajax() method
    jQuery.ajax({
        url: RBDoBookingAjax.ajaxurl,
        type: "POST",
        dataType: "JSON",
        data: data,
        //------------------------------------------------------------------
        beforeSend: function (xhr) {},
        //------------------------------ Code to run if the request succeeds
        success: function (response) {
            if ( other_data != null ) {
                success_callback(response, other_data);
            }
            else {
                success_callback(response);
            }

        },
        //--------------------------------- Code to run if the request fails
        error: function (xhr, status, errorThrown) {},
        //---------------------------- Code to run when the request finishes
        //                  (after success and error callbacks are executed)
        complete: function (xhr, status) {}
    });
};
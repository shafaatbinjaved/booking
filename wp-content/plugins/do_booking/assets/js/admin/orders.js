var rbdobooking_orders = null;

jQuery(document).ready(function() {
    rbdobooking_orders = new RBDoBooking_Orders();
});

function RBDoBooking_Orders() {

    var self = this;
    this.vendor_id = 1;
    this.open_order_id = 0;

    this.component = jQuery("#rbdobooking-orders");

    this.payments_ui_slidepanel = this.component.find(".payments-ui-slidepanel");
    this.payments_ui_btn_close = this.payments_ui_slidepanel.find(".btn-close");
    this.payments_ui_btn_close.on("click",function() {
        self.open_order_id = 0;
        self.payments_ui_slidepanel_hide ()
    });

    this.payments_area = this.component.find(".orders-area .table");
    this.payments_area.find("tbody tr").on("click",function() {
        var order_id = jQuery(this).attr("data-order_id");
        if ( self.open_order_id != order_id ) {
            self.payments_area.find("tbody tr").removeClass("highlight-row");
            jQuery(this).addClass("highlight-row");
            self.open_order_id = order_id;
            self.show_payments_ui_panel_with_relevant_data(
                "show_order_data",
                order_id
            );
        }
    });

    this.register_invoice_download_click = function() {
        self.component.find("tbody tr td .btn-invoice").on("click",function(e) {
            e.preventDefault();
            e.stopPropagation();
            var tr = jQuery(this).closest('tr');
            var row = self.appointments_dataTable.row( tr );
            var id_time_slot = jQuery(this).closest("tr").attr("data-id_time_slot");

            ajax_request(
                {
                    action: "rbdobooking_appointments",
                    security: RBDoBookingAjax.security_appointments,
                    type: "download_invoice",
                    id_time_slot: id_time_slot,
                    vendor_id: self.vendor_id
                },
                function( response ) {
                    console.log( response );
                }
            );
        });
    }

    this.show_payments_ui_panel_with_relevant_data = function( type, order_id ) {

        var post_data = null;

        if ( type == "show_order_data" ) {
            self.open_order_id = order_id;
            post_data = {
                action: "rbdobooking_orders",
                security: RBDoBookingAjax.security_orders,
                type: "show_order_data",
                order_id: order_id,
                "vendor_id": self.vendor_id
            }
        }

        ajax_request(
            post_data,
            function( resp ) {
                if ( resp.success ) {

                    var heading = self.payments_ui_slidepanel.find(".heading");
                    var options = '';

                    if ( type == "show_order_data" ) {

                        var heading_val = heading.attr('data-show_order_data_heading');
                        heading.html( heading_val );

                        self.payments_ui_slidepanel.find(".detail-part").html( resp.data.html );

                        self.payments_ui_slidepanel.show("slow");

                    }

                }
                else {
                    new PNotify( resp.data.notification );
                }
            }
        );

    }

    this.form_count=0;

    this.download_invoice_btn = self.component.find(".btn-download-invoice");
    this.download_invoice_btn.off().on("click",function(e){
        e.preventDefault();
        e.stopPropagation();
        self.form_count++;
        var order_id = jQuery(this).closest("tr").attr("data-order_id");
        var url = RBDoBookingAjax.ajaxurl;
        url += '?action=rbdobooking_invoices';
        url += '&type=download_invoice';
        url += '&vendor_id='+self.vendor_id;
        url += '&order_id='+order_id;
        var form_html = '<form class="some_form_'+self.form_count+'" method="post" action="'+RBDoBookingAjax.ajaxurl+'">';
        form_html += '<input type="hidden" name="action" value="rbdobooking_invoices" />';
        form_html += '<input type="hidden" name="type" value="download_invoice" />';
        form_html += '<input type="hidden" name="vendor_id" value="'+self.vendor_id+'" />';
        form_html += '<input type="hidden" name="order_id" value="'+order_id+'" />';
        form_html += '<input type="hidden" name="security" value="'+RBDoBookingAjax.security_invoices+'" />';
        form_html += btn_html;
        form_html += '</form>';
        var btn_html = '<button type="submit">';
        btn_html += '</a>';
        jQuery("body").append(form_html);
        jQuery(".some_form_"+self.form_count).submit();
    });

    this.payments_ui_slidepanel_hide = function() {
        self.open_payment_id = -1;
        self.payments_area.find("tbody tr").removeClass("highlight-row");
        self.payments_ui_slidepanel.hide("slow");
    }

}
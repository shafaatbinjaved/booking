var RBDoBooking_Staff_Members;

jQuery(document).ready(function() {
    RBDoBooking_Staff_Members = new RBDoBooking_Staff_Members();
});

function RBDoBooking_Staff_Members() {

    var self = this;

    /*jQuery(function() {

    });*/
    jQuery.datepicker._updateDatepicker_original = jQuery.datepicker._updateDatepicker;
    jQuery.datepicker._updateDatepicker = function(inst) {
        jQuery.datepicker._updateDatepicker_original(inst);
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow) {
            afterShow.apply((inst.input ? inst.input[0] : null));  // trigger custom callback
        }
    }

    this.component = jQuery("#rbdobooking-staff-members");

    this.tpl_service_category = this.component.find(".tpl-service-category").html();
    this.tpl_service_item = this.component.find(".tpl-service-item").html();
    this.tpl_break_item = this.component.find(".tpl-break-item").html();
    this.tpl_popup_days_off = this.component.find(".tpl-popup-days-off").html();

    this.open_member_id = "-1";

    this.year = new Date().getFullYear();

    this.holidays = [];

    this.btn_add_new_staff_member = this.component.find(".btn-add");
    this.btn_add_new_staff_member.on("click",function() {
        self.slidepanel_initial_state();
        self.show_staff_member_slide_panel();
    });

    this.btn_search_staff_members = this.component.find('.btn-search-staff-member');
    this.btn_search_staff_members.on('keydown',function() {
        var search_val = jQuery(this).val();
        var item_none = self.staff_members_list_area.find("li.item-none");
        if ( item_none ) {
            item_none.remove();
        }

        var all_staff_member_items = self.staff_members_list_area.find("li:not(:first-child)");

        if ( search_val.length > 1 ) {

            var showing_none = true;

            var first_name = null;
            var last_name = null;
            var service = null;
            jQuery.each(all_staff_member_items,function(index,item) {
                item = jQuery( item );
                first_name = item.find(".first_name");
                last_name = item.find(".last_name");
                service = item.find(".member-service");
                var service_vals = '';
                jQuery.each(service,function(index2,item2){
                    item2 = jQuery(item2);
                    service_vals += jQuery(item2).html().toLowerCase()+"!";
                });

                if ( first_name.html().toLowerCase().indexOf(search_val) >= 0 ||
                     last_name.html().toLowerCase().indexOf(search_val) >= 0 ||
                     service_vals.indexOf(search_val) >= 0
                ) {
                    item.show();
                    showing_none = false;
                }
                else {
                    item.hide();
                }
            });
        }
        else {
            all_staff_member_items.show();
        }
    });

    this.slidepanel_staff_member = this.component.find(".slidepanel");
    this.slidepanel_close_btn = this.slidepanel_staff_member.find(".btn-close");
    this.slidepanel_close_btn.on("click",function() {
        console.log("in close");
        self.hide_staff_member_slide_panel();
    });

    this.slidepanel_connect_btn = this.slidepanel_staff_member.find('.btn-connect');

    this.profile_pic_click_handler = function( ptr ) {
        // If the media frame already exists, reopen it.
        if ( self.file_frame != null ) {
            self.file_frame.open();
            return;
        }
        else {
            console.log(jQuery( ptr ).attr( "data-full_name" ));
            // Create the media frame.
            self.file_frame = wp.media.frames.file_frame = wp.media({
                title: jQuery( ptr ).attr( "data-full_name" ),
                button: {
                    //text: jQuery( ptr ).attr( 'data-uploader_button_text' ),
                },
                multiple: false  // Set to true to allow multiple files to be selected
            });
        }

        // When an image is selected, run a callback.
        self.file_frame.on( 'select', function() {
            // We set multiple to false so only get one image from the uploader
            var attachment = self.file_frame.state().get('selection').first().toJSON();

            // Do something with attachment.id and/or attachment.url here
            console.log(attachment);
            var element = self.slidepanel_staff_member.find(".img-container");
            var img_container = '<img class="img-container" src="'+attachment.url+'" />';
            element.replaceWith(img_container);
            element = self.slidepanel_staff_member.find(".img-container");
            element.on("click",function() {
                self.profile_pic_click_handler( this );
            });
        });

        // Finally, open the modal
        self.file_frame.open();
    }

    this.slidepanel_save_btn = this.slidepanel_staff_member.find(".btn-save");
    this.slidepanel_save_btn.on("click",function() {

        var data = new Object();

        if ( self.open_member_id != "-1" ) {
            data.member_id = self.open_member_id;
            data.services = [];
            //services tab data
            var service_data = self.slidepanel_staff_member_services.find(".service_id");
            jQuery.each(service_data, function(index, item) {
                if ( jQuery(item).prop("checked") ) {
                    var service = new Object();
                    service["service_id"] = jQuery(item).attr("data-service_id");
                    var row = jQuery(item).closest(".inside");
                    var member_service_id = row.attr("data-member_service_id");
                    if ( member_service_id == "" ) {
                        member_service_id = "-1";
                    }
                    service["member_service_id"] = member_service_id;
                    service["member_price"] = row.find(".price").val();
                    service["member_min_capacity"] = row.find(".min_capacity").val();
                    service["member_max_capacity"] = row.find(".max_capacity").val();
                    data.services.push( service );
                }
            });
            data.holidays = self.holidays;
        }
        else {
            data.member_id = self.open_member_id;
        }


        data.details = new Object;
        data.schedule = [];
        //details tab data
        var src = self.slidepanel_staff_member.find(".img-container").attr("src");
        if ( src ) {
            data.details["profile_pic"] = src;
        }
        else {
            data.details["profile_pic"] = "";
        }
        var details_data = self.slidepanel_staff_member_details.find("[data-name]");
        jQuery.each(details_data, function(index, item) {
            var name = jQuery(item).attr("data-name");
            data.details[name] = jQuery(item).val();
        });
        //schedule tab data
        var schedule_data = self.slidepanel_staff_member_schedule.find(".day-row");
        jQuery.each(schedule_data,function(index,item) {
            var day = new Object();
            day.day = jQuery(item).attr("data-day");
            day.timing_id = jQuery(item).attr("data-timing_id");
            if ( jQuery(item).find(".schedule-day-on").prop("checked") ) {
                day.is_day_on = 1;
            }
            else {
                day.is_day_on = 0;
            }
            day.start_time = jQuery(item).find(".start-time").val();
            day.end_time = jQuery(item).find(".end-time").val();
            day.all_breaks = [];
            var break_items = jQuery(item).find(".break-item");

            jQuery.each(break_items,function(break_item_index,break_item) {
                var break_obj = new Object();
                var break_id = jQuery(break_item).attr("data-break_id");
                if ( break_id == "0" ) {
                    break_id = "-1";
                }
                break_obj.break_id = break_id;
                break_obj.break_start = jQuery(break_item).find(".content").attr("data-break_start");
                break_obj.break_stop = jQuery(break_item).find(".content").attr("data-break_stop");
                day.all_breaks.push( break_obj );
            });
            data.schedule.push( day );
        });

        ajax_request(
            {
                action: "rbdobooking_staff_members",
                security: RBDoBookingAjax.security_staff_member,
                data: data,
                type: "save_member_data"
            },
            function (response) {
                if ( response.success ) {
                    self.update_member_data(
                        response.data.member_id,
                        response.data.member_html
                    );
                    self.hide_staff_member_slide_panel();
                }
                else {}
                new PNotify(
                    response.data.notification
                );
            }
        );
        console.log( data );
    });

    this.update_member_data = function ( member_id, member_html ) {
        var element = self.staff_members_list_area.find("li[data-member_id='"+member_id+"']");
        if ( element.length != 0 ) {
            element.off();
            element.replaceWith( member_html );
            element = self.staff_members_list_area.find("li[data-member_id='"+member_id+"']");
        }
        else {
            element = jQuery(member_html);
            self.staff_members_list_area.append( element );
        }
        element.on("click",function() {
            self.show_slidepanel_and_get_data(this);
        });
    }

    this.slidepanel_staff_member_details = this.slidepanel_staff_member.find("#details");
    this.slidepanel_staff_member_services = this.slidepanel_staff_member.find("#services");
    this.slidepanel_staff_member_schedule = this.slidepanel_staff_member.find("#schedule");
    this.slidepanel_staff_member_days_off = this.slidepanel_staff_member.find("#days_off");

    this.service_container_in_slidepanel = this.slidepanel_staff_member_services.find(".services-container");

    this.staff_members_list_area = this.component.find(".staff-members-list-area ul");

    this.slidepanel_initial_state = function() {
        self.open_member_id = "-1";

        var heading = self.slidepanel_staff_member.find(".heading");
        heading.html( heading.attr("data-heading") );

        //details tab
        var details = self.slidepanel_staff_member_details;
        var details_elements = details.find(".form-control");
        jQuery.each(details_elements,function(index,item) {
            jQuery(item).val("");
        });
        details.find("select[data-name='visibility']").val("public");

        //schedule tab
        var schedule = self.slidepanel_staff_member_schedule;

        schedule.find(".popup-btn-add").off();
        schedule.find(".popup-btn-add").on("click", function (){
            self.schedule_day_btn_add_break_handler( this );
        });

        schedule.find(".schedule-day-on").off();
        schedule.find(".schedule-day-on").on("click",function() {
            self.schedule_day_on_click_handler( this );
        });
        var day_rows = schedule.find('.day-row');
        jQuery.each(day_rows,function(index,item) {

            jQuery(item).attr("data-timing_id","-1");
            jQuery(item).find(".schedule-day-on").prop("checked",false);

            var start_time_element = jQuery(item).find(".start-time");
            start_time_element.val(
                start_time_element.attr("data-initial")
            );
            start_time_element.prop("disabled",true);

            var end_time_element = jQuery(item).find(".end-time");
            end_time_element.val(
                end_time_element.attr("data-initial")
            );
            end_time_element.prop("disabled",true);

            jQuery(item).find(".input-group").css("opacity","0.5");
            jQuery(item).find(".btn-add-break").css("opacity","0.5");

            jQuery(item).find(".break-item").remove();
        });


        //services nav-item
        self.slidepanel_staff_member.find(".nav-item-services").hide();

        //days off nav-item
        self.slidepanel_staff_member.find(".nav-item-days_off").hide();

    }

    this.edit_button_staff_members = this.staff_members_list_area.find("li:not(:first-child)");
    this.edit_button_staff_members.on("click",function() {

        self.show_slidepanel_and_get_data( this );

    });

    this.file_frame = null;

    this.show_slidepanel_and_get_data = function( ptr ) {

        var member_id = jQuery(ptr).attr("data-member_id");

        if ( self.open_member_id != member_id ) {

            self.staff_members_list_area.find("li.grey-bg-color").removeClass("grey-bg-color");
            jQuery(ptr).addClass("grey-bg-color");

            ajax_request(
                {
                    action: "rbdobooking_staff_members",
                    security: RBDoBookingAjax.security_staff_member,
                    member_id: member_id,
                    type: "get_member_data"
                },
                function ( response ) {
                    self.open_member_id = member_id;
                    if ( response.success ) {
                        var mem_data = response.data.member;
                        var heading = self.slidepanel_staff_member.find(".heading");
                        heading.html(
                            heading.attr("data-edit") + " " + mem_data.first_name + " " + mem_data.last_name
                        );
                        //.................for details tab
                        var details = self.slidepanel_staff_member_details;
                        details.find("[data-name='first_name']").val( mem_data.first_name );
                        details.find("[data-name='last_name']").val( mem_data.last_name );
                        details.find("[data-name='email']").val( mem_data.email );
                        details.find("[data-name='phone']").val( mem_data.phone );
                        details.find("[data-name='visibility']").val( mem_data.visibility );
                        details.find("[data-name='info_member']").val( mem_data.info_member );
                        if ( mem_data.profile_pic != "" ) {
                            var element = self.slidepanel_staff_member.find(".img-container");
                            var html = '<img class="img-container" src="'+mem_data.profile_pic+'" />';
                            element.replaceWith( html );
                        }
                        else {
                            var img_container = self.slidepanel_staff_member.find(".img-container");
                            var html = '<div class="img-container">';
                            html += '<i class="far fa-user"></i>';
                            html += '</div>';
                            img_container.replaceWith( html );
                        }
                        var img_container = self.slidepanel_staff_member.find(".img-container");
                        img_container.attr( "data-full_name", mem_data.first_name + " " + mem_data.last_name );
                        img_container.on("click",function() {
                            self.profile_pic_click_handler( this );
                        });

                        //.................for services tab
                        self.slidepanel_staff_member.find(".nav-item-services").show();
                        var all_available_services = response.data.services;
                        var service_category = self.tpl_service_category;
                        var service_item = self.tpl_service_item;
                        var obj_service_category = null;
                        var obj_service_item = null;
                        var prev_category = '';
                        self.service_container_in_slidepanel.empty();
                        if ( all_available_services.length == 0 ) {
                            var a = '<div>';
                            a += 'There are no services available for this member, please first add them from services page';00
                            a += '</div>';
                            self.service_container_in_slidepanel.append( a );
                        }
                        jQuery.each(all_available_services,function(index,item){

                            if ( item.service_id != null ) {

                                if ( prev_category == "" || prev_category != item.category ) {
                                    obj_service_category = jQuery(service_category);
                                    obj_service_category.find(".category_name").html( item.category );
                                    var category_id_checkbox = obj_service_category.find(".category_id");
                                    category_id_checkbox.attr( 'data-category_id',item.category );
                                }

                                obj_service_item = jQuery(service_item);
                                obj_service_item.find(".service_name").html( item.name );

                                var service_checkbox = obj_service_item.find(".service_id");
                                service_checkbox.attr( "data-service_id",item.service_id );
                                if ( item.member_service_id != null ) {
                                    obj_service_item.attr("data-member_service_id",item.member_service_id);
                                    service_checkbox.prop("checked",true);
                                    obj_service_item.find(".price").val( item.member_price );
                                    obj_service_item.find(".min_capacity").val( item.member_min_capacity );
                                    obj_service_item.find(".max_capacity").val( item.member_max_capacity );
                                }
                                else {
                                    obj_service_item.find(".price").val( item.price );
                                    obj_service_item.find(".min_capacity").val( item.min_capacity );
                                    obj_service_item.find(".max_capacity").val( item.max_capacity );
                                }
                                obj_service_category.append( obj_service_item );

                                self.service_container_in_slidepanel.append( obj_service_category );
                                prev_category = item.category;
                            }
                        });

                        //.................for schedule tab
                        var schedule = self.slidepanel_staff_member_schedule;
                        var timings = response.data.timings;

                        var prev_timing = '';

                        jQuery.each(timings,function(index,item) {

                            var day_row = schedule.find('.day-row[data-day="'+item.day+'"]');
                            day_row.attr("data-timing_id",item.timing_id);

                            if ( prev_timing == '' || prev_timing != item.timing_id || item.timing_id == null ) {

                                prev_timing = item.timing_id;
                                var break_obj = null;

                                if ( item.is_day_on == "1" ) {// Working day
                                    day_row.find(".start-time").val( item.start_time );
                                    day_row.find(".end-time").val( item.end_time );
                                    day_row.find(".start-time").prop("disabled",false);
                                    day_row.find(".end-time").prop("disabled",false);
                                    day_row.find(".input-group").css("opacity","1");
                                    day_row.find(".schedule-day-on").prop("checked",true);
                                    day_row.find(".btn-add-break").prop("disabled",false);
                                    day_row.find(".btn-add-break").css("opacity","1.0");
                                    //for adding breaks

                                    if ( item.break_id != null && item.timing_id != null ) {
                                        break_obj = jQuery(self.tpl_break_item);
                                        break_obj.attr("data-break_id",item.break_id);
                                        var break_content = break_obj.find(".content");
                                        break_content.html( item.break_start_ui_val + " - " + item.break_stop_ui_val );
                                        break_content.attr("data-break_start",item.break_start);
                                        break_content.attr("data-break_stop",item.break_stop);
                                        var all_breaks = day_row.find(".all-breaks .clear");
                                        all_breaks.empty();
                                        all_breaks.before( break_obj );
                                    }
                                }
                                else {// not working day ie day off
                                    day_row.find(".input-group").css("opacity","0.5");
                                    day_row.find(".start-time").prop("disabled",true);
                                    day_row.find(".end-time").prop("disabled",true);
                                    day_row.find(".btn-add-break").prop("disabled",true);
                                    day_row.find(".btn-add-break").css("opacity","0.5");
                                    day_row.find(".schedule-day-on").prop("checked",false);
                                    day_row.find(".all-breaks .break-item").remove();
                                }

                            }
                            else {
                                if ( item.break_id != null && item.timing_id != null ) {
                                    break_obj = jQuery(self.tpl_break_item);
                                    break_obj.attr("data-break_id",item.break_id);
                                    break_obj.find(".content").html( item.break_start_ui_val + " - " + item.break_stop_ui_val );
                                    break_obj.find(".content").attr("data-break_start",item.break_start);
                                    break_obj.find(".content").attr("data-break_stop",item.break_stop);
                                    day_row.find(".all-breaks .clear").before( break_obj );
                                }
                            }
                            day_row.find(".all-breaks .break-item .close-btn").on("click",function() {
                                jQuery(this).closest(".break-item").remove();
                            });
                        });

                        schedule.find(".popup-btn-add").off();
                        schedule.find(".popup-btn-add").on("click", function (){
                            console.log("in btn add click");
                            self.schedule_day_btn_add_break_handler( this );
                        });

                        schedule.find(".schedule-day-on").off();
                        schedule.find(".schedule-day-on").on("click",function() {
                            self.schedule_day_on_click_handler( this );
                        });

                        //for days off tab
                        var days_off_tab = self.slidepanel_staff_member_days_off;
                        self.slidepanel_staff_member.find(".nav-item-days_off").show();
                        days_off_tab.attr("data-member_id",member_id);
                        self.holidays = response.data.holidays;

                        self.draw_calendar_for_year(new Date().getFullYear(),true);

                    } else {

                    }
                }
            );

            self.edit_button_staff_member_handler( ptr );
        }
    }

    this.schedule_day_on_click_handler = function ( ptr ) {
        var day_row = jQuery(ptr).closest(".day-row");
        if ( jQuery(ptr).prop("checked") ) {
            day_row.find(".input-group").css("opacity","1");
            day_row.find(".start-time").prop("disabled",false);
            day_row.find(".end-time").prop("disabled",false);
            day_row.find(".btn-add-break").prop("disabled",false);
            day_row.find(".btn-add-break").css("opacity","1");
        }
        else {
            day_row.find(".input-group").css("opacity","0.5");
            day_row.find(".start-time").prop("disabled",true);
            day_row.find(".end-time").prop("disabled",true);
            day_row.find(".btn-add-break").prop("disabled",true);
            day_row.find(".btn-add-break").css("opacity","0.5");
        }
    }

    this.schedule_day_btn_add_break_handler = function ( ptr ) {
        var popup_content = jQuery(ptr).closest('.popup-content');

        var break_start = popup_content.find(".break-start-select");
        var break_start_text = break_start.find("option[value='"+break_start.val()+"']").html();

        var break_stop = popup_content.find(".break-stop-select");
        var break_stop_text = break_start.find("option[value='"+break_stop.val()+"']").html();

        var new_break_item = jQuery(self.tpl_break_item);
        new_break_item.attr("data-break_id",-1);
        var content = new_break_item.find(".content");
        content.attr("data-break_start",break_start.val());
        content.attr("data-break_stop",break_stop.val());
        content.html( break_start_text + " - " + break_stop_text );

        new_break_item.find(".close-btn").on("click",function() {
            jQuery(ptr).closest(".break-item").remove();
        });
        var day_row = popup_content.closest(".day-row");

        popup_content.closest(".popup").find(".close-btn").click();

        day_row.find(".all-breaks").append( new_break_item );
    }

    this.highlight_holidays_in_calendar = function () {

        if ( self.holidays.length > 0 ) {
            jQuery.each(self.holidays,function(index,item) {

                var date = item.date;
                date = date.split("-");

                var year = parseInt(date[0]);
                var month = parseInt( date[1] ) - 1;
                var day = parseInt(date[2]);

                var day_element = self.slidepanel_staff_member_days_off.find(".month_"+month).find(".day-"+day+"[data-year='"+year+"']");
                day_element.attr("data-repeat_every_year",item.repeat_every_year);
                day_element.find("a").addClass("ui-state-holiday");
                day_element.attr("data-repeat_every_year",item.repeat_every_year);
                day_element.find("a").addClass("ui-state-holiday");

                var calendar = day_element.closest(".month_"+month);
                var prev = calendar.find(".ui-datepicker-prev");
                if ( prev ) {
                    prev.remove();
                }
                var next = calendar.find(".ui-datepicker-next");
                if ( next ) {
                    next.remove();
                }

            });
        }

    }

    this.edit_button_staff_member_handler = function( ptr ) {
        self.show_staff_member_slide_panel();
    }

    this.show_staff_member_slide_panel = function () {
        self.slidepanel_staff_member.show("slow");
    }

    this.hide_staff_member_slide_panel = function () {
        self.open_member_id = "-1";
        self.staff_members_list_area.find("li.grey-bg-color").removeClass("grey-bg-color");
        self.slidepanel_staff_member.hide("slow");
    }

    this.change_year = this.slidepanel_staff_member_days_off.find(".change-year");
    this.change_year.on("click",function() {
        if ( jQuery(this).hasClass("year-minus") ) {
            self.redraw_calendar( "minus" );
        }
        else if ( jQuery(this).hasClass("year-plus") ) {
            self.redraw_calendar( "plus" );
        }
    });

    this.redraw_calendar = function( type ) {
        if ( type == "plus" ) {
            self.year++;
        }
        else if ( type == "minus" ) {
            self.year--;
        }
        self.draw_calendar_for_year( self.year, true );
    }

    this.on_date_select = function( dateText, obj ) {

        //dateText format day-month-year
        var popup_days_off_obj = jQuery(self.tpl_popup_days_off);
        popup_days_off_obj.find(".date").html( dateText );
        popup_days_off_obj.find(".close-btn").on("click",function() {
            self.slidepanel_staff_member_days_off.find(".datepicker .ui-state-active").removeClass("ui-state-active");
            self.close_popup_days_off( this );
        });

        var date = dateText.split("-");
        var month = parseInt(date[1]) - 1;
        var day = parseInt(date[0]);
        var year = date[2];
        var selected_date = self.slidepanel_staff_member_days_off.find(".month_"+month).find(".day-"+day+"[data-year='"+year+"']");
        var a = selected_date.offset();
        var offset_top = a.top - jQuery(document).scrollTop() - 123;
        var offset_left = a.left - jQuery(document).scrollLeft() - 270;
        popup_days_off_obj.css("top",offset_top);
        popup_days_off_obj.css("left",offset_left);

        if ( selected_date.attr("data-repeat_every_year") == "1" ) {
            popup_days_off_obj.find(".repeat-every-year").prop("checked",true);
        }
        popup_days_off_obj.find(".popup-btn-add").on("click",function() {
            self.save_member_holiday(this);
        });

        var popup_container = self.slidepanel_staff_member_days_off.find(".popup-days-off-container");
        popup_container.html( popup_days_off_obj );

    }

    this.save_member_holiday = function ( ptr ) {
        var member_id = jQuery(ptr).closest("#days_off").attr('data-member_id');
        var popup_days_off = jQuery(ptr).closest(".popup-days-off");
        var date = popup_days_off.find(".date").html();
        date = date.split("-");
        var year = date[2];
        var month = date[1];
        var day = date[0];
        date = date[2]+"-"+date[1]+"-"+date[0];
        var is_holiday = popup_days_off.find(".not-working-on-date").prop("checked");
        var repeat_every_year = popup_days_off.find(".repeat-every-year").prop("checked");

        ajax_request(
            {
                action: "rbdobooking_staff_members",
                security: RBDoBookingAjax.security_staff_member,
                repeat_every_year:  repeat_every_year,
                is_holiday:         is_holiday,
                member_id:          member_id,
                date:               date,
                type: "save_member_holiday"
            },
            function ( response ) {
                if ( response.success ) {
                    self.holidays = response.data.member_holidays;
                    popup_days_off.find(".close-btn").click();
                    self.highlight_holidays_in_calendar();
                    var element = self.slidepanel_staff_member_days_off.find(".month_"+month+" day-"+day+"[data-year='"+year+"']");

                    if (is_holiday ) {
                        element.find("a").addClass("ui-state-holiday");
                    }
                    else {
                        element.find("a").removeClass("ui-state-holiday");
                    }
                }
                else {
                }
                new PNotify( response.data.notification );
            }
        );
    }

    this.close_popup_days_off = function ( ptr ) {
        jQuery( ptr ).closest(".popup-days-off").remove();
    }

    this.draw_calendar_for_year = function(year, destroy) {
        if ( destroy ) {
            self.slidepanel_staff_member_days_off.find(".datepicker").datepicker('destroy');
        }
        self.slidepanel_staff_member_days_off.find(".year").html( year );
        var month = 1;
        var day = 1;
        var date_str = "";
        for ( var $month=0; $month<12; $month++ ) {
            var element = self.slidepanel_staff_member_days_off.find('.month_'+$month);
            element.datepicker({
                changeMonth: false,
                changeYear: false,
                showButtonPanel: false,
                dateFormat: 'dd-mm-yy',
                defaultDate: new Date( year, $month, day),
                onSelect: function( dateText, obj ) {
                    self.on_date_select( dateText, obj );
                },
                beforeShowDay: function(date) {
                    return [true, 'day-'+date.getDate()];
                },
                afterShow: function() {
                    self.highlight_holidays_in_calendar();
                }
            });
            element.find('.ui-datepicker-prev').remove();
            //element.find('.ui-datepicker-year').remove();
            element.find('.ui-datepicker-next').remove();
            var dayElement = element.find('.ui-state-active');
            dayElement.removeClass('ui-state-active');
            dayElement.removeClass('ui-state-hover');
            element.find('.ui-state-highlight').removeClass('ui-state-highlight');
        }
    }

    this.btn_add_break = this.slidepanel_staff_member_schedule.find(".btn-add-break");
    this.btn_add_break.on("click",function() {
        jQuery(this).prev().show("slow");
    });

    this.btn_close_break = this.slidepanel_staff_member_schedule.find(".close-btn");
    this.btn_close_break.on("click",function() {
        jQuery(this).closest(".popup").hide("slow");
    });

    this.all_breaks = this.slidepanel_staff_member_schedule.find(".all-breaks");

    this.break_item_close_btn = this.all_breaks.find(".break-item .close-btn");
    this.break_item_close_btn.on("click",function() {
        jQuery(this).parent().remove();
    });

    this.staff_members_list_area.find("input[type='checkbox']").on("click",function(event) {
        event.stopPropagation();
        if ( jQuery(this).prop("checked") ) {
            jQuery(this).closest(".single-staff-member").addClass("grey-bg-color");
        }
        else {
            jQuery(this).closest(".single-staff-member").removeClass("grey-bg-color");
        }
    });

    this.delete_staff_member_ids = [];

    this.del_staff_member_confirm_modal = this.component.find(".del-staff-member-confirm-modal");
    this.del_staff_member_confirm_modal.find(".delete-staff-member-confirm").on("click",function() {

        ajax_request(
            {
                action: "rbdobooking_staff_members",
                security: RBDoBookingAjax.security_staff_member,
                data: self.delete_staff_member_ids.join(","),
                type: "delete_staff_members"
            },function ( response ) {
                if ( response ) {
                    self.del_staff_member_confirm_modal.modal("hide");
                    jQuery.each(self.delete_staff_member_ids, function (index,item) {
                        self.staff_members_list_area.find("li[data-member_id='"+item+"']").remove();
                    });
                }
                else {}
                new PNotify(response.data.notification);
            }
        );

    });

    this.btn_delete_multi_staff_members = this.component.find(".btn-delete-multi");
    this.btn_delete_multi_staff_members.on("click",function() {
        self.delete_staff_member_ids = [];
        self.staff_members_list_area.find("input[type='checkbox']").each(function(index, item) {
            if ( jQuery(item).prop("checked") ) {
                self.delete_staff_member_ids.push(
                    jQuery(item).closest(".single-staff-member").attr("data-member_id")
                );
            }
        });

        console.log( self.delete_staff_member_ids );
        console.log( self.del_staff_member_confirm_modal );
        var length = self.delete_staff_member_ids.length;
        if ( length > 1 ) {
            self.del_staff_member_confirm_modal.find(".staff-member-1").hide();
            self.del_staff_member_confirm_modal.find(".staff-member-multi").show();
            self.del_staff_member_confirm_modal.modal("show");
            console.log("in if");
        }
        else if ( length == 0 ) {
            new PNotify({
                type: 'warning',
                title: 'Warning',
                text: 'None of staff members is marked'

            });
        }
        else {
            self.del_staff_member_confirm_modal.find(".staff-member-multi").hide();
            self.del_staff_member_confirm_modal.find(".staff-member-1").show();
            self.del_staff_member_confirm_modal.modal("show");
            console.log("in else");
        }
    });

}
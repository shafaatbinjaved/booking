var rbdobooking_calendar = null;

jQuery(document).ready(function() {
    rbdobooking_calendar = new RBDoBooking_Calendar();
    rbdobooking_calendar.initialize_calendar();
    //rbdobooking_calendar.some();
});

function RBDoBooking_Calendar() {

    var self = this;
    this.vendor_id = 1;
    this.open_id_time_slot = 0;

    this.component = jQuery("#rbdobooking-calendar");

    this.calendar_del_modal = this.component.find(".del-calendar-confirm-modal");

    this.calendar_ui_slidepanel = this.component.find(".calendar-ui-slidepanel");
    this.calendar_ui_btn_close = this.calendar_ui_slidepanel.find(".btn-close");
    this.calendar_ui_btn_close.on("click",function() {
        self.calendar_ui_slidepanel_hide ()
    });

    this.advanced_filter_options = this.component.find(".advanced-filter-options");

    /*var dateRange = jQuery("#adSearchForm_{F_NAME}");
    dateRange.datepicker({
        clearBtn: true,
        keepEmptyValues: true,
        format: 'dd.mm.yyyy'
    });*/

    /*this.custom_date_range = this.advanced_filter_options.find("#adSearchForm_");
    this.custom_date_range.datepicker({
        clearBtn: true,
        keepEmptyValues: true,
        format: 'dd.mm.yyyy'
    });*/

    this.appointment_date_range = this.advanced_filter_options.find(".appointment-date-range");
    this.appointment_date_range.find("li").on("click",function(event) {
        event.preventDefault();
        event.stopPropagation();
        self.appointment_date_range.find("li.active").removeClass("active");
        jQuery(this).addClass("active");
        var action_value = parseInt(jQuery(this).attr("data-value"));
        if ( action_value == 7 ) {
            self.appointment_date_range.css("width","750px");
            var left_col = jQuery(this).closest(".left-col");
            var right_col = left_col.next();
            right_col.addClass("show");
            right_col.show();
        }
        else {
            var left_col = jQuery(this).closest(".left-col");
            var right_col = left_col.next();
            if ( right_col.hasClass("show") ) {
                self.appointment_date_range.css("width","207px");
                right_col.hide().removeClass("show");
            }
        }
    });

    this.calendar = this.component.find("#calendar");

    this.members_container = self.component.find(".members-container");
    this.members_container.find(".staff-member-li").on("click",function() {

        var temp = jQuery(this).find("a").attr("data-member_id");

        if ( temp != "all" ) {
            self.selected_member_ids = [ temp ];
        }
        else {
            self.selected_member_ids = temp;
        }

        self.calendar.fullCalendar( 'destroy' );
        //self.initialize_calendar();
        
    });

    this.selected_member_ids = 'all';

    this.initialize_calendar = function() {
        var calendarEl = document.getElementById('calendar');
        self.fullcalendar = new FullCalendar.Calendar(calendarEl,{
            plugins: [ 'bootstrap', 'dayGrid', 'timeGrid', 'list', 'interaction'],//,'resourceTimeGrid' ],
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },
            //themeSystem: themeSystem,
            resources: [
                { id: '1', title: 'Room A' },
                { id: '2', title: 'Room B'},
                { id: '3', title: 'Room C' },
                { id: '4', title: 'Room D' },
                { id: '5', title: 'Room E' },
                { id: '6', title: 'Room F' },
                { id: '7', title: 'Room G' },
                { id: '8', title: 'Room H' },
                { id: '9', title: 'Room I' },
                { id: '10', title: 'Room J' }
            ],
            timeFormat: 'H:mm - H:mm', // 24h format
            defaultView: 'dayGridMonth',//'timeGridWeek',
            allDayDefault: false,
            eventDataTransform: function (event) {
                return event;
            },
            eventSources: [{
                url: RBDoBookingAjax.ajaxurl,
                method: 'POST',
                extraParams: {
                    action: "rbdobooking_calendar",
                    security: RBDoBookingAjax.security_calendar,
                    type: "get_calendar_booking",
                    vendor_id: self.vendor_id,
                    member_ids: self.selected_member_ids
                    //self.members_container.find("a.active").attr("data-member_id")
                }
            }],
            eventRender: function( info ) {
                if ( info.view.type == "dayGridMonth" ) {
                    var element = info.el.cloneNode(true);
                    element = jQuery(element);
                    var html = '';
                    var time = element.find('.fc-time');
                    var html = info.event.extendedProps.formatted_event_time;
                    time.html( html );
                    var title = element.find(".fc-title");
                    html = '<br />';
                    html += info.event.extendedProps.service_name;
                    html += '<br />';
                    html += info.event.extendedProps.customer_full_name;
                    html += '<br />';
                    html += info.event.extendedProps.phone;
                    html += '<br />';
                    html += info.event.extendedProps.email;
                    html += '<br />';
                    html += 'Status: ' + info.event.extendedProps.is_booked;
                    title.html( html );
                    html = '<td>' + element.wrap('<p/>').parent().html() + '</td>';
                    return jQuery(html);
                }
                else if ( info.view.type == "listWeek" ) {
                }
                console.log( info.view.type );
            },
            views: {
                listWeek: {
                    titleFormat: { year: 'numeric', month: '2-digit', day: '2-digit' }
                }
            },
            eventClick: function( event, jsEvent, view ) {
                console.log(
                    "a",
                    event,
                    jsEvent,
                    view
                );
            },
            /*dayClick: function() {
                self.show_calendar_ui_panel_with_relevant_data( "add_time_slot", null );
            },*/
            dateClick: function(info) {
                console.log('Date: ' + info.dateStr, info);
                self.show_calendar_ui_panel_with_relevant_data( "add_time_slot", null, info.dateStr, null );
            }
        });

        self.fullcalendar.render();
    }

    /*this.initialize_calendar = function() {
        console.log("ssomoe");
        var calendarEl = document.getElementById('calendar');//self.calendar;
        //console.log( calendarEl, "in function" );
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'dayGrid', 'timeGrid', 'list', 'interaction' ],
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },
            defaultDate: '2019-04-12',
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [
                {
                    title: 'All Day Event',
                    start: '2019-04-01',
                },
                {
                    title: 'Long Event',
                    start: '2019-04-07',
                    end: '2019-04-10'
                },
                {
                    groupId: 999,
                    title: 'Repeating Event',
                    start: '2019-04-09T16:00:00'
                },
                {
                    groupId: 999,
                    title: 'Repeating Event',
                    start: '2019-04-16T16:00:00'
                },
                {
                    title: 'Conference',
                    start: '2019-04-11',
                    end: '2019-04-13'
                },
                {
                    title: 'Meeting',
                    start: '2019-04-12T10:30:00',
                    end: '2019-04-12T12:30:00'
                },
                {
                    title: 'Lunch',
                    start: '2019-04-12T12:00:00'
                },
                {
                    title: 'Meeting',
                    start: '2019-04-12T14:30:00'
                },
                {
                    title: 'Happy Hour',
                    start: '2019-04-12T17:30:00'
                },
                {
                    title: 'Dinner',
                    start: '2019-04-12T20:00:00'
                },
                {
                    title: 'Birthday Party',
                    start: '2019-04-13T07:00:00'
                },
                {
                    title: 'Click for Google',
                    url: 'http://google.com/',
                    start: '2019-04-28'
                }
            ]
        });

        calendar.render();
    }*/

    this.customer_select_intialize = false;
    this.customers = null;

    this.show_calendar_ui_panel_with_relevant_data = function( type, id_time_slot, date, time ) {

        var post_data = null;

        if ( type == "add_time_slot" ) {
            post_data = {
                action: "rbdobooking_calendar",
                security: RBDoBookingAjax.security_calendar,
                type: "get_calendar_form_data",
                vendor_id: self.vendor_id
            }
        }
        else if ( type == "edit_time_slot" ) {}

        ajax_request(
            post_data,
            function( resp ) {
                if ( resp.success ) {

                    var heading = self.calendar_ui_slidepanel.find(".heading");
                    var options = '';

                    if ( type == "add_time_slot" ) {

                        var heading_val = heading.attr('data-add_heading');
                        heading.html( heading_val );

                        var members_options = '';
                        jQuery.each(resp.data.staff_members,function(index,item){
                            members_options += '<option value="'+item.member_id+'">';
                            members_options += item.first_name+" "+item.last_name;
                            members_options += '</option>';
                        });
                        self.calendar_ui_slidepanel.find(".member_id").html( members_options );

                        var services_options = '';
                        jQuery.each( resp.data.services, function(index,item) {
                            services_options += '<option value="'+item.service_id+'">';
                            services_options += item.name + " ( "+parseFloat(parseInt(item.duration)/60).toFixed(2)+" h )";
                            services_options += '</option>';
                        });
                        self.calendar_ui_slidepanel.find(".service_id").html( services_options );

                        var customer_options = '';
                        self.customers = resp.data.customers;
                        jQuery.each( resp.data.customers, function(index,item) {
                            customer_options += '<option value="'+item.customer_id+'">';
                            customer_options += item.full_name;
                            customer_options += '</option>';
                        });
                        var customer_select = self.calendar_ui_slidepanel.find("select.customer-select");
                        customer_select.html( customer_options );
                        if ( self.customer_select_intialize ) {
                            customer_select.selectpicker("destroy");
                            customer_select.selectpicker();
                            customer_select.off();
                        }
                        else {
                            self.customer_select_intialize = true;
                            customer_select.selectpicker();
                        }
                        customer_select.on("changed.bs.select",function() {
                            console.log("changed customer");
                            var customer_id = jQuery(this).val();
                            jQuery.each(self.customers,function(index,customer){
                                console.log( customer_id, customer.customer_id );
                                if ( customer_id == customer.customer_id ) {
                                    console.log( "in if" );
                                    var customer_names = self.calendar_ui_slidepanel.find(".customer-names");
                                    customer_names.append(
                                        customer_names.find(".customer-name:last-child").clone().html(customer.full_name)
                                    );
                                    var customer_phones = self.calendar_ui_slidepanel.find(".customer-phones");
                                    customer_phones.append(
                                        customer_phones.find(".customer-phone:last-child").clone().html(customer.phone)
                                    );
                                    var customer_emails = self.calendar_ui_slidepanel.find(".customer-emails");
                                    customer_emails.append(
                                        customer_emails.find(".customer-email:last-child").clone().html(customer.email)
                                    );
                                    return false;
                                }
                            });
                        });

                        self.calendar_ui_slidepanel.show("slow");

                    }

                }
                else {
                    new PNotify( resp.data.notification );
                }
            }
        );

    }

    this.btn_save_slidepanel = this.calendar_ui_slidepanel.find(".btn-save");
    this.btn_save_slidepanel.on("click",function() {
        console.log( "something" );
        self.btn_save_slidepanel_click("new");
    });

    this.btn_save_slidepanel_click = function( type ) {
        console.log("");
        var post_data = null;
        var customer_ids = [];
        self.calendar_ui_slidepanel.find(".customer-names").find(".customer-name").each(function(){
            customer_ids.push( jQuery(this).attr("date-customer_id") );
        });
        if ( type == "new" ) {
            post_data = {
                action: "rbdobooking_calendar",
                security: RBDoBookingAjax.security_calendar,
                type: "save_edit_booked_slot",
                data: {
                    member_id: self.calendar_ui_slidepanel.find(".member_id").val(),
                    service_id: self.calendar_ui_slidepanel.find(".service_id").val(),
                    start_date: self.calendar_ui_slidepanel.find(".start_date").val(),
                    start_time: self.calendar_ui_slidepanel.find(".start_time").val(),
                    end_date: self.calendar_ui_slidepanel.find(".start_date").val(),
                    end_time: self.calendar_ui_slidepanel.find(".end_time").val(),
                    status: self.calendar_ui_slidepanel.find(".status").val(),
                    customer_ids: customer_ids,
                },
                vendor_id: self.vendor_id
            };
        }
        ajax_request(
            post_data,
            function( response ) {
                console.log( response );
                if ( response.success ) {

                }
            }
        );
    }

    this.calendar_ui_slidepanel_hide = function() {
        self.calendar_ui_slidepanel.hide("slow");
    }

}
var rbdobooking_appearence = null;

jQuery(document).ready(function() {
    rbdobooking_appearence = new RBDoBooking_Appearance();
});

function RBDoBooking_Appearance() {

    var self =  this;
    this.vendor_id = 1;

    this.component = jQuery("#rbdobooking-appearance");

    this.tabs = this.component.find("ul.nav-tabs");
    this.tabs.on("shown.bs.tab",function( event ) {
        var href = jQuery(event.target).attr("href");
        var step_name = href.split("-");
        step_name = step_name[1];
        var find = false;
        var tab_content = self.component.find(href);
        tab_content.find("li").each(function (index,item) {
            if ( !find  ) {
                jQuery(item).addClass("active");
                if ( jQuery(item).hasClass(step_name+"-step") ) {
                    find = true;
                }

            }
        });

    });

}
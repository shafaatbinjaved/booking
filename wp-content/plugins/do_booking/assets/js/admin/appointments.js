var rbdobooking_appointments = null;

jQuery(function() {
    rbdobooking_appointments = new RBDoBooking_Appointments();
});

function RBDoBooking_Appointments() {

    var self = this;
    this.vendor_id = 1;
    this.open_id_time_slot = 0;

    this.component = jQuery("#rbdobooking-appointments");

    this.appointments_del_modal = this.component.find(".del-apoointments-confirm-modal");

    this.appointments_ui_slidepanel = this.component.find(".appointments-ui-slidepanel");
    this.appointments_ui_btn_close = this.appointments_ui_slidepanel.find(".btn-close");
    this.appointments_ui_btn_close.on("click",function() {
        self.appointments_ui_slidepanel_hide();
    });

    this.advanced_filter_options = this.component.find(".advanced-filter-options");

    this.start = moment().subtract(29, 'days');
    this.end = moment();

    this.appointment_time = this.component.find(".appointment_time");
    this.appointment_time.daterangepicker({
        startDate: self.start,
        endDate: self.end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, function(start,end) {
        var appointment_time_start = self.advanced_filter_options.find(".appointment_time_start");
        appointment_time_start.val(start.format('YYYY-MM-D'));
        var appointment_time_end = self.advanced_filter_options.find(".appointment_time_end");
        appointment_time_end.val(end.format('YYYY-MM-D'));
        self.appointment_time.find('span').html(
            start.format('D.MM.YYYY') + ' - ' + end.format('D.MM.YYYY')
        );
    });
    this.appointment_time.on('apply.daterangepicker', function(ev, picker) {
        self.table_reload();
    });

    this.advanced_filter_member_select = this.advanced_filter_options.find("select.member-select");
    this.advanced_filter_member_select.on("changed.bs.select",function() {
        self.table_reload();
    });

    this.advanced_filter_service_select = this.advanced_filter_options.find("select.service-select");
    this.advanced_filter_service_select.on("changed.bs.select",function() {
        self.table_reload();
    });

    this.advanced_filter_customer_select = this.advanced_filter_options.find("select.customer-select");
    this.advanced_filter_customer_select.on("changed.bs.select",function() {
        self.table_reload();
    });

    this.advanced_filter_status_select = this.advanced_filter_options.find("select.status-select");
    this.advanced_filter_status_select.on("changed.bs.select",function() {
        self.table_reload();
    });

    this.table_reload = function () {
        self.appointments_dataTable.ajax.reload();
    }

    this.creation_time = this.component.find(".creation_time");
    this.creation_time.daterangepicker({
        startDate: self.start,
        endDate: self.end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, function(start,end) {
        var creation_time_start = self.advanced_filter_options.find(".creation_time_start");
        creation_time_start.val(start.format('YYYY-MM-D'));
        var creation_time_end = self.advanced_filter_options.find(".creation_time_end");
        creation_time_end.val(end.format('YYYY-MM-D'));
        self.creation_time.find('span').html(
            start.format('D.MM.YYYY') + ' - ' + end.format('D.MM.YYYY')
        );
    });
    this.creation_time.on('apply.daterangepicker', function(ev, picker) {
        self.appointments_dataTable.ajax.reload();
    });

    this.appointments = this.component.find(".appointments-area .table");

    this.pending_val = this.appointments.attr("data-pending");
    this.approved_val = this.appointments.attr("data-approved");
    this.cancelled_val = this.appointments.attr("data-cancelled");
    this.rejected_val = this.appointments.attr("data-rejected");
    //this.payment_completed_val = this.appointments.attr("data-payment_completed");

    this.appointments_dataTable = self.appointments.DataTable({
        serverSide: true,
        "ajax": {
            "url": RBDoBookingAjax.ajaxurl,
            "type": "POST",
            "data": function ( d ) {
                d.action = 'rbdobooking_appointments';
                d.security = RBDoBookingAjax.security_appointments;
                d.type = 'get_dataTable_data';
                d.vendor_id = self.vendor_id;
                d.member_ids = self.advanced_filter_options.find("select.member-select").val();
                d.service_ids = self.advanced_filter_options.find("select.service-select").val();
                d.customer_ids = self.advanced_filter_options.find("select.customer-select").val();
                d.status = self.advanced_filter_options.find("select.status-select").val();
                d.appointment_time_start = self.advanced_filter_options.find(".appointment_time_start").val();
                d.appointment_time_end = self.advanced_filter_options.find(".appointment_time_end").val();
                d.creation_time_start = self.advanced_filter_options.find(".creation_time_start").val();
                d.creation_time_end = self.advanced_filter_options.find(".creation_time_end").val();
                d.appointment_no = self.advanced_filter_options.find(".appointment_no").val();
            }
        },
        "ordering": false,
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                //"data":           null,
                "defaultContent": ''
            },
            {
                "render": function ( data, type, row) {
                    var element_actions = self.component.find(".row-actions-column");
                    element_actions = element_actions.clone().html();
                    element_actions = jQuery( element_actions );
                    var html = '';
                    html += '<input type="checkbox" />';
                    html +=
                        html += '<i class="fas fa-download"></i>';
                    return element_actions.html();
                }
            },
            {"data": "id_time_slot"},
            { "data": "formatted_date_start" },
            { "data": "member_full_name" },
            { "data": "customer_full_name" },
            { "data": "phone" },
            { "data": "service_name" },
            { "data": "service_id" },
            {//for appointment status
                "render": function ( data, type, row ) {
                    var is_booked = parseInt(row.is_booked);
                    var html = '';
                    switch ( is_booked ) {
                        case 0:
                            html = self.pending_val;
                            break;
                        case 1:
                            html = self.approved_val;
                            break;
                        case 2:
                            html = self.cancelled_val;
                            break;
                        case 3:
                            html = self.rejected_val;
                            break;
                    }
                    return html;
                }
            },
            {//for payment status
                "render": function ( data, type, row ) {
                    var paid_status = parseInt(row.paid_status);
                    var html = '';
                    switch ( paid_status ) {
                        case 0://not paid
                            break;
                        case 1://fully paid
                            html = row.payment_amount + " " + row.payment_method_name;
                            html += ' ' + payment_completed_val;
                            break;
                    }
                    return html;
                },
                "orderable": false
            }
        ],
        columnDefs: [
            /*{ orderable: false, targets: -1 },*/
        ],
        "initComplete": function( settings, json ) {
            self.appointments_dataTable.rows().every(function(rowIdx, tableLoop, rowLoop){
                var rowData = this.data();
                jQuery(this.node()).attr('data-id_time_slot',rowData.id_time_slot);
                jQuery(this.node()).attr('data-email',rowData.email);
                jQuery(this.node()).attr('data-notes',rowData.notes);
            });
            self.register_appointment_row_click();
            self.register_expand_collapse_row_click();
        }
    });//fnReloadAjax

    this.register_appointment_row_click = function () {
        self.appointments_rows = self.component.find("tbody tr");
        self.appointments_rows.off();
        self.appointments_rows.on("click",function() {
            var row = jQuery(this).closest("tr");//highlight-row
            row.addClass("highlight-row");
            var id_time_slot = row.attr("data-id_time_slot");
            self.show_appointments_ui_panel_with_relevant_data( "edit_time_slot", id_time_slot );
        });
    }

    self.format = function( row ) {
        var element = self.component.find(".row-expand-child").clone().html();
        element = jQuery( element );
        element.find(".email").html( row.attr("data-email") );
        element.find(".notes").html( row.attr("data-note") );
        return element.html();
    }

    this.register_expand_collapse_row_click = function () {
        self.component.find("tbody tr td.details-control").on("click",function(e) {
            e.preventDefault();
            e.stopPropagation();
            var tr = jQuery(this).closest('tr');
            var row = self.appointments_dataTable.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( self.format( tr ) ).show();
                tr.addClass('shown');
            }
        });
    }

    this.members_container = self.component.find(".members-container");
    this.members_container.find(".staff-member-li").on("click",function() {

        var temp = jQuery(this).find("a").attr("data-member_id");

        if ( temp != "all" ) {
            self.selected_member_ids = [ temp ];
        }
        else {
            self.selected_member_ids = temp;
        }
        
    });

    this.selected_member_ids = 'all';

    this.show_appointments_ui_panel_with_relevant_data = function( type, id_time_slot ) {

        var post_data = null;

        if ( type == "add_time_slot" ) {
            post_data = {
                action: "rbdobooking_appointments",
                security: RBDoBookingAjax.security_appointments,
                type: "get_new_appointment_form_data"
            }
        }
        else if ( type == "edit_time_slot" ) {
            post_data = {
                action: "rbdobooking_appointments",
                security: RBDoBookingAjax.security_appointments,
                type: "get_existing_appointment_and_form_data",
                id_time_slot: id_time_slot
            }
        }

        ajax_request(
            post_data,
            function( resp ) {
                if ( resp.success ) {

                    var heading = self.appointments_ui_slidepanel.find(".heading");
                    var options = '';

                    var members_options = '';
                    jQuery.each(resp.data.staff_members,function(index,item){
                        members_options += '<option value="'+item.member_id+'">';
                        members_options += item.first_name+" "+item.last_name;
                        members_options += '</option>';
                    });
                    self.appointments_ui_slidepanel.find(".member_id").html( members_options );

                    var services_options = '';
                    jQuery.each( resp.data.services, function(index,item) {
                        services_options += '<option value="'+item.service_id+'">';
                        services_options += item.name + " ( "+parseFloat(parseInt(item.duration)/60).toFixed(2)+" h )";
                        services_options += '</option>';
                    });
                    self.appointments_ui_slidepanel.find(".service_id").html( services_options );

                    if ( type == "add_time_slot" ) {

                        var heading_val = heading.attr('data-add_heading');
                        heading.html( heading_val );

                        self.appointments_ui_slidepanel.show("slow");

                        self.appointments_ui_slidepanel.find(".start_date").val( "" );
                        self.appointments_ui_slidepanel.find(".start_time").val( "" );

                        self.appointments_ui_slidepanel.find(".end_date").val( "" );
                        self.appointments_ui_slidepanel.find(".end_time").val( "" );

                        self.appointments_ui_slidepanel.find(".customer-name").html( "" );
                        self.appointments_ui_slidepanel.find(".customer-phone").html( "" );
                        self.appointments_ui_slidepanel.find(".customer-email").html( "" );

                    }
                    else if ( type == "edit_time_slot" ) {

                        var heading_val = heading.attr('data-edit_heading');
                        heading.html( heading_val );

                        var highlight_row = self.component.find(".appointments-area > table tr[data-id_time_slot='"+id_time_slot+"']");
                        highlight_row.addClass("highlight-row");
                        self.open_id_time_slot = id_time_slot;

                        self.appointments_ui_slidepanel.find(".service_id").val( resp.data.book_slot.service_id );
                        self.appointments_ui_slidepanel.find(".member_id").val( resp.data.book_slot.member_id );

                        var date_start = resp.data.book_slot.date_start;
                        date_start = date_start.split(" ");
                        self.appointments_ui_slidepanel.find(".start_date").val( date_start[0] );
                        self.appointments_ui_slidepanel.find(".start_time").val( date_start[1] );

                        var date_end = resp.data.book_slot.date_end;
                        date_end = date_end.split(" ");
                        self.appointments_ui_slidepanel.find(".end_date").val( date_end[0] );
                        self.appointments_ui_slidepanel.find(".end_time").val( date_end[1] );

                        self.appointments_ui_slidepanel.find(".customer-name").html( resp.data.book_slot.full_name );
                        self.appointments_ui_slidepanel.find(".customer-phone").html( resp.data.book_slot.phone );
                        self.appointments_ui_slidepanel.find(".customer-email").html( resp.data.book_slot.email );

                        var status_select = self.appointments_ui_slidepanel.find("select.status");
                        status_select.val( resp.data.book_slot.is_booked );
                        //status_select.selectpicker("refresh");
                        self.appointments_ui_slidepanel.show("slow");

                    }

                }
                else {
                    new PNotify( resp.data.notification );
                }
            }
        );

    }

    this.appointments_ui_slidepanel_hide = function() {
        self.open_id_time_slot = 0;
        var highlight_row = self.appointments.find("tbody tr.highlight-row");
        highlight_row.removeClass("highlight-row");
        self.appointments_ui_slidepanel.hide("slow");
    }

}
var rbdobooking_email_notifications = null;

jQuery(document).ready(function() {
    rbdobooking_email_notifications = new RBDoBooking_Email_Notifications();
});

function RBDoBooking_Email_Notifications() {

    var self = this;
    this.vendor_id = 1;
    this.open_email_notification_id = 0;

    this.component = jQuery("#rbdobooking-emails");

    this.emails_ui_slidepanel = this.component.find(".emails-ui-slidepanel");
    this.emails_ui_btn_close = this.emails_ui_slidepanel.find(".btn-close");
    this.emails_ui_btn_close.on("click",function() {
        self.emails_ui_slidepanel_hide ()
    });

    this.emails_ui_btn_save = this.emails_ui_slidepanel.find(".btn-save");
    this.emails_ui_btn_save.on("click",function() {

         var enabled_disbaled = self.emails_ui_slidepanel.find(".is_enabled_or_not:checked");

        var post_data = {
            action: "rbdobooking_emails",
            security: RBDoBookingAjax.security_emails,
            type: "save_email_notification_template",
            email_template_id: self.emails_ui_slidepanel.find(".email_template_id").val(),
            template: tinyMCE.editors[0].getContent(),
            subject: self.emails_ui_slidepanel.find(".subject").val(),
            enabled: enabled_disbaled.val()
        };

        ajax_request(
            post_data,
            function( resp ) {
                if ( resp.success ) {
                    var row = self.emails_area.find("tr[data-email_notification_id='"+self.open_email_notification_id+"']");
                    console.log( row );
                    row.find(".enabled-disabled").html( enabled_disbaled.next().html() );

                    self.emails_ui_slidepanel_hide();
                }
                new PNotify( resp.data.notification );
            }
        )
    });

    this.emails_area = this.component.find(".emails-area .table");
    this.emails_area.find("tbody tr").on("click",function() {
        var email_notification_id = jQuery(this).attr("data-email_notification_id");
        if ( self.open_email_notification_id != email_notification_id ) {
            self.emails_area.find("tbody tr").removeClass("highlight-row");
            jQuery(this).addClass("highlight-row");
            self.open_email_notification_id = email_notification_id;
            self.show_emails_ui_panel_with_relevant_data(
                "edit_notification_template",
                email_notification_id
            );
        }
    });

    this.show_emails_ui_panel_with_relevant_data = function( type, email_notification_id ) {

        var post_data = null;

        if ( type == "edit_notification_template" ) {
            post_data = {
                action: "rbdobooking_emails",
                security: RBDoBookingAjax.security_emails,
                type: "get_email_notification_template_form_data",
                email_notification_id: email_notification_id
            }
        }

        ajax_request(
            post_data,
            function( resp ) {
                if ( resp.success ) {

                    var heading = self.emails_ui_slidepanel.find(".heading");
                    var options = '';

                    if ( type == "edit_notification_template" ) {

                        var heading_val = heading.attr('data-edit_heading');
                        heading.html( heading_val );

                        var name = self.emails_ui_slidepanel.find('.email-notification-name');
                        name.val( resp.data.email_template.name );

                        var email_template_id = self.emails_ui_slidepanel.find(".email_template_id");
                        email_template_id.val( resp.data.email_template.email_template_id );

                        var radio_input = self.emails_ui_slidepanel.find(".is_enabled_or_not[value='"+resp.data.email_template.is_enabled+"']");
                        radio_input.prop("checked",true);

                        var subject = self.emails_ui_slidepanel.find(".subject");
                        subject.val( resp.data.email_template.subject );
                        tinyMCE.editors[0].setContent( resp.data.email_template.template );

                        self.emails_ui_slidepanel.show("slow");

                    }

                }
                else {
                    new PNotify( resp.data.notification );
                }
            }
        );

    }

    this.emails_ui_slidepanel_hide = function() {
        self.open_email_notification_id = -1;
        self.emails_area.find("tbody tr").removeClass("highlight-row");
        self.emails_ui_slidepanel.hide("slow");
    }

}
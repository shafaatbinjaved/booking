var rbdobooking_settings = null;

jQuery(document).ready(function() {
    rbdobooking_settings = new RBDoBooking_Settings();
});

function RBDoBooking_Settings() {

    var self = this;
    this.vendor_id = 1;

    this.component = jQuery("#rbdobooking-settings");

    this.all_settings_fields = this.component.find(".rbdobooking-setting");

    this.save_btn = this.component.find(".btn-submit");
    this.save_btn.on("click",function() {
        console.log("in save btn click");
        self.save_settings();
    });

    this.save_settings = function() {
        console.log("in save settings");
        var urlParams = new URLSearchParams(window.location.search);
        var view_type = urlParams.get("view");
        var setting_vals = [];
        var obj = null;

        jQuery.each(self.all_settings_fields,function(index,item){
            obj = new Object();
            obj["id"] = jQuery(item).attr("id");
            obj["name"] = jQuery(item).attr("name");
            obj["value"] = jQuery(item).val();
            setting_vals.push( obj );
        });

        var icon_spin = self.save_btn.find(".icon-spin");
        icon_spin.show();

        ajax_request(
            {
                action: "rbdobooking_settings",
                security: RBDoBookingAjax.security_settings,
                type: "save_settings",
                view_type: view_type,
                settings_val: setting_vals
            },
            function ( resp ) {
                if ( resp.success ) {
                }
                else {}
                new PNotify(resp.data.notification);

                icon_spin.hide();
            }
        );
    }

}
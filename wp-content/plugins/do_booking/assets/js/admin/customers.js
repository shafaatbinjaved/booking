var RBDoBooking_Customers = new RBDoBooking_Customers();

function RBDoBooking_Customers() {

    var self = this;
    this.open_customer_id = 0;

    this.component = jQuery("#rbdobooking-customers");

    this.customers_ui_panel = this.component.find(".customers-ui-slidepanel");

    this.btn_add_new_customer = this.component.find(".btn-add");
    this.btn_add_new_customer.on("click",function() {
        self.customers_ui_panel.find(".full_name").val("");
        self.customers_ui_panel.find(".email").val("");
        self.customers_ui_panel.find(".dob").val("");
        self.customers_ui_panel.find(".phone").val("");
        self.customers_ui_panel.find(".street").val("");
        self.customers_ui_panel.find(".extra_address").val("");
        self.customers_ui_panel.find(".zip").val("");
        self.customers_ui_panel.find(".city").val("");
        self.customers_ui_panel.find(".state").val("");
        self.customers_ui_panel.find(".country").val("");
        var heading = self.customers_ui_panel.find(".heading");
        heading.html( heading.attr("data-add_customer") );
        self.customers_ui_panel.show("slow");
    });

    this.btn_save_panel = this.customers_ui_panel.find('.btn-save');
    this.btn_save_panel.on("click",function () {
        self.save_customers_data_from_ui_panel();
    });

    this.btn_close_panel = this.customers_ui_panel.find(".btn-close");
    this.btn_close_panel.on("click",function() {
        self.close_customers_ui_panel();
    });

    // save the customer data from customer ui panel
    this.save_customers_data_from_ui_panel = function () {
        ajax_request(
            {
                action: "rbdobooking_customers",
                security: RBDoBookingAjax.security_customers,
                data: {
                    customer_id: self.open_customer_id,
                    full_name: self.customers_ui_panel.find(".full_name").val(),
                    email: self.customers_ui_panel.find(".email").val(),
                    dob: self.customers_ui_panel.find(".dob").val(),
                    phone: self.customers_ui_panel.find(".phone").val(),
                    street: self.customers_ui_panel.find(".street").val(),
                    extra_address: self.customers_ui_panel.find(".extra_address").val(),
                    zip: self.customers_ui_panel.find(".zip").val(),
                    city: self.customers_ui_panel.find(".city").val(),
                    state: self.customers_ui_panel.find(".state").val(),
                    country: self.customers_ui_panel.find(".country").val()
                },
                type: "update_customer_data"
            },
            function ( response ) {
                if ( response.success ) {
                    if ( response.data.customer.type == "update" ) {
                        var row = self.component.find("tr[data-customer_id='"+self.open_customer_id+"']");
                        row.find(".full_name").html( response.data.customer.full_name );
                        row.find(".phone").html( response.data.customer.phone );
                        row.find(".dob").html( response.data.customer.dob );
                        row.find(".email").html( response.data.customer.email );
                        row.find(".street").html( response.data.customer.street );
                        row.find(".extra_address").html( response.data.customer.extra_address );
                        row.find(".zip").html( response.data.customer.zip );
                        row.find(".city").html( response.data.customer.city );
                        row.find(".state").html( response.data.customer.state );
                        row.find(".country").html( response.data.customer.country );
                    }
                    else if ( response.data.customer.type == "new" ) {
                        var row = self.component.find("tbody tr:last-child").clone();
                        row.find(".number").html(parseInt(row.find(".number").html())+1);
                        row.find(".full_name").html( response.data.customer.full_name );
                        row.find(".phone").html( response.data.customer.phone );
                        row.find(".dob").html( response.data.customer.dob );
                        row.find(".email").html( response.data.customer.email );
                        row.find(".street").html( response.data.customer.street );
                        row.find(".extra_address").html( response.data.customer.extra_address );
                        row.find(".zip").html( response.data.customer.zip );
                        row.find(".city").html( response.data.customer.city );
                        row.find(".state").html( response.data.customer.state );
                        row.find(".country").html( response.data.customer.country );
                        self.component.find("tbody").append( row );
                    }
                    console.log("success");
                }
                new PNotify(response.data.notification);
            }
        );
    }

    //close the customer ui panel
    this.close_customers_ui_panel = function () {
        self.component.find(".highlight-row").removeClass("highlight-row");
        self.open_customer_id = 0;
        self.customers_ui_panel.hide("slow");
    }

    this.customer_edit = this.component.find("tbody tr");
    this.customer_edit.on("click",function() {
        if ( self.open_customer_id == 0 ) {
            jQuery(this).addClass("highlight-row");
            self.open_customer_id = jQuery(this).closest("tr").attr("data-customer_id");
            ajax_request(
                {
                    action: "rbdobooking_customers",
                    security: RBDoBookingAjax.security_customers,
                    data: self.open_customer_id,
                    type: "get_customer_data"
                },
                function ( response ) {
                    if ( response.success ) {
                        var heading = self.customers_ui_panel.find(".heading");
                        heading.html( heading.attr("data-edit_customer") );
                        self.customers_ui_panel.find(".full_name").val( response.data.full_name );
                        self.customers_ui_panel.find(".email").val( response.data.email );
                        self.customers_ui_panel.find(".phone").val( response.data.phone );
                        //.......
                        self.customers_ui_panel.find(".dob").val( response.data.dob );
                        self.customers_ui_panel.find(".street").val( response.data.street );
                        self.customers_ui_panel.find(".extra_address").val( response.data.extra_address );
                        self.customers_ui_panel.find(".zip").val( response.data.zip );
                        self.customers_ui_panel.find(".city").val( response.data.city );
                        self.customers_ui_panel.find(".state").val( response.data.state );
                        self.customers_ui_panel.find(".country").val( response.data.country );
                        //.......
                        self.customers_ui_panel.show("slow");
                    }
                }
            );
        }
    });

}
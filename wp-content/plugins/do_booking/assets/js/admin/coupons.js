var rbdobooking_coupons = null;

jQuery(document).ready(function() {
    rbdobooking_coupons= new RBDoBooking_Coupons();
});

function RBDoBooking_Coupons() {

    var self = this;
    this.open_coupon_id = 0;

    this.component = jQuery("#rbdobooking-coupons");

    this.coupon_del_modal = this.component.find(".del-coupon-confirm-modal");

    this.coupons_ui_panel = this.component.find(".coupons-ui-slidepanel");
    this.start_time = this.coupons_ui_panel.find(".start_time").datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: "hh:mm:ss"
    });
    this.end_time = this.coupons_ui_panel.find(".end_time").datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: "hh:mm:ss"
    });

    this.coupons_table_area = this.component.find(".table-area");

    this.coupon_head_row_checkbox = this.coupons_table_area.find(".coupon-head-row-checkbox");
    this.coupon_head_row_checkbox.on("click",function() {
        if ( jQuery(this).prop("checked") ) {
            self.coupons_table_area.find(".coupon-checkbox").prop("checked",true);
        }
        else {
            self.coupons_table_area.find(".coupon-checkbox").prop("checked",false);
        }
    });

    this.coupon_checkbox = this.coupons_table_area.find(".coupon-checkbox");
    this.coupon_checkbox.on("click",function(e) {
        e.stopPropagation();
    });

    this.coupons_row = this.coupons_table_area.find(".coupons-row");
    this.coupons_row.on("click",function() {
        var row = self.coupons_table_area.find(".coupons-row[data-coupon_id='"+self.open_coupon_id+"']");
        if ( row.length ) {
            row.removeClass( "highlight-row" );
        }
        jQuery(this).addClass( "highlight-row" );
        self.show_coupon_ui_panel_with_relevant_data(
            "edit_coupon",
            jQuery(this).attr("data-coupon_id")
        );
    });

    this.btn_add_coupon = this.component.find(".btn-add");
    this.btn_add_coupon.on("click",function() {
        self.open_coupon_id = 0;
        self.show_coupon_ui_panel_with_relevant_data("add_coupon",0);
    });

    this.selected_coupons = [];
    this.btn_del_coupon = this.component.find(".btn-delete-multi");
    this.btn_del_coupon.on("click",function() {
        self.coupons_table_area.find(".coupon-checkbox").each(function(index,item){
            var element = jQuery(item);
            if ( element.prop("checked") ) {
                self.selected_coupons.push( element.val() );
            }
        });
        var show_modal = false;
        if ( self.selected_coupons.length > 1 ) {
            self.coupon_del_modal.find(".coupon-multi").show();
            self.coupon_del_modal.find(".coupon-1").hide();
            show_modal = true;
        }
        else if ( self.selected_coupons.length == 1 ) {
            self.coupon_del_modal.find(".coupon-multi").hide();
            self.coupon_del_modal.find(".coupon-1").show();
            show_modal = true;
        }

        if ( show_modal ) {
            self.coupon_del_modal.modal("show");
        }
    });

    this.del_btn_confirm_modal = self.coupon_del_modal.find(".delete-coupon-confirm");
    this.del_btn_confirm_modal.on("click",function() {
        ajax_request(
            {
                action: "rbdobooking_coupons",
                security: RBDoBookingAjax.security_coupons,
                type: "delete_coupons",
                ids: self.selected_coupons.join(",")
            },
            function( resp ) {
                if ( resp.success ) {
                    for ( var $i=0; $i < self.selected_coupons.length; $i++ ) {
                        self.coupons_table_area
                            .find(".coupons-row[data-coupon_id='"+self.selected_coupons[$i]+"']")
                            .remove();
                    }
                    self.selected_coupons = [];
                    self.coupon_del_modal.modal("hide");
                }
                new PNotify(resp.data.notification);
            }
        )
    });

    this.btn_close_panel = this.coupons_ui_panel.find(".btn-close");
    this.btn_close_panel.on("click",function() {
        var row = self.coupons_table_area.find(".coupons-row[data-coupon_id='"+self.open_coupon_id+"']");
        if ( row.length ) {
            row.removeClass( "highlight-row" );
        }
        self.close_coupons_ui_panel();
    });

    //close coupons ui panel
    this.close_coupons_ui_panel = function () {
        self.coupons_ui_panel.hide("slow");
        self.open_coupon_id = 0;
    }

    //save button coupons ui panel
    this.btn_save_coupons_ui_panel = this.coupons_ui_panel.find(".btn-save");
    this.btn_save_coupons_ui_panel.on("click",function() {
        self.save_coupon();
    });

    this.execute_save_coupon = true;
    this.save_coupon = function() {
        if ( self.execute_save_coupon ) {
            self.execute_save_coupon = false;
            var open_coupon_id = parseInt(self.open_coupon_id);
            var data = null;

            if ( open_coupon_id == 0 ) {//edit case
                data = {
                    code: self.coupons_ui_panel.find(".code").val(),
                    discount: self.coupons_ui_panel.find(".discount").val(),
                    deduction: self.coupons_ui_panel.find(".deduction").val(),
                    usage_limit: self.coupons_ui_panel.find(".usage_limit").val(),
                    start_time: self.coupons_ui_panel.find(".start_time").val(),
                    end_time: self.coupons_ui_panel.find(".end_time").val(),
                    service_ids: self.coupons_ui_panel.find("select.service_id").val()
                }
            }
            else {//insert case
                data = {
                    coupon_id: self.open_coupon_id,
                    code: self.coupons_ui_panel.find(".code").val(),
                    discount: self.coupons_ui_panel.find(".discount").val(),
                    deduction: self.coupons_ui_panel.find(".deduction").val(),
                    usage_limit: self.coupons_ui_panel.find(".usage_limit").val(),
                    start_time: self.coupons_ui_panel.find(".start_time").val(),
                    end_time: self.coupons_ui_panel.find(".end_time").val(),
                    service_ids: self.coupons_ui_panel.find("select.service_id").val()
                }
                console.log( self.coupons_ui_panel.find("select.service_id").val() );
            }

            ajax_request(
                {
                    action: "rbdobooking_coupons",
                    security: RBDoBookingAjax.security_coupons,
                    type: "save_coupon_form_data",
                    data: data
                },
                function ( resp ) {
                    self.execute_save_coupon = true;

                    if ( resp.success ) {
                        if ( open_coupon_id == 0 ) {//insert case

                            var last_row = self.coupons_table_area.find("tbody tr").last();
                            var serial_num = parseInt(last_row.children().eq(2).html());
                            serial_num++;

                            var html = jQuery( resp.data.html );
                            html.find(".serial-num").html( serial_num );
                            html.on("click",function() {
                                self.show_coupon_ui_panel_with_relevant_data(
                                    "edit_coupon",
                                    jQuery(this).attr("data-coupon_id")
                                );
                            });

                            var tbody = self.coupons_table_area.find("tbody");
                            tbody.append( html );

                            self.coupons_ui_panel.find(".btn-close").trigger("click");
                        }
                        else {// edit case
                            var html = jQuery( resp.data.html );

                            var row = self.coupons_table_area.find(".coupons-row[data-coupon_id='"+self.open_coupon_id+"']");
                            row.replaceWith( html );
                            row.on("click",function() {
                                self.show_coupon_ui_panel_with_relevant_data(
                                    "edit_coupon",
                                    jQuery(this).attr("data-coupon_id")
                                );
                            });

                            self.coupons_ui_panel.find(".btn-close").trigger("click");

                        }
                    }
                    new PNotify(resp.data.notification);
                }
            );
        }
    }

    this.btn_delete_coupon_from_slidepanel = this.coupons_ui_panel.find(".btn-delete");
    this.btn_delete_coupon_from_slidepanel.on("click",function() {
        self.selected_coupons.push( self.open_coupon_id );
        var show_modal = false;
        if ( self.selected_coupons.length > 1 ) {
            self.coupon_del_modal.find(".coupon-multi").show();
            self.coupon_del_modal.find(".coupon-1").hide();
            show_modal = true;
        }
        else if ( self.selected_coupons.length == 1 ) {
            self.coupon_del_modal.find(".coupon-multi").hide();
            self.coupon_del_modal.find(".coupon-1").show();
            show_modal = true;
        }

        if ( show_modal ) {
            self.coupon_del_modal.modal("show");
        }
    });

    this.show_coupon_ui_panel_with_relevant_data = function ( type, coupon_id ) {
        var post_data = null;

        if ( type == "edit_coupon" ) {
            self.open_coupon_id = coupon_id;
            post_data = {
                action: "rbdobooking_coupons",
                security: RBDoBookingAjax.security_coupons,
                coupon_id: coupon_id,
                type: "get_coupon_details"
            };
        }
        else if ( type == "add_coupon" ) {
            post_data = {
                action: "rbdobooking_coupons",
                security: RBDoBookingAjax.security_coupons,
                type: "get_coupon_form_data"
            };
        }
        ajax_request(
            post_data,
            function ( resp ) {
                if ( resp.success ) {

                    var heading = self.coupons_ui_panel.find(".heading");
                    var options = '';

                    if ( type == "edit_coupon" ) {

                        var heading_val = heading.attr("data-edit_heading");
                        heading.html( heading_val );

                        self.coupons_ui_panel.find(".code").val( resp.data.coupon.code );
                        self.coupons_ui_panel.find(".discount").val( resp.data.coupon.discount );
                        self.coupons_ui_panel.find(".deduction").val( resp.data.coupon.deduction );
                        self.coupons_ui_panel.find(".usage_limit").val( resp.data.coupon.usage_limit );
                        self.coupons_ui_panel.find(".start_time").val( resp.data.coupon.start_time );
                        self.coupons_ui_panel.find(".end_time").val( resp.data.coupon.end_time );

                        var service_ids = resp.data.coupon.service_ids.split(",");
                        var service_names = resp.data.coupon.service_names.split(",");

                        jQuery.each(resp.data.services,function(index,item){
                            options += '<option value="'+item.service_id+'"';
                            var found_service_index = service_ids.indexOf( item.service_id );
                            if ( found_service_index != -1 ) {
                                options += ' selected="selected" ';
                            }
                            options += '>';
                            options += item.name;
                            options += '</option>';
                        });

                    }
                    else if ( type == "add_coupon" ) {

                        var heading_val = heading.attr("data-add_heading");
                        heading.html( heading_val );

                        self.coupons_ui_panel.find(".code").val( "" );
                        self.coupons_ui_panel.find(".discount").val( "" );
                        self.coupons_ui_panel.find(".deduction").val( "" );
                        self.coupons_ui_panel.find(".usage_limit").val( "" );
                        self.coupons_ui_panel.find(".start_time").val( "" );
                        self.coupons_ui_panel.find(".end_time").val( "" );

                        jQuery.each(resp.data.services,function(index,item){
                            options += '<option value="'+item.service_id+'">';
                            options += item.name;
                            options += '</option>';
                        });


                    }

                    if ( options != "" ) {
                        var service_id_select = self.coupons_ui_panel.find("select.service_id");
                        console.log( service_id_select );
                        service_id_select.html( "" );
                        service_id_select.html( options );
                        service_id_select.selectpicker("refresh");
                    }
                }
                else {
                    new PNotify(resp.data.notification);
                }
            }
        );
        self.coupons_ui_panel.show("slow");
    }

}
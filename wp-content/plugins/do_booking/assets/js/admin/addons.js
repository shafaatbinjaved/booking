var RBDoBooking_Addons_Page = null;

jQuery(document).ready(function() {
    RBDoBooking_Addons_Page = new RBDoBooking_Addons();
});

function RBDoBooking_Addons() {

    var self = this;

    this.component = jQuery("#rbdobooking-addons");

    this.action_btns = this.component.find(".btn-addon-action");
    this.running_action_btn = false;
    this.action_btns.on('click',function() {
        if (!self.running_action_btn) {
            var element = jQuery(this);
            var type = element.attr("data-type");
            self.running_action_btn = !self.running_action_btn;
            ajax_request(
                {
                    action: "rbdobooking_addons",
                    security: RBDoBookingAjax.security_addons,
                    data: jQuery(this).attr("data-slug"),
                    type: type

                },
                function(response) {
                    self.running_action_btn = !self.running_action_btn;
                    if (response.success) {
                        if (type === "activate" || type === "deactivate") {
                            if (type === "activate") {
                                console.log(
                                    "in if",
                                    element.attr("data-type")
                                );
                                element.attr("data-type","deactivate");
                            }
                            else {
                                console.log(
                                    "in else",
                                    element.attr("data-type")
                                );
                                element.attr("data-type","activate");
                            }
                            element.html(element.attr("data-"+type));
                        }
                    } else {

                    }
                    new PNotify( response.data.notification );
                }
            );
        }
    });
}
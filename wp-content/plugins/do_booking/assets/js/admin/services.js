var RBDoBooking_Services = new RBDoBooking_Services();

function RBDoBooking_Services() {

    var self = this;
    this.open_service_id = 0;

    this.component = jQuery("#rbdobooking-services");

    this.modal_add_category_class_name = '.add-category-modal';
    this.modal_add_category = this.component.find(this.modal_add_category_class_name);

    this.modal_add_new_category_input = this.modal_add_category.find('.new-category-name');

    this.modal_add_category_save_btn = this.modal_add_category.find(".btn-save");
    this.modal_add_category_save_btn.on("click",function() {
        ajax_request(
            {
                action: "rbdobooking_services",
                security: RBDoBookingAjax.security_services,
                data: self.modal_add_new_category_input.val(),
                type: "save_new_category"
            },
            function( response ) {
                if ( response.success ) {
                    var tpl_category_text = self.tpl_category;
                    var tpl_category = jQuery(tpl_category_text);
                    tpl_category.attr('data-category_id',response.data.category_id );
                    tpl_category.find('.name').html( response.data.category );
                    tpl_category.find('.count').html("0");
                    tpl_category.on("click",function() {
                        self.category_item_click_binder( this );
                    });
                    tpl_category.find(".btn-edit").on("click",function( event ) {
                        event.stopPropagation();
                        self.category_item_edit_click_binder( this );
                    });
                    tpl_category.find(".btn-del").on("click",function( event ) {
                        event.stopPropagation();
                        self.category_item_del_click_binder( this );
                    });
                    self.all_categories.append( tpl_category );
                }
                new PNotify(response.data.notification);
                self.modal_add_new_category_input.val("");
                self.modal_add_category.modal("hide");
            }
        );
    });

    var all_services_count_container = this.component.find(".all-services-count-container");
    all_services_count_container.on("click",function() {
        var item_none = self.service_list_area.find("li.item-none");
        if ( item_none ) {
            item_none.remove();
        }
        var service_list_items = self.service_list_area.find("li:not(:first-child)");
        service_list_items.show();

        self.all_categories.find("li").removeClass("selected-category");
        jQuery(this).addClass("selected-category");
    });

    this.tpl_category = this.component.find(".tpl-category").html();
    this.tpl_service_list_item = this.component.find(".tpl-service-list-item").html();
    this.tpl_service_list_item_none = this.component.find(".tpl-service-list-item-none").html();

    this.all_categories = this.component.find(".all-categories");

    var single_category_item = this.all_categories.find(".single-category");
    single_category_item.on("click",function() {
        self.category_item_click_binder( this );
    });

    this.category_item_click_binder = function ( ptr ) {
        var categroy_id = jQuery(ptr).data('category_id');

        self.component.find(".all-services-count-container").removeClass("selected-category");
        self.all_categories.find("li").removeClass("selected-category");
        jQuery(ptr).addClass("selected-category");

        var service_list_items = self.service_list_area.find("li:not(:first-child)");
        var obj = null;
        var showing_none = true;

        jQuery.each(service_list_items,function(index,item) {

            obj = jQuery(item);
            var list_item_category_id = obj.find(".category_id").val();
            if ( list_item_category_id == categroy_id ) {
                obj.show();
                showing_none = false;
            }
            else {
                obj.hide();
            }

        });

        if ( showing_none ) {
            var list_item_none = jQuery(self.tpl_service_list_item_none);
            list_item_none.find(".msg-zero-services").hide();
            list_item_none.find(".msg-category-select").show();
            self.service_list_area.append( list_item_none );
        }
    }

    this.delete_category_id = 0;

    var btn_delete_category = this.all_categories.find(".btn-del");
    btn_delete_category.on("click",function( event ) {
        event.stopPropagation();
        self.category_item_del_click_binder( this );
    });

    this.category_item_del_click_binder = function( ptr ) {
        self.delete_category_id = jQuery(ptr).closest(".single-category").data("category_id");
        self.component.find('.del-category-confirm-modal').modal("show");
    }

    var btn_delete_category_confirm = this.component.find(".btn-delete-category-confirm");
    btn_delete_category_confirm.on("click",function() {
        ajax_request(
            {
                action: "rbdobooking_services",
                security: RBDoBookingAjax.security_services,
                category_id: self.delete_category_id,
                type: "delete_category"
            },
            function (response) {
                if ( response.success ) {
                    self.component.find('.del-category-confirm-modal').modal("hide");
                    self.all_categories.find("li[data-category_id='"+self.delete_category_id+"']").remove();

                    self.delete_category_id = 0;
                }
                else {}
                new PNotify(response.data.notification);
            }
        );
    });

    this.edit_modal = this.component.find(".edit-category-modal");

    var btn_edit_category = this.all_categories.find(".btn-edit");
    btn_edit_category.on("click",function( event ) {
        event.stopPropagation();
        self.category_item_edit_click_binder( this );
    });

    this.category_item_edit_click_binder = function ( ptr ) {
        var category_id = jQuery( ptr ).closest(".single-category").data("category_id");
        var name = jQuery( ptr ).closest(".single-category").find(".name").html();

        self.edit_modal.find(".category_id").val( category_id );
        self.edit_modal.find(".name").val( name );
        self.edit_modal.modal("show");
    }

    var btn_save_edit_category_modal = this.edit_modal.find(".btn-save");
    btn_save_edit_category_modal.on("click",function() {
        var category_id = self.edit_modal.find(".category_id").val();
        var name = self.edit_modal.find(".name").val();
        ajax_request(
            {
                action: "rbdobooking_services",
                security: RBDoBookingAjax.security_services,
                category_id: category_id,
                name: name,
                type: "edit_category"
            },
            function( response ) {
                if ( response.success ) {
                    self.edit_modal.modal("hide");
                    var category = self.all_categories.find("li[data-category_id='"+category_id+"']");
                    category.find(".name").html( name );
                }
                else {}
                new PNotify(response.data.notification);
            }
        );
    });

    this.service_list_area = this.component.find(".service-list-area ul");

    this.btn_add_category = this.component.find('.btn-add-category');
    this.btn_add_category.on('click',function () {
        jQuery(self.modal_add_category).modal('show');
    });

    this.tpl_service = this.component.find(".tpl-service").html();

    this.service_ui_panel = this.component.find(".service-ui-slidepanel");

    this.btn_close_panel = this.service_ui_panel.find(".btn-close");
    this.btn_close_panel.on("click",function() {
        self.close_service_ui_panel();
    });

    //delete service
    this.delete_service = function ( service_id ) {
        ajax_request(
            {
                action: "rbdobooking_services",
                security: RBDoBookingAjax.security_services,
                service_id: service_id,
                type: "delete_service"
            },
            function ( response ) {
                if ( response.success ) {
                    self.delete_service_item_from_ui_list( service_id );
                    self.close_service_ui_panel();
                }
                else {}
                new PNotify(response.data.notification);
            }
        );
    }

    //input container for searching of service
    var btn_search_service = this.component.find(".btn-search-service");
    btn_search_service.on("keydown",function() {
        var search_val = jQuery(this).val();
        var item_none = self.service_list_area.find("li.item-none");
        if ( item_none ) {
            item_none.remove();
        }

        var all_service_items = self.service_list_area.find("li:not(:first-child)");

        if ( search_val.length > 1 ) {

            var showing_none = true;

            var obj = null;
            jQuery.each(all_service_items,function(index,item) {
                item = jQuery( item );
                obj = item.find(".name");
                if ( obj.html().toLowerCase().indexOf(search_val) >= 0 ) {
                    item.show();
                    showing_none = false;
                }
                else {
                    item.hide();
                }
            });
            if ( showing_none ) {
                var list_item_none = jQuery(self.tpl_service_list_item_none);
                list_item_none.find(".msg-zero-services").hide();
                list_item_none.find(".msg-search").show();
                self.service_list_area.append( list_item_none );
            }
        }
        else {
            all_service_items.show();
        }
    });

    //close the service_ui panel
    this.close_service_ui_panel = function () {
        self.service_ui_panel.hide("slow");
        self.open_service_id = 0;
        self.service_list_item.removeClass('list-item-hover');
    }

    // delete service item from ui list
    this.delete_service_item_from_ui_list = function ( service_id ) {
        console.log("in del service ui list");
        if ( service_id.search(",") == -1 ) {
            self.service_list_area.find("li[data-service_id='"+service_id+"']").remove();
            self.change_category_service_count(
                self.service_list_area.find("li[data-service_id='"+service_id+"'] .category_id").val(),
                "subtract"
            );
        }
        else {
            var service_ids = service_id.split(",");
            jQuery.each(service_ids,function(index,item){
                self.change_category_service_count(
                    self.service_list_area.find("li[data-service_id='"+item+"'] .category_id").val(),
                    "subtract"
                );
                self.service_list_area.find("li[data-service_id='"+item+"']").remove();
            });
        }
        var items = self.service_list_area.find("li:not(:first-child)");
        if ( items.length == 0 ) {
            var list_item_none = jQuery(self.tpl_service_list_item_none);
            self.service_list_area.append(list_item_none);
        }
    }

    //delete button for service
    var btn_delete_service = this.service_ui_panel.find(".btn-delete");
    btn_delete_service.on("click",function() {
        self.delete_service_modal.find(".service-1").show();
        self.delete_service_modal.find(".service-multi").hide();
        self.delete_service_modal.modal("show");
    });

    //delete button confirm in service modal
    var btn_delete_service = this.component.find(".delete-service-confirm");
    btn_delete_service.on("click",function() {
        var delete_service_id = self.service_ui_panel.find(".service_id").val();
        self.delete_service( self.open_service_id );
        self.component.find(".del-service-confirm-modal").modal("hide");
    });

    //save button service ui panel
    var btn_save_service_ui_panel = this.service_ui_panel.find(".btn-save");
    btn_save_service_ui_panel.on("click",function() {

        var data = [];
        var name = '';
        var duration = '';
        var price = '';
        var category_id = '';
        var color = '';

        self.service_ui_panel.find(".form-control").each(function(index,item) {
            if ( jQuery(this).attr("data-name") == "member_id" ) {
                var obj = new Object();
                obj.name = "member_id";
                obj.value = self.service_ui_panel.find("select.member_id").val();
                console.log( "service member ", obj.value );
            }
            else {
                var obj = new Object();
                obj.name = jQuery(this).attr("data-name");
                obj.value = jQuery(this).val();

                if ( obj.name == "name" ) {
                    name = obj.value;
                }
                else if ( obj.name == "duration" ) {
                    duration = obj.value;
                }
                else if ( obj.name == "price" ) {
                    price = obj.value;
                }
                else if ( obj.name == "category_id" ) {
                    category_id = obj.value;
                }
                else if ( obj.name == "color" ) {
                    color = obj.value;
                }
            }

            data.push( obj );
        });

        ajax_request(
            {
                action: 'rbdobooking_services',
                security: RBDoBookingAjax.security_services,
                data: data,
                type: "add_service"
            },
            function( response ) {
                if ( response.success ) {
                    if ( response.data.type == "new" ) {
                        console.log(name,duration,price,category_id);
                        var service_item = jQuery(self.tpl_service_list_item);
                        service_item.attr("data-service_id",response.data.service_id);
                        service_item.find(".name").html( name );
                        service_item.find(".duration").html( duration );
                        service_item.find(".price").html( price );
                        service_item.find(".category_id").val( category_id );
                        service_item.on("click",function() {
                            self.service_list_item_event_handler( response.data.service_id );
                        });
                        service_item.find(".service-checkbox").on("click",function( ptr ) {
                            ptr.stopPropagation();
                        });
                        var item_none = self.service_list_area.find("li.item-none");
                        if ( item_none ) {
                            item_none.remove();
                        }
                        self.service_list_area.append( service_item );

                        self.change_category_service_count( category_id, "add" );
                    }
                    else if ( response.data.type == "old" ) {
                        var service_item = self.service_list_area.find("li[data-service_id='"+response.data.service_id+"']")
                        service_item.find(".service-color").css( "color", color );
                        service_item.find(".name").html( name );
                        service_item.find(".duration").html( duration );
                        service_item.find(".price").html( price );
                    }

                    self.close_service_ui_panel();
                }
                else {

                }
                new PNotify(response.data.notification);
            }
        );

    });

    this.change_category_service_count = function ( category_id, type ) {
        var category_item = self.all_categories.find("li[data-category_id='"+category_id+"']");
        var count = category_item.find(".count").html();

        var total_service_count = self.component.find(".all-services-count-container .count");
        var total_count = total_service_count.html();

        if ( type == "add" ) {
            category_item.find(".count").html( parseInt(count) + 1 );
            total_service_count.html( parseInt(total_count) + 1 );
        }
        else if ( type == "subtract" ) {
            category_item.find(".count").html( parseInt(count) - 1 );
            total_service_count.html( parseInt(total_count) - 1 );
        }
    }

    var service_add_btn = this.component.find('.btn-add');
    service_add_btn.on("click",function() {
        self.show_service_ui_panel_with_relevant_data("add_service",0);
    });

    this.show_service_ui_panel_with_relevant_data = function( type, service_id ) {
        var post_data = null;
        if ( type == "edit_service" ) {
            post_data = {
                action: "rbdobooking_services",
                security: RBDoBookingAjax.security_services,
                service_id: service_id,
                type: "get_service_details"
            };
        }
        else if ( type == "add_service" ) {
            post_data = {
                action: "rbdobooking_services",
                security: RBDoBookingAjax.security_services,
                type: "get_service_form_data"
            };
        }
        ajax_request(
            post_data,
            function( response ) {
                var category_select = self.service_ui_panel.find(".category_id");
                category_select.empty();
                var html = '';
                jQuery.each(response.data.categories,function(index,item) {
                    html += '<option value="'+item.category_id+'">';
                    html += item.name;
                    html += '</option>';
                });
                category_select.append( html );

                var member_select = self.service_ui_panel.find("select.member_id");
                self.service_ui_panel.find(".service-color-picker").wpColorPicker();

                if ( type == "edit_service" ) {
                    self.service_ui_panel.find(".heading").html( response.data.service.name );
                    self.service_ui_panel.find(".name").val( response.data.service.name );

                    var html = '<input type="hidden" class="form-control service_id" ' +
                        'data-name="service_id" value="'+response.data.service.service_id+'" />';
                    self.service_ui_panel.find(".service_id").remove();
                    self.service_ui_panel.prepend( html );

                    self.service_ui_panel.find(".service-color-picker").wpColorPicker('color',response.data.service.color);
                    //self.service_ui_panel.find(".service-color-picker").val( response.data.service.color );
                    self.service_ui_panel.find(".visibility").val( response.data.service.visibility );
                    self.service_ui_panel.find(".price").val( response.data.service.price );
                    self.service_ui_panel.find(".min_capacity").val( response.data.service.min_capacity );
                    self.service_ui_panel.find(".max_capacity").val( response.data.service.max_capacity );
                    self.service_ui_panel.find(".duration").val( response.data.service.duration );
                    self.service_ui_panel.find(".padding").val( response.data.service.padding );
                    category_select.val( response.data.service.category_id );
                    //self.service_ui_panel.find(".member_id").val( response.data.service.member_id );
                    self.service_ui_panel.find(".info").val( response.data.service.info );

                    member_select.empty();
                    var html = '';
                    jQuery.each(response.data.staff_members,function(index,item) {
                        html += '<option value="'+item.member_id+'"';

                        var staff_member_found = jQuery.grep(response.data.service_members,function(n) {
                            return ( n.member_id == item.member_id );
                        });
                        /*console.log(
                            q1,
                            response.data.service_members.indexOf(item.member_id)
                        );*/
                        if ( staff_member_found.length != 0 ) {
                            console.log("in if");
                            html += ' selected';
                        }
                        html += ' data-content="<div class=\'member-select-item\'><img src=\''+item.profile_pic+'\' /><span>'+item.FULL_NAME+'</span></div>"';
                        html += '>';
                        html += item.FULL_NAME;
                        html += '</option>';
                    });
                    member_select.append( html );
                }
                else if ( type == "add_service" ) {
                    var heading_element = self.service_ui_panel.find(".heading");
                    heading_element.html(
                        heading_element.data("heading")
                    );
                    self.service_ui_panel.find(".name").val( "" );
                    self.service_ui_panel.find(".service_id").remove();
                    self.service_ui_panel.find(".visibility").val( "" );
                    self.service_ui_panel.find(".price").val( "");
                    self.service_ui_panel.find(".min_capacity").val( "" );
                    self.service_ui_panel.find(".max_capacity").val( "" );
                    self.service_ui_panel.find(".duration").val( "" );
                    self.service_ui_panel.find(".padding").val( "" );
                    //self.service_ui_panel.find(".member_id").val( "" );
                    self.service_ui_panel.find(".info").val( "" );

                    member_select.empty();
                    var html = '';
                    jQuery.each(response.data.staff_members,function(index,item) {
                        html += '<option value="'+item.member_id+'"';
                        html += ' data-content="<div class=\'member-select-item\'><img src=\''+item.profile_pic+'\' /><span>'+item.FULL_NAME+'</span></div>"';
                        html += '>';
                        html += item.FULL_NAME;
                        html += '</option>';
                    });
                }

                member_select.html( html ).selectpicker("refresh");
                self.service_ui_panel.show("slow");
            }
        );
    };

    this.service_list_item = this.service_list_area.find('li:not(:first-child)');
    this.service_list_item.on("click",function() {
        var service_id = jQuery(this).data("service_id");
        self.service_list_item_event_handler( service_id );
        jQuery(this).addClass('list-item-hover');
    });

    this.service_list_item_event_handler = function( service_id )  {

        if ( self.open_service_id == service_id ) {
            return;
        }
        else {
            self.open_service_id = service_id;
        }

        self.show_service_ui_panel_with_relevant_data( "edit_service", service_id );
    };

    this.btn_delete_multi_select_service_items = this.component.find(".btn-delete-multi");
    this.btn_delete_multi_select_service_items.on("click",function() {

        var service_check_boxes = self.service_list_area.find(".service-checkbox");
        var obj = null;
        var service_ids = [];

        jQuery.each(service_check_boxes,function(index,item){
            obj = jQuery(item);
            if ( obj.prop("checked") ) {
                service_ids.push( obj.closest("li").data("service_id") );
            }
        });
        self.open_service_id = service_ids.join();

        self.delete_service_modal.find(".service-1").hide();
        self.delete_service_modal.find(".service-multi").show();
        self.delete_service_modal.modal("show");

    });

    this.delete_service_modal = this.component.find(".del-service-confirm-modal");
    this.delete_service_modal.on("hidden.bs.modal",function() {
        self.open_service_id = 0;
    });

    this.service_checkbox = this.service_list_area.find(".service-checkbox");
    this.service_checkbox.on("click",function( ptr ) {
        ptr.stopPropagation();
    });

}
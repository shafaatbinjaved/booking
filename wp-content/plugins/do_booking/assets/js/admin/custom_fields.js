var custom_fields = null;

jQuery(document).ready(function() {
    custom_fields = new RBDoBooking_Custom_Fields();
});

function RBDoBooking_Custom_Fields() {

    var self = this;

    this.component = jQuery("#rbdobooking-custom-fields");

    this.tpl_option = this.component.find(".tpl_option").html();

    this.get_template_data = function( template_id ) {
        console.log("in template id");
        ajax_request(
            {
                action: "rbdobooking_custom_fields",
                security: RBDoBookingAjax.security_custom_fields,
                type: "get_template_data",
                template_id: template_id
            },
            function( response ) {
                if ( response.success ) {
                    self.bind_custom_fields_msg.show();
                    self.bind_service_option.show();
                    if ( self.custom_fields_container.hasClass("sortable") ) {
                        self.disable_custom_fields_sortable();
                    }
                    if ( response.data.service_id != null ) {
                        self.bind_service_option.val("enabled");
                        self.bind_service_template.val( response.data.service_id );

                        self.bind_custom_fields_msg_2.show();
                        self.bind_service_template.show();
                    }
                    else {
                        self.bind_service_option.val("disabled");
                        self.bind_service_template.val( "" );

                        self.bind_custom_fields_msg_2.hide();
                        self.bind_service_template.hide();
                    }
                    self.empty_then_add_and_then_attach_events(
                        response.data.fields_html
                    );
                }
                else {}
                new PNotify(response.data.notification);
            }
        );
    }

    this.templates_container = this.component.find(".templates-container");
    this.template_option = this.templates_container.find(".template-option");
    this.template_option.on("click",function() {
        var element = jQuery(this).closest(".radio").next().find(".form-control");
        element.prop("disabled",false);
        self.templates_container.find("."+element.data("opposite")).prop("disabled",true);

        if ( element.hasClass("new-template-name") ) {
            self.bind_custom_fields_msg.show();
            self.bind_service_option.show();
        }

    });

    this.bind_service_option = this.component.find(".bind-service-option");
    this.bind_service_option.on("click",function() {
        if ( jQuery(this).val() == "enabled" ) {
            self.bind_custom_fields_msg_2.show();
            self.bind_service_template.show();
        }
        else {
            self.bind_custom_fields_msg_2.hide();
            self.bind_service_template.hide();
        }
    });

    this.bind_service_template = this.component.find(".bind-service-template");

    this.bind_custom_fields_msg = this.component.find(".bind-custom-fields-msg");
    this.bind_custom_fields_msg_2 = this.component.find(".bind-custom-fields-msg-2");

    this.existing_template_select = this.templates_container.find(".existing-template-name");
    this.existing_template_select.on("change",function() {
        console.log( "123" );
        var template_id = jQuery(this).val();
        if ( template_id != "" ) {
            self.get_template_data( template_id );
        }
    });

    this.active_template_id = this.component.find(".active_template_id");
    if ( parseInt(this.active_template_id.val()) != 0 ) {
        this.existing_template_select.val( this.active_template_id.val() );
        jQuery(self.template_option[0]).trigger("click");
        this.existing_template_select.trigger("change");
        console.log( "",self.template_option[0] );
    }

    this.new_template_select = this.component.find(".new-template-name");

    this.custom_fields_container = this.component.find(".custom-fields-container");

    this.enable_custom_fields_sortable = function () {
        self.custom_fields_container.sortable({
            connectWith: ".custom-field",
            handle: ".move"
        });
    }

    this.enable_custom_fields_options_sortable = function( obj ) {
        if ( typeof obj == "undefined") {
            console.log("in if");
            var a = self.custom_fields_container.find(".custom-field").find(".all-options");
            console.log(a);
            a.sortable({
                connectWith: ".option-row",
                handle: ".option-move"
            });
        }
        else {
            console.log("in else");
            obj.sortable({
                connectWith: ".option-row",
                handle: ".option-move"
            });
        }
    }

    this.disable_custom_fields_options_sortable = function ( obj ) {
        obj.sortable("destroy");
    }

    this.disable_custom_fields_sortable = function () {
        self.custom_fields_container.sortable("destroy");
    }

    this.custom_fields_button = this.component.find(".btn-field");
    this.custom_fields_button.on("click",function() {
        var $tpl_name = jQuery(this).data("tpl");

        var tpl_obj = jQuery(self.component.find("."+$tpl_name).html());

        self.custom_fields_container.append( tpl_obj );
        self.custom_fields_container.off();

        self.custom_fields_container.on("click",".del-btn",function() {
            self.custom_fields_del_click_handler( this );
        });
        self.custom_fields_container.on("click",".btn-add-option",function() {
            self.custom_field_add_option_click_handler( this );
        });
        self.custom_fields_container.on("click",".del-btn-option",function() {
            self.custom_field_option_del_click_handler( this );
        });

        if ( self.custom_fields_container.hasClass("sortable") ) {
            self.disable_custom_fields_sortable();
        }
        self.enable_custom_fields_sortable();
        self.enable_custom_fields_options_sortable( tpl_obj.find(".all-options") );

        console.log( tpl_obj );
    });

    this.custom_field_option_del_click_handler = function( ptr ) {
        jQuery(ptr).closest(".option-row").remove();
    }

    this.custom_field_add_option_click_handler = function ( ptr ) {

        var tpl_obj = jQuery(self.tpl_option);
        var all_options = jQuery(ptr).prev();
        all_options.append( tpl_obj );

        if ( all_options.hasClass("sortable") ) {
            self.disable_custom_fields_options_sortable( all_options );
        }
        self.enable_custom_fields_options_sortable( all_options );

    }

    this.custom_fields_del_click_handler = function( ptr ) {
        var element = jQuery( ptr ).closest(".custom-field");
        element.remove();
    }

    this.save_btn = this.component.find(".btn-save");
    this.save_btn.on("click",function() {

        var template_option = '';
        var save_data = new Object();
        var element = self.templates_container.find(".existing-template-name");
        if ( element.prop("disabled") == false ) {
            save_data.template_id = element.val();
            save_data.template_name = element.find("option[value='"+element.val()+"']").data("option_value");
        }
        else {
            save_data.template_name = jQuery(this).closest(".templates-btn-and-fields-container").find(".new-template-name").val();
        }

        if ( self.bind_service_option.val() == "enabled" ) {
            if ( self.bind_service_template.val() != "" ) {
                save_data.is_service_specific = 1;
                save_data.service_id = self.bind_service_template.val();
            }
            else {
                save_data.is_service_specific = 0;
            }
        }
        else if ( self.bind_service_option.val() == "disabled" ) {
            save_data.is_service_specific = 0;
        }

        var custom_fields = self.custom_fields_container.find(".custom-field");
        var custom_fields_arr = [];

        jQuery.each(custom_fields,function(index,item){
            var field_obj = new Object();
            field_obj.type = jQuery(item).data("key");
            if ( field_obj.type != "text_content" ) {
                if ( jQuery(item).find(".checkbox").prop("checked") ) {
                    field_obj.is_required = 1;
                }
                else {
                    field_obj.is_required = 0;
                }
            }
            else {
                field_obj.is_required = 0;
            }
            field_obj.value = jQuery(item).find(".input").val();
            field_obj.sort = index;
            if ( jQuery(item).data("field_id") != "" ) {
                field_obj.template_field_id = jQuery(item).data("field_id");
            }

            var option_rows = jQuery(item).find(".all-options .option-row");
            if ( option_rows.length != 0 ) {
                field_obj.is_list = 1;
                field_obj.list = [];
                if ( jQuery(item).data("list_id") != "" ) {
                    field_obj.list_id = jQuery(item).data("list_id");
                }
                jQuery.each(option_rows,function(list_index,list_item) {
                    var list = new Object();
                    var element = jQuery(list_item);
                    list.item = element.find('.option-input').val();
                    if ( element.data("option_id") != "" ) {
                        list.item_id = element.data("option_id");
                    }
                    list.sort = list_index;
                    field_obj.list.push( list );
                });
            }
            else {
                field_obj.is_list = 0;
            }
            custom_fields_arr.push( field_obj );
        });

        save_data.custom_fields = custom_fields_arr;

        ajax_request(
            {
                action: "rbdobooking_custom_fields",
                security: RBDoBookingAjax.security_custom_fields,
                type: "save_custom_fields",
                data: save_data
            },
            function( response ) {
                if ( response.success ) {
                    if ( response.data.service_id != null ) {
                        self.bind_service_option.val("enabled");
                        self.bind_service_template.val( response.data.service_id );

                        self.bind_custom_fields_msg_2.show();
                        self.bind_service_template.show();
                    }
                    else {
                        self.bind_service_option.val("disabled");
                        self.bind_service_template.val( "" );

                        self.bind_custom_fields_msg_2.hide();
                        self.bind_service_template.hide();
                    }
                    var element = self.existing_template_select.find("option[value='"+response.data.template_id+"']");
                    console.log( element );
                    if ( element.length != 0 ) {
                        element.val(response.data.template_id);
                        console.log("save in if");
                    }
                    else {
                        console.log("save in else");
                        var html = '<option value="'+response.data.template_id+'">';
                        html += response.data.template_name;
                        html += '</option>';
                        self.existing_template_select.append( html );
                        self.existing_template_select.val( response.data.template_id );
                        var opposite_radio_element = self.templates_container.find("."+self.existing_template_select.data("opposite"));
                        self.existing_template_select.parent().prev().find(".template-option").prop("checked",true);
                        opposite_radio_element.prop("checked",false);
                        var select = opposite_radio_element.closest(".radio").next().find(".form-control").val("");
                        self.existing_template_select.prop("disabled",false);
                        self.new_template_select.prop("disabled",true);
                        self.new_template_select.val("");
                    }
                    self.empty_then_add_and_then_attach_events(
                        response.data.custom_fields_html
                    );
                }
                else {}
                new PNotify(response.data.notification);
            }
        );
        //console.log( custom_fields_arr );
    });

    this.empty_then_add_and_then_attach_events = function( fields_html ) {
        self.custom_fields_container.off();
        self.custom_fields_container.empty();
        self.custom_fields_container.append( fields_html );
        self.custom_fields_container.on("click",".del-btn",function() {
            self.custom_fields_del_click_handler( this );
        });
        self.custom_fields_container.on("click",".btn-add-option",function() {
            self.custom_field_add_option_click_handler( this );
        });
        self.custom_fields_container.on("click",".del-btn-option",function() {
            self.custom_field_option_del_click_handler( this );
        });
        self.enable_custom_fields_sortable();
        self.enable_custom_fields_options_sortable();
    }

}

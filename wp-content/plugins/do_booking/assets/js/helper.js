ajax_request = function (data, success_callback,other_data) {
    if ( typeof(other_data) === undefined ) {
        other_data = null;
    }
    //-------------------------- send request using the core $.ajax() method
    jQuery.ajax({
        url: RBDoBookingAjax.ajaxurl,
        type: "POST",
        dataType: "JSON",
        data: data,
        //------------------------------------------------------------------
        beforeSend: function (xhr) {},
        //------------------------------ Code to run if the request succeeds
        success: function (response) {
            if ( other_data != null ) {
                success_callback(response, other_data);
            }
            else {
                success_callback(response);
            }

        },
        //--------------------------------- Code to run if the request fails
        error: function (xhr, status, errorThrown) {},
        //---------------------------- Code to run when the request finishes
        //                  (after success and error callbacks are executed)
        complete: function (xhr, status) {}
    });
};
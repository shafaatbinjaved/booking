CREATE TABLE `{{plugin_prefix}}booked_time_slots` (
  `id_time_slot` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `timing_id` bigint(20) unsigned NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `service_id` bigint(20) NOT NULL,
  `is_booked` tinyint(4) NOT NULL COMMENT '0 pending, 1 approved, 2 cancelled, 3 rejected',
  `customer_id` bigint(20) unsigned NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `notes` varchar(350) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id_time_slot`),
  KEY `timing_id` (`timing_id`),
  KEY `member_id` (`member_id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `service_id` (`service_id`),
  KEY `customer_id` (`customer_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}booked_time_slots_other_data` (
  `id_time_slot_other_data` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_time_slot` bigint(20) unsigned NOT NULL,
  `data` longtext NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id_time_slot_other_data`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}categories` (
  `category_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `{{plugin_prefix}}coupons` (
  `coupon_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(30) NOT NULL,
  `vendor_id` bigint(20) unsigned NOT NULL,
  `discount` double unsigned NOT NULL,
  `deduction` double unsigned NOT NULL,
  `usage_limit` bigint(20) unsigned NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`coupon_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}coupons_service` (
  `coupon_service_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` bigint(20) unsigned NOT NULL,
  `service_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`coupon_service_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}customers` (
  `customer_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `extra_address` varchar(100) DEFAULT NULL COMMENT 'apartment_no',
  `street` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}email_notification_templates` (
  `email_template_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email_notification_id` bigint(20) unsigned NOT NULL,
  `vendor_id` bigint(20) unsigned NOT NULL,
  `is_enabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(200) NOT NULL,
  `template` text NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`email_template_id`),
  KEY `email_notification_id` (`email_notification_id`),
  KEY `vendor_id` (`vendor_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}email_notification_types` (
  `email_notification_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `name` varchar(300) NOT NULL,
  `is_req_cron` tinyint(4) NOT NULL DEFAULT '0',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`email_notification_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}lists` (
  `list_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` bigint(20) unsigned NOT NULL,
  `list_name` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`list_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}list_items` (
  `item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `list_id` bigint(20) unsigned NOT NULL,
  `item` varchar(30) NOT NULL,
  `item_other_info` varchar(30) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `list_id` (`list_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}orders` (
  `order_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `payment_method_id` bigint(20) unsigned NOT NULL DEFAULT '1',
  `payment_id` bigint(20) unsigned DEFAULT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  `total_without_tax` double unsigned NOT NULL,
  `total_with_tax` double unsigned NOT NULL,
  `paid_status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0 not paid, 1 paid, 2 some amount paid',
  `vendor_id` bigint(20) unsigned NOT NULL DEFAULT '1',
  `paid_stamp` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}orders_coupons` (
  `order_coupons_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `coupon_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`order_coupons_id`),
  KEY `order_id` (`order_id`),
  KEY `coupon_id` (`coupon_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}order_items` (
  `order_item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `id_time_slot` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `quantity` int(10) unsigned NOT NULL,
  `price_with_tax` double unsigned NOT NULL,
  `price_without_tax` double unsigned NOT NULL,
  `tax_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`order_item_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}payments` (
  `payment_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `payment_method_id` bigint(20) unsigned NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `amount` double NOT NULL,
  `currency_code` varchar(5) NOT NULL,
  `payer_id` bigint(20) NOT NULL,
  `payer_name` varchar(100) NOT NULL,
  `payer_email` varchar(255) NOT NULL,
  `is_full` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`payment_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}payment_methods` (
  `payment_method_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `configuration` varchar(200) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`payment_method_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}services` (
  `service_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `visibility` enum('public','private') NOT NULL,
  `color` varchar(10),
  `price` double NOT NULL,
  `min_capacity` int(11) NOT NULL,
  `max_capacity` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `padding_before` int(11) NOT NULL DEFAULT '5',
  `padding_after` int(11) NOT NULL DEFAULT '5',
  `category_id` bigint(20) unsigned NOT NULL,
  `tax_id` bigint(20) unsigned NOT NULL,
  `created_user_id` bigint(20) unsigned NOT NULL,
  `is_active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `vendor_id` bigint(20) unsigned NOT NULL DEFAULT '1',
  `info` text NOT NULL,
  `sort` bigint(20) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`service_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}settings` (
  `settings_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `view_type` varchar(15) NOT NULL,
  `is_list` tinyint(4) NOT NULL DEFAULT '0',
  `list_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`settings_id`),
  UNIQUE KEY `name` (`name`),
  KEY `type` (`type`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}staff_members` (
  `member_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `vendor_id` bigint(20) unsigned NOT NULL DEFAULT '1',
  `is_active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `profile_pic` varchar(300) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `visibility` varchar(9) NOT NULL,
  `info_member` mediumtext NOT NULL,
  `wp_user_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`member_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}staff_member_breaks` (
  `break_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `timing_id` bigint(20) NOT NULL,
  `break_start` time NOT NULL,
  `break_stop` time NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`break_id`),
  KEY `timing_id` (`timing_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}staff_member_holidays` (
  `holidays_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) unsigned NOT NULL,
  `date` date NOT NULL,
  `repeat_every_year` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`holidays_id`),
  KEY `member_id` (`member_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}staff_member_services` (
  `member_service_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` bigint(20) unsigned NOT NULL,
  `member_id` bigint(20) unsigned NOT NULL,
  `member_price` double NOT NULL,
  `member_min_capacity` int(11) NOT NULL,
  `member_max_capacity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`member_service_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}staff_member_timing` (
  `timing_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `vendor_id` bigint(20) NOT NULL DEFAULT '1',
  `day` int(11) NOT NULL,
  `is_day_on` tinyint(4) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`timing_id`),
  KEY `member_id` (`member_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}tax` (
  `tax_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tax_percent` float NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `description` varchar(256) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`tax_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}templates` (
  `template_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `vendor_id` bigint(20) unsigned NOT NULL,
  `is_service_specific` tinyint(4) NOT NULL,
  `service_id` bigint(20) unsigned DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`template_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}template_fields` (
  `template_field_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` bigint(20) unsigned NOT NULL,
  `type` varchar(25) NOT NULL,
  `value` varchar(300) NOT NULL,
  `placeholder` varchar(100) NOT NULL DEFAULT '',
  `is_list` tinyint(4) NOT NULL,
  `list_id` bigint(20) unsigned DEFAULT NULL,
  `is_required` tinyint(3) unsigned DEFAULT NULL,
  `sort` tinyint(4) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`template_field_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}temp_form_data` (
  `form_id` varchar(12) NOT NULL,
  `data` text NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`form_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}vendor_customers` (
  `id_vendor_customers` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` bigint(20) unsigned NOT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id_vendor_customers`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}vendor_specific_settings` (
  `vendor_settings_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `settings_id` bigint(20) unsigned NOT NULL,
  `vendor_id` bigint(20) unsigned NOT NULL,
  `value` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`vendor_settings_id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `settings_id` (`settings_id`)
) {{charset_collate}};


CREATE TABLE `{{plugin_prefix}}vendor_step_configuration` (
  `step_configuration_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` bigint(20) unsigned NOT NULL,
  `steps` varchar(100) NOT NULL,
  `show_cart` varchar(20) NOT NULL,
  `template` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`step_configuration_id`)
) {{charset_collate}};

ALTER TABLE `{{plugin_prefix}}services`
CHANGE `tax_id` `tax_id` bigint(20) unsigned NOT NULL DEFAULT '1' AFTER `category_id`;

CREATE TABLE `{{plugin_prefix}}addons` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `plugin_id` bigint unsigned NOT NULL,
  `type` enum(\'plugin\',\'group\') NOT NULL,
  `title` varchar(300) NOT NULL,
  `slug` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(300) NOT NULL,
  `icon` varchar(300) NOT NULL,
  `price` double NOT NULL,
  `sales` int unsigned NOT NULL,
  `ratings` float unsigned NOT NULL,
  `reviews` int unsigned NOT NULL,
  `published_at` datetime NOT NULL,
  `highlight` tinyint NOT NULL DEFAULT '0',
  `priority` int unsigned NOT NULL,
  `demo_url` varchar(300) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
);

CREATE TABLE `{{plugin_prefix}}provider_oauth` (
  `id_google_oauth` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `member_id` bigint unsigned NOT NULL,
  `vendor_id` bigint unsigned NOT NULL,
  `provider_email` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `refresh_token` varchar(255) NOT NULL,
  `authorization_code` varchar(255) NOT NULL,
  `expires_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
);
<?php
/*
Plugin Name: Do Booking
Plugin URI: https://sjm.com/
Description: Booking plugin.
Version: 1.0
Author: Shafaat Javed
Author URI: https://shafaat.com/booking-plugin/
License: GPLv2 or later
Text Domain: rbdobooking
Domain Path: /languages
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

register_activation_hook(   __FILE__, array( 'RBDoBooking_Activation_DeActivation_Uninstall', 'on_activation' ) );
register_deactivation_hook(   __FILE__, array( 'RBDoBooking_Activation_DeActivation_Uninstall', 'on_deactivation' ) );
register_uninstall_hook(   __FILE__, array( 'RBDoBooking_Activation_DeActivation_Uninstall', 'on_uninstall' ) );

// Don't allow multiple versions to be active
if ( class_exists( 'RBDoBooking' ) ) {

} else {
    /**
     * Main RBDoBooking class.
     *
     * @since 1.0.0
     *
     *
     */
    class RBDoBooking {

        /**
         * One is the loneliest number that you'll ever do.
         *
         * @since 1.0.0
         *
         * @var object
         */
        private static $instance;

        /**
         * Plugin version for enqueueing, etc.
         *
         * @since 1.0.0
         *
         * @var string
         */
        public $version = '1.0.0';

	    /**
	     * plugin prefix for database tables
	     *
	     * @since 1.0.0
	     * @var string
	    */
        public $plugin_prefix = 'rbdobooking_';

	    /*
		 * Vendor Cookie name
		 * @since 1.0.0
		 * @var $vendor_cookie_name
		 */
	    private $vendor_cookie_prefix= "vendor_wp_";

        /**
         * The front-end instance.
         *
         * @since 1.0.0
         *
         * @var object WPForms_Frontend
         */
        public $frontend;

        /**
         * The process instance.
         *
         * @since 1.0.0
         *
         * @var object WPForms_Process
         */
        public $process;

        /**
         * The smart tags instance.
         *
         * @since 1.0.0
         *
         * @var object WPForms_Smart_Tags
         */
        public $smart_tags;

        /**
         * The Logging instance.
         *
         * @since 1.0.0
         *
         * @var object WPForms_Logging
         */
        public $logs;

        /**
         * The Preview instance.
         *
         * @since 1.0.0
         *
         * @var object WPForms_Preview
         */
        public $preview;

        /**
         * The License class instance (Pro).
         *
         * @since 1.0.0
         *
         * @var object WPForms_License
         */
        public $license;

        /**
         * Paid returns true, free (Lite) returns false.
         *
         * @since 1.3.9
         *
         * @var boolean
         */
        public $pro = false;

        /**
         * Main RBDoBooking Instance.
         *
         * Insures that only one instance of RBDoBooking exists in memory at any one
         * time. Also prevents needing to define globals all over the place.
         *
         * @since 1.0.0
         *
         * @return RBDoBooking
         */

        public static function instance() {

            if (!isset( self::$instance ) && !(self::$instance instanceof RBDoBooking)) {

                self::$instance = new RBDoBooking();
                self::$instance->constants();
                self::$instance->load_textdomain();
                //self::$instance->conditional_logic_addon_check();
                self::$instance->includes();

                // Load Pro or Lite specific files
                if ( self::$instance->pro ) {
	                require_once RBDOBOOKING_PLUGIN_DIR . 'pro/rbdobooking-pro.php';
                } else {
                    require_once RBDOBOOKING_PLUGIN_DIR . 'lite/rbdobooking-lite.php';
                }

                add_action( 'plugins_loaded', array( self::$instance, 'objects' ), 10 );
            }
            return self::$instance;
        }

        /**
         * Setup plugin constants.
         *
         * @since 1.0.0
        * */
        private function constants() {

            // Plugin version.
            if ( ! defined( 'RBDOBOOKING_VERSION' ) ) {
                define( 'RBDOBOOKING_VERSION', $this->version );
            }

            // Plugin prefix
	        if ( !defined('RBDOBOOKING_PLUGIN_PREFIX') ) {
            	define( 'RBDOBOOKING_PLUGIN_PREFIX', $this->plugin_prefix );
	        }

	        //plugin cookie
	        if ( !defined('RBDOBOOKING_VENDOR_COOKIE') ) {
            	define('RBDOBOOKING_VENDOR_COOKIE',md5($this->vendor_cookie_prefix.RBDOBOOKING_PLUGIN_PREFIX));
	        }

            // Plugin Folder Path.
            if ( ! defined( 'RBDOBOOKING_PLUGIN_DIR' ) ) {
                define( 'RBDOBOOKING_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
            }

            // Plugin Folder URL.
            if ( ! defined( 'RBDOBOOKING_PLUGIN_URL' ) ) {
                define( 'RBDOBOOKING_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
            }

            // Plugin Root File.
            if ( ! defined( 'RBDOBOOKING_PLUGIN_FILE' ) ) {
                define( 'RBDOBOOKING_PLUGIN_FILE', __FILE__ );
            }

            // Plugin Slug - Determine plugin type and set slug accordingly.
            if ( file_exists( RBDOBOOKING_PLUGIN_DIR . 'pro/rbdobooking-pro.php' ) ) {
                $this->pro = true;
                define( 'RBDOBOOKING_PLUGIN_SLUG', 'rbdobooking' );
            } else {
                define( 'RBDOBOOKING_PLUGIN_SLUG', 'rbdobooking-lite' );
            }

        }

        /**
         * Loads the plugin language files.
         *
         * @since 1.0.0
         */
        public function load_textdomain() {

            load_plugin_textdomain( 'rbdobooking', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
        }

        /**
         * Include files.
         *
         * @since 1.0.0
         */
        private function includes() {

        	// Global include
	        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/functions.php';
	        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/activation.php';

	        // Admin/Dashboard only includes
	        if ( is_admin() ) {
	        	require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/admin.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-menu.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-cache.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-settings.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-calendar.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-appointments.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-appearance.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-services.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-staff-members.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-customers.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-emails.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-orders.php';
		        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-addons.php';
	        }

	        //frontend include
	        require_once RBDOBOOKING_PLUGIN_DIR . 'includes/frontend/class-frontend.php';
        }

        /**
         *
         * @since 1.0.0
         */
        public function objects() {
        	// global objects
	        /*if ( is_admin() ) {
	        	if ( !wp_rbdobooking_setting('hide-announcements',false) ) {

		        }
	        }*/
        }

    }

    /**
     * The function which returns the one RBDoBooking instance.
     *
     * @since 1.0.0
     * @return object
     */
    function rbdobooking() {

        return RBDoBooking::instance();
    }
    rbdobooking();
}

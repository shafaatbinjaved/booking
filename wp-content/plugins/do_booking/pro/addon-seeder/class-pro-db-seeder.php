<?php
/**
 * User: shafaatbinjaved
 * Date: 4/25/2019
 * Time: 12:36 PM
 */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';

class RBDoBooking_Addon_Pro_DB_Seeder {

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	function __construct() {
		add_action('payment_methods_db_seeder', array($this,'insertPaymentMethods'));
	}

	/**
	 * Add records for table email_notification_types
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	private function insertPaymentMethods() {
		$insert_time = date("Y-m-d H:i:s");
		$table_data = array(
			array(
				"name"          =>  "paypal",
				"is_active"     =>  "1",
				"sort"          =>  "0",
				"configuration" =>  "",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"name"          =>  "payoneer",
				"is_active"     =>  "1",
				"sort"          =>  "0",
				"configuration" =>  "",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			)
		);
		foreach ( $table_data as $data ) {
			$this->db->insert(
				"payment_methods",
				$data
			);
		}
	}

}
//new RBDoBooking_Addon_Pro_DB_Seeder();
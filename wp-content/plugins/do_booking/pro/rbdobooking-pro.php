<?php
/**
 * Created by PhpStorm.
 * User: Shafaat Javed
 * Date: 12/9/2017
 * Time: 4:01 PM
 */

class RBDoBooking_Pro {

    /*
     * Primary class constructor
     *
     * @since 1.0
     * */
    public function __construct() {
        $this->includes();
    }

    /**
     * Include files.
     *
     * @since 1.0
     */
    private function includes() {

	    // Admin/Dashboard only includes
	    if ( is_admin() ) {
	    	if (file_exists(RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-custom-fields.php')) {
			    require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-custom-fields.php';
		    }
            if (file_exists(RBDOBOOKING_PLUGIN_DIR . 'includes/admin/integrations/Google_Calendar.php')) {
                require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/integrations/Google_Calendar.php';
            }
		    /*if (file_exists(RBDOBOOKING_PLUGIN_DIR . 'addons/do_booking_invoices/class-invoices.php')) {
			    require_once RBDOBOOKING_PLUGIN_DIR . 'addons/do_booking_invoices/class-invoices.php';
		    }*/
		    /*if (file_exists(RBDOBOOKING_PLUGIN_DIR . 'addons/do_booking_coupons/class-coupons.php')) {
			    //require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/class-coupons.php';
                require_once plugin_dir_path( __FILE__ ) . '../addons/do_booking_coupons/class-coupons.php';
		    }*/
		    //add-on seeder
		    require_once RBDOBOOKING_PLUGIN_DIR . 'pro/addon-seeder/class-pro-db-seeder.php';
	    }

    }

}
new RBDoBooking_Pro();


















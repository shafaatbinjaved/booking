<?php
/**
 * User: shafaatbinjaved
 * Date: 3/31/2018
 * Time: 11:47 AM
 */
/**
 * Get the default capability to manage everything for RBDoBooking.
 *
 * @since 1.0.0
 *
 * @return string
 */
function rbdobooking_get_capability_manage_options() {
	return apply_filters( 'rbdobooking_manage_cap', 'manage_options' );
}

/**
 * Sanitizes string of CSS classes.
 * @since 1.0.0
 *
 * @param array|string $classes
 * @param bool $convert True will convert strings to array and vice versa.
 *
 * @return string|array
*/
function rbdobooking_sanitize_classes( $classes, $convert = false ) {

	$array = is_array( $classes );
	$css = array();

	if ( !empty( $classes ) ) {
		if ( !$array ) {
			$classes = explode(' ', trim($classes));
		}
		foreach ( $classes as $class ) {
			if ( !empty( $class ) ) {
				$css[] = sanitize_html_class( $class );
			}
		}
	}
	if ( $array ) {
		return $convert ? implode(' ',$css) : $css;
	} else {
		return $convert ? $css : implode(' ',$css);
	}
}

/**
 * Get the value of a specific RBDoBooking setting.
 * @since 1.0.0
 *
 * @param string $key
 * @param mixed $default
 * @param string $option
 * @return mixed
*/
function rbdobooking_setting( $key, $default = false, $option = 'rbdobooking_settings' ) {

	$key = rbdobooking_sanitize_key( $key );
	$options = get_option( $option, false );
	$value = is_array( $options ) && !empty($options[$key]) ? $options[$key] : $default;

	return $value;
}

/*
 *  Check permissions for currently logged in user.
 *
 * @since 1.0.0
 *
 * @return bool
 * */
function rbdobooking_current_user_can() {
	$capability = rbdobooking_get_capability_manage_options();

	return apply_filters(
		'rbdobooking_current_user_can',
		current_user_can( $capability ),
		$capability
	);
}

/**
 * Sanitize key, primarily used for looking up options.
 * @since 1.0.0
 * @param string $key
 * @return string
*/
function rbdobooking_sanitize_key( $key = '' ) {
	return preg_replace( '/[^a-zA-Z0-9_\-\.\:\/]/', '', $key );
}
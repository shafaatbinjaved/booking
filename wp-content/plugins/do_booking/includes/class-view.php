<?php
/**
 * User: shafaatbinjaved
 * Date: 5/10/2018
 * Time: 12:46 PM
 */

class View {

    /**
     * @var string $view_location
     */
	private $view_location;
	private $key = '1a9b545312cd4aa2cbd6e9f8cd5e7a9cfge34vrtf';
	private $iv = 'q12331adf2sds1z22Adsa2das23125c2dfew2f12d';
	private $encrypt_method = "AES-256-CBC";
	private $views = array();
	private $view_name;

	function __construct($view_location) {
		$this->view_location = $view_location;
	}
    /**
     * @return string
     */
    public function getViewLocation() {
        return $this->view_location;
    }

    /**
     * @param string $view_location
     */
    public function setViewLocation($view_location) {
        $this->view_location = $view_location;
    }

	private function generate_iv( $id ) {
		$secret_key = $this->key;
		$secret_iv = $this->iv;

		// hash
		$key = hash('sha256', $id.$secret_key);

		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		$iv = substr(hash('sha256', $secret_iv), 0, 16);

		$data = new stdClass();
		$data->iv = $iv;
		$data->key = $key;

		return $data;
	}

	public function encrypt( $string, $id ) {

		$data = $this->generate_iv( $id );

		$output = openssl_encrypt($string, $this->encrypt_method, $data->key, 0, $data->iv);
		$output = base64_encode($output);

		return $output;

	}

	public function decrypt( $string, $id ) {

		$data = $this->generate_iv($id);

		$output = openssl_decrypt(base64_decode($string), $this->encrypt_method, $data->key, 0, $data->iv);

		return $output;
	}

	public function get_view($view_name, $in_script_tag = false, $data = array(), $class_name = '') {
		$this->view_name = $view_name;
		$html = '';
		$full_view = $this->view_location.$view_name.'.php';
		if ( isset($this->views[$view_name]) ) {
			$html = $this->views[$view_name];
		}
		else if ( file_exists( $full_view ) ) {
			$html = file_get_contents( $full_view, TRUE );
			$this->views[$view_name] = $html;
		}
		else {
			echo 'View :' . $full_view . ' is missing' ;
			return $html;
		}
		if ( $in_script_tag && count($data) == 0 ) {
			return '<script type="text/html" class="'.$class_name.'">'.$html.'</script>';
		}
		if ( count($data) > 0 ) {
			extract( $data );
			ob_start();
			eval('?>' . $html . '<?php ');
			$html = ob_get_contents();
			ob_end_clean();
		}
		else {
			ob_start();
			eval('?>' . $html . '<?php ');
			$html = ob_get_contents();
			ob_end_clean();
		}
		if ( $in_script_tag ) {
			return '<script type="text/html" class="'.$class_name.'">'.$html.'</script>';
		}
		return $html;
	}

	function custom_replace($matches,$data,$html) {
		$matches[0] = array_unique($matches[0]);
		$matches[1] = array_unique($matches[1]);

		foreach ( $matches[0] as $index => $row ) {
			if ( isset($data[$matches[1][$index]]) ) {
				$html = str_replace($row, $data[$matches[1][$index]], $html);
			}
			else {
				echo $matches[1][$index] . " is missing in view : " . $this->view_name;
			}
		}
		return $html;
	}

}
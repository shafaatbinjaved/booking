<?php

class RBDoBooking_DB_Customers {

	/**
	 * One is the loneliest number that you'll ever do.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * plugin prefix
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $plugin_prefix = 'rbdobooking_';

	public function __construct() {
	}

	/**
	 * Insures that only one instance of RBDoBooking DB Customers class exists in memory
	 *
	 * @since 1.0.0
	 *
	 * @return RBDoBooking_DB_Customers
	 */
	public static function instance() {

		if ( !isset( self::$instance ) && !( self::$instance instanceof RBDoBooking_DB_Customers ) ) {

			global $wpdb;
			self::$instance = new RBDoBooking_DB_Customers();
			self::$instance->plugin_prefix = $wpdb->prefix . self::$instance->plugin_prefix;

		} else {
			self::$instance;
		}
		return self::$instance;
	}

	public function getAllCustomersForVendor( $vendor_id = 1 ) {
		global $wpdb;

		$sql = 'SELECT 
					b.*, count(c.customer_id) as total_appointments, 
					max(c.date_start) as last_appointment
				FROM 
					'.$this->plugin_prefix.'vendor_customers a
				INNER JOIN 
					'.$this->plugin_prefix.'customers b
				ON 
					a.vendor_id = '.$vendor_id.'
				AND 
					b.customer_id = a.customer_id
				LEFT JOIN 
					'.$this->plugin_prefix.'booked_time_slots c
				ON
					c.customer_id = b.customer_id
				GROUP BY 
					b.customer_id
				ORDER BY 
					c.date_start DESC';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

	public function getCustomerFromCustomerID( $customer_id ) {
		global $wpdb;

		$sql = 'SELECT 
					*
				FROM
					'.$this->plugin_prefix.'customers a
				WHERE 
					a.customer_id = ' . $customer_id;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $customer ) {
			return $customer;
		}

		return array();
	}

	public function checkCustomerExists( $email ) {
		global $wpdb;

		$sql = 'SELECT a.customer_id
				FROM '.$this->plugin_prefix.'customers a
				WHERE a.email = "'.$email.'"';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		if ( count($result) > 0 ) {
			foreach ( $result as $row ) {
				return $row["customer_id"];
			}
		}
		else {
			return 0;
		}
	}

	public function checkVendorCustomerExistsFromCustomerID( $vendor_id, $customer_id ) {
		global $wpdb;

		$sql = 'SELECT count(1) as count
				FROM '.$this->plugin_prefix.'vendor_customers a
				WHERE a.vendor_id = '.$vendor_id.'
				AND a.customer_id = '.$customer_id;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $row ) {
			return (intval($row["count"] > 0 ) ? true : false);
		}
		return false;
	}

	public function insert_vendor_customer( $vendor_id, $customer_id ) {
		global $wpdb;

		$data_vendor_customer = array(
			"vendor_id"     =>  $vendor_id,
			"customer_id"   =>  $customer_id,
			"created_at"    =>  date("Y-m-d H:i:s")
		);

		$result = $wpdb->insert(
			$this->plugin_prefix."vendor_customers",
			$data_vendor_customer
		);

		if ( $result ) {
			return $wpdb->insert_id;
		}
		return false;
	}

	public function insert_customer( $data ) {
		global $wpdb;

		$result = $wpdb->insert(
			$this->plugin_prefix."customers",
			$data
		);

		if ( $result ) {
			$customer_id = $wpdb->insert_id;

			return $customer_id;
		}
		return false;
	}

}
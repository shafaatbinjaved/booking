<?php
/**
 * User: shafaatbinjaved
 * Date: 4/6/2019
 * Time: 6:52 PM
 */

class RBDoBooking_DB_Orders {

	/**
	 * One is the loneliest number that you'll ever do.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * plugin prefix
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $plugin_prefix = 'rbdobooking_';

	public function __construct() {
	}

	/**
	 * Insures that only one instance of RBDoBooking DB Customers class exists in memory
	 *
	 * @since 1.0.0
	 *
	 * @return RBDoBooking_DB_Orders
	 */
	public static function instance() {

		if ( !isset( self::$instance ) && !( self::$instance instanceof RBDoBooking_DB_Orders ) ) {

			global $wpdb;
			self::$instance = new RBDoBooking_DB_Orders();
			self::$instance->plugin_prefix = $wpdb->prefix . self::$instance->plugin_prefix;

		} else {
			self::$instance;
		}
		return self::$instance;
	}

	/*
	 * Get all orders lates by date
	 *
	 * @since 1.0.0
	 *
	 * @return Array
	 * */
	public function getLatestOrders( $vendor_id = 1 ) {
		global $wpdb;

		$sql = 'SELECT 
					a.*, c.full_name, d.name as payment_method_name
				FROM 
					'.$this->plugin_prefix.'orders a
				LEFT JOIN 
					'.$this->plugin_prefix.'payments b
				ON 
					a.order_id = b.order_id
				LEFT JOIN 
					'.$this->plugin_prefix.'customers c
				ON
					a.customer_id = c.customer_id
				LEFT JOIN
					'.$this->plugin_prefix.'payment_methods d
				ON
					a.payment_method_id = d.payment_method_id
				WHERE 
					a.vendor_id = '.$vendor_id.'
				GROUP BY 
					a.order_id
				ORDER BY 
					a.order_id DESC';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

	/*
	 * Get all order data
	 *
	 * @since 1.0.0
	 *
	 * @var int $order_id
	 *
	 * @return array $result
	 * */
	public function getOrderData( $order_id, $vendor_id ) {
		global $wpdb;

		$sql = 'SELECT
					a.*,b.*,t.*, 
					s.name as service_name, 
					mem.first_name, mem.last_name,
					slot.date_start as slot_start_date,
					cust.full_name as customer_name, 
					cust.zip, cust.extra_address, 
					cust.street, cust.city, cust.country, 
					pay_method.name as payment_method_name
				FROM 
					'.$this->plugin_prefix.'orders a
				INNER JOIN 
					'.$this->plugin_prefix.'order_items b
				ON 
					a.order_id = '.$order_id.'
					AND a.order_id = b.order_id
				LEFT JOIN 
					'.$this->plugin_prefix.'tax t
				ON 
					t.tax_id = b.tax_id
				LEFT JOIN 
					'.$this->plugin_prefix.'booked_time_slots slot
				ON
					slot.id_time_slot = b.id_time_slot
				LEFT JOIN 
					'.$this->plugin_prefix.'services s
				ON
					s.service_id = slot.service_id
				LEFT JOIN 
					'.$this->plugin_prefix.'staff_members mem
				ON
					mem.member_id = slot.member_id
				LEFT JOIN 
					'.$this->plugin_prefix.'customers cust
				ON
					cust.customer_id = a.customer_id
				LEFT JOIN
					'.$this->plugin_prefix.'payment_methods pay_method
				ON 
					pay_method.payment_method_id = a.payment_method_id
				WHERE
					a.vendor_id = '.$vendor_id;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

}



















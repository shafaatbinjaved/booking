<?php
/**
 * User: shafaatbinjaved
 * Date: 4/13/2018
 * Time: 9:46 PM
 */

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db-seeder.php';

/**
 * Activation class.
 * @since 1.0.0
*/
class RBDoBooking_Activation_DeActivation_Uninstall {

	/**
	 * Plugin table prefix
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public static $plugin_prefix = '';

	/**
	 * Plugin db version for plugin specific tables
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public static $db_version = RBDOBOOKING_VERSION;

	public static $version_update = array(
		'1.0.0',
		'1.0.1'
	);

	/**
	 * Install function
	 * @since 1.0.0
	*/
	public static function on_activation() {

		if ( ! current_user_can( 'activate_plugins' ) ) {
			return;
		}

		$option_name = RBDOBOOKING_PLUGIN_PREFIX . 'version';

		$installed_ver = get_option( $option_name );

		if ( $installed_ver != self::$db_version ) {//installed version and plugin version not equals
			global $wpdb;
			self::$plugin_prefix = $wpdb->prefix . RBDOBOOKING_PLUGIN_PREFIX;

			$charset_collate = $wpdb->get_charset_collate();

			self::$version_update[$installed_ver];

			$index = 0;

			if ( $installed_ver != false ) {
				$index = array_search($installed_ver, self::$version_update[$installed_ver]);
			}

			$array_count = count(self::$version_update);

			$queries = '';

			ob_start();
			for ( $i=$index; $i<$array_count; $i++ ) {
				if ( file_exists(RBDOBOOKING_PLUGIN_DIR.'patch/'.self::$version_update[$i].'/update.sql') ) {
					require_once RBDOBOOKING_PLUGIN_DIR.'patch/'.self::$version_update[$i].'/update.sql';
				}
			}
			$queries .= ob_get_clean();
			$queries = str_replace('{{plugin_prefix}}',self::$plugin_prefix, $queries);
			$queries = str_replace('{{charset_collate}}',$charset_collate, $queries);

			dbDelta( $queries );

			if ( $installed_ver == "" ) {//installing it for first time seed db
				$db_seeder = new RBDoBooking_DB_Seeder();
				$db_seeder->insertAllItems();
			}

			update_option( $option_name, RBDOBOOKING_VERSION);//self::$db_version );
		}
	}

	/**
	 * Install function
	 * @since 1.0.0
	 */
	public function on_deactivation() {

		if ( ! current_user_can( 'activate_plugins' ) ) {
			return;
		}

	}

	/**
	 * Install function
	 * @since 1.0.0
	 */
	public function on_uninstall() {

		if ( ! current_user_can( 'activate_plugins' ) ) {
			return;
		}

		global $wpdb;
		self::$plugin_prefix = $wpdb->prefix . RBDOBOOKING_PLUGIN_PREFIX;
		$option_name = RBDOBOOKING_PLUGIN_PREFIX . 'version';

		$del_query = 'DROP TABLE 
						`'.self::$plugin_prefix.'booked_time_slots`, 
						`'.self::$plugin_prefix.'booked_time_slots_other_data`, 
						`'.self::$plugin_prefix.'categories`, 
						`'.self::$plugin_prefix.'coupons`, 
						`'.self::$plugin_prefix.'coupons_service`, 
						`'.self::$plugin_prefix.'customers`, 
						`'.self::$plugin_prefix.'email_notification_templates`, 
						`'.self::$plugin_prefix.'email_notification_types`, 
						`'.self::$plugin_prefix.'lists`, 
						`'.self::$plugin_prefix.'list_items`, 
						`'.self::$plugin_prefix.'orders`, 
						`'.self::$plugin_prefix.'orders_coupons`, 
						`'.self::$plugin_prefix.'order_items`, 
						`'.self::$plugin_prefix.'payments`, 
						`'.self::$plugin_prefix.'payment_methods`, 
						`'.self::$plugin_prefix.'services`, 
						`'.self::$plugin_prefix.'settings`, 
						`'.self::$plugin_prefix.'staff_members`, 
						`'.self::$plugin_prefix.'staff_member_breaks`, 
						`'.self::$plugin_prefix.'staff_member_holidays`, 
						`'.self::$plugin_prefix.'staff_member_services`, 
						`'.self::$plugin_prefix.'staff_member_timing`, 
						`'.self::$plugin_prefix.'tax`, 
						`'.self::$plugin_prefix.'templates`, 
						`'.self::$plugin_prefix.'template_fields`, 
						`'.self::$plugin_prefix.'temp_form_data`, 
						`'.self::$plugin_prefix.'vendor_customers`, 
						`'.self::$plugin_prefix.'vendor_specific_settings`, 
						`'.self::$plugin_prefix.'vendor_step_configuration`';

		$wpdb->query( $del_query );
		delete_option($option_name);

	}

}
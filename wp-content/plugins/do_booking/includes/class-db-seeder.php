<?php
/**
 * User: shafaatbinjaved
 * Date: 4/25/2019
 * Time: 8:56 AM
 */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';

class RBDoBooking_DB_Seeder {

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	public function __construct() {
		$this->db = RBDoBooking_DB::instance();
	}

	public function insertAllItems() {
		$this->insertEmailNotificationTypes();
		$this->insertPaymentMethods();
		$this->insertSettings();
		$this->insertVendorStepConfiguration();
		$this->insertTax();
	}

	/**
	 * Add records for table email_notification_types
	 *
	 * @since 1.0.0
	 *
	 * @return void
	*/
	private function insertEmailNotificationTypes() {
		$insert_time = date("Y-m-d H:i:s");
		$table_data = array(
			array(
				"type"          =>  "cust_pend_appoint",
				"name"          =>  "Notification to cusotomer about pending appointment",
				"is_req_cron"   =>  "0",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "mem_pend_appoint",
				"name"          =>  "Notification to staff member about pending appointment",
				"is_req_cron"   =>  "0",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "cust_approve_appoint",
				"name"          =>  "Notification to customer about approved appointment",
				"is_req_cron"   =>  "0",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "mem_approve_appoint",
				"name"          =>  "Notification to staff member about approved appointment",
				"is_req_cron"   =>  "0",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "cust_cancel_appoint",
				"name"          =>  "Notification to customer about cancel appointment",
				"is_req_cron"   =>  "0",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "mem_cancel_appoint",
				"name"          =>  "Notification to staff member about cancel appointment",
				"is_req_cron"   =>  "0",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "cust_reject_appoint",
				"name"          =>  "Notification to customer about rejected appointment",
				"is_req_cron"   =>  "0",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "mem_reject_appoint",
				"name"          =>  "Notification to staff member about rejected appointment",
				"is_req_cron"   =>  "0",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "cust_login_details",
				"name"          =>  "Notification to customer about their login details",
				"is_req_cron"   =>  "0",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "cust_next_day_appoint",
				"name"          =>  "Notification to customer about next day appointment (requires cron setup)",
				"is_req_cron"   =>  "1",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "follow_up_msg_after_appoint",
				"name"          =>  "Follow-up message in the same day after appointment (requires cron setup)",
				"is_req_cron"   =>  "1",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "cust_birthday_greeting",
				"name"          =>  "Customer birthday greeting (requires cron setup)",
				"is_req_cron"   =>  "1",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
			array(
				"type"          =>  "mem_next_day_agenda",
				"name"          =>  "Evening notification for next day agenda to staff member (requires cron setup)",
				"is_req_cron"   =>  "1",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			),
		);
		foreach ( $table_data as $data ) {
			$this->db->insert(
				"email_notification_types",
				$data
			);
		}
	}

	/**
	 * Add records for table email_notification_types
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	private function insertPaymentMethods() {
		$insert_time = date("Y-m-d H:i:s");
		$table_data = array(
			array(
				"name"          =>  "cash",
				"is_active"     =>  "1",
				"sort"          =>  "0",
				"configuration" =>  "",
				"updated_at"    =>  $insert_time,
				"created_at"    =>  $insert_time
			)
		);
		foreach ( $table_data as $data ) {
			$this->db->insert(
				"payment_methods",
				$data
			);
		}
		//do_action("payment_methods_db_seeder");
	}

	private function insertAddons() {
        $insert_time = date("Y-m-d H:i:s");
        $table_data = array(
            [
                'plugin_id' => time(),
                'type'          => 'plugin',
                'title'         => 'Do Booking Coupons',
                'slug'          => 'do_booking_coupons',
                'description'   => 'This addon adds functionality of coupons',
                'url'           => 'https://responsivebit.com',
                'icon'          => 'https://responsivebit.com',
                'price'         => 19.99,
                'sales'         => 10,
                'ratings'       => 4.0,
                'reiews'        => 10,
                'published_at'  => $insert_time,
                'highlight'     => 0,
                'priority'      => 10,
                'demo_url'      => 'https://responsivebit.com',
                'updated_at'    => $insert_time,
                'created_at'    => $insert_time
            ],
            [
                'plugin_id' => time(),
                'type'          => 'plugin',
                'title'         => 'Do Booking Invoices',
                'slug'          => 'do_booking_invoices',
                'description'   => 'This addon adds invoices',
                'url'           => 'https://responsivebit.com',
                'icon'          => 'https://responsivebit.com',
                'price'         => 14.99,
                'sales'         => 10,
                'ratings'       => 4.0,
                'reiews'        => 10,
                'published_at'  => $insert_time,
                'highlight'     => 0,
                'priority'      => 10,
                'demo_url'      => 'https://responsivebit.com',
                'updated_at'    => $insert_time,
                'created_at'    => $insert_time
            ]
        );
        foreach ( $table_data as $data ) {
            $this->db->insert(
                'addons',
                $data
            );
        }
    }

	/**
	 * Add records for table settings
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	private function insertSettings() {
		$insert_time = date("Y-m-d H:i:s");
		$table_data = array(
			array(
				"type"          =>  "business_hours",
				"name"          =>  "time_start",
				"view_type"     =>  "time",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  "09:00:00"
			),
			array(
				"type"          =>  "business_hours",
				"name"          =>  "time_stop",
				"view_type"     =>  "time",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  "18:00:00"
			),
			array(
				"type"          =>  "company",
				"name"          =>  "company_name",
				"view_type"     =>  "text",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  "Example company"
			),
			array(
				"type"          =>  "company",
				"name"          =>  "company_address",
				"view_type"     =>  "textarea",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  "Example street 1, 12345, Example city, example country"
			),
			array(
				"type"          =>  "company",
				"name"          =>  "company_phone",
				"view_type"     =>  "phone",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  "+1123456789"
			),
			array(
				"type"          =>  "company",
				"name"          =>  "company_website",
				"view_type"     =>  "text",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  "https://example.com"
			),
			array(
				"type"          =>  "purchase_code",
				"name"          =>  "purchase_code",
				"view_type"     =>  "text",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  ""
			),
			array(
				"type"          =>  "general",
				"name"          =>  "day_limit",
				"view_type"     =>  "number",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  "365"
			),
			array(
				"type"          =>  "google_calendar",
				"name"          =>  "client_id",
				"view_type"     =>  "text",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  ""
			),
			array(
				"type"          =>  "google_calendar",
				"name"          =>  "client_secret",
				"view_type"     =>  "text",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  ""
			),
			array(
				"type"          =>  "google_calendar",
				"name"          =>  "redirect_uri",
				"view_type"     =>  "text",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  ""
			),
			array(
				"type"          =>  "google_calendar",
				"name"          =>  "event_title",
				"view_type"     =>  "text",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  ""
			),
			array(
				"type"          =>  "woocommerce",
				"name"          =>  "rbdobooking_product",
				"view_type"     =>  "text",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  ""
			),
			array(
				"type"          =>  "woocommerce",
				"name"          =>  "cart_item_data",
				"view_type"     =>  "text",
				"is_list"       =>  "0",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"default"       =>  ""
			),
			array(
				"type"          =>  "payments",
				"name"          =>  "currency",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_currency_items",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Rs",
							"item_other_info"   =>  "pkr",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "$",
							"item_other_info"   =>  "USD",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "€",
							"item_other_info"   =>  "Euro",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "general",
				"name"          =>  "enable_coupon_code",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_enable_coupon_code",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Yes",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "No",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "google_calendar",
				"name"          =>  "2_way_sync",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_google_2_way_sync",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Enabled",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "google_calendar",
				"name"          =>  "limit_fetched_events",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_limit_fetched_events",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "25",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "50",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "75",
							"item_other_info"   =>  "",
							"sort"              =>  "2",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "100",
							"item_other_info"   =>  "",
							"sort"              =>  "3",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "cart",
				"name"          =>  "cart_enable",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_cart_enable",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Enabled",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "woocommerce",
				"name"          =>  "woocommerce_enable",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_woocomerce_enable",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Enabled",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "customers",
				"name"          =>  "create_wp_users",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_create_wp_users",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Enabled",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "customers",
				"name"          =>  "new_user_account_role",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_new_user_account_role",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Subscriber",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Contributor",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Author",
							"item_other_info"   =>  "",
							"sort"              =>  "2",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Editor",
							"item_other_info"   =>  "",
							"sort"              =>  "3",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Administrator",
							"item_other_info"   =>  "",
							"sort"              =>  "4",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "customers",
				"name"          =>  "make_birthday_mandatory",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_make_birthday_mandatory",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Enabled",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "customers",
				"name"          =>  "make_address_mandatory",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_make_address_mandatory",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Enabled",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "general",
				"name"          =>  "allow_staff_members_to_edit",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_allow_staff_members_to_edit",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Enabled",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "general",
				"name"          =>  "min_time_prior_cancel",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_min_time_prior_cancel",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "15 min",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "1 h",
							"item_other_info"   =>  "",
							"sort"              =>  "2",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "general",
				"name"          =>  "min_time_prior_booking",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_min_time_prior_booking",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "30 min",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "1 h",
							"item_other_info"   =>  "",
							"sort"              =>  "2",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "general",
				"name"          =>  "default_appointment_status",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_default_appointment_status",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Approved",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Pending",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "customers",
				"name"          =>  "cancel_appointment_action",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_cancel_appointment_action",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Delete",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Cancel",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			),
			array(
				"type"          =>  "payments",
				"name"          =>  "include_tax",
				"view_type"     =>  "select",
				"is_list"       =>  "1",
				"list_id"       =>  "0",
				"created_at"    =>  $insert_time,
				"list"          =>  array(
					"vendor_id" =>  1,
					"list_name" =>  "1_include_tax_enable",
					"created_at"=>  $insert_time,
					"list_items"    =>  array(
						array(
							"list_id"           =>  "0",
							"item"              =>  "Enabled",
							"item_other_info"   =>  "",
							"sort"              =>  "0",
							"created_at"        =>  $insert_time
						),
						array(
							"list_id"           =>  "0",
							"item"              =>  "Disabled",
							"item_other_info"   =>  "",
							"sort"              =>  "1",
							"created_at"        =>  $insert_time
						)
					)
				),
				"default"       =>  ""
			)
		);
		foreach ( $table_data as $data ) {
			$default = $data["default"];
			unset($data["default"]);
			$settings_id = null;
			if ( $data["is_list"] == "0" ) {//without list
				$settings_id = $this->db->insert(
					"settings",
					$data
				);
			}
			else {//with list
				//first insert list then list items
				$list = $data["list"];
				unset($list["list_items"]);
				$list_id = $this->db->insert(
					"lists",
					$list
				);
				$data["list_id"] = $list_id;
				foreach ( $data["list"]["list_items"] as $index => $list_item ) {
					$temp_list_item = $list_item;
					$temp_list_item["list_id"] = $list_id;
					$item_id = $this->db->insert(
						"list_items",
						$temp_list_item
					);
					if ( $index == 0 ) {
						$default = $item_id;
					}
				}
				$settings_data = $data;
				unset($settings_data["list"]);
				$settings_id = $this->db->insert(
					"settings",
					$settings_data
				);
			}
			$this->db->insert(
				"vendor_specific_settings",
				array(
					"settings_id"   =>  $settings_id,
					"vendor_id"     =>  1,
					"value"         =>  $default,
					"updated_at"    =>  $insert_time,
					"created_at"    =>  $insert_time
				)
			);
		}
	}

	/**
	 * Add records for table vendor_step_configuration
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	private function insertVendorStepConfiguration() {
		$insert_time = date("Y-m-d H:i:s");
		$table_data = array(
			array(
				"vendor_id"     =>  "1",
				"steps"         =>  "service,time,details,payment,done",
				"show_cart"     =>  "Disabled",
				"template"      =>  "default",
				"created_at"    =>  $insert_time
			),
			array(
				"vendor_id"     =>  "1",
				"steps"         =>  "service,time,cart,details,payment,done",
				"show_cart"     =>  "Disabled",
				"template"      =>  "default",
				"created_at"    =>  $insert_time
			)
		);
		foreach ( $table_data as $data ) {
			$this->db->insert(
				"vendor_step_configuration",
				$data
			);
		}
	}

	/**
	 * Add records for table tax
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	private function insertTax(){
		$insert_time = date("Y-m-d H:i:s");
		$table_data = array(
			array(
				"tax_percent"   =>  "19",
				"is_active"     =>  "1",
				"description"   =>  "USA VAT",
				"created_at"    =>  $insert_time
			),
			array(
				"tax_percent"   =>  "0",
				"is_active"     =>  "0",
				"description"   =>  "No tax",
				"created_at"    =>  $insert_time
			)
		);
		foreach ( $table_data as $data ) {
			$this->db->insert(
				"tax",
				$data
			);
		}
	}

}
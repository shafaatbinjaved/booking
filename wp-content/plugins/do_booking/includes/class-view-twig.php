<?php

abstract class ViewTwig {

	private $view_location;
	private $views = array();
	private $view_name;

	function __construct($view_location) {
		$this->view_location = $view_location;
	}

	public function get_view($view_name, $in_script_tag = false) {
		$this->view_name = $view_name;
		$html = '';
		$full_view = $this->view_location.$view_name.'.php';
		if ( isset($this->views[$view_name]) ) {
			$html = $this->views[$view_name];
		}
		else if ( file_exists( $full_view ) ) {

		}
	}

}
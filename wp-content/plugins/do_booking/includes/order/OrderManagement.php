<?php
/**
 * User: shafaatbinjaved
 * Date: 11/24/2019
 * Time: 10:43 PM
 */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';

class OrderManagement {

    /**
     * @var array $orderItems
     */
    private $orderItems = [];

    private $userData = [];

    private $numberOfOrderItems = 0;

    private $db;

    public function __construct() {
        $this->db = RBDoBooking_DB::instance();
    }

    public function setUserData($userData) {
        $this->userData = $userData;
    }

    public function setNumberOfOderItems($numberOfOrderItems) {
        $this->numberOfOrderItems = $numberOfOrderItems;
    }

    public function addOrderItems($userData, $numberOfItems, $customerId, $vendorId) {
        $this->setUserData($userData);
        $this->setNumberOfOderItems($numberOfItems);
        $creationTime = date('Y-m-d H:i:s');
        $totalWithoutTax = 0.0;
        $totalOnlyTax = 0.0;
        $orderItems = [];

        //$this->db->startTransaction();
        for ($iteration = 0; $iteration <= $this->numberOfOrderItems; $iteration++) {
            //adding time to booked time slots
            $idTimeSlot = $this->addTimeToBookedTimeSlots(
                $iteration,
                $customerId,
                $creationTime,
                $vendorId
            );

            //add other data for booked time slot
            $idOtherSlotData = $this->addOtherDataForBookedTimeSlot(
                $idTimeSlot,
                $creationTime
            );

            $includeTax = $this->db->getVendorSpecificSettingValueByName('include_tax', $vendorId) === 'Enabled' ? true : false;
            $tax = null;
            $orderItems[] = $this->addOrderItem(
                $iteration,
                $idTimeSlot,
                $includeTax,
                $creationTime
            );

            $totalWithoutTax += $orderItems[$iteration]['price_without_tax'];
            $totalOnlyTax += $orderItems[$iteration]['item_tax'];
            unset($orderItems[$iteration]['item_tax']);
        }
        $totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds = apply_filters(
            'rbdobooking_deduct_order_coupons',
            [
                'total_without_tax'  => $totalWithoutTax,
                'total_only_tax'  => $totalOnlyTax
            ],
            $this->userData['payment']['coupons']
        );
        $totalWithoutTax = $totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds['total_without_tax'];
        $totalOnlyTax = $totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds['total_only_tax'];

        $order = array(
            'payment_method_id' =>  $this->userData['payment']['payment_method'],
            'customer_id'       =>  $customerId,
            'total_without_tax' =>  round($totalWithoutTax, 2),
            'total_with_tax'    =>  round(($totalWithoutTax + $totalOnlyTax), 2),
            'paid_status'       =>  0,
            'created_at'        =>  $creationTime
        );
        $orderId = $this->db->insert('orders', $order);
        foreach ( $orderItems as $index => $orderItem ) {
            $orderItems[$index]['order_id'] = $orderId;
            $this->db->insert('order_items', $orderItems[$index]);
        }
        apply_filters(
            'rbdobooking_store_order_coupons',
            $totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds,
            $orderId,
            $creationTime
        );
        //$this->db->commitTransaction();
    }

    public function processPayment() {

    }

    private function addOrderItem($iteration, $idTimeSlot, $includeTax, $creationTime) {
        $tax = null;
        $itemTax = 0.0;
        $orderItem = [];

        $serviceNameMemberPriceName = $this->db->getServiceMemberPriceNameAndServiceName(
            $this->userData['time'][$iteration]['selected_time_member_id'],
            $this->userData['service'][$iteration]['service_id']
        );
        if ( $serviceNameMemberPriceName['member_price'] == '0' ) {
            $serviceNameMemberPriceName['member_price'] = $serviceNameMemberPriceName['service_price'];
        }
        $priceOrderItem = (float)$serviceNameMemberPriceName['member_price'];

        if ($includeTax) {
            $tax = $this->db->getServiceTaxFromServiceID( $this->userData["service"][$iteration]["service_id"] );
            $orderItem["tax_id"] = $tax["tax_id"];
            $itemTax = (((float)$tax["tax_percent"])/100) * $priceOrderItem;
            $orderItem["price_with_tax"] = $itemTax + $priceOrderItem;
            $orderItem["price_without_tax"] = $priceOrderItem;
        }
        else {
            $orderItem["tax_id"] = 0;
            $orderItem["price_with_tax"] = round($priceOrderItem, 2);
            $orderItem["price_without_tax"] = round($priceOrderItem, 2);
        }
        $orderItem['item_tax'] = $itemTax;
        $orderItem["product_id"] = 0;
        $orderItem["quantity"] = 1;
        $orderItem["id_time_slot"] = $idTimeSlot;
        $orderItem["created_at"] = $creationTime;

        return $orderItem;
    }

    private function addTimeToBookedTimeSlots($index, $customerId, $creationTime, $vendorId) {
        if ($customerId > 0 && $vendorId > 0) {
            $start = date("Y-m-d H:i:s",strtotime($this->userData["time"][$index]["start"]));
            $end = date("Y-m-d H:i:s",strtotime($this->userData["time"][$index]["end"]));
            $timing_id = $this->userData["time"][$index]["selected_time_timing_id"];
            $member_id = $this->userData["time"][$index]["selected_time_member_id"];
            $service_id = $this->userData["service"][$index]["service_id"];

            $bookedSlot = array(
                "service_id"    =>  $service_id,
                "member_id"     =>  $member_id,
                "vendor_id"     =>  $vendorId,
                "customer_id"   =>  $customerId,
                "timing_id"     =>  $timing_id,
                "date_start"    =>  $start,
                "date_end"      =>  $end,
                "is_booked"     =>  '1',
                "updated_at"    =>  $creationTime,
                "created_at"    =>  $creationTime
            );

            $bookedIdTimeSlot = $this->db->insert(
                "booked_time_slots",
                $bookedSlot
            );

            return $bookedIdTimeSlot;
        }
        return -1;
    }

    private function addOtherDataForBookedTimeSlot($idTimeSlot, $creationTime) {
        $otherData = array();

        foreach ( $this->userData["details"] as $index => $row ) {
            if ( $index != "vendor_id" && $index != "details-email" &&
                 $index != "details-name" && $index != "phone-name" ) {
                $otherData[$index] = $row;
            }
        }

        $slotOtherData = array(
            "id_time_slot"  =>  $idTimeSlot,
            "data"          =>  serialize($otherData),
            "updated_at"    =>  $creationTime,
            "created_at"    =>  $creationTime
        );

        $idSlotOtherData = $this->db->insert(
            "booked_time_slots_other_data",
            $slotOtherData
        );

        return $idSlotOtherData;
    }

    //public function getOrderItems

}
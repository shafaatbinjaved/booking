<?php
/**
 * User: shafaatbinjaved
 * Date: 1/26/2020
 * Time: 8:35 PM
 */
class Paypal {

    private $sandBoxAccount = 'shafaat-124@yahoo.com';
    private $clientID = 'AUfXjhj2AhMBVtuKrTSArmasseSbij1R7yq2u8GLLJg6Pv8qu7cw0EaOueZmh1NTf5xajAvC5e8lc_h3';
    private $clientSecret = 'EE9i0q_BYs7WZBdviqJ0a3kiZKckS5JbNQZgr7wcxRtPo4SysFqMHKKrJM2r4fGMf80D_gL6LsGo3DB5';

    private $endpoints = [
        'oauth-token' => 'https://api.sandbox.paypal.com/v1/oauth2/token',
    ];

    public function __construct() {
    }

    public function get_access_token() {
        $ch = curl_init();

        $url = $this->endpoints['oauth-token'];
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            ''
        ]));

    }

    public function validate($paymentID, $paymentToken, $payerID, $productID){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoints['oauth-token']);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->clientID.":".$this->clientSecret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        $response = curl_exec($ch);
        curl_close($ch);

        if(empty($response)){
            return false;
        }else{
            $jsonData = json_decode($response);
            $curl = curl_init($this->paypalURL.'payments/payment/'.$paymentID);
            curl_setopt($curl, CURLOPT_POST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $jsonData->access_token,
                'Accept: application/json',
                'Content-Type: application/xml'
            ));
            $response = curl_exec($curl);
            curl_close($curl);

            // Transaction data
            $result = json_decode($response);

            return $result;
        }

    }

}
new Paypal();
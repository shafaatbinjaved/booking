<?php

/**
 * Customers backend class
 * @since 1.0.0
 **/

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

class RBDoBooking_Customers extends View {

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	/*
	 * Primary class constructor
	 * @since 1.0.0
	 * */
	public function __construct() {

		parent::__construct(plugin_dir_path( __FILE__ ) . 'tpl/customers/');

		//Load Services page
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_customers',array($this,'customers_ajax_handler'));

		$this->db = RBDoBooking_DB::instance();
	}

	/**
	 * Ajax handler for all requests originating from customers page
	 *
	 * @since 1.0.0
	 */
	public function customers_ajax_handler() {

		check_ajax_referer('rbdobooking-customers-nonce','security');

		if ( $_POST["type"] == "get_customer_data" ) {
			$this->get_customer_data( $_POST["data"] );
		}
		else if ( $_POST["type"] == "update_customer_data" ) {
			$this->update_customer_data( $_POST["data"] );
		}

	}

	/**
	 * Update customer data
	 *
	 * @since 1.0.0
	 */
	private function update_customer_data( $data ) {

		$vendor_id = 1;
		$customer_id = intval($data["customer_id"]);
		$customer_arr = null;

		if ( $customer_id == 0 ) {
			$customer_arr = array(
				"full_name"     =>  $data["full_name"],
				"email"         =>  $data["email"],
				"phone"         =>  $data["phone"],
				"dob"           =>  $data["dob"],
				"street"        =>  $data["street"],
				"extra_address" =>  $data["extra_address"],
				"zip"           =>  $data["zip"],
				"state"         =>  $data["state"],
				"city"          =>  $data["city"],
				"country"       =>  $data["country"],
				"created_at"    =>  date("Y-m-d H:i:s")
			);
			$update_result = $this->db->insert(
				"customers",
				$customer_arr
			);
			$customer_id = $update_result;
			$vendor_customer_arr = array(
				'vendor_id'     =>  $vendor_id,
				'customer_id'   =>  $customer_id
			);
			$this->db->insert(
				'vendor_customers',
				$vendor_customer_arr
			);
			$customer_arr["type"] = "new";
		}
		else {
			$customer_arr = array(
				"full_name" =>  $data["full_name"],
				"email"     =>  $data["email"],
				"phone"     =>  $data["phone"],
				"dob"           =>  $data["dob"],
				"street"        =>  $data["street"],
				"extra_address" =>  $data["extra_address"],
				"zip"           =>  $data["zip"],
				"state"         =>  $data["state"],
				"city"          =>  $data["city"],
				"country"       =>  $data["country"]
			);
			$update_result = $this->db->update(
				"customers",
				$customer_arr,
				array(
					"customer_id" =>  $customer_id
				)
			);
			$customer_id = $update_result;
			$customer_arr["type"] = "update";
		}

		$customer_arr["customer_id"] = $customer_id;
		$success_msg = __('Customer updated successfully','rbdobooking');
		$error_msg = __('Not able to update customer','rbdobooking');

		if ( $update_result ) {
			wp_send_json_success(
				array(
					'notification'  => array(
						'type'  =>  'success',
						'title' =>  __('Success!','rbdobooking'),
						'text'  =>  $success_msg
					),
					'customer'   =>  $customer_arr
				)
			);
		}
		else {
			wp_send_json_error(
				array(
					'notification' => array(
						'type'  =>  'error',
						'title' =>  __('Failed!','rbdobooking'),
						'text'  =>  $error_msg
					)
				)
			);
		}

	}

	/**
	 * Get customer data for admin editing on customers page
	 *
	 * @since 1.0.0
	 */
	private function get_customer_data( $customer_id ) {

		$customer = $this->db->customer->getCustomerFromCustomerID( $customer_id );
		$error_msg = '';

		if ( !empty($customer) ) {
			wp_send_json_success( $customer );
		}
		else {
			$error_msg = __("Wrong customer id provided","rbdobooking");
		}
		wp_send_json_error( $error_msg );
	}

	/*
	 * Determine if the user is viewing the customers page
	 * @since 1.0.0
	 * */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the customers page.
		if ( 'rbdobooking-customers' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_customers_init' );
		}
	}

	/**
	 * Enqueue assets for the customers page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
		// Customers admin script.
		wp_enqueue_script(
			'rbdobooking-bootstrap-select-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/js/bootstrap-select.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-select-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/css/bootstrap-select.css"
		);
		wp_enqueue_script(
			'rbdobooking-customers',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/customers.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
		do_action( 'rbdobooking_customers_enqueue' );
	}

	/**
	 * Build the output for plugin customers page.
	 *
	 * @since 1.0.0
	*/
	public function output() {

		$customers = $this->db->customer->getAllCustomersForVendor();

		$customer_rows = '';

		foreach ( $customers as $index => $customer ) {
			$customer_rows .= $this->get_view(
				"customers.row",
				false,
				array(
					"number"            =>  $index+1,
					"customer_id"       =>  $customer["customer_id"],
					"full_name"         =>  $customer["full_name"],
					"phone"             =>  $customer["phone"],
					"email"             =>  $customer["email"],
					"notes"             =>  "",
					"last_appointment"  =>  $customer["last_appointment"],
					"total_appointments"=>  $customer["total_appointments"]
				)
			);
		}

		$countries = array(
			'pakistan'      =>  'Pakistan',
			'germany'       =>  'Germany',
			'india'         =>  'India',
			'saudia_arabia' =>  'Saudia Arabia',
			'mexico'        =>  'Mexico',
			'france'        =>  'France'
		);

		$data = array(
			'customer_rows'         =>  $customer_rows,
			'customers_slide_panel' => $this->get_view(
				"customers_ui_for_slide_panel",
				false,
				array('countries'=>$countries)
			)
		);

		echo $this->get_view("output",false,$data);
	}

}
new RBDoBooking_Customers();
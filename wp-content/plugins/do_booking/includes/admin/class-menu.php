<?php

/**
 * Register menu elements and do other global tasks.
 *
 * @since      1.0.0
 * @license    GPL-2.0+
 */
class RBDoBooking_Admin_Menu {
	/**
	 * Primary class constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// Let's make some menus.
		add_action( 'admin_menu', array( $this, 'register_menus' ), 9 );

		// Plugins page settings link.
		add_filter( 'plugin_action_links_' . plugin_basename( RBDOBOOKING_PLUGIN_DIR . 'do_booking.php' ), array( $this, 'settings_link' ) );
	}

	/**
	 * Register our menus.
	 *
	 * @since 1.0.0
	 */
	public function register_menus() {
		$menu_cap = rbdobooking_get_capability_manage_options();

		// Default do_booking top level menu item
		add_menu_page(
			esc_html__('RB Do Boooking','rbdobooking'),
			esc_html__('RB Do Boooking','rbdobooking'),
			$menu_cap,
			'rbdobooking-calendar',
			array($this, 'admin_page'),
			'data:image/svg+xml;base64,' . base64_encode( '<svg width="1792" height="1792" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path fill="#9ea3a8" d="M643 911v128h-252v-128h252zm0-255v127h-252v-127h252zm758 511v128h-341v-128h341zm0-256v128h-672v-128h672zm0-255v127h-672v-127h672zm135 860v-1240q0-8-6-14t-14-6h-32l-378 256-210-171-210 171-378-256h-32q-8 0-14 6t-6 14v1240q0 8 6 14t14 6h1240q8 0 14-6t6-14zm-855-1110l185-150h-406zm430 0l221-150h-406zm553-130v1240q0 62-43 105t-105 43h-1240q-62 0-105-43t-43-105v-1240q0-62 43-105t105-43h1240q62 0 105 43t43 105z"/></svg>' ),
			apply_filters( 'rbdobooking_menu_position', '57.7' )
		);

		// Calendar sub menu item.
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Calendar', 'rbdobooking' ),
			esc_html__( 'Calendar', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-calendar',
			array( $this, 'admin_page' )
		);

		// Appointments sub menu item.
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Appointments', 'rbdobooking' ),
			esc_html__( 'Appointments', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-appointments',
			array( $this, 'admin_page' )
		);

		// Staff members sub menu item
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Staff Members', 'rbdobooking' ),
			esc_html__( 'Staff Members', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-staff-members',
			array( $this, 'admin_page' )
		);

		// Services sub menu item
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Services', 'rbdobooking' ),
			esc_html__( 'Services', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-services',
			array( $this, 'admin_page' )
		);

		// Customers sub menu item
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Customers', 'rbdobooking' ),
			esc_html__( 'Customers', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-customers',
			array( $this, 'admin_page' )
		);

		// Email Notifications sub menu item
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Email Notifications', 'rbdobooking' ),
			esc_html__( 'Email Notifications', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-email-notifications',
			array( $this, 'admin_page' )
		);

		// Sms Notifications sub menu item
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Sms Notifications', 'rbdobooking' ),
			esc_html__( 'Sms Notifications', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-sms-notifications',
			array( $this, 'admin_page' )
		);

		// Payments sub menu item
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Orders', 'rbdobooking' ),
			esc_html__( 'Orders', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-orders',
			array( $this, 'admin_page' )
		);

		// Appearance sub menu item
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Appearance', 'rbdobooking' ),
			esc_html__( 'Appearance', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-appearance',
			array( $this, 'admin_page' )
		);

		// Settings sub menu item
		add_submenu_page(
			'rbdobooking-calendar',
			esc_html__( 'RB Do Boooking - Settings', 'rbdobooking' ),
			esc_html__( 'Settings', 'rbdobooking' ),
			$menu_cap,
			'rbdobooking-settings',
			array( $this, 'admin_page' )
		);
	}

	/**
	 * Wrapper for the hook to render our custom settings pages.
	 *
	 * @since 1.0.0
	 */
	public function admin_page() {
		do_action('rbdobooking_admin_page');
	}

	/**
	 * Add settings link to the Plugin page
	 *
	 * @since 1.0.0
	 *
	 * @param array $links
	 *
	 * @return array $links
	 **/
	public function settings_link( $links ) {

		$admin_link = add_query_arg(
			array(
				'page'  =>  'do_booking-settings',
			),
			admin_url( 'admin.php' )
		);

		$settings_link = sprintf(
			'<a href="%s">%s</a>',
			$admin_link,
			esc_html('Settings','RBDoBooking')
		);

		array_unshift( $links, $settings_link );

		return $links;

	}
}

new RBDoBooking_Admin_Menu();
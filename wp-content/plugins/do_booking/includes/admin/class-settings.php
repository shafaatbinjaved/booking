<?php
/**
 * User: shafaatbinjaved
 * Date: 4/1/2018
 * Time: 7:04 PM
 */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

/**
 * Register menu elements and do other global tasks.
 *
 * @package    RB Do Booking
 * @author     RB Do Booking
 * @since      1.0.0
 */
class RBDoBooking_Settings extends View {

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	/*
	 * The current active tab.
	 *
	 * @since 1.0.0
	 * @var string
	 * */
	public $view;

	/*
	 * All setting tabs.
	 *
	 * @since 1.0.0
	 * @var array
	 * */
	private $all_tabs;

	/*
	 * Primary class constructor
	 *
	 * @since 1.0.0
	 * */
	public function __construct() {
		parent::__construct(plugin_dir_path( __FILE__ ) . 'tpl/settings/');

		add_action('admin_init', array( $this, 'init'));
		add_action('wp_ajax_rbdobooking_settings',array($this,'settings_ajax_handler'));
		$this->db = RBDoBooking_DB::instance();
	}

	private function perform_cache_hook_actions( $name ) {
	    if (  $name == "time_start" || $name == "time_stop" ) {

        }
    }

	public function settings_ajax_handler() {

		check_ajax_referer('rbdobooking-settings-nonce','security');

		$type = $_POST["type"];

		$vendor_id = 1;

		if ( $type == "save_settings" ) {
			$settings_val = $_POST["settings_val"];
			$this->db->startTransaction();
			$update_result = true;
		    foreach ( $settings_val as $index => $setting ) {

		        $id = null;
		        $name = $setting["name"];
		        $temp = explode("rbdobooking-setting-",$setting["id"]);
		        if ( isset($temp[1]) ) {
			        $id = $temp[1];
			        $update = array(
			            "vendor_id" =>  $vendor_id,
			            "value"     =>  $setting["value"]
		            );
		            $where = array(
			            "vendor_settings_id" => $id
		            );
			        $result = $this->db->update(
				        "vendor_specific_settings",
				        $update,
				        $where
			        );
			        if ( $result === false ) {
			            $update_result = false;
                    }
                    else {
			            do_action("rbdobooking_cache_refresh",$vendor_id,$name);
                    }
                }
            }
            $return_data = array();
            if ( $update_result ) {
	            $this->db->commitTransaction();
	            $msg = __("Settings save successfully","rbdobooking");
	            $return_data["notification"] = array(
	                'type'  =>  'success',
                    'title' =>  __('Success','rbdobooking'),
                    'text'  =>  $msg
                );
	            wp_send_json_success( $return_data );
            }
            else {
		        $this->db->rollbackTransaction();
	            $msg = __("Not able to save settings.","rbdobooking");
	            $return_data["notfication"] = array(
		            'type'  =>  'error',
		            'title' =>  __('Failed','rbdobooking'),
		            'text'  =>  $msg
	            );
	            wp_send_json_error( $return_data );
            }
        }

    }

	/**
	 * Determine if the user is viewing the settings page, if so, party on.
	 *
	 * @since 1.0.0
	 */
	public function init() {

		// Check what page we are on
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the settings page.
		if ( $page === 'rbdobooking-settings' ) {

			// Include API callbacks and functions.
			require_once RBDOBOOKING_PLUGIN_DIR . 'includes/admin/settings-api.php';

			// Watch for triggered save.
			$this->save_settings();

			// Determine the current active settings tab.
			$this->view = isset( $_GET['view'] ) ? esc_html( $_GET['view'] ) : 'general';

			add_action('admin_enqueue_scripts',array($this,'enqueues'));
			//add_action('rbdobooking_admin_settings_after', array( $this, 'captcha_addon_notice'));
			add_action('rbdobooking_admin_page', array( $this, 'output'));

			// Hook for addons.
			do_action( 'rbdobooking_settings_init' );

		}
	}

	/**
	 * Return array containing markup for all the appropriate settings fields.
	 *
	 * @since 1.0.0
	 * @param string $view
	 * @return array
	 * */
	public function get_settings_fields( $view = '' ) {

		$fields = array();
		$settings = $this->get_registered_settings( $view );

		foreach ( $settings as $id => $args ) {

			$fields[ $id ] = rbdobooking_settings_output_field( $args );

		}

		return apply_filters( 'rbdobooking_settings_fields', $fields, $view );
	}

	/**
	 * @since 1.0.0
	 * @param string $view
	 * @return array
	*/
	public function get_registered_settings( $view = '' ) {
	    $disabled_enabled_arr = array(
		    'disabled'  =>  esc_html__('Disabled','rbdobooking'),
		    'enabled'   =>  esc_html__('Enabled','rbdobooking'),
	    );
	    $pending_approved = array(
		    'pending'   =>  esc_html__('Pending','rbdobooking'),
		    'approved'  =>  esc_html__('Approved','rbdobooking')
        );
	    $min_time = array(
		    'disabled'  =>  'Disabled',
		    '1'         =>  '1 h',
		    '2'         =>  '2 h',
		    '3'         =>  '3 h',
		    '4'         =>  '4 h',
		    '5'         =>  '5 h',
		    '6'         =>  '6 h',
		    '7'         =>  '7 h',
		    '8'         =>  '8 h',
		    '9'         =>  '9 h',
		    '10'        =>  '10 h',
		    '11'        =>  '11 h',
		    '12'        =>  '12 h',
		    '24'        =>  '24 h'
	    );
		$defaults = array(
			//General Settings tab.
			'general'   =>  array(
				'license-heading' => array(
					'id'        => 'license-heading',
					'content'   => '<h4>' . esc_html__('License','rbdobooking') . '</h4><p>' . esc_html__('Your license key provides access to updates and addons.','rbdobooking') . '</p>',
					'type'      => 'content',
					'no_label'  => true,
					'class'     => array( 'section-heading' )
				),
				'license-key'        => array(
					'id'   => 'license-key',
					'name' => esc_html__( 'License Key', 'wpforms' ),
					'type' => 'license',
				),
				'general-heading' => array(
					'id'       => 'general-heading',
					'content'  => '<h4>' . esc_html__( 'General', 'wpforms' ) . '</h4>',
					'type'     => 'content',
					'no_label' => true,
					'class'    => array( 'section-heading', 'no-desc' ),
				),
				'disable-css'     => array(
					'id'        => 'disable-css',
					'name'      => esc_html__( 'Include Form Styling', 'wpforms' ),
					'desc'      => esc_html__( 'Determines which CSS files to load for the site.', 'wpforms' ),
					'type'      => 'select',
					'choicesjs' => true,
					'default'   => 1,
					'options'   => array(
						1 => esc_html__( 'Base and form theme styling', 'wpforms' ),
						2 => esc_html__( 'Base styling only', 'wpforms' ),
						3 => esc_html__( 'No styling', 'wpforms' ),
					),
				),
				'global-assets'   => array(
					'id'   => 'global-assets',
					'name' => esc_html__( 'Load Assets Globally', 'wpforms' ),
					'desc' => esc_html__( 'Check this if you would like to load WPForms assets site-wide. Only check if your site is having compatibility issues or instructed to by support.', 'wpforms' ),
					'type' => 'checkbox',
				)
			),
			'general2'  =>  array(
				'time-slot-length'  =>  array(
					'id'    =>  'time-slot-length',
					'name'  =>  esc_html__('Time slot length','rbodobooking'),
					'desc'  =>  esc_html__('','rbdobooking'),
					'type'  =>  'select',
					'options'   =>  array(
						'5_min' =>  esc_html__('5 min','rbdobooking'),
						'10_min' =>  esc_html__('10 min','rbdobooking'),
						'15_min' =>  esc_html__('15 min','rbdobooking'),
						'20_min' =>  esc_html__('20 min','rbdobooking'),
						'25_min' =>  esc_html__('25 min','rbdobooking'),
					)
				),
                'service-slot-length'   =>  array(
                    'id'        =>  'service-slot-length',
                    'name'      =>  esc_html__('Service duration as slot length'),
                    'desc'      =>  esc_html__('Time slot for building all time slots in the system'),
                    'type'      =>  'radio',
                    'default'   =>  'disabled',
                    'options'   =>  $disabled_enabled_arr
                ),
				'default-appointment-status'   =>  array(
					'id'        =>  'default-appointment-status',
					'name'      =>  esc_html__('Default appointment status','rbdobooking'),
					'desc'      =>  esc_html__('Select status for newly booked appointments.','rbdobooking'),
					'type'      =>  'radio',
					'default'   =>  'approved',
					'options'   =>  $pending_approved
				),
				'min-time-prior-booking'   =>  array(
					'id'        =>  'min-time-prior-booking',
					'name'      =>  esc_html__('Default appointment status','rbdobooking'),
					'desc'      =>  esc_html__('Select status for newly booked appointments.','rbdobooking'),
					'type'      =>  'select',
					'default'   =>  'disabled',
					'options'   =>  $min_time
				),
				'min-time-prior-cancelling'   =>  array(
					'id'        =>  'min-time-prior-cancelling',
					'name'      =>  esc_html__('Minimum time requirement prior to canceling','rbdobooking'),
					'desc'      =>  esc_html__('Set how late appointments can be canceled (for example, require customers to cancel at least 1 hour before the appointment time).','rbdobooking'),
					'type'      =>  'select',
					'default'   =>  'disabled',
					'options'   =>  $min_time
				),
				'approve-appointment-url'   =>  array(
					'id'        =>  'approve-appointment-url',
					'name'      =>  esc_html__('Approve appointment URL','rbdobooking'),
					'desc'      =>  esc_html__('Set the URL of a page that is shown to staff after they approve their appointment.','rbdobooking'),
					'type'      =>  'text'
				),
				'cancel-appointment-url-success'   =>  array(
					'id'        =>  'cancel-appointment-url-success',
					'name'      =>  esc_html__('Cancel appointment URL (success)','rbdobooking'),
					'desc'      =>  esc_html__('Set the URL of a page that is shown to clients after they successfully cancelled their appointment.','rbdobooking'),
					'type'      =>  'text'
				),
				'cancel-appointment-url-denied'   =>  array(
					'id'        =>  'cancel-appointment-url-denied',
					'name'      =>  esc_html__('Cancel appointment URL (denied)','rbdobooking'),
					'desc'      =>  esc_html__('Set the URL of a page that is shown to clients when the cancellation of appointment is not available anymore.','rbdobooking'),
					'type'      =>  'text'
				),
				'days-available-for-booking'   =>  array(
					'id'        =>  'days-available-for-booking',
					'name'      =>  esc_html__('Number of days available for booking','rbdobooking'),
					'desc'      =>  esc_html__('Set how far in the future the clients can book appointments.','rbdobooking'),
					'type'      =>  'text',
                    'default'   =>  '365'
				),
				'client-time-zone'   =>  array(
					'id'        =>  'client-time-zone',
					'name'      =>  esc_html__('Display available time slots in client\'s time zone','rbdobooking'),
					'desc'      =>  esc_html__('The value is taken from client’s browser.','rbdobooking'),
					'type'      =>  'select',
					'default'   =>  'disabled',
					'options'   =>  $disabled_enabled_arr
				),
				'final-step-url'   =>  array(
					'id'        =>  'final-step-url',
					'name'      =>  esc_html__('Final step URL','rbdobooking'),
					'desc'      =>  esc_html__('Set the URL of a page that the user will be forwarded to after successful booking. If disabled then the default Done step is displayed.','rbdobooking'),
					'type'      =>  'select',
					'default'   =>  'disabled',
					'options'   =>  $disabled_enabled_arr
				),
				'staff-members-edit-profile'   =>  array(
					'id'        =>  'staff-members-edit-profile',
					'name'      =>  esc_html__('Allow staff members to edit their profiles','rbdobooking'),
					'desc'      =>  esc_html__('If this option is enabled then all staff members who are associated with WordPress users will be able to edit their own profiles, services, schedule and days off.','rbdobooking'),
					'type'      =>  'select',
					'default'   =>  'disabled',
					'options'   =>  $disabled_enabled_arr
				)
			),
			// Email settings tab.
			'email'        => array(
				'email-heading'          => array(
					'id'       => 'email-heading',
					'content'  => '<h4>' . esc_html__( 'Email', 'wpforms' ) . '</h4>',
					'type'     => 'content',
					'no_label' => true,
					'class'    => array( 'section-heading', 'no-desc' ),
				),
				'email-template'         => array(
					'id'      => 'email-template',
					'name'    => esc_html__( 'Template', 'wpforms' ),
					'desc'    => esc_html__( 'Determines how email notifications will be formatted. HTML Templates are the default.', 'wpforms' ),
					'type'    => 'radio',
					'default' => 'default',
					'options' => array(
						'default' => esc_html__( 'HTML Template', 'wpforms' ),
						'none'    => esc_html__( 'Plain text', 'wpforms' ),
					),
				),
				'email-header-image'     => array(
					'id'   => 'email-header-image',
					'name' => esc_html__( 'Header Image', 'wpforms' ),
					'desc' => wp_kses( __( 'Upload or choose a logo to be displayed at the top of email notifications.<br>Recommended size is 300x100 or smaller for best support on all devices.', 'wpforms' ), array( 'br' => array() ) ),
					'type' => 'image',
				),
				'email-background-color' => array(
					'id'      => 'email-background-color',
					'name'    => esc_html__( 'Background Color', 'wpforms' ),
					'desc'    => esc_html__( 'Customize the background color of the HTML email template.', 'wpforms' ),
					'type'    => 'color',
					'default' => '#e9eaec',
				),
				'email-carbon-copy'      => array(
					'id'   => 'email-carbon-copy',
					'name' => esc_html__( 'Carbon Copy', 'wpforms' ),
					'desc' => esc_html__( 'Check this if you would like to enable the ability to CC: email addresses in the form notification settings.', 'wpforms' ),
					'type' => 'checkbox',
				),
			),
		);

		$defaults = apply_filters( 'rbdobooking_settings_defaults', $defaults );

		return empty( $view ) ? $defaults : $defaults[$view];
	}

	/**
	 * Return registered settings tabs.
	 * @since 1.0.0
	 * @return array
	*/
	public function get_tabs() {

		$this->all_tabs = array(
			'general'           =>  array(
				'name'  =>  esc_html__('General','rbdobooking'),
				'form'  =>  true,
				'submit'  =>  esc_html__('Save Settings','rbdobooking'),
			),
			'company'           =>  array(
				'name'  =>  esc_html__('Company','rbdobooking'),
				'form'  =>  true,
				'submit'  =>  esc_html__('Save Settings','rbdobooking'),
			),
			'customers'         =>  array(
				'name'  =>  esc_html__('Customers','rbdobooking'),
				'form'  =>  true,
				'submit'  =>  esc_html__('Save Settings','rbdobooking'),
			),
			'google_calendar'   =>  array(
				'name'  =>  esc_html__('Google calendar','rbdobooking'),
				'form'  =>  true,
				'submit'  =>  esc_html__('Save Settings','rbdobooking'),
			),
			'woocommerce'      =>  array(
				'name'  =>  esc_html__('Woocommerce','rbdobooking'),
				'form'  =>  true,
				'submit'  =>  esc_html__('Save Settings','rbdobooking'),
			),
			'cart'              =>  array(
				'name'  =>  esc_html__('Cart','rbdobooking'),
				'form'  =>  true,
				'submit'  =>  esc_html__('Save Settings','rbdobooking'),
			),
			'payments'          =>  array(
				'name'  =>  esc_html__('Payments','rbdobooking'),
				'form'  =>  true,
				'submit'  =>  esc_html__('Save Settings','rbdobooking'),
			),
			'business_hours'    =>  array(
				'name'  =>  esc_html__('Business hours','rbdobooking'),
				'form'  =>  true,
				'submit'  =>  esc_html__('Save Settings','rbdobooking'),
			),
			/*'holidays'          =>  array(
				'name'  =>  esc_html__('Holidays','rbdobooking'),
				'form'  =>  true,
				'submit'  =>  esc_html__('Save Settings','rbdobooking'),
			),*/
			'purchase_code'     =>  array(
			        'name'  =>  esc_html__('Purchase code','rbdobooking'),
			        'form'  =>  true,
			        'submit'  =>  esc_html__('Save Settings','rbdobooking'),
            )
        );

		return apply_filters( 'rbdobooking_settings_tabs', $this->all_tabs );
	}

	/*
	 * Sanitize and save settings
	 *
	 * @since 1.0.0
	 * */
	public function save_settings() {

		// Check nonce and other various security checks.
		if ( ! isset( $_POST['rbdobooking-settings-submit'] ) ) {
			return;
		}

		if ( ! wp_verify_nonce( $_POST['nonce'], 'rbdobooking-settings-nonce' ) ) {
			return;
		}

		if ( ! rbdobooking_current_user_can() ) {
			return;
		}

		if ( empty($_POST['view']) ) {
			return;
		}

		// Get registered fields and current settings
		$fields = $this->get_registered_settings( $_POST['view'] );
		$settings = get_option( 'rbdobooking_settings', array() );

		if ( empty( $fields ) || !is_array( $fields ) ) {
			return;
		}

		// Sanitize and prep each field
		foreach ( $fields as $id => $field ) {

			// Certain field types are not valid for saving and are skipped.
			$exclude = apply_filters( 'rbdobooking_settings_exclude_type', array('content','license', 'providers') );

			if ( empty( $field['type'] ) || in_array( $field['type'], $exclude, true ) ) {
				continue;
			}

			$value = isset( $_POST[ $id ] ) ? trim( $_POST[ $id ] ) : false;
			$value_prev = isset( $settings[ $id ] ) ? $settings[ $id ] : false;

			// Custom filter can be provided for sanitizing, otherwise use
			// defaults.
			if ( ! empty( $field['filter'] ) && function_exists( $field['filter'] ) ) {

				$value = call_user_func( $field['filter'], $value, $id, $field, $value_prev );

			} else {

				switch ( $field['type'] ) {
					case 'checkbox':
						$value = (bool) $value;
						break;
					case 'image':
						$value = esc_url_raw( $value );
						break;
					case 'color':
						$value = wpforms_sanitize_hex_color( $value );
						break;
					case 'text':
					case 'radio':
					case 'select':
					default:
						$value = sanitize_text_field( $value );
						break;
				}
			}

			// Add to settings.
			$settings[ $id ] = $value;
		}

		// Save settings.
		update_option( 'rbdobooking_settings', $settings );

		WPForms_Admin_Notice::success( esc_html__( 'Settings were successfully saved.', 'rbdobooking' ) );
	}

	/*
	 * Enqueue assets for the settings page.
	 *
	 * @since 1.0.0
	 * */
	public function enqueues() {
		wp_enqueue_script(
			'rbdobooking-settings',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/settings.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
		do_action( 'rbdobooking_settings_enqueue' );
	}

	/**
	 *  Output tab navigation area.
	 * @since 1.0.0
	*/
	public function tabs() {

		$tabs = $this->get_tabs();
		return $this->get_view("tabs",
            false,
            array("tabs"=>$tabs)
        );
	}
	
	/**
	 * Build the output for the plugin settings page.
	 * 
	 * @since 1.0.0
	*/
	public function output() {

	    $vendor_id = 1;

	    $view_type = null;

	    if ( isset($_GET["view"]) ) {
		    $view_type = $_GET["view"];
        }
        else {
	        $view_type = "general";
        }

	    $fields = $this->db->getVendorSettings($view_type,$vendor_id);

	    $fields_html = $this->get_view(
	            "fields.row",
                false,
                array("fields"=>$fields)
        );

	    echo $this->get_view(
	            "output",
                false,
                array(
                    "tabs"      =>  $this->tabs(),
                    "fields"    =>  $fields_html,
                    "view_type" =>  $this->all_tabs[$view_type]["name"]
                )
        );
	    return;
		$tabs = $this->get_tabs();
		$fields = $this->get_settings_fields( $this->view );
		?>
		<div id="rbdobooking-settings" class="wrap rbdobooking-admin-wrap">
			
			<?php echo $this->tabs(); ?>
			
			<h1 class="rbdobooking-h1-placeholder">Settings</h1>
			
			<?php
			if ( rbdobooking()->pro && class_exists('RBDoBooking_License') ) {
				//RBDoBooking()->license->notices( true );
			}
			?>
			
			<div class="rbdobooking-admin-content rbdobooking-admin-settings">
				
				<?php
				// Some tabs rely on AJAX and do not contain a form, such as Integrations
				if ( !empty($tabs[$this->view]['form']) ) {
				?>
					<form method="post" class="rbdobooing-admin-settings-form">
						<input type="hidden" name="action" value="update-settings">
						<input type="hidden" name="view" value="<?php echo esc_attr( $this->view ); ?>">
						<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('rbdobooking-settings-nonce'); ?>">
				<?php
				}

				do_action('rbdobooking_admin_settings_before',$this->view, $fields );

				foreach ( $fields as $field ) {
					echo $field;
				}

				if ( !empty( $tabs[$this->view]['submit'] ) ) {
					?>
					<p class="submit">
						<button class="rbdobooking-btn rbdobooking-btn-md rbdobooing-btn-orange"
							name="rbdobooking-settings-submit"
							type="submit"><?php echo $tabs[$this->view]['submit']; ?></button>
					</p>
					<?php
				}

				do_action('rbdobooking_admin_settings_before',$this->view, $fields);
				if ( ! empty( $tabs[ $this->view ]['form'] ) ) {
					?>
					</form>
					<?php
				}
				?>
				
			</div>
		</div>
		<?php
	}
}

new RBDoBooking_Settings();
<?php
/**
 * User: shafaatbinjaved
 * Date: 1/5/2020
 * Time: 1:40 AM
 */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

class Google_Calendar {

    /**
     * @var View $view
     */
    private $view;

    /**
     * @var string $tplPath
     */
    private $tplPath;

    /**
     * Holds instance of plugin db class
     *
     * @since 1.0.0
     *
     * @var RBDoBooking_DB
     */
    private $db;

    private $client_id = '';
    private $client_secret = '';
    private $redirect_uri = '';

    private $endpoints = [
        'auth'                  => 'https://accounts.google.com/o/oauth2/v2/auth',
        'exchange_auth_code'    => 'https://oauth2.googleapis.com/token'
    ];

    function __construct() {

        $this->tplPath = RBDOBOOKING_PLUGIN_DIR . 'includes/admin/tpl/calendar/';
        $this->view = new View($this->tplPath);

        $this->db = RBDoBooking_DB::instance();

        $this->redirect_uri = $this->db->getVendorSpecificSettingValueByName(
            'redirect_uri',
            1,
            'google_calendar'
        );

        $this->set_client_id_and_secret();

        add_action('rbdobooking_staff_members_init', array($this, 'staff_members_init'));

        add_action('rbdobooking_attach_or_connect_google_calendar_ui', array($this, 'attach_or_connect_google_calendar_ui'));
    }

    function get_calendar_events($member_id) {
        $ch = curl_init();

        $accessToken = 'ya29.Il-4B6aBSMDhTnGMM1p533cTYeOI5VYHACzqzh-x5jp9NMYBO5YQSGbUZijaYEHYfH6HnUJJKiqjon6P3JmDrGKcwtUbMEfyZgYXr0T-yAVKWbE-_AsWPPj0-SH0oxedeg';

        $header = array();
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer ' . $accessToken;

        curl_setopt($ch, CURLOPT_URL,"https://www.googleapis.com/calendar/v3/freeBusy");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);

        $d = array(
            'timeMin'   => '2020-01-06T09:00:00+01:00',
            'timeMax'   => '2020-01-06T23:30:00+01:00',
            //'timeZone'  => 'UTC+1',
            'items'     => [
                [
                    'id' => 'shafaatbinjaved@gmail.com',"a"=>1
                ]
            ]
        );

        // In real life you should use something like:
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($d));

        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);
    }

    function staff_members_init($query_params) {
        if (isset($query_params['code'])) {
            $state = explode('.', $query_params['state']);
            $member_id = explode('member_id_', $state[0]);
            $vendor_id = explode('vendor_id_', $state[1]);

            $result = $this->perform_exchange_auth_code(
                $query_params['code'],
                $query_params['state']
            );

            $created_at = date('Y-m-d H:i:s');
            $updated_at = $created_at;

            $data = [
                'type'                  => 'google',
                'authorization_code'    => $query_params['code'],
                'member_id'             => $member_id[1],
                'vendor_id'             => $vendor_id[1],
                'updated_at'            => $updated_at,

            ];

            if (isset($result['access_token'])) {
                $data['access_token'] = $result['access_token'];
            }
            if (isset($result['refresh_token'])) {
                $data['refresh_token'] = $result['refresh_token'];
            }
            if (isset($result['expires_in'])) {
                $time = time() + (int)$result['expires_in'];
                $data['expires_at'] = date('Y-m-d H:i:s', $time);
            }

            if (isset($result['id_token'])) {
                $google_response = json_decode(
                    base64_decode(
                        str_replace('_', '/', str_replace('-','+',explode('.', $result['id_token'])[1]))
                    ),
                    true
                );
                if (isset($google_response['email'])) {
                    $data['provider_email'] = $google_response['email'];
                }
            }

            if ($this->db->isProviderMemberOAuthExists($member_id[1], 'google')) {
                $data['updated_at'] = $updated_at;

                $this->db->update(
                    'provider_oauth',
                    $data,
                    ['member_id' => $member_id[1]]
                );
            }
            else {
                $data['created_at'] = $created_at;
                $this->db->insert(
                    'provider_oauth',
                    $data
                );
            }
        }
    }

    private function perform_exchange_auth_code($code, $state) {

        $output = $this->perform_curl(
            $this->endpoints['exchange_auth_code'],
            [],
            'POST',
            [
                'code'          => $code,
                'state'         => $state,
                'client_id'     => $this->client_id,
                'client_secret' => $this->client_secret,
                'redirect_uri'  => $this->redirect_uri,
                'grant_type'    => 'authorization_code'
            ]
        );

        return json_decode($output, true);
    }

    private function get_perform_auth_url() {
        $scopes = [
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/calendar',
            'https://www.googleapis.com/auth/calendar.events',
            'https://www.googleapis.com/auth/calendar.events.readonly',
            'https://www.googleapis.com/auth/calendar.readonly',
            'https://www.googleapis.com/auth/calendar.settings.readonly'
        ];

        //TODO: get member id and vendor id of logged in user
        $parameters = [
            'client_id'     => $this->client_id,
            'redirect_uri'  => $this->redirect_uri,
            'scope'         => implode(' ', $scopes),
            'access_type'   => 'offline',
            'state'         => 'member_id_1.vendor_id_1',
            'response_type' => 'code',
        ];

        $query_params = http_build_query($parameters);

        $url = $this->endpoints['auth'];
        $url .= '?' . $query_params;

        return $url;
    }

    function perform_curl($url, $header = [], $method, $post_data = []) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        if (count($header) > 0) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }

        if ($method === 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
        }

        if (count($post_data) > 0) {
            // In real life you should use something like:
            //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
        }

        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);

        return $server_output;
    }

    /*function connect_calendar_google() {
        $output = $this->perform_auth();
        wp_send_json_success($output);
    }*/

    function set_client_id_and_secret() {
        $vendor_id = 1;
        $this->client_id = $this->db->getVendorSpecificSettingValueByName(
            'client_id',
            $vendor_id,
            'google_calendar'
        );
        $this->client_secret = $this->db->getVendorSpecificSettingValueByName(
            'client_secret',
            $vendor_id,
            'google_calendar'
        );
    }

    function attach_or_connect_google_calendar_ui() {
        if ($this->client_id !== '' && $this->client_secret !== '') {

            //..............
            /*$scopes = [
                'https://www.googleapis.com/auth/calendar',
                'https://www.googleapis.com/auth/calendar.events',
                'https://www.googleapis.com/auth/calendar.events.readonly',
                'https://www.googleapis.com/auth/calendar.readonly',
                'https://www.googleapis.com/auth/calendar.settings.readonly'
            ];

            //TODO: get member id and vendor id from ui
            $parameters = [
                'client_id'     => $this->client_id,
                'redirect_uri'  => 'http://localhost/do_booking/wp-admin/admin.php?page=rbdobooking-staff-members',
                'scope'         => implode(' ', $scopes),
                'access_type'   => 'offline',
                'response_type' => 'code',

            ];

            $query_params = http_build_query($parameters);

            $url = $this->endpoints['auth'];
            $url .= '?' . $query_params;*/
            $url = $this->get_perform_auth_url();
            //..............

            echo $this->view->get_view(
                'calendar_settings_connect_btn',
                false,
                [
                    'type'          => 'google',
                    'connect_link'  => $url
                ]
            );
        }
        else {
            echo $this->view->get_view(
                'calendar_settings_not_present',
                false,
                [
                    'type' => 'google'
                ]
            );
        }
    }

}
new Google_Calendar();
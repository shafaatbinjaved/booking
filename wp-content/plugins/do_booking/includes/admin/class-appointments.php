<?php

/*
 * Appointments class.
 * @since 1.0.0
 * */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

class RBDobooking_Appointments extends View {

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	/*
	 * Primary class constructor
	 * @since 1.0.0
	 * */
	public function __construct() {

		parent::__construct(plugin_dir_path( __FILE__ ) . 'tpl/appointments/');

		//Load Appointemnts page
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_appointments',array($this,'appointments_ajax_handler'));

		$this->db = RBDoBooking_DB::instance();
	}

	/**
	 * Ajax handler for all requests originating from appointments page
	 *
	 * @since 1.0.0
	 */
	public function appointments_ajax_handler() {

		check_ajax_referer('rbdobooking-appointments-nonce','security');

		$type = $_POST["type"];

		switch ($type) {
			case "get_dataTable_data":
				$draw = $_POST["draw"];
				$vendor_id = $_POST["vendor_id"];

				$member_ids = $_POST["member_ids"];
				$service_ids = $_POST["service_ids"];
				$customer_ids = $_POST["customer_ids"];
				$status = $_POST["status"];

				$booked_slots = null;
				$start = $_POST["appointment_time_start"] . " 00:00:00";//date('Y-m-d H:i:s');//
				$end = $_POST["appointment_time_end"] . " 00:00:00";
				$start_formatted = date("Y-m-d H:i:s",strtotime($start));
				$end_formatted = date("Y-m-d H:i:s",strtotime($end));

				$creation_time_start = $_POST["creation_time_start"] . " 00:00:00";
				$creation_time_end = $_POST["creation_time_end"] . " 00:00:00";
				$creation_time_start_formatted = date("Y-m-d H:i:s",strtotime($creation_time_start));
				$creation_time_end_formatted = date("Y-m-d H:i:s",strtotime($creation_time_end));

				$limit = $_POST["length"];
				$offset = $_POST["start"];

				$search = $_POST["search"]["value"];

				$result = $this->db->getBookedTimeSlots(
					$vendor_id,
					$start_formatted,
					$end_formatted,
					$member_ids,
					$service_ids,
					$customer_ids,
					$status,
					$creation_time_start_formatted,
					$creation_time_end_formatted,
					$limit,
					$offset,
					$search
				);
				$booked_slots = $result["result"];
				foreach ( $booked_slots as $index => $booked_slot ) {
					$booked_slots[$index]["formatted_date_start"] = date("d.m.Y g:i a",strtotime($booked_slot["date_start"]));
				}
				$return_data = array(
					"draw"          =>  $draw,
					"data"          =>  $booked_slots,
					"recordsTotal"  =>  $result["found_rows"],
					"recordsFiltered"  =>  $result["found_rows"]
				);
				die(json_encode($return_data));
				//wp_send_json_success( $booked_slots );
				break;
			case "get_new_appointment_form_data":
				$vendor_id = 1;
				$staff_members = $this->db->getVendorAllStaffMembersOnly( $vendor_id );
				$services = $this->db->getAllVendorServices( $vendor_id );
				$return_data = array(
					"staff_members" =>  $staff_members,
					"services"      =>  $services
				);
				wp_send_json_success( $return_data );
				break;
			case "get_existing_appointment_and_form_data":
				$vendor_id = 1;
				$id_time_slot = $_POST["id_time_slot"];
				$staff_members = $this->db->getVendorAllStaffMembersOnly( $vendor_id );
				$services = $this->db->getAllVendorServices( $vendor_id );
				$book_slot = $this->db->getBookedTimeSlotBySlotID( $id_time_slot );
				/*echo '<pre>';
				var_dump( $services );
				die();
				echo '</pre>';*/
				$return_data = array();
				if ( $book_slot != null ) {
					$return_data["staff_members"] = $staff_members;
					$return_data["services"] = $services;
					$return_data["book_slot"] = $book_slot;
					wp_send_json_success( $return_data );
				}
				else {
					$msg = __("No book slot is found.","rbdobooking");
					$return_data["notfication"] = array(
						'type'  =>  'error',
						'title' =>  __('Failed','rbdobooking'),
						'text'  =>  $msg
					);
					wp_send_json_error( $return_data );
				}
				break;
			case "download_invoice":
				break;
		}
	}

	/*
	 * Determine if the user is viewing the appointments page
	 * @since 1.0.0
	 * */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the services page.
		if ( 'rbdobooking-appointments' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_appointments_init' );
		}
	}

	/**
	 * Enqueue assets for the appointments page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
		// Appointments admin script.
		wp_enqueue_script(
			'rbdobooking-dataTables-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/datatables/datatables.min.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-dataTables-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/datatables/datatables.min.css"
		);
		/*wp_enqueue_script(
			'rbdobooking-bootstrap-datepicker-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-datepicker-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"
		);*/
		wp_enqueue_script(
			'rbdobooking-moment-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/daterangepicker/moment.min.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script(
			'rbdobooking-bootstrap-daterangepicker-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/daterangepicker/daterangepicker.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-daterangepicker-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/daterangepicker/daterangepicker.css"
		);
		wp_enqueue_script(
			'rbdobooking-bootstrap-select-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/js/bootstrap-select.js",
			array( 'jquery','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-select-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/css/bootstrap-select.css"
		);
		wp_enqueue_script(
			'rbdobooking-appointments',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/appointments.js",
			array( 'jquery','rbdobooking-dataTables-js' ),
			RBDOBOOKING_VERSION,
			true
		);
		do_action( 'rbdobooking_appointments_enqueue' );
	}

	/**
	 * Build the output for the plugin appointments page.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		$vendor_id = 1;

		$appointments_slidepanel = $this->get_view("appointments_ui_for_slide_panel");

		$advanced_filter_options = $this->get_view(
			"advanced_filter_options",
			false,
			array(
				'staff_members' => $this->db->getVendorAllStaffMembersOnly( $vendor_id ),
				'services'      => $this->db->getAllVendorServices( $vendor_id ),
				'customers'     =>  $this->db->customer->getAllCustomersForVendor($vendor_id)
			)
		);

		echo $this->get_view(
			"output",
			false,
			array(
				'advanced_filter_options'   =>  $advanced_filter_options,
				'appointments_slidepanel'   =>  $appointments_slidepanel,
				'appointments_del_modal'    =>  ""
			)
		);

	}

}
new RBDobooking_Appointments();
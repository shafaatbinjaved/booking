<?php

$translations = array(
	'add_new_service'   =>  __('Add New Service','rbdobooking'),
	'save'              =>  __('Save','rbdbooking'),
	'close'             =>  __('Close','rbdbooking'),
	'delete'            =>  __('Delete','rbdbooking'),
	'title'             =>  __('Title','rbdobooking'),
	'visibility'        =>  __('Visibility','rbdobooking'),
	'price'             =>  __('Price','rbdobooking'),
	'min_capacity'      =>  __('Min Capacity','rbdobooking'),
	'max_capacity'      =>  __('Max Capacity','rbdobooking'),
	'duration'          =>  __('Duration','rbdobooking'),
	'padding'           =>  __('Padding','rbdobooking'),
	'category'          =>  __('category','rbdobooking'),
	'member'            =>  __('Member','rbdobooking'),
	'info'              =>  __('Info','rbdobooking')
);
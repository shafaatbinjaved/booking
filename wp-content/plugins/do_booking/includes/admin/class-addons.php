<?php

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';

class RBDoBooking_Addons {

    /**
     * @var View $view
     */
    private $view;

    /**
     * @var string $tplPath
     */
    private $tplPath;

    /**
     * Holds instance of plugin db class
     *
     * @since 1.0.0
     *
     * @var RBDoBooking_DB
     */
    private $db;

    public function __construct() {

        $this->tplPath = plugin_dir_path( __FILE__ ) . 'tpl/addons/';
        $this->view = new View($this->tplPath);

        $this->db = RBDoBooking_DB::instance();

        add_action( 'admin_menu', array( $this, 'register_menus' ), 9 );

        //Load addons page
        add_action('admin_init', array($this,'init'));

        add_action('wp_ajax_rbdobooking_addons',array($this,'addons_ajax_handler'));

    }

    public function addons_ajax_handler() {
        check_ajax_referer('rbdobooking-addons-nonce','security');

        $type = $_POST['type'];
        $name = $_POST['data'];

        switch ($type) {
            case 'activate':
                //TODO : also check from db
                $result = false;
                $pluginFile  = $name.'/'.$name.'.php';
                if ($this->db->isAddonExists($name)) {
                    $result = activate_plugin($pluginFile);
                }
                if ($result === null) {
                    $msg = __('Addon successfully activated.','rbdobooking');
                    wp_send_json_success(
                        array(
                            'notification' => array(
                                'type'  =>  'success',
                                'title' =>  __('Success!','rbdobooking'),
                                'text'  =>  $msg
                            )
                        )
                    );
                }
                else {
                    $msg = __('Unable to activate addon. Please see logs.','rbdobooking');
                    wp_send_json_error(
                        array(
                            'notification' => array(
                                'type'  =>  'error',
                                'title' =>  __('Failed!','rbdobooking'),
                                'text'  =>  $msg
                            )
                        )
                    );
                }
                break;
            case 'deactivate':
                $pluginFile = $name.'/'.$name.'.php';
                $result = deactivate_plugins($pluginFile);//activate_plugin($pluginFile);
                $msg = __('Addon successfully deactivated.','rbdobooking');
                wp_send_json_success(
                    array(
                        'notification' => array(
                            'type'  =>  'success',
                            'title' =>  __('Success!','rbdobooking'),
                            'text'  =>  $msg
                        )
                    )
                );
                break;
            case 'download':
                break;
        }
    }

    /**
     * Register menu for addons page
     * @since 1.0.0
     */
    public function register_menus() {
        $menu_cap = rbdobooking_get_capability_manage_options();

        // Custom Fields sub menu item
        add_submenu_page(
            'rbdobooking-calendar',
            esc_html__( 'RB Do Boooking - Addons', 'rbdobooking' ),
            esc_html__( 'Addons', 'rbdobooking' ),
            $menu_cap,
            'rbdobooking-addons',
            array( $this, 'admin_page' )
        );
    }

    /**
     * Wrapper for the hook to render our custom settings pages.
     *
     * @since 1.0.0
     */
    public function admin_page() {
        do_action('rbdobooking_admin_page');
    }

    /*
	 * Determine if the user is viewing the addons page
	 * @since 1.0.0
	 * */
    public function init() {

        // Check what page we are on.
        $page = isset( $_GET['page'] ) ? $_GET['page'] : '';

        // Only load if we are actually on the customers page.
        if ( 'rbdobooking-addons' === $page ) {

            add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
            add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

            // Hook for addons.
            do_action( 'rbdobooking_coupons_init' );
        }
    }

    /**
     * Include files
     *
     * @since 1.0
     */
    private function includes() {
        //Admin dashboard only includes
        if (is_admin()) {
            require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';
        }
    }

    /**
     * Enqueue assets for the coupons page.
     *
     * @since 1.0.0
     */
    public function enqueues() {
        // Addons admin script.
        wp_enqueue_script(
            'rbdobooking-addons',
            RBDOBOOKING_PLUGIN_URL . "assets/js/admin/addons.js",
            array( 'jquery' ),
            RBDOBOOKING_VERSION,
            true
        );

        do_action( 'rbdobooking_addons_enqueue' );
    }

    /**
     * Build the output for plugin customers page.
     *
     * @since 1.0.0
     */
    public function output() {
        $vendor_id = 1;

        $addons = $this->db->getAddons();

        echo $this->view->get_view(
            'output',
            false,
            [
                'addonsList' => $addons
            ]
        );
    }

}
new RBDoBooking_Addons();
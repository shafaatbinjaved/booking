<?php

/**
 * Staff members class
 * @since 1.0.0
 */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

class RBDoBooking_StaffMembers extends View {

    public $time_options = null;

	/**
	 * Primary class constructor
	 * @since 1.0.0
	*/
	public function __construct() {

	    parent::__construct(plugin_dir_path( __FILE__ ) . 'tpl/staff-members/');

		//Load Staff Members page
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_staff_members',array($this,'staff_members_ajax_handler'));

		$this->db = RBDoBooking_DB::instance();
	}

	/**
     * Ajax handler for all ajax requests originating from staff members page
     *
     * @since 1.0.0
     *
	*/
	public function staff_members_ajax_handler() {
		check_ajax_referer('rbdobooking-staff-members-nonce','security');

		if ( $_POST["type"] == "get_member_data" ) {
			$this->get_staff_member();
		}
		else if ( $_POST["type"] == "save_member_holiday" ) {
		    $this->save_member_holiday();
        }
        else if ( $_POST["type"] == "save_member_data" ) {
		    $this->save_member_data();
        }
        else if ( $_POST["type"] == "delete_staff_members" ) {
			$this->delete_staff_members();
        }
    }

    /**
     * Delete staff member
     *
     * @since 1.0.0
    */
    private function delete_staff_members() {
		$data = $_POST["data"];
		$success = false;
		$return_data = array();
		$msg = '';

		if ( $data ) {
			$member_ids = explode(",",$data);

			foreach ( $member_ids as $member_id ) {
				$this->db->deleteStaffMembers( $member_id );
				$success = true;
			}

			$msg = __('Staff member deleted successfully.','rbdobooking');
		}
		else {
			$msg = __('Data is not correct.','rbdobooking');
		}

		if ( $success ) {
			$return_data = array(
				'notification'  =>  array(
					'type'  =>  'success',
					'title' =>  __('Success','rbdobooking'),
					'text'  =>  $msg
				)
			);
			wp_send_json_success($return_data);
		}
		else {
			wp_send_json_error($return_data);
		}
    }

    /**
     * Save member data
     *
     * @since 1.0.0
    */
    function save_member_data() {
        $data = $_POST["data"];
        $member_id = $data["member_id"];
        $success = false;
        $msg = '';

        if ( $data ) {
            if ( $member_id != "-1" ) {
                $where = array(
                    "member_id" =>  $member_id
                );
                $this->db->update(
                     "staff_members",
                     $data["details"],
                    $where
                );
                foreach ( $data["services"] as $index => $service ) {
                    $member_service_id = $service["member_service_id"];
                    unset($service["member_service_id"]);
                    $service["member_id"] = $member_id;
                    if ( $member_service_id != "-1" ) {
                        $this->db->update(
                                "staff_member_services",
                                $service,
                                array(
                                    "member_service_id" =>  $member_service_id
                                )
                        );
                    }
                    else {
	                    $this->db->insert(
	                            "staff_member_services",
                                $service
                        );
                    }
                }
                foreach ( $data["schedule"] as $index => $schedule ) {
                    $timing_id = $schedule["timing_id"];
                    unset( $schedule["timing_id"] );
                    $schedule["member_id"] = $member_id;
                    $timing = array(
                        "member_id"     =>  $member_id,
                        "day"           =>  $schedule["day"],
                        "is_day_on"     =>  $schedule["is_day_on"],
                        "start_time"    =>  $schedule["start_time"],
                        "end_time"      =>  $schedule["end_time"]
                    );
                    $this->db->update(
                        "staff_member_timing",
                        $timing,
                        array("timing_id" => $timing_id)
                    );
                    if ( isset($schedule["all_breaks"]) ) {
                        $this->db->delete_member_breaks_for_day( $member_id, $schedule["day"] );
	                    foreach ( $schedule["all_breaks"] as $break_item_index => $break_item ) {
		                    unset( $break_item["break_id"] );
		                    $break_item["timing_id"] = $timing_id;
		                    $break_item["created_at"] = date("y-m-d H:i:s");
		                    $this->db->insert(
			                    "staff_member_breaks",
			                    $break_item
		                    );
                        }
                    }
                    else {
	                    $this->db->delete_member_breaks_for_day( $member_id, $schedule["day"] );
                    }
                }
                $success = true;
	            $msg = __('Member data successfully updated','rbdobooking');
            }
            else {
                $details_tab = $this->db->insert(
                        "staff_members",
                        $data["details"]
                );
	            if ( $details_tab != false ) {
		            $member_id = $details_tab;

		            foreach ( $data["schedule"] as $index => $schedule ) {
			            $schedule["member_id"] = $member_id;
			            $timing = array(
				            "member_id"     =>  $member_id,
				            "day"           =>  $schedule["day"],
				            "is_day_on"     =>  $schedule["is_day_on"],
				            "start_time"    =>  $schedule["start_time"],
				            "end_time"      =>  $schedule["end_time"],
				            "created_at"    =>  date("Y-m-d H:i:s")
			            );
			            $timing_id = $this->db->insert(
				            "staff_member_timing",
				            $timing
			            );
			            if ( isset($schedule["all_breaks"]) ) {
				            foreach ( $schedule["all_breaks"] as $break_item_index => $break_item ) {
					            unset( $break_item["break_id"] );
					            $break_item["timing_id"] = $timing_id;
					            $break_item["created_at"] = date("y-m-d H:i:s");
					            $this->db->insert(
						            "staff_member_breaks",
						            $break_item
					            );
				            }
			            }
		            }

		            $success = true;
		            $msg = __('New staff member successfully created','rbdobooking');
	            }
	            else {
		            $msg = __('Failed to create new staff member','rbdobooking');
	            }
            }
        }
	    if ( $success ) {
        	$member_data = $this->db->getStaffMember(
        		$member_id,
		        'a.member_id, a.profile_pic, a.visibility, a.first_name, a.last_name, GROUP_CONCAT(c.name) as service_name'
	        );
        	$member_html = $this->get_single_staff_member_html( $member_data[0] );
		    wp_send_json_success(
			    array(
				    'notification'  =>  array(
					    'type'  =>  'success',
					    'title' =>  __('Success!','rbdobooking'),
					    'text'  =>  $msg
				    ),
				    'member_id'     =>  $member_id,
				    'member_html'   =>  $member_html
			    )
		    );
	    }
	    else {
		    wp_send_json_error(
			    array(
				    'notification'  =>  array(
					    'type'  =>  'success',
					    'title' =>  __('Success!','rbdobooking'),
					    'text'  =>  $msg
				    )
			    )
		    );
	    }
    }

    /**
     * Save member holiday
     *
     * @since 1.0.0
    */
    function save_member_holiday() {
        $member_id = $_POST["member_id"];
        $date = $_POST["date"];
        $is_holiday = $_POST["is_holiday"];
        $repeat_every_year = $_POST["repeat_every_year"];
        $msg = '';
        $success = false;
        $member_holidays = null;

        if ( $is_holiday == "false" ) {
            $result = $this->db->deleteSpecificMemberHoliday($member_id, $date);

            if ( $result ) {
                $msg = __('Date successfully removed from days off','rbdobooking');
                $success = true;
            }
        }
        else if ( $is_holiday == "true" ) {
            if ( $repeat_every_year == "true" ) {
	            $repeat_every_year = 1;
            }
            else if ( $repeat_every_year == "false" ) {
                $repeat_every_year = 0;
            }
            $holiday_id = $this->db->isMemberHolidayExists( $member_id, $date );

            if ( $holiday_id == false ) {
	            $result = $this->db->saveMemberHoliday($member_id, $date, $repeat_every_year);

	            if ( $result ) {
		            $msg = __('Date successfully added for days off','rbdobooking');
		            $success = true;
	            }
            }
            else {
                $data = array(
	                "member_id"         =>  $member_id,
	                "date"              =>  $date,
	                "repeat_every_year" =>  $repeat_every_year
                );
                $where_holiday_id = array(
                    "holidays_id" => $holiday_id
                );

                $result = $this->db->updateMemberHoliday( $data, $where_holiday_id );

                if ( $result ) {
	                $msg = __('Date successfully updated for days off','rbdobooking');
                    $success = true;
                }
                else {
	                $msg = __('Date successfully updated for days off','rbdobooking');
	                $success = true;
                }
            }
        }

        if ( $success ) {
            $member_holidays = $this->db->getMemberHolidays( $member_id );
	        wp_send_json_success(
		        array(
		            'member_holidays' => $member_holidays,
			        'notification' => array(
				        'type'  =>  'success',
				        'title' =>  __('Success!','rbdobooking'),
				        'text'  =>  $msg
			        ),
		        )
	        );
        }
        else {
	        wp_send_json_error(
		        array(
			        'notification' => array(
				        'type'  =>  'error',
				        'title' =>  __('Error!','rbdobooking'),
				        'text'  =>  $msg
			        )
		        )
	        );
        }
    }

    /**
     * Insert default timings for member
     *
     * @since 1.0.0
     * @var int member_id
     * @return void
    */
    function insert_default_timings_for_member( $member_id ) {
        for ( $day=1; $day <= 7; $day++ ) {
	        $data_arr = array(
		        "member_id"     =>  $member_id,
		        "day"           =>  $day,
                "is_day_on"     =>  0,
                "start_time"    =>  "09:00:00",
                "end_time"      =>  "17:00:00",
                "created_at"    =>  date("Y-m-d H:i:s")
	        );
	        $this->db->insert("staff_member_timing",$data_arr);
        }
    }

    /**
     * Get staff member
     *
     * @since 1.0.0
    */
    function get_staff_member() {
        $member_id = $_POST["member_id"];
        $member_data = $this->db->getStaffMemberData( $member_id );

        if ( !is_null($member_data) ) {
	        $services = $this->db->getAllPossibleServicesWithCategoryNamesForMember($member_id,"*",true);

	        foreach ( $services as $index => $service ) {
		        if ( $services[$index]["member_price"] == "0" ) {
			        $services[$index]["member_price"] = $services[$index]["price"];
		        }
		        if ( $services[$index]["member_min_capacity"] == "0" ) {
			        $services[$index]["member_min_capacity"] = $services[$index]["min_capacity"];
		        }
		        if ( $services[$index]["member_max_capacity"] == "0" ) {
			        $services[$index]["member_max_capacity"] = $services[$index]["max_capacity"];
		        }
	        }

	        $timings = $this->db->getMemberTimingsWithBreaks( $member_id );
	        if ( is_null($timings) ) {
		        $this->insert_default_timings_for_member( $member_id );
		        $timings = $this->db->getMemberTimingsWithBreaks( $member_id );
            }
	        $holidays = $this->db->getMemberHolidays( $member_id );

	        $return_data = array(
                "member"    =>  $member_data[0],
                "services"  =>  $services,
                "timings"   =>  $timings,
                "holidays"  =>  $holidays
            );
            wp_send_json_success( $return_data );
        }
        else {
            wp_send_json_error(
	            array(
		            'notification' => array(
			            'type'  =>  'error',
			            'title' =>  __('Failed!','rbdobooking'),
			            'text'  =>  __('Failed to get member data','rbdobooking')
		            )
	            )
            );
        }
    }

    /**
     * Get all time options
     *
     * @since 1.0.0
    */
    public function get_all_time_options() {
        if ( is_null($this->time_options) ) {
	        $html = '';
	        for ( $i=0;$i<24;$i++ ) {
		        for ( $j=0; $j<4;$j++ ) {
			        $hours = sprintf("%02d", $i);
			        $minutes = sprintf("%02d", $j*15);
			        $time = $hours.':'.$minutes.':00';
			        $html .= '<option value="'.$time.'">';
			        $html .= date("g:i a",strtotime($time));
			        $html .= '</option>';
		        }
	        }
	        $this->time_options = $html;
        }
        return $this->time_options;
    }

    /*
     * Determine if the user is viewing the staff memebers page
     * @since 1.0.0
     */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the services page.
		if ( 'rbdobooking-staff-members' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_staff_members_init', $_GET);
		}
	}

	/**
	 * Enqueue assets for the staff members page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {

		wp_enqueue_media();

		// Services admin script.
		wp_enqueue_script(
			'rbdobooking-staff-members',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/staff_members.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script('jquery-ui-datepicker');

		wp_enqueue_style(
			'jquery-ui-theme',
			RBDOBOOKING_PLUGIN_URL .'assets/css/jquery-ui/jquery-ui.min.css',
			array(),
			'all'
		);

		do_action( 'rbdobooking_staff_members_enqueue' );
	}

	/**
	 * get the html of single staff member output
	 *
	 * @since 1.0.0
	*/
	public function get_single_staff_member_html( $member ) {
		$member_services = explode(",",$member["service_name"]);
		$member_services_html = '';
		foreach ( $member_services as $service ) {
			if ( !empty($service) ) {
				$member_services_html .= $this->get_view(
					"single_staff_member_services",
					false,
					array("service_name"=>$service)
				);
			}
		}
		$member["service_name"] = $member_services_html;

		if ( $member["profile_pic"] == "" ) {
			$member["profile_pic"] = $this->get_view(
				"profile_pic_default",
				false
			);
		}
		else {
			$member["profile_pic"] = $this->get_view(
				"profile_pic",
				false,
				array("profile_pic"=>$member["profile_pic"])
			);
		}

		$timing_with_breaks = $this->db->getSatffMemberTimingAndBreaks( $member["member_id"] );

		$timing_html = '';
		foreach ( $timing_with_breaks as  $timing_break ) {
			$day = strtotime( 'next Monday +' . $timing_break["day"] . ' days' );
			$d_short = date("D", $day);
			$d_full = date("l", $day);
			$end_time = date("g:i a",strtotime($timing_break["end_time"]));
			$start_time = date("g:i a",strtotime($timing_break["start_time"]));
			$time = $start_time . ' - ' . $end_time;
			if ( $timing_break["start_time"] == "00:00:00" && $timing_break["end_time"] == "00:00:00" ) {
				$time = __('Not available','rbdobooking');
			}

			$all_breaks = explode(",",$timing_break["break_id"]);
			$breaks_start = explode(",",$timing_break["break_start"]);
			$breaks_stop = explode(",",$timing_break["break_stop"]);

			$all_breaks_html = '';

			foreach ( $all_breaks as $index => $single_break ) {
				if ( $single_break != "" ) {
					$b_start = date("g:i a",strtotime($breaks_start[$index]));
					$b_stop  = date("g:i a",strtotime($breaks_stop[$index]));

					$all_breaks_html .= $this->get_view(
						'single_staff_member_break',
						false,
						array("break"=>$b_start . ' - ' . $b_stop)
					);
				}
			}
			if ( $all_breaks_html == "" ) {
				$all_breaks_html .= $this->get_view(
					'single_staff_member_break',
					false,
					array("break"=>__('Not available','rbdobooking'))
				);
			}

			$timing_html .= $this->get_view(
				"single_staff_member_timing",
				false,
				array(
					"day_short"     =>  $d_short,
					"day_full"      =>  $d_full,
					"time"          =>  $time,
					"all_breaks"    =>  $all_breaks_html
				)
			);
		}
		$member["schedule"] = $timing_html;

		return $this->get_view(
			'single_staff_member',
			false,
			$member
		);
	}

	/**
	 * Build the output for the plugin services page.
	 *
	 * @since 1.0.0
	 */
	public function output() {

	    $staff_members = $this->db->getAllStaffMembers(
	    	'a.member_id, a.profile_pic, a.visibility, a.first_name, a.last_name, GROUP_CONCAT(c.name) as service_name' );

	    $staff_members_html = '';
		foreach ( $staff_members as $member ) {
			$staff_members_html .= $this->get_single_staff_member_html( $member );
		}

		$slide_panel_data = array(
			'all_time_options'  =>  $this->get_all_time_options(),
			'year'              =>  date("Y")
	);

		$main_output_data = array(
		    "staff_members_html"                =>  $staff_members_html,
            "delete_modal"                      =>  $this->get_view("delete_modal",false),
            "staff_member_ui_for_slidePanel"    =>  $this->get_view("staff_member_ui_for_slidePanel",false,$slide_panel_data),
            "single_staff_member"               =>  $this->get_view("single_staff_member", true,array(),'tpl-staff-member-list-item'),
            "break_item"                        =>  $this->get_view("break_item", true,array(),'tpl-break-item'),
            "service_category"                  =>  $this->get_view("service_category", true,array(),'tpl-service-category'),
            "service_item"                      =>  $this->get_view("service_item", true,array(),'tpl-service-item'),
            "popup_days_off"                    =>  $this->get_view("popup_days_off", true,array(),'tpl-popup-days-off')
        );

		echo $this->get_view(
		        "output",
                false,
                $main_output_data
        );

	}

}
new RBDoBooking_StaffMembers();
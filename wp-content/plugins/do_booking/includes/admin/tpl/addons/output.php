<?php
/**
 * @var $addonsList
 */
?>
<div id="rbdobooking-addons" class="wrap rbdobooking-admin-wrap">

    <div class="container-fluid">

        <h1 class="rbdobooking-h1-placeholder"><?php _e('Addons','rbdobooking') ?></h1>

        <div class="rbdobooking-admin-content rbdobooking-admin-addons">

            <div class="row rbdobooking-addons-container">
                <div class="col-12">
                    <div class="header">
                        <div class="float-left">
                            <h4><?php _e('Addons', 'rbdobooking') ?></h4>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="row addon-rows pb-3">
                        <?php
                        $siteActivePlugins = get_option('active_plugins');
                        $active_plugins = [];
                        $available_plugins = [];
                        $allPlugins = get_plugins();
                        foreach ($allPlugins as $key => $plugin) {
                            if (in_array($key, $siteActivePlugins)) {
                                $active_plugins[] = $plugin['Name'];
                            }
                            $available_plugins[] = $plugin['Name'];
                        }
                        foreach ($addonsList as $index => $addon) {
                        ?>
                            <div class="col-4">
                                <div class="card text-left">
                                    <div class="card-body">
                                        <h5 class="card-title"><?php echo $addon['title'] ?></h5>
                                        <div class="card-text">
                                            <div class="row">
                                                <div class="col-8 text-left">
                                                    <?php echo $addon['description'] ?>
                                                </div>
                                                <div class="col-4 text-right">
                                                    <div class="price"><?php echo '$'.$addon['price'] ?></div>
                                                    <div class="sales"><?php echo 'Sales ' . $addon['sales'] ?></div>
                                                    <div class="reviews"><?php echo 'Reviews ' . $addon['reviews'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-muted text-center">
                                        <?php
                                        if (in_array($addon['title'], $active_plugins)) {
                                        ?>
                                            <button class="btn btn-theme-color btn-addon-action btn-primary"
                                                    type="button"
                                                    data-type="deactivate"
                                                    data-slug="<?php echo $addon['slug'] ?>"
                                                    data-deactivate="<?php _e('Activate','rbdobooking') ?>"
                                                    data-activate="<?php _e('Deactivate','rbdobooking') ?>"
                                            >
                                                <?php _e('Deactivate','rbdobooking') ?>
                                            </button>
                                        <?php
                                        }
                                        elseif (in_array($addon['title'], $available_plugins)) {
                                        ?>
                                            <button class="btn btn-theme-color btn-addon-action btn-primary"
                                                    type="button"
                                                    data-type="activate"
                                                    data-slug="<?php echo $addon['slug'] ?>"
                                                    data-activate="<?php _e('Deactivate','rbdobooking') ?>"
                                                    data-deactivate="<?php _e('Activate','rbdobooking') ?>"
                                            >
                                                <?php _e('Activate','rbdobooking') ?>
                                            </button>
                                        <?php
                                        }
                                        /*else {
                                            */?><!--<button class="btn btn-theme-color btn-addon-action btn-primary" type="button">Download</button>--><?php
/*                                        }*/
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
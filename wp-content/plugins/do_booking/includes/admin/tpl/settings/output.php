<div id="rbdobooking-settings" class="wrap rbdobooking-admin-wrap">

	<?php echo $tabs; ?>

	<h1 class="rbdobooking-h1-placeholder"><?php _e("Settings","rbdobooking"); ?></h1>

	<div class="rbdobooking-admin-content rbdobooking-admin-settings">

		<div class="row">
			<div class="col-12 all-settings-fields">

                <h3><?php echo $view_type ?></h3>

				<?php echo $fields; ?>

                <p class="submit">
                    <button class="btn btn-default btn-orange btn-submit"
                            name="rbdobooking-settings-submit"
                            type="button"
                    >
                        <i class="fas fa-spinner fa-spin icon-spin"></i>
                        <span><?php _e("Save Settings","rbdobooking") ?></span>
                    </button>
                </p>
			</div>
		</div>

        <div class="row">
            <div class="col-12">

            </div>
        </div>

	</div>

</div>
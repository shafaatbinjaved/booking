<ul class="rbdobooking-admin-tabs">
	<?php
	foreach ( $tabs as $id => $tab ) {
		$active = $id === $this->view ? 'active' : '';
		$name = esc_html( $tab['name'] );
		$link = esc_url_raw(
			add_query_arg(
				'view',
				$id,
				admin_url('admin.php?page=rbdobooking-settings')
			)
		);

		echo '<li><a href="'.$link.'" class="'.$active.'">'.$name.'</a></li>';
	}
	?>
</ul>
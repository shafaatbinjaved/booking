<?php

$all_settings = array(
	"cancel_appointment_action"    =>  array(
		"name"  =>  __("Cancel appointment action","rbdobooking"),
		"desc"  =>  __("Cancel appointment desc","rbdobooking")
	),
	"default_appointment_status"    =>  array(
		"name"  =>  __("Default appointment status","rbdobooking"),
		"desc"  =>  __("Default appointment status desc","rbdobooking")
	),
	"min_time_prior_booking" =>  array(
		"name"  =>  __("Minimum time prior to booking","rbdobooking"),
		"desc"  =>  __("Minimum time prior to book apoointment","rbdobooking")
	),
	"min_time_prior_cancel" =>  array(
		"name"  =>  __("Minimum time prior to cancel","rbdobooking"),
		"desc"  =>  __("Minimum time required prior to cancel apoointment","rbdobooking")
	),
	"allow_staff_members_to_edit"   =>  array(
		"name"  =>  __("Allow staff members to edit","rbdobooking"),
		"desc"  =>  __("this is some description","rbdobooking")
	),
	"make_birthday_mandatory"   =>  array(
		"name"  =>  __("Make birthday mandatory","rbdobooking"),
		"desc"  =>  __("Make birthday mandatory desc","rbdobooking")
	),
	"make_address_mandatory"    =>  array(
		"name"  =>  __("Make address mandatory","rbdobooking"),
		"desc"  =>  __("Make addres mandatory desc","rbdobooking")
	),
	"new_user_account_role" =>  array(
		"name"  =>  __("New user account role","rbdobooking"),
		"desc"  =>  __("New user account role desc","rbdobooking")
	),
	"create_wp_users"   =>  array(
		"name"  =>  __("Create WP user accounts","rbdobooking"),
		"desc"  =>  __("Create wordpress user accounts for customers desc","rbdobooking")
	),
	"cart_item_data"    =>  array(
		"name"  =>  __("Cart item data","rbdobooking"),
		"desc"  =>  __("Cart item data desc","rbdobooking")
	),
	"rbdobooking_product"   =>  array(
		"name"  =>  "RB Do Booking " . __("product","rbdobooking"),
		"desc"  =>  __("RB Do Booking desc","rbdobooking")
	),
	"woocommerce_enable"    =>  array(
		"name"  =>  __("Woocommerce","rbdobooking"),
		"desc"  =>  __("Enable or disabled woocommerce","rbdobooking")
	),
	"cart_enable"   =>  array(
		"name"  =>  __("cart","rbdobooking"),
		"desc"  =>  __("Option to enable several appointments at once.<br />Note: If woocommerce is enabled then it won't work.","rbdobooking")
	),
	"client_id"     =>  array(
		"name"  =>  __("Client ID","rbdobooking"),
		"desc"  =>  __("Client ID desc","rbdobooking")
	),
	"client_secret"     =>  array(
		"name"  =>  __("Client secret","rbdobooking"),
		"desc"  =>  __("Client secret desc","rbdobooking")
	),
	"redirect_uri"     =>  array(
		"name"  =>  __("Redirect URI","rbdobooking"),
		"desc"  =>  __("Redirect URI desc","rbdobooking")
	),
	"2_way_sync"     =>  array(
		"name"  =>  __("2 way sync","rbdobooking"),
		"desc"  =>  __("2 way sync desc","rbdobooking")
	),
	"limit_fetched_events"     =>  array(
		"name"  =>  __("Limit feteched events","rbdobooking"),
		"desc"  =>  __("Limit feteched events desc","rbdobooking")
	),
	"event_title"     =>  array(
		"name"  =>  __("Event title","rbdobooking"),
		"desc"  =>  __("Event title desc","rbdobooking")
	),
	"day_limit"     =>  array(
		"name"  =>  __("Day limit","rbdobooking"),
		"desc"  =>  __("Day limit desc","rbdobooking")
	),
	"enable_coupon_code"    =>  array(
		"name"  =>  __("Enable coupon code","rbdobooking"),
		"desc"  =>  __("Enable coupon code desc","rbdobooking")
	),
	"time_start"    =>  array(
		"name"  =>  __("Time start","rbdobooking"),
		"desc"  =>  __("Time start","rbdobooking")
	),
	"time_stop"     =>  array(
		"name"  =>  __("Time stop","rbdobooking"),
		"desc"  =>  __("Time stop desc","rbdobooking")
	),
	"company_name"          =>  array(
		"name"  =>  __("Company name","rbdobooking"),
		"desc"  =>  __("Company name desc","rbdobooking")
	),
	"company_address"       =>  array(
		"name"  =>  __("Address","rbdobooking"),
		"desc"  =>  __("Address description","rbdobooking")
	),
	"company_phone"         =>  array(
		"name"  =>  __("Phone","rbdobooking"),
		"desc"  =>  __("Phone desc","rbdobooking")
	),
	"company_website"       =>  array(
		"name"  =>  __("Website","rbdobooking"),
		"desc"  =>  __("Website desc","rbdobooking")
	),
	"purchase_code"          =>  array(
		"name"  =>  __("Purchase code","rbdobooking"),
		"desc"  =>  __("Purschase code desc","rbdobooking")
	),
	"currency"      =>  array(
		"name"  =>  __("Currency","rbdobooking"),
		"desc"  =>  __("Currency desc","rbdobooking")
	),
	"include_tax"   =>  array(
		"name"  =>  __("Include tax","rbdobooking"),
		"desc"  =>  __("Tax description","rbdobooking")
	)
);

$html = '';
foreach ( $fields as $field ) {
	$element = call_user_func(
		"field_".$field["view_type"],
		$field
	);
	$html .= field_general_html( $field, $all_settings[$field["name"]], $element, '');
}

function field_general_html ( $field, $setting, $element, $class = '' ) {
	$output = '<div class="rbdobooking-setting-row rbdobooking-clear ' . $class . '">';

	$output .= '<span class="rbdobooking-setting-label">';
	$output .= '<label for="rbdobooking-setting-' . $field['vendor_settings_id']  . '">';
	$output .=  $setting["name"];
	$output .= '</label>';
	$output .= '</span>';

	$output .= '<span class="rbdobooking-setting-field">';
	$output .= $element;
	$output .= '<p class="desc">';
	$output .= $setting["desc"];
	$output .= '</p>';
	$output .= '</span>';

	$output .= '</div>';

	return $output;
}

function field_select( $field ) {
	$html = '<select name="'.$field["name"].'" class="rbdobooking-setting rbdobooking-time" id="rbdobooking-setting-'.$field["vendor_settings_id"].'">';
	foreach ( $field["list_items"] as $row ) {
		$html .= '<option value="'.$row["item_id"].'"';
		if ( $field["value"] == $row["item_id"] ) {
			$html .= ' selected';
		}
		$html .= '>';
		$html .= $row["item"];
		$html .= '</option>';
	}
	$html .= '</select>';
	return $html;
}

function field_time( $field ) {
	$html = '<input name="'.$field["name"].'" type="text" value="'.$field["value"].'" class="rbdobooking-setting rbdobooking-time" id="rbdobooking-setting-'.$field["vendor_settings_id"].'" />';
	return $html;
}

function field_number( $field ) {
	$html = '<input name="'.$field["name"].'" type="number" value="'.$field["value"].'" class="rbdobooking-setting rbdobooking-text" id="rbdobooking-setting-'.$field["vendor_settings_id"].'" />';
	return $html;
}

function field_text( $field ) {
	$html = '<input name="'.$field["name"].'" type="text" value="'.$field["value"].'" class="rbdobooking-setting rbdobooking-text" id="rbdobooking-setting-'.$field["vendor_settings_id"].'" />';
	return $html;
}

function field_phone( $field ) {
	$html = '<input name="'.$field["name"].'" type="text" value="'.$field["value"].'" class="rbdobooking-setting rbdobooking-text" id="rbdobooking-setting-'.$field["vendor_settings_id"].'" />';
	return $html;
}

function field_textarea( $field ) {
	$html = '<textarea name="'.$field["name"].'" class="rbdobooking-setting rbdobooking-textarea" id="rbdobooking-setting-'.$field["vendor_settings_id"].'">';
	$html .= $field["value"];
	$html .= '</textarea>';
	return $html;
}

echo $html;
?>
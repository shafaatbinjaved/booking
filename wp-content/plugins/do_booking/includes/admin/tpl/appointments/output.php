<div id="rbdobooking-appointments" class="wrap rbdobooking-admin-wrap">

	<div class="container-fluid">

		<h1 class="rbdobooking-h1-placeholder"><?php _e('Appointments','rbdobooking') ?></h1>

		<div class="rbdobooking-admin-content rbdobooking-admin-appointments">

			<div class="row rbdobooking-appointments-container">
				<div class="col-12">
					<div class="header">
						<div class="float-left">
							<h4><?php _e('Appointments','rbdobooking'); ?></h4>
						</div>
						<div class="clear"></div>
					</div>
                    <?php echo $advanced_filter_options; ?>
					<div class="appointments-area">
                        <table class="table table-striped"
                               data-pending="<?php _e('Pending','rbdobooking') ?>"
                               data-approved="<?php _e('Approved','rbdobooking') ?>"
                               data-cancelled="<?php _e('Cancelled','rbdobooking') ?>"
                               data-rejected="<?php _e('Rejected','rbdobooking') ?>"
                               data-payment_completed="<?php _e('Completed','rbdobooking') ?>"
                        >
                            <thead>
                            <tr>
                                <th></th>
                                <th><?php _e("Action","rbdobooking") ?></th>
                                <th><?php _e("No.","rbdobooking") ?></th>
                                <th><?php _e("Date","rbdobooking") ?></th>
                                <th><?php _e("Member","rbdobooking") ?></th>
                                <th><?php _e("Customer","rbdobooking") ?></th>
                                <th><?php _e("Phone","rbdobooking") ?></th>
                                <th><?php _e("Service","rbdobooking") ?></th>
                                <th><?php _e("Duration","rbdobooking") ?></th>
                                <th><?php _e("Status","rbdobooking") ?></th>
                                <th><?php _e("Payment","rbdobooking") ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php //echo $booked_slots; ?>
                            </tbody>
                        </table>
                    </div>

				</div>
			</div>

		</div>

	</div>

    <?php
    echo $appointments_slidepanel;
    echo $appointments_del_modal;
    ?>

    <script type="text/html" class="row-actions-column">
        <div>
            <input type="checkbox" />
        </div>
    </script>

    <script type="text/html" class="row-expand-child">
        <div>
            <div class="row">
                <div class="col-6">
                    <span><?php _e("Email","rbdobooking"); ?>:</span>
                    <span class="email"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <span><?php _e("Notes","rbdobooking"); ?>:</span>
                    <span class="notes"></span>
                </div>
            </div>
        </div>
</script>

</div>
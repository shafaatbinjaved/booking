<div class="row advanced-filter-options">
	<div class="col-md-1 col-sm-4">
        <label for="appointment_no"><?php _e('No.','rbdobooking') ?></label>
        <br>
		<input type="text"
               id="appointment_no"
               placeholder="No."
               class="form-control appointment_no">
	</div>
	<div class="col-md-2 col-sm-4">
        <label for="appointment_time"><?php _e('Appointment start time','rbdobooking') ?></label>
        <br>
        <input name="appointment_time_start"
               class="appointment_time_start"
               value="<?php echo date("Y-m-").'01'; ?>"
               type="hidden" />
        <input name="appointment_time_end"
               class="appointment_time_end"
               value="<?php echo date("Y-m-t"); ?>"
               type="hidden" />
        <div class="appointment_time reportrange text-center"
             id="appointment_time"
             data-name="appointment_time">
            <i class="fa fa-calendar"></i>&nbsp;
            <span><?php echo "01.".date("m.Y") . " - " . date("t.m.y") ?></span>
            <i class="fa fa-caret-down"></i>
        </div>
	</div>
    <div class="col-md-2 col-sm-4">
        <label for="creation_time"><?php _e('Appointment creation time','rbdobooking') ?></label>
        <br>
        <input name="creation_time_start"
               class="creation_time_start"
               value="<?php echo date("Y-m-").'01'; ?>"
               type="hidden" />
        <input name="creation_time_end"
               class="creation_time_end"
               value="<?php echo date("Y-m-t"); ?>"
               type="hidden" />
        <div class="creation_time reportrange text-center"
             title="<?php _e('Creation time','rbdobooking') ?>"
             data-name="creation_time">
            <i class="fa fa-calendar"></i>&nbsp;
            <span><?php echo "01.".date("m.Y") . " - " . date("t.m.y") ?></span>
            <i class="fa fa-caret-down"></i>
        </div>
    </div>
	<div class="col-md-2 col-sm-4">
        <label for="member-select"><?php _e('Select staff members','rbdobooking') ?></label>
        <br>
		<select name="member-select" class="form-control member-select selectpicker"
                id="member-select"
                multiple
                title="<?php _e('Select staff member','rbdobooking') ?>"
                data-live-search="true">
            <option></option>
			<?php
			foreach ( $staff_members as $member ) {
				?>
				<option value="<?php echo $member['member_id'] ?>" selected>
					<?php echo $member['first_name'] . ' ' . $member['last_name'] ?>
				</option>
				<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-4">
        <label for="member-select"><?php _e('Select services','rbdobooking') ?></label>
        <br>
		<select name="service-select" class="form-control service-select selectpicker"
                id="service-select"
                multiple
                title="<?php _e('Select service','rbdobooking') ?>"
                data-live-search="true">
            <option></option>
			<?php
			foreach ( $services as $service ) {
				?>
				<option value="<?php echo $service['service_id'] ?>" selected>
					<?php echo $service['name'] ?>
				</option>
				<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-4">
        <label for="customer-select"><?php _e('Select customer','rbdobooking') ?></label>
        <br>
		<select name="customer-select" class="form-control customer-select selectpicker"
                id="customer-select"
                multiple
                title="<?php _e('Select customer','rbdobooking') ?>"
                data-live-search="true">
            <option></option>
			<?php
			foreach ( $customers as $customer ) {
				?>
				<option value="<?php echo $customer['customer_id'] ?>" selected>
					<?php echo $customer['full_name'] ?>
				</option>
				<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-1 col-sm-4">
        <label for="status-select"><?php _e('Status','rbdobooking') ?></label>
        <br>
		<select name="status-select" id="status-select"
                class="form-control status-select selectpicker" multiple>
			<option value="1" selected><?php _e('Approved','rbdobooking') ?></option>
            <option value="0" selected><?php _e('Pending','rbdobooking') ?></option>
			<option value="2" selected><?php _e('Cancelled','rbdobooking') ?></option>
			<option value="3" selected><?php _e('Rejected','rbdobooking') ?></option>
		</select>
	</div>
</div>
<div id="rbdobooking-orders" class="wrap rbdobooking-admin-wrap">

	<div class="container-fluid">

		<h1 class="rbdobooking-h1-placeholder"><?php _e('Payments','rbdobooking') ?></h1>

		<div class="rbdobooking-admin-content rbdobooking-admin-orders">

			<div class="row rbdobooking-orders-container">
				<div class="col-12">
					<div class="header">
						<div class="float-left">
							<h4><?php _e('Orders','rbdobooking'); ?></h4>
						</div>
						<div class="clear"></div>
					</div>
					<div class="orders-area">
						<table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th><?php _e('Order ID','rbdobooking') ?></th>
                                <th><?php _e('Date','rbdobooking') ?></th>
                                <th><?php _e('Customer Name','rbdobooking') ?></th>
                                <th><?php _e('Amount','rbdobooking') ?></th>
                                <th><?php _e('Status','rbdobooking') ?></th>
                                <!--<th><?php /*_e('Payment ID','rbdobooking') */?></th>-->
                                <th><?php _e('Payment method','rbdobooking') ?></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php echo $payment_rows; ?>
                            </tbody>
                        </table>
					</div>

				</div>
			</div>

		</div>

	</div>

	<?php
	echo $payments_slidepanel;
	?>

    <script type="text/html" class="row-actions-column">
        <div>
            <input type="checkbox" />
            <i class="fas fa-download btn-invoice" title="<?php _e('Download Invoice') ?>"></i>
        </div>
    </script>

</div>
<div class="payments-ui-slidepanel slidepanel">
	<div class="row">
		<div class="col-12">
			<div class="row header">
				<div class="col-12">
					<div class="float-left">
						<h3 class="heading"
                            data-show_order_data_heading="<?php echo __('Order view','rbdobooking'); ?>"
                        ></h3>
					</div>
					<div class="float-right">
						<!--<i class="far fa-save btn-save" title="<?php /*echo __('Save','rbdbooking'); */?>"></i>-->
						<i class="fas fa-times btn-close" title="<?php echo __('Close','rbdbooking'); ?>"></i>
						<!--<i class="fas fa-trash circle btn-delete" title="<?php /*echo __('Delete','rbdbooking'); */?>"></i>-->
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-1"></div>
				<div class="col-10 detail-part">
				</div>
				<div class="col-1"></div>
			</div>
		</div>
	</div>
</div>
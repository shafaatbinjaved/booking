<?php

foreach ( $payment_rows as $index => $row ) {
	?>
    <tr data-order_id="<?php echo $row["order_id"]; ?>">
        <td>
            <input type="checkbox" />
        </td>
        <td><?php echo $row["order_id"]; ?></td>
        <td><?php echo date("d.m.Y",strtotime($row["created_at"])); ?></td>
        <td><?php echo $row["full_name"]; ?></td>
        <td><?php echo $currency.$row["total_with_tax"] ?></td>
        <td>
            <?php
            switch ( $row["paid_status"] ) {
                case "0"://not paid
                    _e('Not paid','rbodobooking');
	                break;
	            case "1"://paid
		            _e('Paid','rbodobooking');
		            break;
	            case "2"://partially paid
		            _e('Partially paid','rbodobooking');
		            break;
            }
            ?>
        </td>
        <!--<td><?php /*echo ( is_null($row["payment_id"]) ? "" : $row["payment_id"] ); */?></td>-->
        <td><?php echo $row["payment_method_name"] ?></td>
        <td>
            <?php
            do_action("rbdobooking_show_invoice_btn",$row["order_id"],$row["vendor_id"]);
            ?>
        </td>
    </tr>
	<?php
}
?>
<div id="rbdobooking-appearance" class="wrap rbdobooking-admin-wrap">

	<div class="container-fluid">

		<h1 class="rbdobooking-h1-placeholder"><?php _e('Appearance','rbdobooking') ?></h1>

		<div class="rbdobooking-admin-content rbdobooking-admin-appearance">

			<div class="row rbdobooking-appearance-container">
				<div class="col-12">
					<div class="header">
						<h4><?php _e('Appearance','rbdobooking'); ?></h4>
					</div>
					<div class="appearance-area">
						<div class="row">
							<div class="col-4">Select Color</div>
							<div class="col-4">Show steps</div>
							<div class="col-4">Alignment of buttons</div>
						</div>
						<ul class="nav nav-tabs">
							<?php
							foreach ($steps as $index => $step) {
								?>
								<li class="nav-item">
									<a class="nav-link<?php echo ($index==0) ? " active show" : "" ?>"
									   data-toggle="tab"
									   href="#step-<?php echo $step ?>"
                                    ><?php echo ($index+1).". ".$step ?></a>
								</li>
							<?php
							}
							?>
						</ul>
						<div class="tab-content">
							<?php
							foreach ($steps as $index => $step) {
								?>
								<div id="step-<?php echo $step ?>"
								     class="tab-pane<?php echo ( $index==0 ? " active" : "" ) ?>">
									<div class="col-1"></div>
									<div class="col-9">
										<?php
										if ( isset($steps_data[$step]) ) {
											echo $steps_data[$step];
										}
										else {
											echo $step;
										}
										?>
									</div>
								</div>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

	<?php
	echo $appearance_slidepanel;
	?>

</div>
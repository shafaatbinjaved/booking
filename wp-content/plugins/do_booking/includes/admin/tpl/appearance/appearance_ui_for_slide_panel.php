<div class="appearance-ui-slidepanel slidepanel">
	<div class="row">
		<div class="col-12">
			<div class="row header">
				<div class="col-12">
					<div class="float-left">
						<h3 class="heading"
						    data-add_heading="<?php echo __('Add New Appearance','rbdobooking'); ?>"
						    data-edit_heading="<?php echo __('Edit Appearance','rbdobooking'); ?>"
						></h3>
					</div>
					<div class="float-right">
						<i class="far fa-save btn-save" title="<?php echo __('Save','rbdbooking'); ?>"></i>
						<i class="fas fa-times btn-close" title="<?php echo __('Close','rbdbooking'); ?>"></i>
						<i class="fas fa-trash circle btn-delete" title="<?php echo __('Delete','rbdbooking'); ?>"></i>
					</div>
				</div>
			</div>
			<div class="row detail-part">
				<div class="col-1"></div>
				<div class="col-10">
					<div class="form-horizontal">

						<div class="form-group">
							<div class="row">
								<div class="col-6">
									<label class="control-label" for="sel-member_id"><?php echo __('Provider','rbdobooking'); ?></label>
									<select name="member_id" data-name="member_id"
									        id="sel-member_id" class="form-control member_id"></select>
								</div>
								<div class="col-6">
									<label class="control-label" for="sel-service_id"><?php echo __('Service','rbdobooking'); ?></label>
									<select name="service_id" data-name="service_id"
									        id="sel-service_id" class="form-control service_id"></select>
								</div>
							</div>
							<div class="row">
								<div class="col-4">
									<label class="control-label" for="input-date"><?php echo __('Date','rbdobooking'); ?></label>
									<input type="text" name="date" class="form-control date"
									       id="input-date" data-name="date" value="" />
								</div>
								<div class="col-4">
									<label class="control-label" for="input-start"><?php echo __('Start','rbdobooking'); ?></label>
									<input type="text" name="start" class="form-control start"
									       id="input-start" data-name="start" value="" />
								</div>
								<div class="col-4">
									<label class="control-label" for="input-end"><?php echo __('End','rbdobooking'); ?></label>
									<input type="text" name="end" class="form-control end"
									       id="input-end" data-name="end" value="" />
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<label class="control-label" for=""><?php echo __('Customers','rbdobooking'); ?></label>
									<div class="row">
										<div class="col-7">
											<span class="customer-details"></span>
										</div>
										<div class="col-5">
                                            <span class="dropdown">
                                                <button class="btn btn-sm btn-default btn-status dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true" type="button"
                                                        aria-expanded="false"><i class="fas fa-check"></i></button>
                                                <div class="dropdown-menu"></div>
                                            </span>
											<button class="btn btn-sm btn-default btn-attach-link"
											        type="button"><i class="fas fa-link"></i></button>
											<button class="btn btn-sm btn-default btn-users" type="button">
												<i class="fas fa-user"></i>
											</button>
											<i class="fas fa-trash"></i>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<label class="control-label" for="sel-send_notification"><?php echo __('Send Notifications','rbdobooking'); ?></label>
									<select name="send_notification" id="sel-send_notification"
									        data-name="send_notification" class="form-control send_notification"></select>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<label for="textarea-internal_note" class="control-label"><?php echo __('Internal Note','rbdobooking'); ?></label>
									<textarea name="internal_note" id=textarea-internal_note""
									          data-name="internal_note" class="form-control"></textarea>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-1"></div>
			</div>
		</div>
	</div>
</div>
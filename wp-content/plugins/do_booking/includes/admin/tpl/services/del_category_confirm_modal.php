<div class="modal fade del-category-confirm-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php _e('Delete category','rbdobooking'); ?></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">

				<div class="form-horizontal">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div><?php _e('Do you really want to delete this category ?','rbdobooking'); ?></div>
								<div class="float-right">
									<button type="button" class="btn-delete-category-confirm btn btn-del-confirm">
										<?php _e('Delete','rbdobooking'); ?>
									</button>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="service-ui-slidepanel slidepanel">
    <div class="row">
        <div class="col-12">
            <div class="row header">
                <div class="col-12">
                    <div class="float-left">
                        <h3 class="heading" data-heading="<?php _e('Add New Service','rbdobooking'); ?>"></h3>
                    </div>
                    <div class="float-right">
                        <i class="far fa-save btn-save" title="<?php _e('Save','rbdbooking'); ?>"></i>
                        <i class="fas fa-times btn-close" title="<?php _e('Close','rbdbooking'); ?>"></i>
                        <i class="fas fa-trash circle btn-delete" title="<?php _e('Delete','rbdbooking'); ?>"></i>
                    </div>
                </div>
            </div>
            <div class="row detail-part">
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12">
                                    <label class="control-label" for=""><?php _e('Title','rbdobooking'); ?></label>
                                    <input type="text" class="form-control name"
                                           data-name="name" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <label class="control-label" for=""><?php _e('Service color','rbdobooking'); ?></label>
                                    <input type="text" data-name="color" class="form-control service-color-picker" value=""  />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <label for="" class="control-label"><?php _e('Visibility','rbdobooking'); ?></label>
                                    <select name="" id="" class="form-control visibility" data-name="visibility">
                                        <option value="public"><?php _e('Public','rbdobooking'); ?></option>
                                        <option value="private"><?php _e('Private','rbdobooking'); ?></option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <label for="" class="control-label"><?php _e('Price','rbdobooking'); ?></label>
                                    <input type="number" class="form-control price"
                                           data-name="price" value="" />
                                </div>
                                <div class="col-3">
                                    <label for="" class="control-label"><?php _e('Min Capacity','rbdobooking'); ?></label>
                                    <input type="number" class="form-control min_capacity"
                                           data-name="min_capacity" value="" />
                                </div>
                                <div class="col-3">
                                    <label for="" class="control-label"><?php _e('Max Capacity','rbdobooking'); ?></label>
                                    <input type="number" class="form-control max_capacity"
                                           data-name="max_capacity" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label for="" class="control-label"><?php _e('Duration','rbdobooking'); ?></label>
                                    <select name="" id="" class="form-control duration" data-name="duration">
                                        <option value="20">20 min</option>
                                        <option value="40">40 min</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <label for="" class="control-label"><?php _e('Padding Before','rbdobooking'); ?></label>
                                    <select name="" id="" class="form-control padding_before" data-name="padding_before">
                                        <option value="20">20 min</option>
                                        <option value="40">40 min</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <label for="" class="control-label"><?php _e('Padding After','rbdobooking'); ?></label>
                                    <select name="" id="" class="form-control padding_after" data-name="padding_after">
                                        <option value="20">20 min</option>
                                        <option value="40">40 min</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label for="" class="control-label"><?php _e('category','rbdobooking'); ?></label>
                                    <select name="" id="" class="form-control category_id" data-name="category_id">
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label for="" class="control-label"><?php _e('Member','rbdobooking'); ?></label>
                                    <select name="" id="" class="selectpicker form-control member_id" multiple
                                            data-name="member_id"
                                            data-selected-text-format="count > 1",
                                            data-live-search="true"
                                    >
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label for="" class="control-label"><?php _e('Info','rbdobooking'); ?></label>
                                    <textarea name="" id="" class="form-control info" data-name="info"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </div>
</div>
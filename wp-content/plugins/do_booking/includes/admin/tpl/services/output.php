<?php
/**
 * @var $services_count
 * @var $all_categories
 * @var $services_html
 * @var $service_none_item
 * @var $del_cat_cnf_modal
 * @var $edit_cat_modal
 * @var $add_cat_modal
 * @var $del_serv_cnf_modal
 * @var $single_category
 * @var $service_list_item
 * @var $service_show_none
 * @var $service_slidepanel
**/
?>
<div id="rbdobooking-services" class="wrap rbdobooking-admin-wrap">

	<div class="container-fluid">

		<h1 class="rbdobooking-h1-placeholder"><?php _e('Services','rbdobooking') ?></h1>

		<div class="rbdobooking-admin-content rbdobooking-admin-services">

			<div class="row rbdobooking-services-container">
				<div class="col-12">
					<div class="row">
						<div class="col-3 services-categories-container">
							<ul>
								<li class="all-services-count-container selected-category">
									<div class="row">
										<div class="col-2">
											<i class="fas fa-star"></i>
										</div>
										<div class="col-8">
											<span class="name"><?php _e('All Services','rbdobooking'); ?></span>
										</div>
										<div class="col-2">
											<span class="count"><?php echo $services_count; ?></span>
										</div>
									</div>
								</li>
							</ul>
							<div class="row">
								<div class="col-12 services-categories-heading">
									<span><?php _e('Categories','rbdobooking'); ?></span>
								</div>
							</div>
							<ul class="all-categories"><?php echo $all_categories; ?></ul>
							<ul class="add-category">
								<li class="single-category btn-add-category">
									<div class="row">
										<div class="col-12">
											<?php echo " +  " . __('Add New Category','rbdobooking'); ?>
										</div>
									</div>
								</li>
							</ul>

						</div>
						<div class="col-9 all-service-container">
							<div class="header">
								<div class="row">
									<div class="col-4">
										<h4><?php _e('Services List','rbdobooking') ?></h4>
									</div>
									<div class="col-4"></div>
									<div class="col-4">
										<div class="input-group">
											<div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <i class="fa fa-search"></i>
                                                        </span>
											</div>
											<input type="text" class="form-control btn-search-service" placeholder="Search service" />
										</div>
									</div>
								</div>
							</div>
							<div class="service-list-area">
								<ul>
									<li class="header">
										<div class="row service-list-item">
											<div class="col-1"></div>
											<div class="col-1"></div>
											<div class="col-4 font-weight-bold"><?php _e('Name','rbdobooking') ?></div>
											<div class="col-3 font-weight-bold"><?php _e('Duration','rbdobooking') ?></div>
											<div class="col-2 font-weight-bold"><?php _e('Price','rbdobooking') ?></div>
											<div class="col-1"></div>
										</div>
									</li>
									<?php echo $services_html; ?>
									<?php echo $service_none_item; ?>
								</ul>
							</div>
						</div>
					</div>
					<i class="fa fa-plus btn-add" type="button"
					   title="<?php _e("Add New Service","rbdobooking") ?>"></i>
					<i class="fas fa-trash btn-delete-multi" type="button"
					   title="<?php _e("Delete marked items","rbdobooking") ?>"></i>
				</div>
			</div>

		</div>

	</div>

	<?php echo $del_cat_cnf_modal; ?>
	<?php echo $edit_cat_modal; ?>
	<?php echo $add_cat_modal; ?>
	<?php echo $del_serv_cnf_modal; ?>
	<?php echo $single_category; ?>
	<?php echo $service_list_item; ?>
	<?php echo $service_show_none; ?>
	<?php echo $service_slidepanel; ?>

</div>
<li class="single-category" data-category_id="<?php echo $category_id; ?>">
	<div class="row">
		<div class="col-8">
			<span class="name"><?php echo $name; ?></span>
		</div>
		<div class="col-4">
			<span class="count float-right"><?php echo $count; ?></span>
			<span class="icons float-right">
                <i class="fas fa-pencil-alt btn-edit"></i>
                <i class="fas fa-trash btn-del"></i>
			</span>
		</div>
	</div>
</li>
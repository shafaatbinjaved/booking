<div class="modal fade del-service-confirm-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php _e('Delete service','rbdobooking'); ?></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">

				<div class="form-horizontal">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="service-1"><?php _e('Do you really want to delete this service ?','rbdobooking'); ?></div>
								<div class="service-multi" style="display: none;"><?php echo __('Do you really want to delete these services ?','rbdobooking'); ?></div>
								<div class="float-right">
									<button type="button" class="delete-service-confirm btn">
										<?php _e('Delete','rbdobooking'); ?>
									</button>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
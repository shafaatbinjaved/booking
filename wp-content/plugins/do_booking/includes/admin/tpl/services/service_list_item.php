<?php
/**
 * @var $service_id
 * @var $category_id
 * @var $name
 * @var $duration
 * @var $price
 **/
$service_color = (!empty($color) ? 'color: '.$color.';' : '');
?>

<li data-service_id="<?php echo $service_id; ?>">
	<input type="hidden" class="category_id" value="<?php echo $category_id; ?>" />
	<div class="row service-list-item">
		<div class="col-1"><input type="checkbox" class="service-checkbox" /></div>
        <div class="col-1">
            <i class="fas fa-circle service-color" style="<?php echo $service_color ?>"></i>
        </div>
		<div class="col-4 name"><?php echo $name; ?></div>
		<div class="col-3 duration"><?php echo $duration; ?></div>
		<div class="col-2 price"><?php echo $price; ?></div>
		<div class="col-1"></div>
	</div>
</li>
<li class="item-none">
    <div class="row service-list-item">
        <div class="col-12 msg-category-select" style="display: none;"><?php _e('There are no services assigned to this category.','rbdobooking') ?></div>
        <div class="col-12 msg-search" style="display: none;"><?php _e('Your search won\'t match any service','rbdobooking') ?></div>
        <div class="col-12 msg-zero-services"><?php _e('There are no services in the system yet.','rbdobooking') ?></div>
    </div>
</li>
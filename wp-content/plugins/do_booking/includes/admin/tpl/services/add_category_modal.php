<div class="modal fade add-category-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php _e('Add New Category','rbdobooking'); ?></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">

				<div class="form-horizontal">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<input type="text" class="form-control new-category-name"
								       placeholder="<?php _e('Category Name','rbdobooking'); ?>"
								       name="new-category-name" />
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-save"><?php _e('Save','rbdobooking') ?></button>
				<button type="button" class="btn btn-danger" data-dismiss="modal"><?php _e('Close','rbdobooking') ?></button>
			</div>
		</div>
	</div>
</div>
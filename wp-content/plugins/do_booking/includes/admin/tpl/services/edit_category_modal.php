<div class="modal fade edit-category-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php _e('Edit category','rbdobooking'); ?></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">

				<div class="form-horizontal">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" value="" class="category_id" />
								<input type="text" value="" class="form-control name" />
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-save">
					<?php _e("Save","rbdobooking"); ?>
				</button>
			</div>
		</div>
	</div>
</div>
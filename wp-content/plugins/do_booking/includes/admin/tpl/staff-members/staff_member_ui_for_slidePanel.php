<div class="staff-member-ui-slidepanel slidepanel">
	<div class="row">
		<div class="col-12">
			<div class="row header">
				<div class="col-12">
					<div class="float-left">
						<!--<img class="img-container" alt=""
						     src="http://localhost/do_booking/wp-content/uploads/2018/04/shafaat-pic.jpg" />-->
                        <div class="img-container">
                            <i class="far fa-user"></i>
                        </div>
					</div>
					<div class="float-left">
						<h3 class="heading" data-edit="<?php _e('Edit','rbdobooking') ?>"
                            data-heading="<?php _e('Add New Staff Member','rbdobooking'); ?>">
							<?php _e('Add New Staff Member','rbdobooking'); ?>
						</h3>
					</div>
					<div class="float-right">
						<i class="far fa-save btn-save" title="<?php _e('Save','rbdbooking'); ?>"></i>
						<i class="fas fa-times btn-close" title="<?php _e('Close','rbdbooking'); ?>"></i>
						<i class="fas fa-trash circle btn-delete" title="<?php _e('Delete','rbdbooking'); ?>"></i>
					</div>
				</div>
			</div>
			<div class="row detail-part">
				<div class="col-12">
					<ul class="nav nav-tabs nav-justified">
						<li class="nav-item nav-item-details">
							<a class="nav-link active" data-toggle="tab" href="#details">
								<i class="fas fa-bars"></i>
								<span><?php echo __('Details','rbdobooking') ?></span>
							</a>
						</li>
						<li class="nav-item nav-item-services">
							<a class="nav-link" data-toggle="tab" href="#services">
								<i class="fab fa-servicestack"></i>
								<span><?php echo __('Services','rbdobooking') ?></span>
							</a>
						</li>
						<li class="nav-item nav-item-schedule">
							<a class="nav-link nav-item-" data-toggle="tab" href="#schedule">
								<i class="fas fa-clipboard-list"></i>
								<span><?php echo __('Schedule','rbdobooking') ?></span>
							</a>
						</li>
						<li class="nav-item nav-item-days_off">
							<a class="nav-link" data-toggle="tab" href="#days_off">
								<i class="far fa-calendar"></i>
								<span><?php echo __('Days off','rbdobooking') ?></span>
							</a>
						</li>
					</ul>
					<div class="form-horizontal">

						<div class="tab-content">
							<div class="tab-pane active" id="details">
								<div class="form-group">
									<div class="row">
										<div class="col-6">
											<label for="first-name" class="control-label"><?php echo __('First Name','rbdobooking'); ?></label>
											<input type="text" id="first-name" class="form-control first-name" data-name="first_name" />
										</div>
										<div class="col-6">
											<label for="last-name" class="control-label"><?php echo __('Last Name','rbdobooking'); ?></label>
											<input type="text" id="last-name" class="form-control last-name" data-name="last_name" />
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<label for="email" class="control-label"><?php echo __('Email','rbdobooking'); ?></label>
											<input type="email" id="email" class="form-control email" data-name="email" />
										</div>
										<div class="col-6">
											<label for="phone" class="control-label"><?php echo __('Phone','rbdobooking'); ?></label>
											<input type="text" id="phone" class="form-control phone" data-name="phone" />
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<label for="email" class="control-label"><?php echo __('Visibility','rbdobooking'); ?></label>
											<select id="visibility" class="form-control visibility" data-name="visibility">
												<option value="public"><?php echo __('Public','rbdobooking') ?></option>
												<option value="private"><?php echo __('Private','rbdobooking') ?></option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col-12">
											<label for="info_member" class="control-label"><?php echo __('Info','rbdobooking'); ?></label>
											<textarea type="text" id="info_member" class="form-control info" data-name="info_member"></textarea>
										</div>
									</div>
                                    <?php do_action('rbdobooking_attach_or_connect_google_calendar_ui'); ?>
								</div>
							</div>
							<div class="tab-pane" id="services">
								<div class="row services-container">
								</div>
							</div>
							<div class="tab-pane" id="schedule">
								<div class="form-group">
									<div class="row header-row">
										<div class="col-2"><?php echo __('Day on','rbdobooking'); ?></div>
										<div class="col-7"><?php echo __('Time (Start - End)','rbdobooking'); ?></div>
										<div class="col-3"><?php echo __('Break','rbdobooking'); ?></div>
									</div>
									<?php
									$days = array(
										__('mon','rbdobooking'),
										__('tue','rbdobooking'),
										__('wed','rbdobooking'),
										__('thu','rbdobooking'),
										__('fri','rbdobooking'),
										__('sat','rbdobooking'),
										__('sun','rbdobooking'),
									);
									for ( $i=1;$i<=7;$i++ ) {
										?>
										<div class="row day-row" data-day="<?php echo $i ?>" data-timing_id="">
											<div class="col-2">
												<label class="switch">
													<input type="checkbox" class="schedule-day-on" />
													<span class="slider round"></span>
												</label>
											</div>
											<div class="col-7">
												<div class="input-group">
													<div class="input-group-prepend">
														<span class="input-group-text day-name text-capitalize"><?php echo $days[$i-1]; ?></span>
													</div>
													<select class="form-control start-time" data-initial="00:00:00">
                                                        <?php echo $all_time_options; ?>
													</select>
													<div class="input-group-append">
														<span class="input-group-text">-</span>
													</div>
													<select class="form-control end-time" data-initial="23:45:00">
                                                        <?php echo $all_time_options; ?>
													</select>
												</div>
												<div class="all-breaks">
													<div class="clear"></div>
												</div>
											</div>
											<div class="col-3">
												<div class="popup">
													<div class="popup-head">
														<div class="float-left"><?php echo __("Add break","rbdobooking"); ?></div>
														<div class="float-right close-btn">x</div>
														<div class="clear"></div>
													</div>
													<div class="popup-content">
														<div class="row">
															<div class="col-10">
																<div class="input-group">
																	<select class="break-start-select form-control">
                                                                        <?php echo $all_time_options; ?>
																	</select>
																	<div class="input-group-append">
																		<span class="input-group-text">-</span>
																	</div>
																	<select name="" class="break-stop-select form-control">
                                                                        <?php echo $all_time_options; ?>
																	</select>
																</div>
															</div>
															<div class="col-2">
																<i class="fa fa-plus popup-btn-add"></i>
															</div>
														</div>
													</div>
												</div>
												<button class="btn btn-add-break"><?php echo __('Add break','rbdobooking') ?></button>
											</div>
										</div>
										<?php
									}
									?>
								</div>
							</div>
							<div class="tab-pane" id="days_off">
								<div class="row">
									<div class="col-12 popup-days-off-container">
									</div>
								</div>
								<div class="row">
									<div class="col-12 text-center year-holder-container">
                                                    <span class="year-holder">
                                                        <span class="pointer year-minus change-year">&lt;</span>
                                                        <span class="year"><?php echo $year; ?></span>
                                                        <span class="pointer year-plus change-year">&gt;</span>
                                                    </span>
									</div>
								</div>
								<div class="row">
									<div class="col-4 datepicker month_0"></div>
									<div class="col-4 datepicker month_1"></div>
									<div class="col-4 datepicker month_2"></div>
								</div>
								<div class="row">
									<div class="col-4 datepicker month_3"></div>
									<div class="col-4 datepicker month_4"></div>
									<div class="col-4 datepicker month_5"></div>
								</div>
								<div class="row">
									<div class="col-4 datepicker month_6"></div>
									<div class="col-4 datepicker month_7"></div>
									<div class="col-4 datepicker month_8"></div>
								</div>
								<div class="row">
									<div class="col-4 datepicker month_9"></div>
									<div class="col-4 datepicker month_10"></div>
									<div class="col-4 datepicker month_11"></div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<li class="single-staff-member" data-member_id="<?php echo $member_id; ?>">
	<div class="row">
		<div class="col-3">
            <div>
                <div class="actions">
                    <input type="checkbox" class="" />
                    <!--<button type="button" class="btn btn-sm btn-default btn-edit btn-circle"
                            title="<?php /*echo __('Edit Member','rbdobooking') */?>">
                        <i class="fas fa-pencil-alt"></i>
                    </button>-->
                </div>
                <?php echo $profile_pic; ?>
                <div class="full_name float-left">
                    <span class="first_name"><?php echo $first_name; ?></span>
                    <span class="last_name"> <?php echo $last_name; ?></span>
                    <br />
                    <span class="visibility <?php echo $visibility; ?>">
                        <?php echo $visibility; ?>
                    </span>
                </div>
            </div>
            <div class="clear"></div>
		</div>
		<div class="col-5">
            <div class="services-container">
                <?php echo $service_name; ?>
                <div class="clear"></div>
            </div>
        </div>
		<div class="col-4">
            <?php echo $schedule; ?>
            <div class="clear"></div>
        </div>
	</div>
</li>
<div class="popup-days-off">
	<div class="head">
		<div class="float-left date"><?php echo __('date','rbdobooking'); ?></div>
		<div class="float-right close-btn">x</div>
		<div class="clear"></div>
	</div>
	<div class="content">
		<div class="row">
			<div class="col-10">
				<div>
					<label class="switch">
						<input class="not-working-on-date" checked type="checkbox">
						<span class="slider round"></span>
					</label>
					<span><?php echo __('We are not working this year','rbdobooking') ?></span>
				</div>
				<div>
					<label class="switch">
						<input class="repeat-every-year" type="checkbox">
						<span class="slider round"></span>
					</label>
					<span><?php echo __('Repeat every year','rbdobooking') ?></span>
				</div>
			</div>
			<div class="col-2">
				<i class="fas fa-save popup-btn-add"></i>
			</div>
		</div>
	</div>
</div>
<div class="day">
	<div class="name"><?php echo $day_short; ?></div>
	<div class="popup">
		<div class="popup-head">
			<?php echo $day_full; ?>
		</div>
		<div class="popup-content">
			<div class="timing-container">
				<div class="name"><?php _e('Timing','rbdobooking') ?> :</div>
				<div class="timing">
					<?php echo $time; ?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="break-container">
				<div class="name"><?php _e('Break','rbdobooking') ?> :</div>
				<div class="breaks">
					<?php echo $all_breaks; ?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade del-staff-member-confirm-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php echo esc_html__('Delete staff members','rbdobooking'); ?></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">

				<div class="form-horizontal">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="staff-member-1"><?php echo __('Do you really want to delete this staff member ?','rbdobooking'); ?></div>
								<div class="staff-member-multi"><?php echo __('Do you really want to delete these staff members ?','rbdobooking'); ?></div>
								<div class="float-right">
									<button type="button" class="delete-staff-member-confirm btn btn-delete">
										<?php echo __('Delete','rbdobooking'); ?>
									</button>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="rbdobooking-staff-members" class="wrap rbdobooking-admin-wrap">
	<div class="container-fluid">

	<h1 class="rbdobooking-h1-placeholder"><?php echo __('Staff Members','rbdobooking') ?></h1>

	<?php
	if ( rbdobooking()->pro && class_exists('RBDoBooking_License') ) {
		//RBDoBooking()->license->notices( true );
	}
	?>

	<div class="rbdobooking-admin-content rbdobooking-admin-staff-members">

		<?php do_action('rbdobooking_admin_staff_members_before','') ?>

		<div class="row rbdobooking-staff-members-container">
			<div class="col-12 ">
				<div class="row header">
                    <div class="col-4">
                        <h4><?php _e('Staff Members','rbdobooking') ?></h4>
                    </div>
                    <div class="col-5"></div>
                    <div class="col-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">
                                        <i class="fa fa-search"></i>
                                    </span>
                            </div>
                            <input type="text" class="form-control btn-search-staff-member"
                                   placeholder="<?php _e('Search staff members','rbdobooking') ?>">
                        </div>
                    </div>
				</div>
				<div class="row">
					<div class="col-12 staff-members-list-area">
						<ul>
							<li class="single-staff-member">
								<div class="row">
									<div class="col-3"><?php echo __('Name & Visibilty','rbdobooking') ?></div>
									<div class="col-5"><?php echo __('Services','rbdobooking') ?></div>
									<div class="col-4"><?php echo __('Schedule','rbdobooking') ?></div>
								</div>
							</li>
							<?php echo $staff_members_html; ?>
						</ul>
					</div>
				</div>

                <i class="fa fa-plus btn-add" type="button"
                   title="<?php echo __("Add New Staff Member","rbdobooking") ?>"></i>
                <i class="fas fa-trash btn-delete-multi" type="button"
                   title="<?php echo __("Delete marked Staff Members","rbdobooking") ?>"></i>

			</div>
		</div>

	</div>
</div>
    <?php echo $delete_modal; ?>
	<?php echo $staff_member_ui_for_slidePanel; ?>
	<?php echo $single_staff_member; ?>
	<?php echo $break_item; ?>
	<?php echo $service_category; ?>
	<?php echo $service_item; ?>
	<?php echo $popup_days_off; ?>
</div>
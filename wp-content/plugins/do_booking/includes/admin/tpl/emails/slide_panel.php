<?php
$settings = array(
	"media_buttons" =>  false,
	"teeny"         =>  true
);
?>
<div class="emails-ui-slidepanel slidepanel">
	<div class="row">
		<div class="col-12">
			<div class="row header">
				<div class="col-12">
					<div class="float-left">
						<h3 class="heading"
                            data-add_heading="<?php echo __('Add New Appointment','rbdobooking'); ?>"
                            data-edit_heading="<?php echo __('Edit email notification template','rbdobooking'); ?>"
                        ></h3>
					</div>
					<div class="float-right">
						<i class="far fa-save btn-save" title="<?php echo __('Save','rbdbooking'); ?>"></i>
						<i class="fas fa-times btn-close" title="<?php echo __('Close','rbdbooking'); ?>"></i>
						<i class="fas fa-trash circle btn-delete" title="<?php echo __('Delete','rbdbooking'); ?>"></i>
					</div>
				</div>
			</div>
			<div class="row detail-part">
				<div class="col-1"></div>
				<div class="col-10">
					<div class="form-horizontal">

						<div class="form-group">
                            <div class="row">
                                <div class="col-12">
                                    <label class="control-label">
                                        <span>Notification type </span>
                                        <span class=""></span>
                                    </label>
                                    <input type="hidden" class="email_template_id" />
                                    <input type="text" disabled
                                           class="form-control disabled email-notification-name" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <label class="radio-inline">
                                        <input value="1" type="radio" class="is_enabled_or_not" name="is_enabled_or_not" />
                                        <span><?php echo __('Enabled','rbdobooking'); ?></span>
                                    </label>
                                </div>
                                <div class="col-3">
                                    <label class="radio-inline">
                                        <input value="0" type="radio" class="is_enabled_or_not" name="is_enabled_or_not" />
                                        <span><?php echo __('Disabled','rbdobooking'); ?></span>
                                    </label>
                                </div>
                            </div>
							 <div class="row">
                                 <div class="col-12">
                                     <label class="control-label" for="subject"><?php echo __('Subject','rbdobooking'); ?></label>
                                     <input type="text" name="subject" data-name="subject"
                                            id="subject" class="form-control subject" />
                                 </div>
							 </div>
                            <div class="row">
                                <div class="col-12">
                                    <label class="control-label" for="message"><?php echo __('Message','rbdobooking'); ?></label>
                                    <?php
                                    wp_editor( "", "message", $settings );
                                    ?>
                                    <!--<textarea id="message"></textarea>-->
                                    <div id="message"></div>
                                </div>
                            </div>
						</div>

					</div>
				</div>
				<div class="col-1"></div>
			</div>
		</div>
	</div>
</div>
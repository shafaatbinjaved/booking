<div id="rbdobooking-emails" class="wrap rbdobooking-admin-wrap">

	<div class="container-fluid">

		<h1 class="rbdobooking-h1-placeholder"><?php _e('Email Notifications','rbdobooking') ?></h1>

		<div class="rbdobooking-admin-content rbdobooking-admin-emails">

			<div class="row rbdobooking-emails-container">
				<div class="col-12">
					<div class="header">
						<div class="float-left">
							<h4><?php _e('Emails','rbdobooking'); ?></h4>
						</div>
						<div class="clear"></div>
					</div>
					<div class="emails-area">
						<table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th><?php _e('Notification type','rbdobooking') ?></th>
                                <th><?php _e('Requires cron setup','rbdobooking') ?></th>
                                <th><?php _e('Enabled or Disabled','rbdobooking'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php echo $email_rows; ?>
                            </tbody>
                        </table>
					</div>

				</div>
			</div>

		</div>

	</div>

	<?php
	echo $emails_slidepanel;
	?>

</div>
<?php

foreach ( $email_rows as $index => $row ) {
	?>
    <tr data-email_notification_id="<?php echo $row["email_notification_id"] ?>">
        <td>
            <?php echo $index+1; ?>
        </td>
        <td><?php echo $row["name"] ?></td>
        <td>
            <?php
            if ( $row["is_req_cron"] == "1" ) {
                _e("Yes","rbdobooking");
            }
            else {
	            _e("No","rbdobooking");
            }
            ?>
        </td>
        <td class="enabled-disabled">
            <?php
            if ( $row["is_enabled"] == "1" ) {
                _e('Enabled','rbdobooking');
            }
            else {
	            _e('Disabled','rbdobooking');
            }
            ?>
        </td>
    </tr>
	<?php
}
?>
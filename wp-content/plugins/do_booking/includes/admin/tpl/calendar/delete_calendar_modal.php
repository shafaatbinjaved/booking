<div class="modal fade del-calendar-confirm-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php _e('Delete calendar','rbdobooking'); ?></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="calendar-1"><?php _e('Do you really want to delete this appointment ?','rbdobooking'); ?></div>
                                <div class="calendar-multi" style="display: none;"><?php _e('Do you really want to delete these appointments ?','rbdobooking'); ?></div>
                                <div class="float-right">
                                    <button type="button" class="delete-calendar-confirm btn">
										<?php _e('Delete','rbdobooking'); ?>
                                    </button>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
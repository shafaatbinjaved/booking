<div class="calendar-ui-slidepanel slidepanel">
	<!--<div class="row">
		<div class="col-12">
			<div class="row header">
				<div class="col-12">
					<div class="float-left">
						<h3 class="heading"
                            data-add_heading="<?php /*_e('Add New Calendar','rbdobooking'); */?>"
                            data-edit_heading="<?php /*_e('Edit Calendar','rbdobooking'); */?>"
                        ></h3>
					</div>
					<div class="float-right">
						<i class="far fa-save btn-save" title="<?php /*_e('Save','rbdbooking'); */?>"></i>
						<i class="fas fa-times btn-close" title="<?php /*_e('Close','rbdbooking'); */?>"></i>
						<i class="fas fa-trash circle btn-delete" title="<?php /*_e('Delete','rbdbooking'); */?>"></i>
					</div>
				</div>
			</div>
			<div class="row detail-part">
				<div class="col-1"></div>
				<div class="col-10">
					<div class="form-horizontal">

						<div class="form-group">
							<div class="row">
								<div class="col-6">
									<label class="control-label" for="sel-member_id"><?php /*_e('Provider','rbdobooking'); */?></label>
                                    <select name="member_id" data-name="member_id"
                                            id="sel-member_id" class="form-control member_id"></select>
								</div>
                                <div class="col-6">
                                    <label class="control-label" for="sel-service_id"><?php /*_e('Service','rbdobooking'); */?></label>
                                    <select name="service_id" data-name="service_id"
                                            id="sel-service_id" class="form-control service_id"></select>
                                </div>
							</div>
                            <div class="row">
                                <div class="col-4">
                                    <label class="control-label" for="input-date"><?php /*_e('Date','rbdobooking'); */?></label>
                                    <input type="date" name="date" class="form-control date"
                                           id="input-date" data-name="date" value="" />
                                </div>
                                <div class="col-4">
                                    <label class="control-label" for="input-start"><?php /*_e('Start','rbdobooking'); */?></label>
                                    <input type="time" name="start" class="form-control start"
                                           id="input-start" data-name="start" value="" />
                                </div>
                                <div class="col-4">
                                    <label class="control-label" for="input-end"><?php /*_e('End','rbdobooking'); */?></label>
                                    <input type="time" name="end" class="form-control end"
                                           id="input-end" data-name="end" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label class="control-label" for=""><?php /*_e('Customers','rbdobooking'); */?></label>
                                    <div class="row">
                                        <div class="col-7">
                                            <span class="customer-details"></span>
                                        </div>
                                        <div class="col-5">
                                            <span class="dropdown">
                                                <button class="btn btn-sm btn-default btn-status dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true" type="button"
                                                        aria-expanded="false"><i class="fas fa-check"></i></button>
                                                <div class="dropdown-menu"></div>
                                            </span>
                                            <button class="btn btn-sm btn-default btn-attach-link"
                                                    type="button"><i class="fas fa-link"></i></button>
                                            <button class="btn btn-sm btn-default btn-users" type="button">
                                                <i class="fas fa-user"></i>
                                            </button>
                                            <i class="fas fa-trash"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label class="control-label" for="sel-send_notification"><?php /*_e('Send Notifications','rbdobooking'); */?></label>
                                    <select name="send_notification" id="sel-send_notification"
                                            data-name="send_notification" class="form-control send_notification"></select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label for="textarea-internal_note" class="control-label"><?php /*_e('Internal Note','rbdobooking'); */?></label>
                                    <textarea name="internal_note" id=textarea-internal_note""
                                              data-name="internal_note" class="form-control"></textarea>
                                </div>
                            </div>
						</div>

					</div>
				</div>
				<div class="col-1"></div>
			</div>
		</div>
	</div>-->
    <div class="row">
        <div class="col-12">
            <div class="row header">
                <div class="col-12">
                    <div class="float-left">
                        <h3 class="heading"
                            data-add_heading="<?php echo __('Add New Appointment','rbdobooking'); ?>"
                            data-edit_heading="<?php echo __('Edit Appointment','rbdobooking'); ?>"
                        ></h3>
                    </div>
                    <div class="float-right">
                        <i class="far fa-save btn-save" title="<?php echo __('Save','rbdbooking'); ?>"></i>
                        <i class="fas fa-times btn-close" title="<?php echo __('Close','rbdbooking'); ?>"></i>
                        <i class="fas fa-trash circle btn-delete" title="<?php echo __('Delete','rbdbooking'); ?>"></i>
                    </div>
                </div>
            </div>
            <div class="row detail-part">
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label" for="sel-member_id"><?php echo __('Provider','rbdobooking'); ?></label>
                                    <select name="member_id" data-name="member_id"
                                            id="sel-member_id" class="form-control member_id"></select>
                                </div>
                                <div class="col-6">
                                    <label class="control-label" for="sel-service_id"><?php echo __('Service','rbdobooking'); ?></label>
                                    <select name="service_id" data-name="service_id"
                                            id="sel-service_id" class="form-control service_id"></select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label" for="input-date"><?php _e('Start date','rbdobooking'); ?></label>
                                    <input type="date" name="start_date" class="form-control start_date"
                                           id="input-date" data-name="start_date" value="" />
                                </div>
                                <div class="col-6">
                                    <label class="control-label" for="input-start"><?php _e('Start time','rbdobooking'); ?></label>
                                    <input type="time" name="start_time" class="form-control start_time"
                                           id="input-start" data-name="start_time" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label" for="input-date"><?php _e('End Date','rbdobooking'); ?></label>
                                    <input type="date" name="end_date" class="form-control end_date"
                                           id="input-date" data-name="end_date" value="" />
                                </div>
                                <div class="col-6">
                                    <label class="control-label" for="input-end"><?php echo __('End time','rbdobooking'); ?></label>
                                    <input type="time" name="end_time" class="form-control end_time"
                                           id="input-end" data-name="end_time" value="" />
                                </div>
                            </div>
                            <div class="row customer-row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <select data-live-search="true" name="customer-select" class="customer-select form-control">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4 customer-names">
                                    <label class="control-label" for=""><?php _e('Customer Name','rbdobooking'); ?></label>
                                    <br>
                                    <div class="customer-name customer-entry"></div>
                                </div>
                                <div class="col-4 customer-phones">
                                    <label class="control-label" for=""><?php _e('Customers Phone','rbdobooking'); ?></label>
                                    <br>
                                    <div class="customer-phone customer-entry"></div>
                                </div>
                                <div class="col-4 customer-emails">
                                    <label class="control-label" for=""><?php _e('Customers email','rbdobooking'); ?></label>
                                    <br>
                                    <div class="customer-email customer-entry"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <label class="control-label" for=""><?php _e('Status','rbdobooking'); ?></label>
                                    <select name="status" data-name="status" class="form-control status">
                                        <option value="0"><?php _e('Pending','rbdobooking') ?></option>
                                        <option value="1"><?php _e('Approved','rbdobooking') ?></option>
                                        <option value="2"><?php _e('Cancelled','rbdobooking') ?></option>
                                        <option value="3"><?php _e('Rejected','rbdobooking') ?></option>
                                    </select>
                                    <!--<option value="0" data-content="<i class='fas fa-clock'></i>&nbsp;<?php /*_e('Pending','rbdobooking') */?>"><?php /*_e('Pending','rbdobooking') */?></option>
                                        <option value="1" data-content="<i class='fas fa-check'></i>&nbsp;<?php /*_e('Approved','rbdobooking') */?>"><?php /*_e('Approved','rbdobooking') */?></option>
                                        <option value="2" data-content="<i class='fas fa-times'></i>&nbsp;;<?php /*_e('Cacnelled','rbdobooking') */?>"><?php /*_e('Cancelled','rbdobooking') */?></option>
                                        <option value="3" data-content="<i class='far fa-calendar-times'></i>&nbsp;<?php /*_e('Rejected','rbdobooking') */?>"><?php /*_e('Rejected','rbdobooking') */?></option>-->
                                </div>
                                <div class="col-4">
                                    <label class="control-label" for=""><?php _e('Attach money','rbdobooking'); ?></label>
                                    <br>
                                    <div>
                                        <button class="btn btn-sm btn-default btn-attach-link"
                                                type="button"><i class="fas fa-link"></i></button>
                                        <!--<button class="btn btn-sm btn-default btn-users" type="button">
                                            <i class="fas fa-user"></i>
                                        </button>-->
                                    </div>
                                </div>
                                <div class="col-4">
                                    <label class="control-label" for=""><?php _e('Remove customer','rbdobooking'); ?></label>
                                    <br>
                                    <button class="btn btn-sm btn-default"
                                            type="button"><i class="fas fa-trash"></i></button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label class="control-label" for="sel-send_notification"><?php echo __('Send Notifications','rbdobooking'); ?></label>
                                    <select name="send_notification" id="sel-send_notification"
                                            data-name="send_notification" class="form-control send_notification"></select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <label for="textarea-internal_note" class="control-label"><?php echo __('Internal Note','rbdobooking'); ?></label>
                                    <textarea name="internal_note" id=textarea-internal_note""
                                              data-name="internal_note" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </div>
</div>
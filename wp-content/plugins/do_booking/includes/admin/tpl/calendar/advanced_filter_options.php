<?php
/** @var $staff_members array
 *  @var $services array
 *  @var $customers array
 */
?>
<div class="row advanced-filter-options">
    <div class="col-md-1 col-sm-4">
        <input type="text"
               placeholder="No."
               class="form-control appointment_no">
    </div>
    <div class="col-md-2 col-sm-4">
        <input name="appointment_time_start"
               class="appointment_time_start"
               value="<?php echo date("Y-m-").'01'; ?>"
               type="hidden" />
        <input name="appointment_time_end"
               class="appointment_time_end"
               value="<?php echo date("Y-m-t"); ?>"
               type="hidden" />
        <div class="appointment_time reportrange text-center"
             data-name="appointment_time">
            <i class="fa fa-calendar"></i>&nbsp;
            <span><?php echo "01.".date("m.Y") . " - " . date("t.m.y") ?></span>
            <i class="fa fa-caret-down"></i>
        </div>
    </div>
    <div class="col-md-2 col-sm-4">
        <input name="creation_time_start"
               class="creation_time_start"
               value="<?php echo date("Y-m-").'01'; ?>"
               type="hidden" />
        <input name="creation_time_end"
               class="creation_time_end"
               value="<?php echo date("Y-m-t"); ?>"
               type="hidden" />
        <div class="creation_time reportrange text-center"
             title="<?php _e('Creation time','rbdobooking') ?>"
             data-name="creation_time">
            <i class="fa fa-calendar"></i>&nbsp;
            <span><?php echo "01.".date("m.Y") . " - " . date("t.m.y") ?></span>
            <i class="fa fa-caret-down"></i>
        </div>
    </div>
    <div class="col-md-2 col-sm-4">
        <select name="member-select" class="form-control member-select selectpicker"
                multiple
                title="<?php _e('Select staff member','rbdobooking') ?>"
                data-live-search="true">
            <option></option>
			<?php
			foreach ( $staff_members as $member ) {
				?>
                <option value="<?php echo $member['member_id'] ?>" selected>
					<?php echo $member['first_name'] . ' ' . $member['last_name'] ?>
                </option>
				<?php
			}
			?>
        </select>
    </div>
    <div class="col-md-2 col-sm-4">
        <select name="service-select" class="form-control service-select selectpicker"
                multiple
                title="<?php _e('Select service','rbdobooking') ?>"
                data-live-search="true">
            <option></option>
			<?php
			foreach ( $services as $service ) {
				?>
                <option value="<?php echo $service['service_id'] ?>" selected>
					<?php echo $service['name'] ?>
                </option>
				<?php
			}
			?>
        </select>
    </div>
    <div class="col-md-2 col-sm-4">
        <select name="customer-select" class="form-control customer-select selectpicker"
                multiple
                title="<?php _e('Select customer','rbdobooking') ?>"
                data-live-search="true">
            <option></option>
			<?php
			foreach ( $customers as $customer ) {
				?>
                <option value="<?php echo $customer['customer_id'] ?>" selected>
					<?php echo $customer['full_name'] ?>
                </option>
				<?php
			}
			?>
        </select>
    </div>
    <div class="col-md-1 col-sm-4">
        <select name=""class="form-control status-select selectpicker" multiple>
            <option value="0" selected><?php _e('Pending','rbdobooking') ?></option>
            <option value="1" selected><?php _e('Approved','rbdobooking') ?></option>
            <option value="2" selected><?php _e('Cancelled','rbdobooking') ?></option>
            <option value="3" selected><?php _e('Rejected','rbdobooking') ?></option>
        </select>
    </div>
</div>
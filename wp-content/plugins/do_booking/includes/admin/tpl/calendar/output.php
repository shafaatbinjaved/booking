<?php
/** @var $advanced_filter_options
 *  @var $staff_members array
 *  @var $customers array
 *  @var $calendar_slidepanel
 *  @var $calendar_del_modal
 */
?>
<div id="rbdobooking-calendar" class="wrap rbdobooking-admin-wrap">

	<div class="container-fluid">

		<h1 class="rbdobooking-h1-placeholder"><?php _e('Calendar','rbdobooking') ?></h1>

		<div class="rbdobooking-admin-content rbdobooking-admin-calendar">

			<div class="row rbdobooking-calendar-container">
				<div class="col-12">
					<div class="header">
						<div class="float-left">
							<h4><?php _e('Calendar','rbdobooking'); ?></h4>
						</div>
						<div class="clear"></div>
					</div>
                    <?php //echo $advanced_filter_options; ?>
                    <div class="members-container">
                        <ul class="nav nav-tabs">
                            <li class="nav-item staff-member-li">
                                <a data-toggle="tab" data-member_id="all"
                                   href="#" class="nav-link active">
				                    <?php _e('All','rbdobooking'); ?>
                                </a>
                            </li>
	                        <?php
	                        foreach ( $staff_members as $staff_member ) {
		                        ?>
                                <li class="nav-item staff-member-li">
                                    <a data-toggle="tab"
                                       data-member_id="<?php echo $staff_member['member_id'] ?>"
                                       href="#" class="nav-link">
				                        <?php echo $staff_member['first_name'] . " " .$staff_member['last_name'] ?>
                                    </a>
                                </li>
		                        <?php
	                        }
	                        ?>
                        </ul>
                    </div>
					<div id="calendar"></div>

				</div>
			</div>

		</div>

	</div>

    <?php
    echo $calendar_slidepanel;
    echo $calendar_del_modal;
    ?>

</div>
<?php
/**
 * @var string $type
 */
$names = [
    'google' => [
        'displayName' => __('Google','rbdobooking')
    ],
    'microsoft' => [
        'displayName' => __('Microsoft','rbdobooking')
    ]
];
?>
<div class="row">
    <div class="col-12">
        <h5>
            <?php echo $names[$type]['displayName'] . ' ' . __('Calendar Integration','rbdobooking') ?>
        </h5>
        <div class="control-label">
            <?php _e('Synchronize staff member appointments with Google Calendar.', 'rbdobooking') ?>
        </div>
        <div>
            <?php _e('Kindly, first configure Google Calendar ','rbdobooking') ?><a href="<?php echo RBDOBOOKING_PLUGIN_URL . 'wp-admin/admin.php?page=rbdobooking-settings&view=google_calendar' ?>"><?php _e('settings', 'rbdobooking') ?></a>.
        </div>
    </div>
</div>

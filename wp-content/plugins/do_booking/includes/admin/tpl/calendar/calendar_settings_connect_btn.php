<?php
/**
 * @var string $type
 * @var string $connect_link
 */
?>
<div class="row">
    <div class="col-3">
        <a class="btn btn-primary btn-connect"
           href="<?php echo $connect_link ?>">
                <?php _e('Connect','rbdobooking'); ?>
        </a>
    </div>
</div>

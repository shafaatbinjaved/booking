<div class="customers-ui-slidepanel slidepanel">
	<div class="row">
		<div class="col-12">
			<div class="row header">
				<div class="col-12">
					<div class="float-left">
						<h3 class="heading"
						    data-add_customer="<?php echo __('Add New Customer','rbdobooking'); ?>"
						    data-edit_customer="<?php echo __('Edit Customer','rbdobooking'); ?>"></h3>
					</div>
					<div class="float-right">
						<i class="far fa-save btn-save" title="<?php echo __('Save','rbdbooking'); ?>"></i>
						<i class="fas fa-times btn-close" title="<?php echo __('Close','rbdbooking'); ?>"></i>
						<i class="fas fa-trash circle btn-delete" title="<?php echo __('Delete','rbdbooking'); ?>"></i>
					</div>
				</div>
			</div>
			<div class="row detail-part">
				<div class="col-1"></div>
				<div class="col-10">
					<div class="form-horizontal">

						<div class="form-group">
							<div class="row">
								<div class="col-12">
									<label class="control-label" for=""><?php _e('Full Name','rbdobooking'); ?></label>
									<input type="text" class="form-control full_name"
									       data-name="full_name" value="" />
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<label class="control-label" for=""><?php _e('Email','rbdobooking'); ?></label>
									<input type="text" class="form-control email"
									       data-name="email" value="" />
								</div>
							</div>
							<div class="row">
                                <div class="col-6">
                                    <label class="control-label" for=""><?php _e('Date of birth','rbdobooking'); ?></label>
                                    <input type="date" class="form-control dob"
                                           data-name="dob" value="" />
                                </div>
								<div class="col-6">
									<label class="control-label" for=""><?php _e('Phone','rbdobooking'); ?></label>
									<input type="text" class="form-control phone"
									       data-name="phone" value="" />
								</div>
							</div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label" for=""><?php _e('Street','rbdobooking'); ?></label>
                                    <input type="text" class="form-control street"
                                           data-name="street" value="" />
                                </div>
                                <div class="col-6">
                                    <label class="control-label" for=""><?php _e('Additional address','rbdobooking'); ?></label>
                                    <input type="text" class="form-control extra_address"
                                           data-name="extra_address" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label" for=""><?php _e('ZIP','rbdobooking'); ?></label>
                                    <input type="text" class="form-control zip"
                                           data-name="zip" value="" />
                                </div>
                                <div class="col-6">
                                    <label class="control-label" for=""><?php _e('City','rbdobooking'); ?></label>
                                    <input type="text" class="form-control city"
                                           data-name="city" value="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label" for=""><?php _e('State','rbdobooking'); ?></label>
                                    <input type="text" class="form-control state"
                                           data-name="state" value="" />
                                </div>
                                <div class="col-6">
                                    <label class="control-label" for=""><?php _e('Country','rbdobooking'); ?></label>
                                    <select class="form-control country"
                                            data-name="country">
                                        <option value=""><?php _e('Select country','rbdobooking') ?></option>
			                            <?php
			                            foreach ( $countries as $key => $country ) {
				                            ?>
                                            <option value="<?php echo $key ?>"
                                            ><?php _e($country,'rbdobooking') ?></option>
				                            <?php
			                            }
			                            ?>
                                    </select>
                                </div>
                            </div>
						</div>

					</div>
				</div>
				<div class="col-1"></div>
			</div>
		</div>
	</div>
</div>
<tr data-customer_id="<?php echo $customer_id; ?>">
    <td>
        <input class="customer_checkbox" type="checkbox" />
    </td>
    <td class="number"><?php echo $number; ?></td>
    <td class="full_name"><?php echo $full_name; ?></td>
    <td class="phone"><?php echo $phone; ?></td>
    <td class="email"><?php echo $email; ?></td>
    <td class="notes"><?php echo $notes; ?></td>
    <td class="last_appointment">
        <?php
        if (!is_null($last_appointment)) {
            echo date("d-m-Y",strtotime($last_appointment));
        }
        ?>
    </td>
    <td class="total_appointments"><?php echo $total_appointments; ?></td>
</tr>
<div id="rbdobooking-customers" class="wrap rbdobooking-admin-wrap">

    <div class="container-fluid">

        <h1 class="rbdobooking-h1-placeholder"><?php _e('Services','rbdobooking') ?></h1>

        <div class="rbdobooking-admin-content rbdobooking-admin-customers">

            <div class="row rbdobooking-customers-container">
                <div class="col-12">
                    <div class="header">
                        <div class="float-left">
                            <h4><?php _e('Customers','rbdobooking'); ?></h4>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="table-area">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>
                                    <input value="all" type="checkbox" />
                                </th>
                                <th><?php _e('No.','rbdobooking') ?></th>
                                <th><?php _e('Name','rbdobooking') ?></th>
                                <th><?php _e('Phone','rbdobooking') ?></th>
                                <th><?php _e('Email','rbdobooking') ?></th>
                                <th><?php _e('Notes','rbdobooking') ?></th>
                                <th><?php _e('Last Appointment','rbdobooking') ?></th>
                                <th><?php _e('Total Appointments','rbdobooking') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php echo $customer_rows ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <i class="fa fa-plus btn-add" type="button"
               title="<?php echo __("Add New Customer","rbdobooking") ?>"></i>
            <i class="fas fa-trash btn-delete-multi" type="button"
               title="<?php echo __("Delete marked Customers","rbdobooking") ?>"></i>

        </div>

    </div>

    <?php echo $customers_slide_panel; ?>

</div>
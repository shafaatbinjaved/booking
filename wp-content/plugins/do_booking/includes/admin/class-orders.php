<?php

/*
 * Emails class.
 * @since 1.0.0
 * */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

class RBDobooking_Orders extends View {

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	/*
	 * Primary class constructor
	 * @since 1.0.0
	 * */
	public function __construct() {

		parent::__construct(plugin_dir_path( __FILE__ ) . 'tpl/orders/');

		//Load Emails page
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_orders',array($this,'orders_ajax_handler'));
		add_action('get_order_data',array($this,'get_order_data'));

		$this->db = RBDoBooking_DB::instance();
	}

	/**
	 * Ajax handler for all requests originating from orders page
	 *
	 * @since 1.0.0
	 */
	public function orders_ajax_handler() {

		check_ajax_referer('rbdobooking-orders-nonce','security');

		$type = $_POST["type"];

		switch ( $type ) {
			case "show_order_data":
				$order_id = $_POST["order_id"];
				$vendor_id = $_POST["vendor_id"];
				$order_data = $this->db->orders->getOrderData( $order_id, $vendor_id );
				if ( !empty($order_data) ) {
					$currency = $this->db->getVendorSpecificSettingValueByName( "currency", $vendor_id );
					$html = $this->get_view(
						"order_view",
						false,
						array(
							"order_data"    =>  $order_data,
							"currency"      =>  $currency,
							"order_type"    =>  "order"
						)
					);
					wp_send_json_success(
						array(
							"html"  =>  $html
						)
					);
				}
				else {
					$msg = __('Failed to load order data','rbdobooking');
					wp_send_json_error(
						array(
							'notification' =>   array(
								'type'  =>  'error',
								'title' =>  __('Failed!','rbdobooking'),
								'text'  =>  $msg
							)
						)
					);
				}
				break;
			default:
				break;
		}
	}

	public function get_order_data( $order_id, $vendor_id, $type ) {
		$order_data = $this->db->orders->getOrderData( $order_id, $vendor_id );
		$currency = $this->db->getVendorSpecificSettingValueByName( "currency", $vendor_id );
		$html = $this->get_view(
			"order_view",
			false,
			array(
				"order_data"    =>  $order_data,
				"currency"      =>  $currency,
				"order_type"    =>  $type
			)
		);
		echo $html;
	}

	/*
	 * Determine if the user is viewing the payments page
	 * @since 1.0.0
	 * */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the payments page.
		if ( 'rbdobooking-orders' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_orders_init' );
		}
	}

	/**
	 * Enqueue assets for the emails notifications page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
		// Calendar admin script.
		wp_enqueue_script(
			'rbdobooking-bootstrap-select-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/js/bootstrap-select.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-select-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/css/bootstrap-select.css"
		);
		wp_enqueue_script(
			'rbdobooking-payments',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/orders.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
		do_action( 'rbdobooking_payments_enqueue' );
	}

	/**
	 * Build the output for the plugin order page.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		$vendor_id = 1;
		//$payment_rows = array(1,2,3,4,5,6,7,8,9,10);//$this->db->getEmailNotificationTypes( $vendor_id );
		$payment_rows = $this->db->orders->getLatestOrders( $vendor_id );
		$payments_rows_html = $this->get_view(
			'payment.row',
			false,
			array(
				"payment_rows"          =>  $payment_rows,
				"currency"              =>  $this->db->getVendorSpecificSettingValueByName( "currency", $vendor_id ),
			)
		);
		$payments_slidepanel = $this->get_view('slide_panel');

		echo $this->get_view(
			"output",
			false,
			array(
				"payment_rows"        =>  $payments_rows_html,
				"payments_slidepanel" =>  $payments_slidepanel
			)
		);

	}

}
new RBDobooking_Orders();
<?php
/**
 * User: shafaatbinjaved
 * Date: 11/3/2018
 * Time: 2:32 PM
 */

//require_once RBDOBOOKING_PLUGIN_DIR . "includes/class-db.php";

/**
 * Cache class.
 *
 * @package    RB Do Booking
 * @author     RB Do Booking
 * @since      1.0.0
 */
class RBDoBooking_Cache {

	private $cache_frontend = null;

	/*
	 * Primary class constructor
	 *
	 * @since 1.0.0
	 * */
	public function __construct() {
		//parent::__construct(plugin_dir_path( __FILE__ ) . 'tpl/settings/');

		//add_action('admin_init', array( $this, 'init'));
		//add_action('wp_ajax_rbdobooking_settings',array($this,'settings_ajax_handler'));
		//$this->db = RBDoBooking_DB::instance();

		add_action('rbdobooking_cache_refresh',array($this,'cache_refresh'), 10, 3);
		$this->cache_frontend = RBDOBOOKING_PLUGIN_DIR . 'cache/includes/frontend/tpl/';
	}

	public function cache_refresh( $vendor_id, $name, $other_id = 0 ) {
		if ( $name == "time_start" || $name == "time_stop" ) {
			$cache_file = $this->cache_frontend . "time_options/time_options.";
			$cache_file .= $vendor_id.".htm";

			if ( file_exists($cache_file) ) {
				unlink( $cache_file );
			}
			$service_cache_file = $this->cache_frontend . "default/steps/service/service.".$vendor_id.".htm";
			if ( file_exists($service_cache_file) ) {
				unlink( $service_cache_file );
			}
		} elseif ( $name == "custom_fields" ) {
		    $cache_file = $this->cache_frontend . "custom_fields/custom_fields.".$other_id.".htm";

		    if ( file_exists($cache_file) ) {
		        unlink( $cache_file );
            }
        }
	}

	public function init() {

		// Check what page we are on
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on plugins page
		$position = strpos($page,"rbdobooking-");

		if ( $position !== false ) {

		}

	}

}
new RBDoBooking_Cache();
<?php

/*
 * Emails class.
 * @since 1.0.0
 * */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

class RBDobooking_Emails extends View {

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	/*
	 * Primary class constructor
	 * @since 1.0.0
	 * */
	public function __construct() {

		parent::__construct(plugin_dir_path( __FILE__ ) . 'tpl/emails/');

		//Load Emails page
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_emails',array($this,'emails_ajax_handler'));

		$this->db = RBDoBooking_DB::instance();
	}

	/**
	 * Ajax handler for all requests originating from emails page
	 *
	 * @since 1.0.0
	 */
	public function emails_ajax_handler() {

		check_ajax_referer('rbdobooking-emails-nonce','security');

		$type = $_POST["type"];

		if ( $type == "get_email_notification_template_form_data" ) {

			$vendor_id = 1;
			$email_notification_id = $_POST["email_notification_id"];
			$result = $this->db->getEmailNotificationTypeTemplate( $vendor_id, $email_notification_id );

			$msg = null;

			if ( $result != null ) {
				$msg = __('Successfully form loaded','rbdobooking');
				wp_send_json_success(
					array(
						'notification' =>   array(
							'type'  =>  'success',
							'title' =>  __('Success!','rbdobooking'),
							'text'  =>  $msg
						),
						"email_template"  =>  $result
					)
				);
			}
			else {
				$msg = __('Failed to load email notification template','rbdobooking');
				wp_send_json_error(
					array(
						'notification' =>   array(
							'type'  =>  'error',
							'title' =>  __('Failed!','rbdobooking'),
							'text'  =>  $msg
						)
					)
				);
			}
		}
		else if ( $type == "save_email_notification_template" ) {
			$result = $this->db->update(
				'email_notification_templates',
				array(
					'subject'       =>  $_POST["subject"],
					'template'      =>  $_POST["template"],
					'is_enabled'    =>  $_POST["enabled"],
					'updated_at'    =>  date("Y-m-d H:i:s")
				),
				array(
					'email_template_id' =>  $_POST["email_template_id"]
				)
			);
			if ( $result ) {
				$msg = __('Successfully updated email notification type','rbdobooking');
				wp_send_json_success(
					array(
						'notification' =>   array(
							'type'  =>  'success',
							'title' =>  __('Success!','rbdobooking'),
							'text'  =>  $msg
						)
					)
				);
			}
			else {
				$msg = __('Failed to save email notification template','rbdobooking');
				wp_send_json_error(
					array(
						'notification' =>   array(
							'type'  =>  'error',
							'title' =>  __('Failed!','rbdobooking'),
							'text'  =>  $msg
						)
					)
				);
			}
			die();
		}
	}

	/*
	 * Determine if the user is viewing the emails notification page
	 * @since 1.0.0
	 * */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the services page.
		if ( 'rbdobooking-email-notifications' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_emails_notifications_init' );
		}
	}

	/**
	 * Enqueue assets for the emails notifications page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
		// Calendar admin script.
		wp_enqueue_script(
			'rbdobooking-bootstrap-select-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/js/bootstrap-select.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-select-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/css/bootstrap-select.css"
		);
		wp_enqueue_script(
			'rbdobooking-emails-notifications',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/emails_notifications.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
		do_action( 'rbdobooking_emails_notification_enqueue' );
	}

	/**
	 * Build the output for the plugin emails notifications page.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		$vendor_id = 1;

		//$appointments_slidepanel = $this->get_view("appointments_ui_for_slide_panel");
		$email_rows = $this->db->getEmailNotificationTypes( $vendor_id );
		$email_rows_html = $this->get_view(
			'email_notification.row',
			false,
			array("email_rows"=>$email_rows)
		);
		$emails_slidepanel = $this->get_view('slide_panel');

		echo $this->get_view(
			"output",
			false,
			array(
				"email_rows"        =>  $email_rows_html,
				"emails_slidepanel" =>  $emails_slidepanel
			)
		);

	}

}
new RBDobooking_Emails();
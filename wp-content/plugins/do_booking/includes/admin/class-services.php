<?php

/*
 * Services class.
 * @since 1.0.0
 * */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

class RBDoBooking_Services extends View {

    /**
     * Holds instance of plugin db class
     *
     * @since 1.0.0
     *
     * @var RBDoBooking_DB
    */
    private $db;

	/*
	 * Primary class constructor
	 * @since 1.0.0
	 * */
	public function __construct() {

		parent::__construct(plugin_dir_path( __FILE__ ) . 'tpl/services/');

		//Load Services page
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_services',array($this,'services_ajax_handler'));

		$this->db = RBDoBooking_DB::instance();
	}

	/**
     * Ajax handler for all requests originating from services page
     *
     * @since 1.0.0
	*/
	public function services_ajax_handler() {

	    check_ajax_referer('rbdobooking-services-nonce','security');

	    if ( $_POST["type"] == "save_new_category" ) {
	        $this->save_new_category();
        }
        else if ( $_POST["type"] == "edit_category" ) {
	        $this->edit_category();
        }
        else if ( $_POST["type"] == "get_service_form_data" ) {
	        $this->get_service_form_data();
        }
        else if ( $_POST["type"] == "get_service_details" ) {
	        $this->get_service_details();
        }
        else if ( $_POST["type"] == "add_service" ) {
            $this->save_new_service();
        }
        else if ( $_POST["type"] == "delete_service" ) {
            $this->delete_service();
        }
        else if ( $_POST["type"] == "delete_category" ) {
            $this->delete_category();
        }
        else {
	        $msg = __('Wrong place','rbdobooking' );
	        wp_send_json_error(
		        array(
			        'notification' => array(
				        'type'  =>  'error',
				        'title' =>  __('Failed!','rbdobooking'),
				        'text'  =>  $msg
			        )
		        )
	        );
        }
    }

    /**
     * Edit category
     *
     * @since 1.0.0
    */
    public function edit_category() {
        $category_id = $_POST["category_id"];
        $name = $_POST["name"];

        $result = $this->db->editCategoryByCategoryID( $name, $category_id );

	    if ( $result ) {
		    $msg = __('Category updated successfully.','rbdobooking');
		    wp_send_json_success(
			    array(
				    'notification' => array(
					    'type'  =>  'success',
					    'title' =>  __('Success!','rbdobooking'),
					    'text'  =>  $msg
				    ),
				    "service_id"    =>  $category_id
			    )
		    );
	    }
	    $msg = __('Failed to update category.','rbdobooking');
	    wp_send_json_error(
		    array(
			    'notification' => array(
				    'type'  =>  'error',
				    'title' =>  __('Error!','rbdobooking'),
				    'text'  =>  $msg
			    ),
			    "service_id"    =>  $category_id
		    )
	    );
    }

    /**
     * Delete category
     *
     * @since 1.0.0
    */
    public function delete_category() {
        $category_id = $_POST["category_id"];
        $result = $this->db->deleteCategoryByCategoryID( $category_id );

	    if ( $result ) {
		    $msg = __('Category deleted successfully.','rbdobooking');
		    wp_send_json_success(
			    array(
				    'notification' => array(
					    'type'  =>  'success',
					    'title' =>  __('Success!','rbdobooking'),
					    'text'  =>  $msg
				    ),
				    "service_id"    =>  $category_id
			    )
		    );
	    }
	    $msg = __('Failed to delete category.','rbdobooking');
	    wp_send_json_error(
		    array(
			    'notification' => array(
				    'type'  =>  'error',
				    'title' =>  __('Error!','rbdobooking'),
				    'text'  =>  $msg
			    ),
			    "service_id"    =>  $category_id
		    )
	    );
    }

    /**
     * Delete service
     *
     * @since 1.0.0
    */
    public function delete_service() {
        $service_id = $_POST['service_id'];
	    $result  = $this->db->deleteServiceByServiceID( $service_id );

	    if ( $result ) {
	        $msg = __('Service deleted successfully.','rbdobooking');
		    wp_send_json_success(
			    array(
				    'notification' => array(
					    'type'  =>  'success',
					    'title' =>  __('Success!','rbdobooking'),
					    'text'  =>  $msg
				    ),
				    "service_id"    =>  $service_id
			    )
		    );
        }
        $msg = __('Failed to delete service.','rbdobooking');
	    wp_send_json_error(
		    array(
			    'notification' => array(
				    'type'  =>  'error',
				    'title' =>  __('Error!','rbdobooking'),
				    'text'  =>  $msg
			    ),
			    "service_id"    =>  $service_id
		    )
	    );
    }


    /**
     *
     * Get Service form initial data
     *
     * @since 1.0.0
    */
    function get_service_form_data() {
        $categories = $this->db->getAllServiceCategories();

	    $all_staff_members = $this->db->getAllStaffMembers(
		    'a.member_id, CONCAT(a.first_name," ",a.last_name) as FULL_NAME, a.profile_pic'
	    );

        $return_data = array(
            "categories"  =>  $categories,
	        "staff_members" =>  $all_staff_members
        );
        wp_send_json_success( $return_data );
    }

    /**
     * Get Service details
     *
     * @since 1.0.0
     */
    function get_service_details() {
        $error_msg = '';
        if ( isset($_POST["service_id"]) ) {
            $service = $this->db->getServiceDetailsByServiceId( $_POST["service_id"] );

            if ( !is_null($service) ) {

            	$all_staff_members = $this->db->getAllStaffMembers(
            		'a.member_id, CONCAT(a.first_name," ",a.last_name) as FULL_NAME, a.profile_pic'
	            );

	            $service_members = $this->db->getServiceMembers( $_POST["service_id"] );

                $return_data = array(
                    "categories"    =>  $this->db->getAllServiceCategories(),
                    "service"       =>  $service[0],
	                "staff_members" =>  $all_staff_members,
	                "service_members"=> $service_members
                );
                wp_send_json_success( $return_data );
            } else {
                $error_msg = __('Wrong service id provided','rbdobooking');
            }
        }
        else {
            $error_msg = __('Service id is missing','rbdobooking');
        }
        wp_send_json_error( $error_msg );
    }

    /**
     * Saves new service
     *
     * @since 1.0.0
    */
    private function save_new_service() {

        $msg = '';
        $type = '';

        if ( isset($_POST["data"]) ) {

            if ( $_POST["data"][0]["name"] == "service_id" ) {
	            $service_id = $_POST["data"][0]["value"];
	            unset( $_POST["data"][0] );
	            $this->db->updateService( $_POST["data"], $service_id );
	            $msg = __("Service updated successfully","rbdobooking");
	            $type = "old";
            }
            else {
	            $service = $_POST["data"];
	            $service_id = $this->db->insertNewService( $service );
	            $msg = __("New service created successfully","rbdobooking");
	            $type = 'new';
            }
	        wp_send_json_success(
		        array(
			        'notification' => array(
				        'type'  =>  'success',
				        'title' =>  __('Success!','rbdobooking'),
				        'text'  =>  $msg
			        ),
                    "service_id"    =>  $service_id,
                    'type'          =>  $type
		        )
	        );
        }
        else {
            $msg = __('There is no data to save or update.','rbbdobooking');
        }
	    wp_send_json_error(
		    array(
			    'notification' => array(
				    'type'  =>  'error',
				    'title' =>  __('Failed!','rbdobooking'),
				    'text'  =>  $msg
			    )
		    )
	    );
    }

    /**
     * Save new category and returns json msg
     *
     * @since 1.0.0
    */
    private function save_new_category() {

	    $category = $_POST["data"];
	    if ( !empty($category) ) {
		    $isCategoryExists = $this->db->isCategoryExistsByName( $category, true );

		    if ( !$isCategoryExists ) {
			    $idNewCategory = $this->db->insertNewCategory( $_POST["data"] );

			    if ( $idNewCategory ) {
				    $msg = __('Category successfully added','rbdobooking');
				    wp_send_json_success(
					    array(
						    'category_id'  => $idNewCategory,
						    'category'     => $category,
						    'notification' => array(
							    'type'  =>  'success',
							    'title' =>  __('Success!','rbdobooking'),
							    'text'  =>  $msg
						    )
					    )
				    );
			    }
		    }

		    $msg = __('Category with this name already exists.','rbdobooking');

        } else {
	      $msg = __('Category with empty name not possible','rbdobooking');
        }

	    wp_send_json_error(
		    array(
			    'notification' => array(
				    'type'  =>  'error',
				    'title' =>  __('Failed!','rbdobooking'),
				    'text'  =>  $msg
			    )
		    )
	    );
    }

	/*
	 * Determine if the user is viewing the services page
	 * @since 1.0.0
	 * */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the services page.
		if ( 'rbdobooking-services' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_services_init' );
		}
	}

	/**
	 * Enqueue assets for the services page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
		// Services admin script.
		wp_enqueue_script(
			'rbdobooking-bootstrap-select-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/js/bootstrap-select.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-select-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/css/bootstrap-select.css"
		);
		wp_enqueue_script(
			'rbdobooking-services',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/services.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script('wp-color-picker');
		wp_enqueue_style( 'wp-color-picker' );
		do_action( 'rbdobooking_services_enqueue' );
	}

	/**
	 * Build the output for the plugin services page.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		$categories = $this->db->getAllServiceCategories();
		$columns = ' service_id, category_id, name, duration, price, color';
		$services = $this->db->getAllServices( $columns );

		$all_categories_html = '';
		/*echo '<prE>';
		var_dump( $categories );die();
		echo '</pre>';*/
		foreach ( $categories as $single_category ) {
			if ( is_null($single_category["service_id"]) ) {
				$single_category["count"] = 0;
			}
			$all_categories_html .= $this->get_view("single_category",false,$single_category);
		}

		$services_html = '';
		foreach ($services as $service) {
			$services_html .= $this->get_view(
				"service_list_item",
				false,
				$service
			);
		}

		$service_show_none_item_html = '';
		if ( count($services) == 0 ) {
			$service_show_none_item_html .= $this->get_view(
				'service_show_none_item'
			);
		}

		$del_cat_cnf_modal = $this->get_view('del_category_confirm_modal');
		$edit_category_modal = $this->get_view('edit_category_modal');
		$add_category_modal = $this->get_view('add_category_modal');
		$single_category = $this->get_view("single_category",true,array(),'tpl-category');
		$service_list_item = $this->get_view("service_list_item",true,array(),'tpl-service-list-item');
		$service_show_none = $this->get_view( 'service_show_none_item', true);
		$service_slidepanel = $this->get_view("service_ui_for_slide_panel");
		$del_serv_cnf_modal = $this->get_view('del_service_confirm_modal');

		$data = array(
            'services_count'    =>  count($services),
            'categories'        =>  $categories,
            'all_categories'    =>  $all_categories_html,
            'services_html'     =>  $services_html,
            'service_none_item' =>  $service_show_none_item_html,
            'del_cat_cnf_modal' =>  $del_cat_cnf_modal,
            'edit_cat_modal'    =>  $edit_category_modal,
            'add_cat_modal'     =>  $add_category_modal,
            'del_serv_cnf_modal'=>  $del_serv_cnf_modal,
            'single_category'   =>  $single_category,
            'service_list_item' =>  $service_list_item,
            'service_show_none' =>  $service_show_none,
            'service_slidepanel'=>  $service_slidepanel
        );

		echo $this->get_view("output",false,$data);
	}
}
new RBDoBooking_Services();
<?php
/**
 * User: shafaatbinjaved
 * Date: 3/30/2018
 * Time: 8:44 PM
 */

/**
 * Upgrade link used within the various admin pages.
 * @since 1.0.0
*/
function rbdobooking_admin_upgrade_link() {

	// Check if there's a constant.
	return esc_url( "http://shafaat.com" );
}

/**
 * Helper function to determine if viewing a RBDoBooking related admin page.
 * @since 1.0.0
 * @return boolean
 */
function rbdobooking_is_admin_page() {

	// Bail if we're not on a RBDoBooking screen or page.
	if ( ! is_admin() || empty( $_REQUEST['page'] ) || strpos( $_REQUEST['page'], 'rbdobooking' ) === false ) {
		return false;
	}

	return true;
}

/**
 * Load styles for all RBDoBooking-related admin screens
*/
function rbdobooking_admin_styles() {

	if ( !rbdobooking_is_admin_page() ) {
		return;
	}

	// Bootstrap style
	wp_enqueue_style(
		'rbdobooking-bootstrap',
		RBDOBOOKING_PLUGIN_URL . 'assets/css/bootstrap/bootstrap.css',
		array(),
		RBDOBOOKING_VERSION
	);

	// Font Awesome
	wp_enqueue_style(
		'rbdobooking-font-awesome',
		RBDOBOOKING_PLUGIN_URL . 'assets/css/fontawesome/css/fontawesome-all.css',
		array(),
		RBDOBOOKING_VERSION
	);

	// Main admin styles.
	wp_enqueue_style(
		'rbdobooking-admin',
		RBDOBOOKING_PLUGIN_URL . 'assets/css/admin.css',
		array(),
		RBDOBOOKING_VERSION
	);

	// Pnotify styles.
	wp_enqueue_style(
		'rbdobooking-pnotify',
		RBDOBOOKING_PLUGIN_URL . 'assets/css/pnotify/pnotify.min.css',
		array(),
		RBDOBOOKING_VERSION
	);
}
add_action( 'admin_enqueue_scripts','rbdobooking_admin_styles');

/**
 * Load scripts for all RBDoBooking-related admin screens.
 * @since 1.0.0
*/
function rbdobooking_admin_scripts() {

    if ( !rbdobooking_is_admin_page() ) {
        return;
    }

    wp_enqueue_media();

	wp_localize_script( 'jquery', 'RBDoBookingAjax', array(
		// URL to wp-admin/admin-ajax.php to process the request
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		// generate a nonce with a unique ID "myajax-post-comment-nonce"
		// so that you can check it later when an AJAX request is sent
		'security_calendar'         => wp_create_nonce( 'rbdobooking-calendar-nonce' ),
		'security_emails'           => wp_create_nonce( 'rbdobooking-emails-nonce' ),
		'security_orders'           => wp_create_nonce( 'rbdobooking-orders-nonce' ),
		'security_appointments'     => wp_create_nonce( 'rbdobooking-appointments-nonce' ),
        'security_services'         => wp_create_nonce( 'rbdobooking-services-nonce' ),
		'security_staff_member'     => wp_create_nonce( 'rbdobooking-staff-members-nonce' ),
		'security_custom_fields'    => wp_create_nonce( 'rbdobooking-custom-fields-nonce' ),
		'security_customers'        => wp_create_nonce( 'rbdobooking-customers-nonce' ),
		'security_coupons'          => wp_create_nonce( 'rbdobooking-coupons-nonce' ),
		'security_settings'         => wp_create_nonce( 'rbdobooking-settings-nonce' ),
		'security_invoices'         => wp_create_nonce( 'rbdobooking-invoices-nonce' ),
		'security_addons'           => wp_create_nonce( 'rbdobooking-addons-nonce' )
	));

	wp_enqueue_script(
		'rbdobooking-topper',
		RBDOBOOKING_PLUGIN_URL . "assets/js/topper/tooltip.min.js",
		array( 'jquery', ),
		RBDOBOOKING_VERSION,
		true
	);
    // Bootstrap
	wp_enqueue_script(
		'rbdobooking-bootstrap',
		RBDOBOOKING_PLUGIN_URL . "assets/js/bootstrap/bootstrap.min.js",
		array( 'jquery','rbdobooking-topper' ),
		RBDOBOOKING_VERSION,
		true
	);

	// Main helper functions script.
	wp_enqueue_script(
		'rbdobooking-helper',
		RBDOBOOKING_PLUGIN_URL . "assets/js/helper.js",
		array( 'jquery' ),
		RBDOBOOKING_VERSION,
		true
	);

	// Pnotify script.
	wp_enqueue_script(
		'rbdobooking-pnotify',
		RBDOBOOKING_PLUGIN_URL . "assets/js/pnotify/pnotify.min.js",
		array( 'jquery', ),
		RBDOBOOKING_VERSION,
		true
	);

}
add_action('admin_enqueue_scripts','rbdobooking_admin_scripts');

/**
 * Outputs the RBDoBooking admin header.
 * @since 1.0.0
*/
function rbdobooking_admin_header() {

	// Bail if we're not on a RBDoBooking screen or page.
	if ( ! rbdobooking_is_admin_page() ) {
		return;
	}

	// Omit header from Welcome activation screen.
	if ( 'rbdobooking-getting-started' === $_REQUEST['page'] ) {
		return;
	}
	?>
	<div id="rbdobooking-header-temp"></div>
	<div id="rbdobooking-header" class="rbdobooking-header">
		<img class="rbdobooking-header-logo"
		     src="<?php echo RBDOBOOKING_PLUGIN_URL; ?>assets/images/logo.png"
		     alt="RBDoBooking Logo" />
	</div>
	<?php
}
add_action( 'in_admin_header', 'rbdobooking_admin_header', 1000 );

/**
 * Add body class to RBDoBooking admin pages for easy reference
 * @since 1.0.0
 * @param string $classes
 * @return string
*/
function rbdobooking_admin_body_class( $classes ) {

	if ( ! rbdobooking_is_admin_page() ) {
		return $classes;
	}

	return "$classes rbdobooking-admin-page rbdobooking-bootstrap-wrapper";
}
add_filter( 'admin_body_class', 'rbdobooking_admin_body_class', 10, 1 );
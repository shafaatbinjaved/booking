<?php
/**
 * User: shafaatbinjaved
 * Date: 4/2/2018
 * Time: 9:35 PM
 */

/**
 * Settings output wrapper.
 * @since 1.0.0
 * @param array $args
 * @return string
*/
function rbdobooking_settings_output_field( $args ) {

	// Define default callback for this field type.
	$callback = !empty( $args['type'] ) && function_exists('rbdobooking_settings_'.$args['type'].'_callback' ) ? 'rbdobooking_settings_' . $args['type'] . '_callback' : 'rbdobooking_settings_missing_callback';

	// Allow custom callback to be provided via arg.
	if ( !empty( $args['callback'] && function_exists( $args['callback'] ) ) ) {
		$callback = $args['callback'];
	}

	// Store returned markup from callback.
	$field = call_user_func( $callback, $args );

	// Allow arg to bypass standard field wrap for custom display.
	if ( !empty( $args['wrap'] ) ) {
		return $field;
	}

	// Custom row classes/
	$class = !empty( $args['class'] ) ? rbdobooking_sanitize_classes((array) $args['class'], true) : '';

	// Build standard field markup and return.
	$output = '<div class="rbdobooking-setting-row rbdobooking-setting-row-' . sanitize_html_class( $args['type'] ) . ' rbdobooking-clear ' . $class . '" id="rbdobooking-setting-row-' . rbdobooking_sanitize_key( $args['id'] ) .'">';

	if ( !empty( $args['name'] ) && empty( $args['no_label'] ) ) {
		$output .= '<span class="rbdobooking-setting-label">';
		$output .= '<label for="rbdobooking-setting-' . rbdobooking_sanitize_key( $args['id'] ) . '">';
		$output .= esc_html( $args['name'] );
		$output .= '</label>';
		$output .= '</span>';
	}

	$output .= '<span class="rbdobooking-setting-field">';
	$output .= $field;
	$output .= '</span>';

	$output .= '</div>';

	return $output;
}

/**
 * Missing Callback
 * If a function is missing for settings callbacks alert the user
 * @since 1.0.0
 * @param array $args Arguments passed by the setting.
 * @return string
*/
function rbdobooking_settings_missing_callback( $args ) {

	return sprintf(
	/* translators: %s - ID of a setting. */
		esc_html__( 'The callback function used for the %s setting is missing.', 'rbdobooking' ),
		'<strong>' . rbdobooking_sanitize_key( $args['id'] ) . '</strong>'
	);
}

/**
 * Settings ocntent field callback
 * @since 1.0.0
 * @param array $args
 * @return string
*/
function rbdobooking_settings_content_callback( $args ) {
	return !empty( $args['content'] ) ? $args['content'] : '';
}

/**
 * Settings license field callback.
 * @since 1.0.0
 * @param array $args
 * @return string
*/
function rbdobooking_settings_license_callback( $args ) {

	// Lite users don't need to worry about license keys.
	if ( !rbdobooking()->pro || !class_exists('RBDoBooking_License') ) {
		$output = '<p>' . esc_html__('You\'re using RBDoBooking Lite - no license needed. Enjoy!','RBDoBooking') . ' 🙂</p>';
		$output .=
			'<p>' .
			sprintf(
				wp_kses(
				/* translators: %s - WPForms.com upgrade URL. */
					__( 'To unlock more features consider <a href="%s" target="_blank" rel="noopener noreferrer" class="rbdobooking-upgrade-modal">upgrading to Pro</a>.', 'RBDoBooking' ),
					array(
						'a' => array(
							'href'   => array(),
							'class'  => array(),
							'target' => array(),
							'rel'    => array(),
						),
					)
				),
				rbdobooking_admin_upgrade_link()
			) .
			'</p>';

		return $output;
	}

	$key  = rbdobooking_setting( 'key', '', 'rbdobooking_license' );
	$type = rbdobooking_setting( 'type', '', 'rbdobooking_license' );

	$output = '<input type="password" id="rbdobooking-setting-license-key" value="'.esc_attr($key).'" />';
	$output .= '<button id="rbdobooking-setting-license-key-verify" class="rbdobooking-btn rbdobooking-btn-md rbdobooking-btn-orange">' . esc_html__( 'Verify Key', 'rbdobooking' ) . '</button>';

	// Offer option to deactivate the key.
	$class   = empty( $key ) ? 'rbdobooking-hide' : '';
	$output .= '<button id="rbdobooking-setting-license-key-deactivate" class="rbdobooking-btn rbdobooking-btn-md rbdobooking-btn-light-grey ' . $class . '">' . esc_html__( 'Deactivate Key', 'rbdobooking' ) . '</button>';

	// If we have previously looked up the license type, display it.
	$class   = empty( $type ) ? 'rbdobooking-hide' : '';
	$output .= '<p class="type ' . $class . '">' .
	           sprintf(
	           /* translators: $s - license type. */
		           esc_html__( 'Your license key type is %s.', 'rbdobooking' ),
		           '<strong>' . esc_html( $type ) . '</strong>'
	           ) .
	           '</p>';
	$output .= '<p class="desc ' . $class . '">' .
	           wp_kses(
		           __( 'If your license has been upgraded or is incorrect, <a href="#" id="rbdobooking-setting-license-key-refresh">click here to force a refresh</a>.', 'rbdobooking' ),
		           array(
			           'a' => array(
				           'href' => array(),
				           'id'   => array(),
			           ),
		           )
	           ) .
	           '</p>';

	return $output;
}

/**
 * Settings text input field callback.
 * @since 1.0.0
 * @param array $args
 * @return string
 */
function rbdobooking_settings_text_callback( $args ) {

	$default = isset( $args['default'] ) ? esc_html( $args['default'] ) : '';
	$value = rbdobooking_setting( $args['id'], $default );
	$id = rbdobooking_sanitize_key( $args['id'] );

	$output = '<input type="text" id="rbdobooking-setting-' . $id . '" name="'.$id.'" value="'.esc_attr($value).'" />';

	if ( !empty( $args['desc'] ) ) {
		$output .= '<p class="desc">'.wp_kses_post( $args['desc'] ).'</p>';
	}

	return $output;
}

/**
 * Settings number input field callback
 * @since 1.0.0
 * @param array $args
 * @return string
*/
function rbdobooking_settings_number_callback( $args ) {

	$default = isset( $args['default'] ) ? esc_html( $args['default'] ) : '';
	$value = rbdobooking_setting( $args['id'], $default );
	$id = rbdobooking_sanitize_key( $args['id'] );

	$output = '<input type="number" id="rbdobooking-setting-' . $id . '" name="'.$id.'" value="'.esc_attr($value).'" />';

	if ( !empty( $args['desc'] ) ) {
		$output .= '<p class="desc">'.wp_kses_post( $args['desc'] ).'</p>';
	}

	return $output;
}

/**
 * Settings select field callback
 * @since 1.0.0
 * @param array $args
 * @return string
*/
function rbdobooking_settings_select_callback( $args ) {

	$default = isset( $args['default'] ) ? esc_html( $args['default'] ) : '';
	$value = rbdobooking_setting( $args['id'], $default );
	$id = rbdobooking_sanitize_key( $args['id'] );
	$class = !empty( $args['choicesjs'] ) ? 'choicesjs-select' : '';
	$choices = !empty( $args['choicesjs'] ) ? true : false;
	$data = '';

	if ( $choices && !empty( $args['search'] ) ) {
		$data = ' data-search="true"';
	}

	$output = $choices ? '<span class="choicesjs-select-wrap">' : '';
	$output .= '<select id="rbdobooking-setting-'.$id.'" name="'.$id.'" class="'.$class.'"'.$data.'>';

	foreach ( $args['options'] as $option => $name ) {
		$selected = selected($value,$option,false);
		$output .= '<option value="'.esc_attr($option).'" '.$selected.'>'.esc_html($name).'</option>';
	}

	$output .= '</select>';
	$output .= $choices ? '</span>' : '';

	if ( !empty( $args['desc'] ) ) {
		$output .= '<p class="desc">'.wp_kses_post( $args['desc'] ).'</p>';
	}

	return $output;
}

/**
 * Settings checkbox field callback.
 * @since 1.0.0
 * @param array $args
 * @return string
*/
function rbdobooking_settings_checkbox_callback( $args ) {

	$value = rbdobooking_setting( $args['id'] );
	$id = rbdobooking_sanitize_key( $args['id'] );
	$checked = !empty($value) ? checked(1,$value,false) : '';

	$output = '<input type="checkbox" id="rbdobooking-setting-'.$id.'" name="'.$id.'" '.$checked.' />';

	if ( !empty( $args['desc'] ) ) {
		$output .= '<p class="desc">'.wp_kses_post( $args['desc'] ).'</p>';
	}

	return $output;
}

/**
 * Settings radio field callback.
 * @since 1.0.0
 * @param array $args
 * @return string
*/
function rbdobooking_settings_radio_callback( $args ) {

	$default = isset( $args['default'] ) ? esc_html( $args['default'] ) : '';
	$value   = rbdobooking_setting( $args['id'], $default );
	$id      = rbdobooking_sanitize_key( $args['id'] );
	$output  = '';
	$x       = 1;

	foreach ( $args['options'] as $option => $name ) {

		$checked = checked( $value, $option, false );
		$output .= '<label for="rbdobooking-setting-' . $id . '[' . $x . ']" class="option-' . sanitize_html_class( $option ) . '">';
		$output .= '<input type="radio" id="rbdobooking-setting-' . $id . '[' . $x . ']" name="' . $id . '" value="' . esc_attr( $option ) . '" ' . $checked . '>';
		$output .= esc_html( $name );
		$output .= '</label>';
		$x ++;
	}

	if ( ! empty( $args['desc'] ) ) {
		$output .= '<p class="desc">' . wp_kses_post( $args['desc'] ) . '</p>';
	}

	return $output;
}

/**
 * Settings image upload field callback.
 * @since 1.0.0
 * @param array $args
 * @return string
 */
function rbdobooking_settings_image_callback( $args ) {

	$default = isset( $args['default'] ) ? esc_html( $args['default'] ) : '';
	$value   = rbdobooking_setting( $args['id'], $default );
	$id      = rbdobooking_sanitize_key( $args['id'] );
	$output  = '';

	if ( ! empty( $value ) ) {
		$output .= '<img src="' . esc_url_raw( $value ) . '">';
	}

	$output .= '<input type="text" id="rbdobooking-setting-' . $id . '" name="' . $id . '" value="' . esc_url_raw( $value ) . '">';
	$output .= '<button class="rbdobooking-btn rbdobooking-btn-md rbdobooking-btn-light-grey">' . esc_html__( 'Upload Image', 'rbdobooking' ) . '</button>';

	if ( ! empty( $args['desc'] ) ) {
		$output .= '<p class="desc">' . wp_kses_post( $args['desc'] ) . '</p>';
	}

	return $output;
}

/**
 * Settings color picker field callback.
 * @since 1.0.0
 * @param array $args
 * @return string
 */
function rbdobooking_settings_color_callback( $args ) {

	$default = isset( $args['default'] ) ? esc_html( $args['default'] ) : '';
	$value   = rbdobooking_setting( $args['id'], $default );
	$id      = rbdobooking_sanitize_key( $args['id'] );

	$output = '<input type="text" id="rbdobooking-setting-' . $id . '" class="rbdobooking-color-picker" name="' . $id . '" value="' . esc_attr( $value ) . '">';

	if ( ! empty( $args['desc'] ) ) {
		$output .= '<p class="desc">' . wp_kses_post( $args['desc'] ) . '</p>';
	}

	return $output;
}

/**
 * Settings providers field callback - this is for the Integrations tab.
 * @since 1.0.0
 * @param array $args
 * @return string
 */
function rbdobooking_settings_providers_callback( $args ) {

	$providers = get_option( 'rbdobooking_providers', false );
	$active    = apply_filters( 'rbdobooking_providers_available', array() );

	$output = '<div id="rbdobooking-settings-providers">';

	ob_start();
	do_action( 'rbdobooking_settings_providers', $active, $providers );
	$output .= ob_get_clean();

	$output .= '</div>';

	return $output;
}
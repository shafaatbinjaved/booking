<?php

/*
 * Appearance class.
 * @since 1.0.0
 * */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/frontend/tpl/class-default.php';

class RBDoBooking_Appearance {

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	/**
	 * Holds instance of plugin view class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $view;

	/*
	 * Primary class constructor
	 * @since 1.0.0
	 * */
	public function __construct() {

		//Load Appointemnts page
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_appearance',array($this,'appearance_ajax_handler'));

		$this->db = RBDoBooking_DB::instance();
		$this->view = new View(plugin_dir_path( __FILE__ ) . 'tpl/appearance/');
	}

	/**
	 * Ajax handler for all requests originating from appearance page
	 *
	 * @since 1.0.0
	 */
	public function appearance_ajax_handler() {
		check_ajax_referer('rbdobooking-appearance-nonce','security');

		$type = $_POST["type"];

		switch ($type) {
			default:
				break;
		}
	}

	/*
	 * Determine if the user is viewing the appearance page
	 * @since 1.0.0
	 * */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the services page.
		if ( 'rbdobooking-appearance' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_appearance_init' );
		}
	}

	/**
	 * Enqueue assets for the appearance page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
		// Appearance admin script.
		wp_enqueue_script(
			'rbdobooking-bootstrap-select-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/js/bootstrap-select.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-select-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/css/bootstrap-select.css"
		);
		wp_enqueue_style(
			'rbdobooking-default',
			RBDOBOOKING_PLUGIN_URL . "assets/css/booking_form.css"
		);
		wp_enqueue_script(
			'rbdobooking-appearance',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/appearance.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);
		do_action( 'rbdobooking_appearance_enqueue' );
	}

	/**
	 * Build the output for the plugin appearance page.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		$vendor_id = 1;
		$cart_enable_disabled = $this->db->getVendorSpecificSettingValueByName( "cart_enable", $vendor_id );
		$vendor_steps_configuration_with_template_name = $this->db->getVendorStepConfigurationWithTemplateName( $cart_enable_disabled, $vendor_id );
		$template = $vendor_steps_configuration_with_template_name["template"];
		$steps = explode(",",$vendor_steps_configuration_with_template_name["steps"]);
		$steps_data = array();
		$template_class = null;

		if ( $template == "default" ) {
			$template_class = RBDoBooking_Frontend_Default::instance(
				$steps,
				"",
				$vendor_id
			);
		}
		foreach ( $steps as $step ) {
			$steps_data[$step] = $template_class->prepare_step_content_with_dummy_data(
				$step
			);
		}

		$appearance_slidepanel = $this->view->get_view("appearance_ui_for_slide_panel");

		echo $this->view->get_view(
			"output",
			false,
			array(
				'steps'                     =>  $steps,
				'steps_data'                =>  $steps_data,
				'appearance_slidepanel'     =>  $appearance_slidepanel,
			)
		);

	}
}
new RBDoBooking_Appearance();
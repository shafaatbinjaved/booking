<?php

/*
 * Calendar class.
 * @since 1.0.0
 * */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';

class RBDobooking_Calendar {

    /**
     * @var View $view
     */
    private $view;

    /**
     * @var string $tplPath
     */
    private $tplPath;

	/**
	 * Holds instance of plugin db class
	 *
	 * @since 1.0.0
	 *
	 * @var RBDoBooking_DB
	 */
	private $db;

	/*
	 * Primary class constructor
	 * @since 1.0.0
	 * */
	public function __construct() {

		$this->tplPath = plugin_dir_path( __FILE__ ) . 'tpl/calendar/';
        $this->view = new View($this->tplPath);

		//Load Services page
		add_action('admin_init', array($this,'init'));
		add_action('wp_ajax_rbdobooking_calendar',array($this,'calendar_ajax_handler'));

		$this->db = RBDoBooking_DB::instance();
	}

	/**
	 * Ajax handler for all requests originating from calendar page
	 *
	 * @since 1.0.0
	 */
	public function calendar_ajax_handler() {

		check_ajax_referer('rbdobooking-calendar-nonce','security');

		$type = $_POST["type"];

		switch ($type) {
			case  "get_calendar_booking":
				$vendor_id = 1;
				$start = $_POST["start"];
				$start_formatted = date("Y-m-d H:i:s",strtotime($start));
				$end = $_POST["end"];
				$end_formatted = date("Y-m-d H:i:s",strtotime($end));

				$member_ids = $_POST["member_ids"];

				$result = null;
				$booked_slots = null;

				if ( $member_ids == "all" ) {
					$result = $this->db->getBookedTimeSlots( $vendor_id, $start_formatted, $end_formatted );
				}
				else {
					$result = $this->db->getBookedTimeSlots( $vendor_id, $start_formatted, $end_formatted, $member_ids );
				}

				if ( !is_null($result) ) {
					$booked_slots = $result["result"];
				}

				$final = array();

				foreach ( $booked_slots as $index => $booked_slot ) {
					$formatted_event_time = date("h:i a",strtotime($booked_slot["date_start"]));
					$formatted_event_time .= ' - ';
					$formatted_event_time .= date("h:i a",strtotime($booked_slot["date_end"]));
					$booked_slot["start"] = str_replace(" ","T",$booked_slot["date_start"])."-00:00";
					$booked_slot["end"] = str_replace(" ","T",$booked_slot["date_end"])."-00:00";
					$booked_slot["formatted_event_time"] = $formatted_event_time;
					$booked_slot["title"] = $booked_slot["service_name"] . " " . $booked_slot["member_full_name"];
					//$booked_slot["hmtlTitle"] = "<br> Shafaat Javed <br> temp".$booked_slot["id_time_slot"];//$booked_slot["id_time_slot"];
					//$booked_slot["description"] = "<br> Shafaat Javed <br> temp"+$booked_slot["id_time_slot"];
					$final[] = $booked_slot;
				}

				header('Content-Type: application/json');
				//die( json_encode($some) );
				die( json_encode($final) );
			case "get_calendar_form_data":
				$vendor_id = 1;
				$staff_members = $this->db->getVendorAllStaffMembersOnly( $vendor_id );
				$services = $this->db->getAllVendorServices( $vendor_id );
				$customers = $this->db->customer->getAllCustomersForVendor( $vendor_id );
				$return_data = array(
					"staff_members" =>  $staff_members,
					"services"      =>  $services,
					"customers"     =>  $customers
				);
				wp_send_json_success( $return_data );
				break;

			case "save_edit_booked_slot":

				$vendor_id = $_POST["vendor_id"];
				$service_id = $_POST["data"]["service_id"];
				$member_id = $_POST["data"]["member_id"];
				$date_start = $_POST["data"]["start_date"] . " " . $_POST["data"]["start_time"];
				$date_end = $_POST["data"]["end_date"] . " " . $_POST["data"]["end_time"];
				$status = $_POST["data"]["status"];
				$customer_ids = $_POST["data"]["customer_ids"];
				$creation_time = date("Y-m-d H:i:s");

				$booked_slot = array(
					"service_id"    =>  $service_id,
					"member_id"     =>  $member_id,
					"vendor_id"     =>  $vendor_id,
					"customer_id"   =>  $customer_ids[0],
					"timing_id"     =>  0,
					"date_start"    =>  $date_start,
					"date_end"      =>  $date_end,
					"is_booked"     =>  $status,
					"updated_at"    =>  $creation_time,
					"created_at"    =>  $creation_time
				);

				$booked_id_time_slot = $this->db->insert(
					"booked_time_slots",
					$booked_slot
				);

				$service_name_member_price_name = $this->db->getServiceMemberPriceNameAndServiceName(
					$member_id,
					$service_id
				);
				if ( $service_name_member_price_name["member_price"] == "0" ) {
					$service_name_member_price_name["member_price"] = $service_name_member_price_name["service_price"];
				}

				$order_item = array();
				$include_tax = $this->db->getVendorSpecificSettingValueByName( "include_tax", $this->vendor_id );
				$tax = null;
				$price_order_item = doubleval( $service_name_member_price_name["member_price"] );
				if ( $include_tax == "Enabled" ) {
					$tax = $this->db->getServiceTaxFromServiceID( $service_id );
					$order_item["tax_id"] = $tax["tax_id"];
					$order_item["price_with_tax"] = ((doubleval($tax["tax_percent"])/100) * $price_order_item)+$price_order_item;
					$order_item["price_without_tax"] = $price_order_item;
				}
				else {
					$order_item["tax_id"] = 0;
					$order_item["price_with_tax"] = $price_order_item;;
					$order_item["price_without_tax"] = $price_order_item;
				}
				$order_item["product_id"] = 0;
				$order_item["quantity"] = 1;
				$order_item["id_time_slot"] = $booked_id_time_slot;
				$order_item["created_at"] = $creation_time;

				$total_with_tax = $order_item["price_with_tax"];
				$total_without_tax = $order_item["price_without_tax"];

				$order = array(
					"payment_method_id" =>  1,//$this->userData["payment"]["payment_method"],
					"customer_id"       =>  $customer_ids[0],// todo : fix customer for multiple customers
					"total_without_tax" =>  $total_without_tax,
					"total_with_tax"    =>  $total_with_tax,
					"paid_status"       =>  0,
					"created_At"        =>  $creation_time
				);
				$order_id = $this->db->insert("orders",$order);
				$order_item["order_id"] = $order_id;
				$this->db->insert("order_items",$order_item);
		}
	}

	/*
	 * Determine if the user is viewing the calendars page
	 * @since 1.0.0
	 * */
	public function init() {

		// Check what page we are on.
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		// Only load if we are actually on the services page.
		if ( 'rbdobooking-calendar' === $page ) {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
			add_action( 'rbdobooking_admin_page', array( $this, 'output' ) );

			// Hook for addons.
			do_action( 'rbdobooking_services_init' );
		}
	}

	/**
	 * Enqueue assets for the calendar page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
		// Calendar admin script.
		wp_enqueue_script(
			'rbdobooking-bootstrap-select-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/js/bootstrap-select.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-select-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-select/css/bootstrap-select.css"
		);
		/*wp_enqueue_script(
			'rbdobooking-fullcalendar-moment-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/lib/moment.min.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);*/
		wp_enqueue_style(
			'rbdobooking-fullcalendar-bootstrap-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/bootstrap/main.min.css"
		);
		wp_enqueue_style(
			'rbdobooking-fullcalendar-core-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/core/main.min.css"
		);
		wp_enqueue_style(
			'rbdobooking-fullcalendar-daygrid-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/daygrid/main.min.css"
		);
		wp_enqueue_style(
			'rbdobooking-fullcalendar-timegrid-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/timegrid/main.min.css"
		);
		wp_enqueue_style(
			'rbdobooking-fullcalendar-list-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/list/main.min.css"
		);
		wp_enqueue_script(
			'rbdobooking-fullcalendar-core-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/core/main.min.js",
			array( 'jquery'),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script(
			'rbdobooking-fullcalendar-interaction-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/interaction/main.min.js",
			array( 'jquery','rbdobooking-fullcalendar-core-js'),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script(
			'rbdobooking-fullcalendar-bootstrap-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/bootstrap/main.min.js",
			array( 'jquery','rbdobooking-fullcalendar-core-js'),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script(
			'rbdobooking-fullcalendar-daygrid-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/daygrid/main.min.js",
			array( 'jquery','rbdobooking-fullcalendar-core-js'),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script(
			'rbdobooking-fullcalendar-timegrid-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/timegrid/main.min.js",
			array( 'jquery','rbdobooking-fullcalendar-core-js'),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_script(
			'rbdobooking-fullcalendar-list-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/packages/list/main.min.js",
			array( 'jquery','rbdobooking-fullcalendar-core-js'),
			RBDOBOOKING_VERSION,
			true
		);
		/*wp_enqueue_script(
			'rbdobooking-bootstrap-datepicker-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-datepicker-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"
		);*/
		wp_enqueue_script(
			'rbdobooking-bootstrap-daterangepicker-js',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/daterangepicker/daterangepicker.js",
			array( 'jquery','rbdobooking-topper','rbdobooking-bootstrap' ),
			RBDOBOOKING_VERSION,
			true
		);
		wp_enqueue_style(
			'rbdobooking-bootstrap-daterangepicker-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/daterangepicker/daterangepicker.css"
		);
		wp_enqueue_script(
			'rbdobooking-calendar',
			RBDOBOOKING_PLUGIN_URL . "assets/js/admin/calendar.js",
			array( 'jquery','rbdobooking-fullcalendar-core-js' ),
			RBDOBOOKING_VERSION,
			true
		);
		/*wp_enqueue_style(
			'rbdobooking-fullcalendar-css',
			RBDOBOOKING_PLUGIN_URL . "assets/plugins/fullcalendar/fullcalendar.css"
		);*/
		do_action( 'rbdobooking_calendar_enqueue' );
	}

	/**
	 * Build the output for the plugin calendar page.
	 *
	 * @since 1.0.0
	 */
	public function output() {

		$vendor_id = 1;

		$staff_members = $this->db->getVendorAllStaffMembersOnly( $vendor_id );

		$calendar_slidepanel = $this->view->get_view("calendar_ui_for_slide_panel");

		$advanced_filter_options = $this->view->get_view(
			"advanced_filter_options",
			false,
			array(
				'staff_members' =>  $staff_members,
				'services'      =>  $this->db->getAllVendorServices( $vendor_id ),
				'customers'     =>  $this->db->customer->getAllCustomersForVendor($vendor_id)
			)
		);

		echo $this->view->get_view(
			"output",
			false,
			array(
				'advanced_filter_options'   =>  $advanced_filter_options,
				'staff_members'             =>  $staff_members,
				'calendar_slidepanel'       =>  $calendar_slidepanel,
				'calendar_del_modal'        =>  "",

			)
		);

	}

}
new RBDobooking_Calendar();
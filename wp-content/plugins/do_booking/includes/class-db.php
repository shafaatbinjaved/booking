<?php
/**
 * User: shafaatbinjaved
 * Date: 4/14/2018
 * Time: 11:51 AM
 */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/db/class-customers-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/db/class-orders-db.php';

/**
 * RBDoBooking DB classs responsible for all plugin specific database functions
 *
 * @since 1.0.0
*/
class RBDoBooking_DB {



	/**
	 * One is the loneliest number that you'll ever do.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * plugin prefix
	 *
	 * @since 1.0.0
	 *
	 * @var string
	*/
	private $plugin_prefix = 'rbdobooking_';

	public $customer = '';
    /**
     * @var string|RBDoBooking_DB_Coupons $coupons
     */
	public $coupons = '';
	public $orders = '';

	public function __construct() {
		$this->customer = RBDoBooking_DB_Customers::instance();
		//$this->coupons = RBDoBooking_DB_Coupons::instance();
		$this->orders = RBDoBooking_DB_Orders::instance();
	}

	/**
	 * Insures that only one instance of RBDoBooking DB class exists in memory
	 *
	 * @since 1.0.0
	 *
	 * @return RBDoBooking_DB
	*/
	public static function instance() {

		if ( !isset( self::$instance ) && !( self::$instance instanceof RBDoBooking_DB ) ) {

			self::$instance = new RBDoBooking_DB();
			global $wpdb;
			self::$instance->plugin_prefix = $wpdb->prefix . self::$instance->plugin_prefix;

		} else {
			self::$instance;
		}
		return self::$instance;
	}

	/**
	 * To get specific service category
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 * @var bool
	 * @return bool|Array
	*/
	public function isCategoryExistsByName( $name, $checkOnly = false ) {

		global $wpdb;
		$sql = 'SELECT * 
					FROM ' . $this->plugin_prefix.'categories a 
					WHERE a.category = "'.$name.'"' ;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		if ( $checkOnly ) {
			if ( count($result) > 0 ) {
				return true;
			}
			return false;
		}
		return $result[0];
	}

	/**
	 * Get Service details
	 *
	 * @since 1.0.0
	 * @var int
	 * @return Array|null
	*/
	function getServiceDetailsByServiceId( $service_id ) {
		global $wpdb;

		$sql = 'SELECT *
					FROM '.$this->plugin_prefix.'services a
					WHERE a.service_id = ' . $service_id;

		$result = $wpdb->get_results($sql,'ARRAY_A');

		if ( $result ) {
			return $result;
		}
		return null;
	}

	/**
	 * Get all services
	 *
	 * @since 1.0.0
	 * @var String $columns column names
	 * @return Array
	*/
	public function getAllServices( $columns = '*', $order_by = false, $vendor_id = 1 ) {
		global $wpdb;

		$sql = 'SELECT 
					'.$columns.'
				FROM 
					'.$this->plugin_prefix.'services a
				WHERE 
					a.vendor_id = '.$vendor_id.'
					AND a.visibility = "public"' ;
		if ( $order_by ) {
			$sql .= ' ORDER BY a.category_id';
		}

		$result = $wpdb->get_results($sql,'ARRAY_A');

		if ( $result )
		{
			return $result;
		}
		return array();
	}

	/**
	 * Get all services
	 *
	 * @since 1.0.0
	 * @var int $member_id
	 * @var String $columns column names
	 * @var bool $order_by
	 * @return array
	 */
	public function getAllPossibleServicesWithCategoryNamesForMember( $member_id, $columns = '*', $order_by = false ) {
		global $wpdb;

		$sql = 'SELECT a.*,b.*,c.member_service_id, c.member_price, c.member_min_capacity, c.member_max_capacity
					FROM '.$this->plugin_prefix.'categories a
					LEFT JOIN '.$this->plugin_prefix.'services b
					ON b.category_id = a.category_id
					LEFT JOIN '.$this->plugin_prefix.'staff_member_services c
					ON c.service_id = b.service_id
					WHERE c.member_id = ' . $member_id;
		if ( $order_by ) {
			$sql .= ' ORDER BY b.service_id desc';
		}

		$result = $wpdb->get_results($sql,'ARRAY_A');

		if ( $result )
		{
			return $result;
		}
		return array();
	}

	/**
	 * Common insert method
	 *
	 * @since 1.0.0
	 *
	 * @return int|bool
	*/
	function insert( $table_name, $data ) {
		global $wpdb;

		$result = $wpdb->insert(
			$this->plugin_prefix.$table_name,
			$data
		);

		if ( $result ) {
			return $wpdb->insert_id;
		}
		return false;
	}

	/**
	 * Update temp form data
	 *
	 * @since 1.0.0
	*/
	function updateTempFormData( $form_id, $form_data ) {
		global $wpdb;

		$where = array(
			"form_id"   =>  $form_id
		);
		$form_data = array(
			"data"          =>  $form_data,
			"updated_at"    =>  date("Y-m-d H:i:s")
		);

		$return_value = $wpdb->update(
			$this->plugin_prefix."temp_form_data",
			$form_data,
			$where
		);
		if ( $return_value ) {
			return true;
		}
		return false;
	}

	/**
	 * Get temp form data
	 *
	 * @since 1.0.0
	*/
	function getTempFormData( $form_id ) {
		global $wpdb;

		$sql = 'SELECT *
				FROM '.$this->plugin_prefix.'temp_form_data a
				WHERE a.form_id = "'.$form_id.'"';

		$result = $wpdb->get_results($sql,'ARRAY_A');

		foreach ( $result as $row ) {
			return $row;
		}

		return array();
	}

	/**
	 * check form exists or not
	 *
	 * @since 1.0.0
	 *
	 * @return bool
	*/
	function checkFormIDExistsOrNot( $form_id ) {
		global $wpdb;

		$sql = 'SELECT count(1) as count
				FROM '.$this->plugin_prefix.'temp_form_data a
				WHERE a.form_id = "'.$form_id.'"';

		$result = $wpdb->get_results($sql,'ARRAY_A');

		return ( intval($result[0]["count"]) > 0 ) ? true : false ;
	}

	/**
	 * Start transaction
	 *
	 * @since 1.0.0
	*/
	function startTransaction() {
		global $wpdb;
		$wpdb->query("START TRANSACTION");
	}

	/**
	 * Commit transaction
	 *
	 * @since 1.0.0
	 */
	function commitTransaction() {
		global $wpdb;
		$wpdb->query("COMMIT");
	}

	/**
	 * Rollback transaction
	 *
	 * @since 1.0.0
	 */
	function rollbackTransaction() {
		global $wpdb;
		$wpdb->query("ROLLBACK");
	}

	/**
	 * Get vendor all templates
	 *
	 * @since 1.0.0
	*/
	function getVendorAllTemplates( $vendorId ) {
		global $wpdb;
		$sql = 'SELECT * 
					FROM '.$this->plugin_prefix.'templates a
					WHERE a.vendor_id = ' . $vendorId;

		return $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	/**
	 * Common update method
	 *
	 * @since 1.0.0
	 *
	 * @return int|bool
	*/
	function update( $table_name, $data, $where ) {
		global $wpdb;

		return $wpdb->update(
			$this->plugin_prefix.$table_name,
			$data,
			$where
		);
	}

	/**
	 * Get all active payment methods
	 *
	 * @since 1.0.0
	 * @return array
	**/
	function getPaymentMethods() {
		global $wpdb;

		$sql = 'SELECT *
				FROM '.$this->plugin_prefix.'payment_methods a
				WHERE a.is_active = 1
				ORDER BY a.sort ASC';

		return $wpdb->get_results( $sql , 'ARRAY_A');
	}

	/**
	 * Get service tax from service_id
	 *
	 * @since 1.0.0
	 * @var int $service_id
	 * @return array()
	*/
	function getServiceTaxFromServiceID( $service_id ) {
		global $wpdb;

		$sql = 'SELECT 
					b.*
				FROM '.$this->plugin_prefix.'services a
				INNER JOIN '.$this->plugin_prefix.'tax b
				ON a.service_id = ' . $service_id . '
				AND a.tax_id = b.tax_id';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $row ) {
			return $row;
		}
		return array();
	}

	/**
	 * Get service name, Member name and service member price
	 *
	 * @since 1.0.0
	*/
	function getServiceMemberPriceNameAndServiceName( $member_id, $service_id ) {
		global $wpdb;

		$sql = 'SELECT a.service_id, a.member_id, a.member_price, c.name as service_name, 
					CONCAT(b.first_name, " ", b.last_name) as member_full_name,
					((d.tax_percent/100)*a.member_price) as tax_amount,
					(((d.tax_percent/100)*a.member_price)+a.member_price) as member_price_with_tax,
					d.tax_percent
					FROM '.$this->plugin_prefix.'staff_member_services a
				INNER JOIN '.$this->plugin_prefix.'staff_members b
					ON a.member_id = '.$member_id.'
					AND a.service_id = '.$service_id.'
					AND b.member_id = a.member_id
				INNER JOIN '.$this->plugin_prefix.'services c
					ON c.service_id = a.service_id
				LEFT JOIN '.$this->plugin_prefix.'tax d
                    ON d.tax_id = c.tax_id';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $row ) {
			return $row;
		}
		return null;
	}

	/**
	 * Common delete method
	 *
	 * @since 1.0.0
	 * @return bool|int
	*/
	/*function delete( $table_name, $column_name, $id ) {
		global $wpdb;
		$sql = 'DELETE FROM ' . $this->plugin_prefix.$table_name. ' WHERE ('.$column_name.'='.$id.')';
		return $wpdb->query( $sql );
	}*/

	/**
	 * Delete all member breaks
	 *
	 * @since 1.0.0
	 * @var int $member_id
	 * @return bool|int
	*/
	function delete_member_breaks_for_day( $member_id, $day ) {
		global $wpdb;

		$sql = 'DELETE b.* 
					FROM '.$this->plugin_prefix.'staff_member_timing a
				  	INNER JOIN '.$this->plugin_prefix.'staff_member_breaks b
					ON b.timing_id = a.timing_id
					AND a.day = '.$day.'
					AND a.member_id = ' . $member_id;
		return $wpdb->query( $sql );
	}

	/**
	 * Get member timings
	 *
	 * @since 1.0.0
	 *
	 * @var int member_id
	 * @return array|null
	*/
	function getMemberTimingsWithBreaks( $member_id ) {
		global $wpdb;

		$sql = 'SELECT a.*, b.break_id, b.break_start, b.break_stop
					FROM '.$this->plugin_prefix.'staff_member_timing a
					LEFT JOIN '.$this->plugin_prefix.'staff_member_breaks b
					ON b.timing_id = a.timing_id 
					WHERE a.member_id = ' . $member_id;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		if ( $result ) {
			foreach ( $result as $index => $row ) {
				if ( !is_null($row["break_id"]) ) {
					$result[$index]['break_start_ui_val'] = date("g:i a",strtotime($row["break_start"]));
					$result[$index]['break_stop_ui_val'] = date("g:i a",strtotime($row["break_stop"]));
				}
			}
			return $result;
		}
		return null;
	}

	/**
	 * get all member holidays
	 *
	 * @since 1.0.0
	 *
	 * @var int member_id
	 * @return array
	*/
	function getMemberHolidays( $member_id ) {
		global $wpdb;

		$sql = 'SELECT *
					FROM '.$this->plugin_prefix.'staff_member_holidays a
					WHERE a.member_id = ' . $member_id;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		if ( $result ) {
			return $result;
		}
		return array();
	}

	/**
	 * Edit category
	 *
	 * @var String $name
	 * @var int $category_id
	 * @return bool|int
	*/
	function editCategoryByCategoryID( $name, $category_id ) {
		global $wpdb;

		$data_array = array(
			"category"   =>  $name
		);

		$where = array( "category_id" => $category_id );

		return $wpdb->update(
			$this->plugin_prefix."categories",
			$data_array,
			$where
		);
	}


	/**
	 * Delete service with service_id
	 *
	 * @var int $service_id
	 * @return bool|int
	*/
	function deleteServiceByServiceID( $service_id ) {
		global $wpdb;

		$query = null;

		if ( strpos($service_id,',') == false ) {
			$query = 'DELETE
						FROM '.$this->plugin_prefix.'services
						WHERE service_id = '.$service_id;
		}
		else {
			$query = 'DELETE
						FROM '.$this->plugin_prefix.'services 
						WHERE service_id IN ('.$service_id.')';
		}

		return $wpdb->query($query);
	}

	/**
	 * Delete category by category_id
	 *
	 * @var int $category_id
	 * @return bool|int
	*/
	function deleteCategoryByCategoryID( $category_id ) {
		global $wpdb;

		$data_array = array(
			"category_id" => $category_id
		);

		return $wpdb->delete($this->plugin_prefix.'categories',$data_array);
	}

	/**
	 * Save new service
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 * @return bool|int
	*/
	public function insertNewService( $service ) {
		global $wpdb;

		$data_array = array();

		$service_member = null;

		foreach ( $service as $index => $attribute ) {
			if ( isset( $attribute["name"] ) ) {
				if ( $attribute["name"] == "member_id" ) {
					$service_member = $attribute["value"];
				}
				else {
					$data_array[$attribute["name"]] = $attribute["value"];
				}
			}
		}
		$data_array['created_at'] = date("Y-m-d H:i:s");

		$result = $wpdb->insert(
			$this->plugin_prefix.'services',
			$data_array
		);

		if ( $result ) {
			$service_id = $wpdb->insert_id;
			foreach ( $service_member as $member ) {
				$service_member_data = array(
					"service_id"    =>  $service_id,
					"member_id"     =>  $member,
					"created_at"    =>  date("Y-m-d H:i:s")
				);

				$result = $wpdb->insert(
					$this->plugin_prefix."staff_member_services",
					$service_member_data
				);
			}
			return $service_id;
		}
		return false;
	}

	/**
	 * Delete staff member services
	 *
	 * @since 1.0.0
	 *
	 * @var int service_id
	 * @return bool
	*/
	public function deleteServiceMembersByServiceID( $service_id ) {
		global $wpdb;

		$sql = 'DELETE FROM '.$this->plugin_prefix.'staff_member_services
					WHERE (service_id = "'.$service_id.'")';

		$result = $wpdb->get_results( $sql );

		return $result;
	}

	/**
	 * Update existing service
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 * @var int
	 * @return bool|int
	 */
	public function updateService( $service, $service_id ) {
		global $wpdb;

		$data_array = array();

		foreach ( $service as $index => $attribute ) {

			if ( isset($attribute["name"]) ) {
				if ( $attribute["name"] == "member_id" ) {

					$this->deleteServiceMembersByServiceID( $service_id );

					foreach ( $attribute["value"] as $member ) {
						$service_member_data = array(
							"service_id"    =>  $service_id,
							"member_id"     =>  $member,
							"created_at"    =>  date("Y-m-d H:i:s")
						);

						$result = $wpdb->insert(
							$this->plugin_prefix."staff_member_services",
							$service_member_data
						);
					}

				}
				else {
					$data_array[$attribute["name"]] = $attribute["value"];
				}
			}
		}

		$where = array(
			"service_id" => $service_id
		);

		$result = $wpdb->update(
			$this->plugin_prefix.'services',
			$data_array,
			$where
		);

		return $result;
	}

	/**
	 * Get service members
	 *
	 * @since 1.0.0
	 *
	 * @var int $service_id
	 * @var int $vendor_id
	 * @var int|null $limit
	 * @var boolean $rand
	 * @return array
	 */
	public function getServiceMembers( $service_id, $vendor_id = 1, $limit = null, $rand = false ) {
		global $wpdb;

		$sql = 'SELECT 
					b.*
				FROM '.$this->plugin_prefix.'staff_members a 
				INNER JOIN '.$this->plugin_prefix.'staff_member_services b
					ON a.vendor_id = '.$vendor_id.'
					AND a.member_id = b.member_id
					AND b.service_id = '.$service_id;
		if ( $rand ) {
			$sql .= ' ORDER BY rand() ';
		}
		if ( $limit != null ) {
			$sql .= ' LIMIT ' . $limit;
		}

		$result = $wpdb->get_results(  $sql, ARRAY_A );

		if ( $result )
		{
			return $result;
		}
		return array();
	}

	/**
	 * Save new service category
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 * @return bool|int
	 */
	public function insertNewCategory( $name ) {
		global $wpdb;

		$data_array = array(
			'category'  =>  $name,
			'created_at'    =>  date('Y-m-d H:i:s')
		);

		$result = $wpdb->insert(
			$this->plugin_prefix.'categories',
			$data_array
		);

		if ( $result ) {
			return $wpdb->insert_id;
		}
		return false;

	}

	/**
	 * Get all service categories
	 *
	 * @since 1.0.0
	 *
	 * @return array
	*/
	public function getAllServiceCategories() {
		global $wpdb;

		$sql = 'SELECT a.category_id, a.category as "name", count(1) as count, b.service_id
					FROM '.$this->plugin_prefix.'categories a
					LEFT JOIN '.$this->plugin_prefix.'services b
					ON b.category_id = a.category_id
					GROUP BY a.category_id';

		$result = $wpdb->get_results( $sql, ARRAY_A );

		if ( $result )
		{
			return $result;
		}
		return array();
	}

	/**
	 * Get staff member data
	 *
	 * @since 1.0.0
	 *
	 * @var int $member_id
	 * @return array()
	*/
	function getStaffMemberData( $member_id ) {
		global $wpdb;

		$sql = 'SELECT *
					FROM '.$this->plugin_prefix.'staff_members a
					WHERE a.member_id = ' . $member_id;

		$result = $wpdb->get_results( $sql, ARRAY_A );

		if ( $result ) {
			return $result;
		}
		return null;
	}

	/**
	 * Delete specific holiday of member
	 *
	 * @since 1.0.0
	 *
	 * @var int $member_id
	 * @var string $date
	 * @return bool|int
	*/
	function deleteSpecificMemberHoliday( $member_id, $date ) {
		global $wpdb;

		$sql = 'DELETE FROM '.$this->plugin_prefix.'staff_member_holidays
					WHERE (member_id = "'.$member_id.'" AND date = "'.$date.'")';

		$result = $wpdb->get_results( $sql );

		return $result;
	}

	/**
	 * Save member holiday
	 *
	 * @since 1.0.0
	 *
	 * @var int $member_id
	 * @var string $date
	 * @var int $repeat_every_year
	 * @return int|bool
	*/
	function saveMemberHoliday( $member_id, $date, $repeat_every_year ) {
		global $wpdb;

		$data_array = array(
			"member_id"         =>  $member_id,
			"date"              =>  $date,
			"repeat_every_year" =>  $repeat_every_year,
			"created_at"        => date("Y-m-d H:i:s")
		);

		$result = $wpdb->insert(
			$this->plugin_prefix.'staff_member_holidays',
			$data_array
		);

		if ( $result ) {
			return $wpdb->insert_id;
		}
		return false;
	}

	/**
	 * update member holiday
	 *
	 * @since 1.0.0
	 *
	 * @var array $data
	 * @var array $where
	 * @return bool|int
	*/
	function updateMemberHoliday( $data, $where ) {
		global $wpdb;

		$result = $wpdb->update(
			$this->plugin_prefix."staff_member_holidays",
			$data,
			$where
		);

		return $result;
	}

	/**
	 * check is member holiday exists
	 *
	 * @since 1.0.0
	 *
	 * @var int $member_id
	 * @var string $date
	 * @return bool|holiday_id
	*/
	function isMemberHolidayExists( $member_id, $date ) {
		global $wpdb;

		$sql = 'SELECT *
					FROM '.$this->plugin_prefix.'staff_member_holidays a
					WHERE a.member_id = '.$member_id.'
					AND a.date = "'.$date.'"';

		$result = $wpdb->get_results( $sql, "ARRAY_A" );

		if ( $result ) {
			return $result[0]["holidays_id"];
		}
		return false;
	}

	/**
	 * Get all staff members
	 *
	 * @since 1.0.0
	 *
	 * @var string $columns
	 * @return array
	*/
	function getAllStaffMembers( $columns = '*' ) {
		global $wpdb;

		$sql = 'SELECT '.$columns.'
					FROM '.$this->plugin_prefix.'staff_members a
					LEFT JOIN '.$this->plugin_prefix.'staff_member_services b
					ON b.member_id = a.member_id
					AND a.visibility = "public"
					LEFT JOIN '.$this->plugin_prefix.'services c
					ON c.service_id = b.service_id
					GROUP BY a.member_id';

		return $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	function getVendorAllStaffMembersOnly( $vendor_id = 1 ) {
		global $wpdb;

		$sql = 'SELECT *
				FROM '.$this->plugin_prefix.'staff_members a
				WHERE a.vendor_id = '.$vendor_id.' 
				AND a.is_active = 1';

		return $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	function getStaffMember( $member_id, $columns = '*' ) {
		global $wpdb;

		$sql = 'SELECT '.$columns.'
					FROM '.$this->plugin_prefix.'staff_members a
					LEFT JOIN '.$this->plugin_prefix.'staff_member_services b
					ON b.member_id = a.member_id
					LEFT JOIN '.$this->plugin_prefix.'services c
					ON c.service_id = b.service_id
					WHERE a.member_id = ' . $member_id;
		return $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	function getVendorStaffMembersWithNamePicAndID( $vendor_id = 1 ) {
		global $wpdb;

		$sql = 'SELECT a.member_id, a.first_name, a.last_name, a.profile_pic
				FROM '.$this->plugin_prefix.'staff_members a
				WHERE a.vendor_id = '. $vendor_id .'
				AND a.is_active = 1';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );


		return $result;
	}

	/**
	 * Get staff member timings and breaks
	 *
	 * @since 1.0.0
	 *
	 * @var int member_id
	 * @return array
	*/
	function getSatffMemberTimingAndBreaks( $member_id ) {
		global $wpdb;

		$sql = 'SELECT a.*, 
					GROUP_CONCAT(b.break_id) as break_id, 
					GROUP_CONCAT(b.break_start) as break_start, 
					GROUP_CONCAT(b.break_stop) as break_stop
				FROM '.$this->plugin_prefix.'staff_member_timing a
				LEFT JOIN '.$this->plugin_prefix.'staff_member_breaks b
					ON b.timing_id = a.timing_id
				WHERE a.member_id = '.$member_id.'
				GROUP BY a.timing_id';

		return $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	/**
	 * Delete staff member
	 *
	 * @since 1.0.0
	*/
	function deleteStaffMembers( $member_id ) {
		global $wpdb;

		$wpdb->delete(
			$this->plugin_prefix.'staff_members',
			array(
				"member_id" =>  $member_id
			)
		);
		$wpdb->delete(
			$this->plugin_prefix.'staff_member_holidays',
			array(
				"member_id" =>  $member_id
			)
		);
		$sql = 'DELETE a.*, b.*
					FROM '.$this->plugin_prefix.'staff_member_timing a
					LEFT JOIN '.$this->plugin_prefix.'staff_member_breaks b
					ON b.timing_id = a.timing_id
					WHERE a.member_id = ' . $member_id;
		$wpdb->query( $sql );
	}

	/**
	 * Get template data
	*/
	function getCustomFieldTemplateData( $template_id ) {
		global $wpdb;

		$sql = 'SELECT a.*, b.template_field_id, b.type, 
					b.value, b.is_list, b.list_id, b.sort, b.is_required
					FROM '.$this->plugin_prefix.'templates a
					INNER JOIN '.$this->plugin_prefix.'template_fields b
					ON a.template_id = '.$template_id.'
					AND b.template_id = a.template_id
					ORDER BY b.sort ASC';

		return $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	/**
	 * Get List data
	*/
	function getListItems( $list_id ) {
		global $wpdb;

		$sql = 'SELECT a.*, b.item_id, b.item 
					FROM '.$this->plugin_prefix.'lists a
					INNER JOIN '.$this->plugin_prefix.'list_items b
					ON a.list_id = ' . $list_id . '
					AND a.list_id = b.list_id
					ORDER BY b.sort ASC';

		return $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	/**
	 * Delete template field ids
	*/
	function deleteCustomFieldTemplateFieldIds( $template_id, $template_field_ids ) {
		global $wpdb;

		$sql = 'DELETE a.*
					FROM '.$this->plugin_prefix.'template_fields a
					WHERE a.template_id = '.$template_id.'
					AND a.template_field_id NOT IN ('.$template_field_ids.')';

		return $wpdb->query( $sql );
	}

	/**
	 * Delete list items
	*/
	function deleteListItems($list_id,$item_ids) {
		global $wpdb;

		$sql = 'DELETE a.*
					FROM '.$this->plugin_prefix.'list_items a
					WHERE a.list_id = ' . $list_id . ' 
					AND a.item_id NOT IN ('.$item_ids.')';

		return $wpdb->query( $sql );
	}

	/**
	 * Count of List items
	*/
	function countListItems( $list_id ) {
		global $wpdb;

		$sql = 'SELECT count(1) as count
					FROM '.$this->plugin_prefix.'list_items a
					WHERE a.list_id = ' . $list_id;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $index => $row ) {
			return $row["count"];
		}
		return -1;
	}

	/**
	 * Delete list
	*/
	function deleteList( $list_id ) {
		global $wpdb;

		$sql = 'DELETE a.*
					FROM '.$this->plugin_prefix.'lists a
					WHERE a.list_id = ' . $list_id;

		return $wpdb->query( $sql );
	}

	/**
	 * Get all vendor services
	*/
	function getAllVendorServices( $vendor_id ) {
		global $wpdb;

		$sql = 'SELECT *
					FROM '.$this->plugin_prefix.'services a
					WHERE a.vendor_id = ' . $vendor_id.'
					AND a.is_active = 1';

		return $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	/**
	 * Get all public services with category names
	*/
	function getPublicServicesWithCategoryNameAndStaffMemberName() {
		global $wpdb;

		$sql = 'SELECT 
					a.*, b.category, c.first_name, c.last_name, c.member_id
				FROM '.$this->plugin_prefix.'services a 
				INNER JOIN '.$this->plugin_prefix.'categories b 
					ON a.category_id = b.category_id 
					AND a.visibility = "public" 
				INNER JOIN '.$this->plugin_prefix.'staff_member_services d
					ON d.service_id = a.service_id
				INNER JOIN '.$this->plugin_prefix.'staff_members c 
					ON c.member_id = d.member_id 
				ORDER BY a.sort ASC;';

		return $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	/**
	 * Get vendor step configuration
	*/
	function getVendorStepConfigurationWithTemplateName( $cart_enable_disable, $vendor_id = 1 ) {
		global $wpdb;

		$sql = 'SELECT a.*
					FROM '.$this->plugin_prefix.'vendor_step_configuration a
					WHERE a.vendor_id = ' . $vendor_id.'
					AND a.show_cart = "'.$cart_enable_disable.'"';

		$step_configuration = $wpdb->get_results( $sql, 'ARRAY_A');

		if ( count($step_configuration) > 0 ) {
			return $step_configuration[0];//explode(",",$step_configuration[0]["steps"]);
		}
		return null;
	}

	/**
	 * get available time slots
	*/
	function getAvailableTimeSlots($data) {
		global $wpdb;

		$sql = 'SELECT 
					*
				FROM '.$this->plugin_prefix.'staff_member_timing a
				INNER JOIN '.$this->plugin_prefix.'staff_member_services b
					ON b.service_id = '.$data["service_id"].'
					AND b.member_id = '.$data["member_id"].'
				INNER JOIN '.$this->plugin_prefix.'services c
					ON c.service_id = b.service_id
					AND b.member_id = a.member_id
					AND a.is_day_on = 1
					AND a.vendor_id = '.$data["vendor_id"].'
					AND ( "'.$data["start_time"].'" BETWEEN a.start_time AND a.end_time )
					AND ( "'.$data["end_time"].'" BETWEEN a.start_time AND a.end_time )
				LEFT JOIN '.$this->plugin_prefix.'booked_time_slots z
					ON z.timing_id = a.timing_id';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );
	}

	function getVendorMemberTiming( $data ) {
		global $wpdb;

		$sql = 'SELECT 
					a.*, 
					GROUP_CONCAT(a.member_id) as member_ids,
					GROUP_CONCAT(a.timing_id) as timing_ids
				FROM '.$this->plugin_prefix.'staff_member_timing a
				WHERE a.vendor_id = '.$data["vendor_id"] .'
				AND a.member_id IN ('.implode(",",$data["member_id"]).')
				AND a.day IN ( '.implode(",",$data["day"]).' )
					AND a.is_day_on = 1
					GROUP BY a.start_time,a.end_time, a.day
				ORDER BY a.day ASC';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

	function getServiceNameByServiceID( $service_id ) {
		global $wpdb;

		$sql = 'SELECT 
					a.*
				FROM '.$this->plugin_prefix.'services a
				WHERE a.service_id = ' . $service_id;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $row ) {
			return $row["name"];
		}

		return "";
	}

	function slotIsAvailableOrNot( $slot_start, $slot_end, $timing_id, $member_id, $vendor_id ) {
		global $wpdb;

		$sql = 'SELECT 
					count(1) as count 
				FROM 
					'.$this->plugin_prefix.'booked_time_slots a
				WHERE 
					a.vendor_id = '.$vendor_id.'
					AND a.member_id = '.$member_id.'
					AND a.timing_id = '.$timing_id.'
					AND a.date_start = "'.$slot_start.'"
					AND a.date_end = "'.$slot_end.'"';

		$result = $wpdb->get_results( $sql, 'ARRAY_A');

		foreach ( $result as $row ) {
			return intval($row["count"]);
		}
		return 0;
	}

	function getServiceDuration( $service_id ) {
		global $wpdb;

		$sql = 'SELECT 
					*
				FROM '.$this->plugin_prefix.'services a
				WHERE
					a.service_id = ' . $service_id;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $row ) {
			return intval($row["duration"])+intval($row["padding_before"])+intval($row["padding_after"]);
		}

		return null;
	}

	function getVendorOrServiceSpecificTemplateFields( $vendor_id, $service_id ) {
		global $wpdb;

		$template_id = $this->getVendorOrServiceSpecificTemplate( $vendor_id, $service_id );
		$template_fields = null;

		if ( is_null($template_id) ) {
			$sql = 'SELECT *
					FROM '.$this->plugin_prefix.'templates a
					WHERE a.vendor_id = '.$vendor_id.'
					AND a.is_service_specific = 0
					AND a.is_active = 1;';
			$result = $wpdb->get_results( $sql, 'ARRAY_A' );

			foreach ( $result as $row ) {
				$template_fields = $this->getCustomFieldTemplateData( $row["template_id"] );
			}
		}
		else {
			$template_fields = $this->getCustomFieldTemplateData( $template_id );
		}
		return $template_fields;
	}

	function isVendorServiceOrOnlyVendorSpecificTemplateExists( $vendor_id, $service_id ) {
		global $wpdb;

		$sql = 'SELECT count(1) as count
				FROM '.$this->plugin_prefix.'templates a
				WHERE a.vendor_id = '.$vendor_id.'
				AND a.service_id = '.$service_id.'
				AND a.is_active = 1';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $row ) {
			if ( intval($row["count"]) >= 1 ) {
				return 2;//vendor service specific template
			}
			else {
				$sql = 'SELECT count(1) as count
						FROM '.$this->plugin_prefix.'templates a
						WHERE a.vendor_id = '.$vendor_id.'
						AND a.is_active = 1';

				$result_for_vendors = $wpdb->get_results( $sql, 'ARRAY_A' );

				foreach ( $result_for_vendors as $row_vendor ) {
					if ( intval($row_vendor["count"]) >= 1 ) {
						return 1;//vendor specific template
					}
				}
			}
		}
		return 0;//no template available
	}

	function disableAllOtherVendorTemplatesExceptTemplateID( $template_id, $vendor_id ) {
	    global $wpdb;

	    $sql = 'UPDATE '.$this->plugin_prefix.'templates a
                SET a.is_active = 0
                WHERE a.vendor_id = '.$vendor_id.'
                AND a.template_id != '.$template_id;

	    $result = $wpdb->query( $sql );

	    return $result;
    }

	function getVendorOrServiceSpecificTemplate( $vendor_id, $service_id ) {
		global $wpdb;

		$sql = 'SELECT *
				FROM '.$this->plugin_prefix.'templates a
				WHERE a.vendor_id = '.$vendor_id.'
				AND a.service_id = '.$service_id.'
				AND a.is_active = 1';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $row ) {
			return $row['template_id'];
		}
		return null;
	}

	function getMemberServices() {

	}

	function getVendorStartStopTimings( $vendor_id = 1 ) {
		global $wpdb;

		$sql = 'SELECT
					GROUP_CONCAT(b.value) as group_value
				FROM '.$this->plugin_prefix.'settings a
				INNER JOIN '.$this->plugin_prefix.'vendor_specific_settings b
					ON a.type = "business_hours"
					AND ( a.name = "time_start" OR a.name = "time_stop")
					AND b.vendor_id = '.$vendor_id.'
					AND b.settings_id = a.settings_id';

		$result = $wpdb->get_results( $sql, "ARRAY_A" );

		foreach ( $result as $row ) {
			return explode(",",$row["group_value"]);
		}

		return null;
	}

	function getBookedTimeSlots( $vendor_id = 1, $start, $end, $member_ids = null,
		$service_ids = null, $customer_ids = null, $status = null,
		$creation_time_start = null, $creation_time_end = null,
		$limit = 10, $offset = 0, $search = null ) {
		global $wpdb;

		$select = array(
			'a.*,a.member_id as resourceId',
			'CONCAT(b.first_name," ",b.LAST_NAME) as member_full_name',
			'c.full_name as customer_full_name, c.email, c.phone',
			'd.name as service_name, d.color',
			'o.order_id, o.paid_status',
			'o.paid_stamp, SUM(payment.amount) as total_amount',
			'pay_method.name as payment_method_name'
		);

		$left_join = array();
		$order_by = '';

		$sql = 'SELECT 
				  SQL_CALC_FOUND_ROWS
				  '.implode(", ",$select).'
				FROM '.$this->plugin_prefix.'booked_time_slots a
				INNER JOIN '.$this->plugin_prefix.'staff_members b ON a.vendor_id = '.$vendor_id;
		if ( !empty($member_ids) ) {
			$sql .= ' AND a.member_id IN ('.implode(",",$member_ids).') ';
		}
		if ( !empty($service_ids) ) {
			$sql .= ' AND a.service_id IN ('.implode(",",$service_ids).') ';
		}
		if ( !empty($customer_ids) ) {
			$sql .= ' AND a.customer_id IN ('.implode(",",$customer_ids).') ';
		}
		if ( !empty($status) ) {
			$sql .= ' AND a.is_booked IN ('.implode(",",$status).') ';
		}
		$sql .= ' AND a.date_start >= "'.$start.'"
				 AND a.date_end <= "'.$end.'"
				 AND a.member_id = b.member_id
				 INNER JOIN '.$this->plugin_prefix.'customers c
				 ON c.customer_id = a.customer_id 
				 INNER JOIN '.$this->plugin_prefix.'services d
				 ON d.service_id = a.service_id
				 LEFT JOIN 
				    '.$this->plugin_prefix.'order_items o_item
				 ON
				    o_item.id_time_slot = a.id_time_slot
				 LEFT JOIN
				    '.$this->plugin_prefix.'orders o
				 ON
				    o.order_id = o_item.order_id
				 LEFT JOIN 
				    '.$this->plugin_prefix.'payments payment
				 ON 
				    payment.payment_id = o.payment_id
				 LEFT JOIN 
				    '.$this->plugin_prefix.'payment_methods pay_method
				 ON 
				    pay_method.payment_method_id = payment.payment_method_id
				 GROUP BY
				    a.id_time_slot ';
		if ( !empty($search) ) {
			$sql .= 'HAVING 
						customer_full_name LIKE "%'.$search.'%"
					OR
						member_full_name LIKE "%'.$search.'%"
					OR
						service_name LIKE "&'.$search.'&"';
		}
		$sql .= '
				 ORDER BY 
				    a.id_time_slot DESC
				 LIMIT ' . $limit.'
				 OFFSET ' . $offset;

		$result = $wpdb->get_results( $sql, "ARRAY_A" );
		$found_rows = 0;
		$temp = $wpdb->get_results("SELECT FOUND_ROWS() as FOUND_ROWS","ARRAY_A");
		foreach ( $temp as $row ) {
			$found_rows = $row["FOUND_ROWS"];
		}
		$return_value = array(
			"result"        =>  $result,
			"found_rows"    =>  $found_rows
		);

		return $return_value;
	}

	function getVendorSettings( $view_type, $vendor_id ) {
		global $wpdb;

		$sql = 'SELECT a.settings_id, a.type, a.name, a.view_type, a.is_list, a.list_id,
				b.vendor_settings_id, b.vendor_id, b.value, b.updated_at, b.created_at
				FROM '.$this->plugin_prefix.'settings a
				LEFT JOIN '.$this->plugin_prefix.'vendor_specific_settings b
				ON b.settings_id = a.settings_id
				AND b.vendor_id = '.$vendor_id.'
				WHERE a.type = "'.$view_type.'"';

		$result = $wpdb->get_results( $sql, "ARRAY_A" );

		$date = date("Y-m-d H:i:s");

		foreach ( $result as $index => $row ) {
			if ( $row["vendor_settings_id"] == null ) {

				$vendor_settings_row = array(
					"settings_id"   =>  $row["settings_id"],
					"vendor_id"     =>  $vendor_id,
					"value"         =>  "",
					"updated_at"    =>  $date,
					"created_at"    =>  $date

				);
				$vendor_settings_id = $this->insert(
					"vendor_specific_settings",
					$vendor_settings_row
				);
				if ( $vendor_settings_id !== false ) {
					$result[$index]["vendor_settings_id"] = $vendor_settings_id;
					$result[$index]["settings_id"] = $vendor_settings_id;
					$result[$index]["vendor_id"] = $vendor_id;
					$result[$index]["value"] = $vendor_settings_row["value"];
					$result[$index]["updated_at"] = $date;
					$result[$index]["created_at"] = $date;
				}
			}
			if ( $row["is_list"] == "1" ) {
				$list_items = $this->getListItems( $row["list_id"] );
				$result[$index]["list_items"] = $list_items;
			}
		}

		return $result;
	}

	/**
	 * Get Vendor specific settings by settings name
	 *
	 * @param string $settings_name
	 * @param int $vendor_id
	 * @param string $type
	 * @return string $value
	*/
	function getVendorSpecificSettingValueByName( $settings_name, $vendor_id = 1, $type = '' ) {
		global $wpdb;

		$sql = 'SELECT 
					*
				FROM '.$this->plugin_prefix.'settings a
				INNER JOIN '.$this->plugin_prefix.'vendor_specific_settings b
				ON a.name = "'.$settings_name.'"
				AND b.settings_id = a.settings_id
				AND b.vendor_id = '.$vendor_id;
		if ($type !== '') {
		    $sql .= ' AND a.type = "' . $type .'"';
        }

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );
		$value = null;

		foreach ( $result as $row ) {
			$value = $row["value"];

			if ( $row["is_list"] == "1" ) {

				$sql2 = 'SELECT a.item
						FROM '.$this->plugin_prefix.'list_items a
						WHERE a.item_id = '.$value.'
						AND a.list_id = '.$row["list_id"];

				$list_value_result = $wpdb->get_results( $sql2, 'ARRAY_A' );

				if ( count($list_value_result) > 0 ) {
					$value = $list_value_result[0]["item"];
				}
			}
		}
		return $value;
	}

	function getBookedTimeSlotBySlotID( $id_time_slot ) {
		global $wpdb;

		$sql = 'SELECT 
					a.*,b.full_name, b.email, b.phone
				FROM 
					'.$this->plugin_prefix.'booked_time_slots a
				INNER JOIN 
					'.$this->plugin_prefix.'customers b
				ON
					a.id_time_slot = ' . $id_time_slot .'
					AND a.customer_id = b.customer_id
				';

		$result = $wpdb->get_results( $sql, "ARRAY_A" );

		foreach ( $result as $row ) {
			return $row;
		}
		return null;
	}

	function getEmailNotificationTypes( $vendor_id = 1 ) {
		global $wpdb;

		$sql = 'SELECT 
					a.*,b.is_enabled
				FROM '.$this->plugin_prefix.'email_notification_types a
				LEFT JOIN '.$this->plugin_prefix.'email_notification_templates b
					ON a.email_notification_id = b.email_notification_id
					AND b.vendor_id = 1';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

	function getEmailNotificationTypeTemplate( $vendor_id, $email_notification_id, $iteration =0) {
		global $wpdb;

		$sql = 'SELECT a.*, b.name
				FROM '.$this->plugin_prefix.'email_notification_templates a
				INNER JOIN '.$this->plugin_prefix.'email_notification_types b
				ON a.vendor_id = '.$vendor_id.'
				AND a.email_notification_id = '.$email_notification_id.'
				AND b.email_notification_id = a.email_notification_id';

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		foreach ( $result as $row ) {
			return $row;
		}
		if ( $iteration == 0 ) {
			$date = date("Y-m-d H:i:s");
			$id = $this->insert(
				'email_notification_templates',
				array(
					"email_notification_id" =>  $email_notification_id,
					"vendor_id"             =>  $vendor_id,
					"subject"               =>  "",
					"template"              =>  "",
					"updated_at"            =>  $date,
					"created_at"            =>  $date
				)
			);
			return $this->getEmailNotificationTypeTemplate( $vendor_id, $email_notification_id, 1 );
		}
		else {
			return null;
		}
	}

	public function getAddons() {
	    global $wpdb;

	    $sql = 'SELECT * FROM '.$this->plugin_prefix.'addons';

        return $wpdb->get_results($sql, 'ARRAY_A');
    }

    public function isAddonExists($addonSlug) {
        global $wpdb;

        $sql = 'SELECT 
                    * 
                FROM 
                    '.$this->plugin_prefix.'addons a
                WHERE a.slug = "'.$addonSlug.'"';

        $result = $wpdb->get_results($sql, 'ARRAY_A');

        foreach ($result as $row) {
            return true;
        }

        return false;
	}

	public function isProviderMemberOAuthExists($member_id, $type) {
	    global $wpdb;

	    $sql = 'SELECT 
	                *
	            FROM
	                '.$this->plugin_prefix.'provider_oauth a
	            WHERE a.member_id = ' . $member_id;

	    $result = $wpdb->get_results($sql, 'ARRAY_A');

	    foreach ($result as $row) {
	        return true;
        }
	    return false;
    }
}
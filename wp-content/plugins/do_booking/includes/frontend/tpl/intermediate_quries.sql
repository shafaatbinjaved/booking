SELECT *
FROM wp_staff_member_timing a
INNER JOIN wp_member_services b
ON b.service_id = 2
AND b.member_id = 1
AND b.member_id = a.member_id
AND a.is_day_on = 1
AND a.vendor_id = 1
AND ( "10:00:00" BETWEEN a.start_time AND a.end_time )
AND ( "12:00:00" BETWEEN a.start_time AND a.end_time )
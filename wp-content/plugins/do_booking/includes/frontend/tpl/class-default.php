<?php

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/order/OrderManagement.php';

class RBDoBooking_Frontend_Default extends View {

	private $db;
	private static $instance;
	private $form_id;
	private $vendor_id;
	private $userData;
	private $step_container;
	private $all_steps;
	private $cart_no;
	//private $total_cart_items;
	private $current_step;

	public function __construct() {
		parent::__construct(
			plugin_dir_path( __FILE__ ) . 'default/'
		);
		$this->db = RBDoBooking_DB::instance();
	}

	public static function instance( $all_steps, $form_id = "", $vendor_id = 1 ) {

		if ( !isset( self::$instance ) && !( self::$instance instanceof RBDoBooking_Frontend_Default ) ) {
			self::$instance = new RBDoBooking_Frontend_Default();
		} else {
			self::$instance;
		}
		if ( $form_id != "" ) {
			self::$instance->form_id = $form_id;
			self::$instance->loadUserDataFromDB( $form_id );
		}
		self::$instance->vendor_id = $vendor_id;
		self::$instance->all_steps = $all_steps;
		self::$instance->step_container = self::$instance->get_steps_configuration( $all_steps, $vendor_id );
		self::$instance->cart_no = 0;
		//self::$instance->total_cart_items = 0;

		return self::$instance;

	}

	public function setCartNo( $cart_no ) {
		$this->cart_no = $cart_no;
	}

	public function loadUserDataFromDB( $form_id ) {
		$formIDExists = $this->db->checkFormIDExistsOrNot( $form_id );
		if ( $formIDExists ) {
			$this->userData = $this->db->getTempFormData( $form_id );
			$this->userData = unserialize( $this->userData["data"] );
            /*echo '<pre>';
            var_dump(
                $formIDExists,
                $this->userData
            );
            //die();
            echo '</pre>';*/
		}
	}

	public function saveUserDataToDB( $form_id, $form_data ) {
		$this->db->updateTempFormData( $form_id, serialize($form_data) );
	}

	public function shortcode_scripts( $vendor_id, $step ) {

		$data = new stdClass();
		$data->vendor_id = 1;
		$data->user_id = "-1";

		wp_enqueue_style( 'rbdobooking-bootstrap' );
		wp_enqueue_script('jquery');
		// jquery ui datepicker
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style(
			"jquery-ui-theme-rbdobooking",
			RBDOBOOKING_PLUGIN_URL . "assets/css/jquery-ui/jquery-ui.min.css",
			array(),
			"1.0"
		);
		// end jquery ui datepicker
		wp_enqueue_style('rbdobooking-booking-form-style' );
		// Main helper functions script.
		wp_enqueue_script(
			'rbdobooking-helper',
			RBDOBOOKING_PLUGIN_URL . "assets/js/helper.js",
			array( 'jquery' ),
			RBDOBOOKING_VERSION,
			true
		);

		$form_id = time();
		$formIDExists = $this->db->checkFormIDExistsOrNot( $form_id );

		while ( $formIDExists == true ) {
			$form_id = time();
			$formIDExists = $this->db->checkFormIDExistsOrNot( $form_id );
		}
		$this->db->insert(
			"temp_form_data",
			array(
				"form_id"       =>  $form_id,
				"data"          =>  "",
				"updated_at"    =>  date("Y-m-d H:i:s"),
				"created_at"    =>  date("Y-m-d H:i:s")
			)
		);

		wp_localize_script( 'rbdobooking-helper', 'RBDoBookingHelper', array(
			'vendor_id'               => $vendor_id,
			'step'                    => $step,
			'form_id'                 => $form_id
		));

		//load dashicons
		wp_enqueue_style( 'dashicons' );

		wp_enqueue_script(
			'rbdobooking-booking-form',
			RBDOBOOKING_PLUGIN_URL . "assets/js/frontend/do_booking_default.js",
			array('jquery'),
			RBDOBOOKING_VERSION,
			true
		);

		do_action('rbdobooking_enqueue_addons_script');
	}

	public function submit_step_content( $step, $posted_data ) {
		$this->cart_no = intval($posted_data["cart_number"]);
		$step_html = '';
		switch ( $step ) {
			case "service":
				$category_id = null;
				$service_id = null;
				$member_id = null;
				$date_available = null;
				$time_start = null;
				$time_end = null;
				$selected_days = null;
				$member_ids = array();
				foreach ( $posted_data["step_data"] as $row ) {
					switch ($row["name"]) {
						case "category_id":
							$category_id = $row["value"];
							break;
						case "service_id":
							$service_id = $row["value"];
							break;
						case "member_id":
							$member_id = $row["value"];
							break;
						case "date_available":
							$date_available = $row["value"];
							break;
						case "time-start":
							$time_start = $row["value"];
							break;
						case "time-end":
							$time_end = $row["value"];
							break;
						case "selected-days":
							$selected_days = explode(',',$row["value"]);
							break;
					}
				}
				if ( $member_id == "any" ) {
					$service_members = $this->db->getServiceMembers( $service_id, $this->vendor_id );
					if ( count($service_members) > 0 ) {
						foreach ( $service_members as $service_member ) {
							array_push($member_ids,$service_member["member_id"]);
						}
					}
				}
				$data = array(
					'category_id'       =>  $category_id,
					'service_id'        =>  $service_id,
					'member_id'         =>  count($member_ids) > 0 ? $member_ids : array($member_id),
					'vendor_id'         =>  $this->vendor_id,
					'day'               =>  $selected_days,//array(1,2,3,4,5,6,7),
					'start_time'        =>  $time_start,
					'end_time'          =>  $time_end,
					'date_on_or_after'  =>  $date_available
				);
				if ( !isset($this->userData["service"]) ) {
					$this->userData["service"] = array();
				}
				$this->userData["service"][$this->cart_no] = $data;
				break;
			case "time":
				$f_data = $posted_data["step_data"];
				$f_data["vendor_id"] = $posted_data["vendor_id"];

				$time_member_ids = explode(",",$f_data["member_ids"]);
				$time_timing_ids = explode(",",$f_data["timing_ids"]);
				$random_id_select = mt_rand() % count($time_member_ids);

				$f_data["selected_time_member_id"] = $time_member_ids[$random_id_select];
				$f_data["selected_time_timing_id"] = $time_timing_ids[$random_id_select];

				if ( !isset($this->userData["time"]) ) {
					$this->userData["time"] = array();
				}
				$this->userData["time"][$this->cart_no] = $f_data;

				$count = count($this->userData["time"]);
				if ( $count != 0 ) {
					$count--;
					$this->cart_no = $count;
				}

				break;
			case "details":
				$f_data = $posted_data["step_data"];
				$f_data["vendor_id"] = $posted_data["vendor_id"];
				$this->userData["details"] = $f_data;
				break;
			case "payment":
				$f_data = $posted_data["step_data"];
				$f_data["vendor_id"] = $posted_data["vendor_id"];
				$coupons = [];
				if (isset($this->userData['payment']['coupons'])) {
                    $coupons = $this->userData['payment']['coupons'];
                }
				$f_data['coupons'] = $coupons;
				$this->userData["payment"] = $f_data;
				//..................................
				$customer_email = $this->userData["details"]["details-email"];
				$customer_id = $this->db->customer->checkCustomerExists( $customer_email );
				$creation_time = date("Y-m-d H:i:s");

				if ( $customer_id == 0 ) {
					$customer_data = array(
						"full_name"     =>  $this->userData["details"]["details-name"],
						"phone"         =>  $this->userData["details"]["details-phone"],
						"email"         =>  $this->userData["details"]["details-email"],
						"created_at"    =>  $creation_time
					);
					$customer_id = $this->db->customer->insert_customer( $customer_data );

					if ( $customer_id ) {
						$this->db->customer->insert_vendor_customer( $this->vendor_id, $customer_id );
					}
				}
				else {
					$customer_vendor_exists = $this->db->customer->checkVendorCustomerExistsFromCustomerID(
						$this->vendor_id,
						$customer_id
					);

					if ( !$customer_vendor_exists ) {
						$this->db->customer->insert_vendor_customer( $this->vendor_id, $customer_id );
					}
				}
				if ( $customer_id ) {
                    /*
                     * @var OrderManagement $order
                     */
                    $order = new OrderManagement();
                    $order->addOrderItems($this->userData, $this->cart_no, $customer_id, $this->vendor_id);
				}
				break;
			case "done":
				break;
		}
		$this->saveUserDataToDB(
			$this->form_id,
			$this->userData
		);
		return true;
	}

	public function prepare_step_content_with_dummy_data( $step ) {
	    $vendor_id = 1;
		$step_html = '';
		switch ( $step ) {
			case "service":
				$category_options = array(
					array(
						"id"    =>  "1",
						"name"  =>  "Dentures"
					),
					array(
						"id"    =>  "2",
						"name"  =>  "Invisalign"
					)
				);
				$category_options_html = '';
				foreach ( $category_options as $index => $category_option ) {
					$category_options_html .= $this->get_view(
						"options",
						false,
						$category_option
					);
				}
				$service_options = array(
					array(
						"id"    =>  "1",
						"name"  =>  "Dentures"
					),
					array(
						"id"    =>  "2",
						"name"  =>  "Invisalign (invisable braces)"
					)
				);
				$service_options_html = '';
				foreach ( $service_options as $index => $service_option ) {
					$service_options_html .= $this->get_view(
						"options",
						false,
						$service_option
					);
				}
				$member_options = array(
					array(
						"id"    =>  "1",
						"name"  =>  "Dentures"
					),
					array(
						"id"    =>  "2",
						"name"  =>  "Invisalign (invisable braces)"
					)
				);
				$member_options_html = '';
				foreach ( $member_options as $index => $member_option ) {
					$member_options_html .= $this->get_view(
						"options",
						false,
						$member_option
					);
				}
				$time_options = array(
					array(
						"id"        =>  "09:00:00",
						"name"      =>  "09:00"
					),
					array(
						"id"        =>  "10:00:00",
						"name"      =>  "10:00"
					),
					array(
						"id"        =>  "11:00:00",
						"name"      =>  "11:00"
					),
					array(
						"id"        =>  "12:00:00",
						"name"      =>  "12:00"
					)
				);
				$time_options_html = '';
				foreach ( $time_options as $index => $time_option ) {
					$time_options_html .= $this->get_view(
						"options",
						false,
						$time_option
					);
				}
				$step_html = $this->get_view(
					"service_step",
					false,
					array(
						'category_options'          =>  $category_options_html,
						'service_options'           =>  $service_options_html,
						'member_options'            =>  $member_options_html,
						'category_with_services'    =>  '',
						'category_with_members'     =>  '',
						'service_with_members'      =>  '',
						'service_with_categories'   =>  '',
						'time_start_options'        =>  $time_options_html,
						'time_end_options'          =>  $time_options_html
					)
				);
				break;
			case "time":
				$slots = array();
				$time_start = time();
				for ( $i=0;$i<10;$i++ ) {
					$slots[] = array(
						"start"         =>  $time_start,
						"end"           =>  $time_start+1800,
						"timing_ids"    =>  "1,2",
						"member_ids"    =>  "1,2"
					);
					$time_start = $time_start+1860;
				}

				$step_html = $this->get_view(
					"time_step",
					false,
					array(
						'calendar_time_slots'  =>  array(
							date("Y-m-d")  =>  array(
								"timestamp" =>  time(),
								"slots"     =>  $slots
							)
						)
					)
				);
				break;
			case "cart":
				$step_html = $this->get_view(
					"cart_step",
					false,
					array(
						'all_possible_bookings' =>  array(
							array(
								"iteration"     =>  0,
								"service_name"  =>  "Crown and bridges",
								"date"          =>  date("d.m.Y"),
								"time"          =>  date("H:i"),
								"member_name"   =>  "Employee",
								"currency"      =>  "$",
								"price"         =>  "12"
							),
							array(
								"iteration"     =>  1,
								"service_name"  =>  "Dentures",
								"date"          =>  date("d.m.Y"),
								"time"          =>  date("H:i"),
								"member_name"   =>  "Employee",
								"currency"      =>  "$",
								"price"         =>  "12"
							)
						)
					)
				);
				break;
			case "details":
				$step_html = $this->get_view(
					"details_step",
					false,
					array(
						"date"          =>  date("F j, Y"),
						"time"          =>  date("g:i a"),
						"member"        =>  "Shafaat Javed",
						"price"         =>  400,
						"service"       =>  "Crown and Bridges",
						"currency"      =>  $this->db->getVendorSpecificSettingValueByName( "currency", $this->vendor_id ),
						"custom_fields" =>  ""
					)
				);
				break;
			case "payment":
                $currency = $this->db->getVendorSpecificSettingValueByName( "currency", $vendor_id );
				$order_view = $this->get_view(
					"order_view",
					false,
					array(
						"time"          =>  date("j M, g:i a"),
						"order_items"   =>  array(
							array(
								"service_name"      =>  "Dentures",
								"member_full_name"  =>  "Shafaat Javed",
								"member_price"      =>  "400"
							),
							array(
								"service_name"      =>  "Invisalign (invisable braces)",
								"member_full_name"  =>  "Shujaat Javed",
								"member_price"      =>  "600"
							)
						),
                        "currency"      =>  $currency,
                        "include_tax"   =>  false
					)
				);
				$step_html = $this->get_view(
					"payment_step",
					false,
					array(
						"payment_methods"   =>  $this->db->getPaymentMethods(),
						"order_view"        =>  $order_view
					)
				);
				break;
			case "done":
				$step_html = $this->get_view(
					"done_step"
				);
				break;
		}
		$html = $this->get_view(
			"output",
			false,
			array(
				"template"          =>  'default',
				'step_container'    =>  $this->step_container,
				"step"              =>  $step_html
			)
		);
		return $html;
	}

	public function prepare_step_content( $step ) {
		$step_html = '';
		$this->current_step = $step;
		switch ( $step ) {
			case "service":
				$step_html = $this->get_service_step_html();
				break;
			case "time":
				$step_html = $this->get_time_step_html();
				break;
			case "cart":
				$step_html = $this->get_cart_step_html();
				break;
			case "details":
				$step_html = $this->get_details_step_html();
				break;
			case "payment":
				$step_html = $this->get_payment_step_html();
				break;
			case "done":
				$step_html = $this->get_done_step_html();
				break;
		}
		$html = $this->get_view(
			"output",
			false,
			array(
				"template"          =>  'default',
				'step_container'    =>  $this->step_container,
				"step"              =>  $step_html
			)
		);
		return $html;
	}

	private function get_steps_configuration( $all_steps, $vendor_id = 1 ) {

		$html = '';

		$hash = md5(serialize(array($all_steps,$vendor_id,static::class)));

		$filePath = RBDOBOOKING_PLUGIN_DIR;
		$filePath .= 'cache/includes/frontend/tpl/default/steps/step_container/';
		$filePath .= 'step_container.'.$hash.'.htm';

		if ( file_exists($filePath) && 1==2 ) {
			$html = file_get_contents( $filePath );
		}
		else {
			$formatted_all_steps = array();
			foreach ( $all_steps as $index => $step ) {
				$step_class = '';
				if ( $index == 0 ) {
					$step_class = $step."-step active";
				}
				else {
					$step_class = $step."-step";
				}
				$formatted_all_steps[] = array(
					'number'    =>  $index+1,
					'name'      =>  $step,
					'step_class'=>  $step_class
				);
			}

			$html = $this->get_view(
				'step_container',
				false,
				array(
					"total_steps_class" =>  "steps-".count($all_steps),
					"all_steps" =>  $formatted_all_steps
				)
			);
			//file_put_contents( $filePath, $html );
		}
		return $html;
	}

	public function get_service_step_html() {

		$service_filePath = RBDOBOOKING_PLUGIN_DIR;
		$service_filePath .= 'cache/includes/frontend/tpl/default/steps/service/';
		$service_filePath .= 'service.'.$this->vendor_id.'.htm';
		$html = '';

		if ( file_exists($service_filePath) ) {
			$html = file_get_contents( $service_filePath );
		}
		else {
			$process_category_ids = [];
			$process_service_ids = [];
			$process_member_ids = [];

			$category_options_html = '';
			$service_options_html = '';
			$employee_options_html = '';
			$service_with_categories = $this->db->getPublicServicesWithCategoryNameAndStaffMemberName();

			$category_with_services = array();
			$service_with_categories_json = array();
			$service_with_members = array();
			$category_with_members = array();

			foreach ( $service_with_categories as $sc ) {
				if ( !in_array($sc['service_id'],$process_service_ids) ) {
					array_push($process_service_ids,$sc['service_id']);
					$service_options_html .= $this->get_view(
						'options',
						false,
						array(
							"id"            =>  $sc["service_id"],
							"name"          =>  $sc["name"]
						)
					);
				}

				if ( isset($category_with_services["category_".$sc["category_id"]]) ) {
					if ( !in_array($sc["service_id"],$category_with_services["category_".$sc["category_id"]]) ) {
						$category_with_services["category_".$sc["category_id"]][] = $sc["service_id"];
					}
				}
				else {
					$category_with_services["category_".$sc["category_id"]][] = $sc["service_id"];
				}

				if ( isset($service_with_categories_json["service_".$sc["service_id"]]) ) {
					if ( !in_array($sc["category_id"],$service_with_categories_json["service_".$sc["service_id"]]) ) {
						$service_with_categories_json["service_".$sc["service_id"]][] = $sc["category_id"];
					}
				}
				else {
					$service_with_categories_json["service_".$sc["service_id"]][] = $sc["category_id"];
				}

				if ( isset($service_with_members["service_".$sc["service_id"]]) ) {
					$service_with_members["service_".$sc["service_id"]][] = $sc["member_id"];
				}
				else {
					$service_with_members["service_".$sc["service_id"]][] = $sc["member_id"];
				}

				if ( isset($category_with_members["category_".$sc["category_id"]]) ) {
					$category_with_members["category_".$sc["category_id"]][] = $sc["member_id"];
				}
				else {
					$category_with_members["category_".$sc["category_id"]][] = $sc["member_id"];
				}

				if ( !in_array($sc['category_id'],$process_category_ids) ) {
					array_push($process_category_ids,$sc['category_id']);
					$category_options_html .= $this->get_view(
						'options',
						false,
						array(
							"id"    =>  $sc["category_id"],
							"name"  =>  $sc["category"]
						)
					);
				}

				if ( !in_array($sc['member_id'], $process_member_ids) ) {
					array_push($process_member_ids,$sc['member_id']);
					$employee_options_html .= $this->get_view(
						'options',
						false,
						array(
							"id"    =>  $sc["member_id"],
							"name"  =>  $sc["first_name"]." ".$sc["last_name"]
						)
					);
				}
			}

			$filePath = RBDOBOOKING_PLUGIN_DIR;
			$filePath .= 'cache/includes/frontend/tpl/time_options/';
			$filePath .= 'time_options.'.$this->vendor_id.'.htm';

			$time_all_options = '';

			if ( !file_exists($filePath) ) {
				$duration = "+" . "60" . " minutes";
				$time_options = $this->db->getVendorStartStopTimings( $this->vendor_id );
				$end_time = $time_options[1];
				$next_interval = $time_options[0];
				$iteration = 0;

				if ( !is_null($time_options) ) {

					while ( $next_interval != $end_time && $iteration < 30 ) {
						$iteration++;
						$strtime = strtotime($next_interval);
						$time_data = array(
							"name"  =>  date("g:i a",$strtime),
							"id"    =>  date("H:i:s",$strtime)
						);
						$time_all_options .= $this->get_view(
							'options',
							false,
							$time_data
						);
						$next_interval = strtotime($duration,$strtime);
						$next_interval = date("H:i:s",$next_interval);
					}
					$end_time = strtotime($end_time);
					$time_data = array(
						"name"  =>  date("g:i a",$end_time),
						"id"    =>  date("H:i:s",$end_time)
					);
					$time_all_options .= $this->get_view(
						'options',
						false,
						$time_data
					);

				}
				//file_put_contents( $filePath, $time_all_options );
			}
			else {
				$time_all_options = file_get_contents( $filePath );
			}

			$current_step_data_arr = array(
				'category_options'          =>  $category_options_html,
				'service_options'           =>  $service_options_html,
				'member_options'            =>  $employee_options_html,
				'time_start_options'        =>  $time_all_options,
				'time_end_options'          =>  $time_all_options,
				"category_with_services"    =>  htmlentities(json_encode($category_with_services)),
				"service_with_members"      =>  htmlentities(json_encode($service_with_members)),
				"category_with_members"     =>  htmlentities(json_encode($category_with_members)),
				"service_with_categories"   =>  htmlentities(json_encode($service_with_categories_json))
			);

			$html = $this->get_view(
				"service_step",
				false,
				$current_step_data_arr

			);
			//file_put_contents( $service_filePath, $html );
		}
		if ( isset($this->userData["service"]) ) {
			$returned_data = array(
				"prev_step" =>  "service",
				"step_data" =>  $this->userData["service"],
				"html"      =>  $html
			);
			die(wp_send_json_success($returned_data));
		}
		return $html;
	}

	public function get_time_step_html() {
		$category_id = null;
		$service_id = null;
		$member_id = null;
		$date_available = null;
		$time_start = null;
		$time_end = null;
		$selected_days = null;

		if ( isset($this->userData["service"][$this->cart_no]) ) {
			$category_id = $this->userData["service"][$this->cart_no]["category_id"];
			$service_id = $this->userData["service"][$this->cart_no]["service_id"];
			$member_id = $this->userData["service"][$this->cart_no]["member_id"];
			$date_available = $this->userData["service"][$this->cart_no]["date_on_or_after"];
			$time_start = $this->userData["service"][$this->cart_no]["start_time"];
			$time_end = $this->userData["service"][$this->cart_no]["end_time"];
			$selected_days = $this->userData["service"][$this->cart_no]["day"];
			$vendor_id = $this->userData["service"][$this->cart_no]["vendor_id"];
		}

		$data = array(
			"vendor_id"     =>  $vendor_id,
			"member_id"     =>  $member_id,
			"day"           =>  $selected_days,
			"start_time"    =>  $time_start,
			"end_time"      =>  $time_end
		);
		$result = $this->db->getVendorMemberTiming( $data );

		$service_duration = $this->db->getServiceDuration( $service_id );
		$service_duration = "+" . $service_duration . " minutes";

		$generated_time_slots = array();

		$days_arr = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");

		$working_days = [];

		foreach ( $result as $row ) {

			$working_days[] = intval($row["day"]);

			$day_start_time = explode(":", $row["start_time"]);
			$day_end_time = explode(":",$row["end_time"]);

			$slot_start = new DateTime();
			$slot_end = new DateTime();

			$form_start_time = explode(":",$data["start_time"]);
			$form_end_time = explode(":",$data["end_time"]);

			$form_start_slot = new DateTime();
			$form_end_slot = new DateTime();

			$form_start_slot->setTime(
				intval($form_start_time[0]),
				intval($form_start_time[1]),
				intval($form_start_time[2])
			);

			$form_end_slot->setTime(
				intval($form_end_time[0]),
				intval($form_end_time[1]),
				intval($form_end_time[2])
			);

			$slot_start->setTime(
				intval($day_start_time[0]),
				intval($day_start_time[1]),
				intval($day_start_time[2])
			);

			$slot_end->setTime(
				intval($day_end_time[0]),
				intval($day_end_time[1]),
				intval($day_end_time[2])
			);

			$today = date("N");

			if ( $today == intval($row["day"]) ) {
				$temp_date = date("Y-m-d");
			}
			else {//looking for previous day time
				$temp_date = date("Y-m-d",strtotime( "previous ".$days_arr[$today-1] ));

			}
			//......
			$temp_date = explode("-",$temp_date);
			$slot_start->setDate(
				intval($temp_date[0]),
				intval($temp_date[1]),
				intval($temp_date[2])
			);
			$slot_end->setDate(
				intval($temp_date[0]),
				intval($temp_date[1]),
				intval($temp_date[2])
			);
			//......
			$form_start_slot->setDate(
				intval($temp_date[0]),
				intval($temp_date[1]),
				intval($temp_date[2])
			);
			$form_end_slot->setDate(
				intval($temp_date[0]),
				intval($temp_date[1]),
				intval($temp_date[2])
			);
			//......
			$start_stamp = $slot_start->getTimestamp();
			$end_stamp = strtotime($service_duration,$start_stamp);

			$day_end_stamp = $slot_end->getTimestamp();

			$form_start_stamp = $form_start_slot->getTimestamp();
			$form_end_stamp = $form_end_slot->getTimestamp();

			if ( $start_stamp <= $form_start_stamp ) {
				$start_stamp = $form_start_stamp;
				$end_stamp = strtotime($service_duration,$start_stamp);
			}

			while ( $end_stamp <= $day_end_stamp &&
			        $end_stamp <= $form_end_stamp ) {
				if ( !isset($generated_time_slots[ $row["day"] ]) ) {
					$generated_time_slots[ $row["day"] ] = array();
				}

				$formatted_start_timestamp = date("Y-m-d H:i:s",$start_stamp);
				$formatted_end_timestamp = date("Y-m-d H:i:s",$end_stamp);

				if ( array_key_exists($start_stamp,$generated_time_slots[ $row["day"] ]) ) {
					$generated_time_slots[ $row["day"] ][$start_stamp]["timing_ids"] .= ",".$row["timing_ids"];
					$generated_time_slots[ $row["day"] ][$start_stamp]["member_ids"] .= ",".$row["member_ids"];
				}
				else {
					$generated_time_slots[ $row["day"] ][$start_stamp] = array(
						"start"         =>  $start_stamp,
						"start_format"  =>  $formatted_start_timestamp,//date("Y-m-d H:i:s",$start_stamp),
						"end"           =>  $end_stamp,
						"end_format"    =>  $formatted_end_timestamp,//date("Y-m-d H:i:s",$end_stamp),
						"timing_ids"    =>  $row["timing_ids"],
						"member_ids"    =>  $row["member_ids"]
					);
				}
				$start_stamp = $end_stamp;
				$end_stamp = strtotime($service_duration,$start_stamp);
			}

		}

		$current_day = time();
		$last_day_of_month = strtotime("+15 day",$current_day);
		$calendar_time_slots = array();

		while ( $current_day <= $last_day_of_month ) {
			$day_of_week = date("N",$current_day);
			if ( in_array($day_of_week,$working_days) ) {
				$day_allowed_slots = array();

				//......
				foreach ( $generated_time_slots[$day_of_week] as $day_of_week_index => $time_slot ) {

					$current_day_fornatted = date("Y-m-d",$current_day);

					$member_ids = explode(',',$time_slot["member_ids"]);
					$timing_ids = explode(',',$time_slot["timing_ids"]);

					$allowed_timing_ids = array();
					$allowed_member_ids = array();

					foreach ( $timing_ids as $index => $timing_id ) {
						$count = $this->db->slotIsAvailableOrNot(
							$current_day_fornatted." ".date("H:i:s",$time_slot["start"]),
							$current_day_fornatted." ".date("H:i:s",$time_slot["end"]),
							$timing_ids[$index],
							$member_ids[$index],
							$vendor_id
						);

						if ( $count == 0 ) {
							array_push($allowed_member_ids,$member_ids[$index]);
							array_push($allowed_timing_ids,$timing_ids[$index]);
						}
					}
					if ( count($allowed_timing_ids) > 0 ) {
						$time_slot["member_ids"] = implode(",",$allowed_member_ids);
						$time_slot["timing_ids"] = implode(",",$allowed_timing_ids);
						array_push( $day_allowed_slots, $time_slot );
					}
				}
				//......
				//......................
				$calendar_time_slots[date("Y-m-d",$current_day)] = array(
					"slots"     =>  $day_allowed_slots,//$generated_time_slots[$day_of_week],
					"timestamp" =>  $current_day
				);
			}
			$current_day = strtotime("+1 day",$current_day);
		}

		$html = $this->get_view(
			"time_step",
			false,
			array("calendar_time_slots"=>$calendar_time_slots)
		);

		$returned_data = null;

		$returned_data = array(
			"next_step" =>  "time",
			"html"      =>  $html
		);

		if ( isset($this->userData["time"]) ) {
			$returned_data["prev_step"] = "time";
			$returned_data["step_data"] = $this->userData["time"];
			$returned_data["step_data_already_exists"] = true;
		}

		die(wp_send_json_success( $returned_data ));

	}

	public function get_cart_step_html() {

		$html = '';

		$all_possible_bookings = array();

		for ( $iteration = 0; $iteration <= $this->cart_no; $iteration++ ) {

			$start_datetime = strtotime( $this->userData["time"][$iteration]["start"] );
			$item = array();
			$item["iteration"] = $iteration;
			$item["service_id"] = $this->userData["service"][$iteration]["service_id"];
			$item["date"] = date("d.m.Y",$start_datetime);
			$item["time"] = date("H:i:s",$start_datetime);
			$item["service_name"] = $this->db->getServiceNameByServiceID( $this->userData["service"][$iteration]["service_id"] );
			$member = $this->db->getStaffMember( $this->userData["time"][$iteration]["selected_time_member_id"] );
			if ( count($member) > 0 && isset($member[0]["member_id"]) ) {
				$member = $member[0];
			}
			$item["member_id"] = $member["member_id"];
			$item["member_name"] = $member["first_name"] . " " . $member["last_name"];
			$service_name_member_price_name = $this->db->getServiceMemberPriceNameAndServiceName(
				$this->userData["time"][$iteration]["selected_time_member_id"],
				$item["service_id"]
			);
			if ( $service_name_member_price_name["member_price"] == "0" ) {
				$service_name_member_price_name["member_price"] = $service_name_member_price_name["service_price"];
			}
			$item["price"] = $service_name_member_price_name["member_price"];
			$item["currency"] = $this->db->getVendorSpecificSettingValueByName( "currency", $this->vendor_id );

			array_push($all_possible_bookings,$item);
		}

		$html = $this->get_view(
			"cart_step",
			false,
			array(
				"all_possible_bookings" =>  $all_possible_bookings
			)
		);

		$returned_data = array(
			"next_step"     =>  "cart",
			"prev_step"     =>  "cart",
			"html"          =>  $html,
			"cart_count"    =>  $this->cart_no
		);

		die(wp_send_json_success( $returned_data ));
	}

	public function get_details_step_html() {

		$service_id = null;
		$time_member_ids = null;
		$time_timing_ids = null;
		$random_id_select = null;
		$tempData = null;
		$service_name_member_price_name = null;
		$already_data = null;
		$vendor_id = $this->vendor_id;

		if ( isset($this->userData["time"][$this->cart_no]) ) {
			$service_id = $this->userData["service"][$this->cart_no]["service_id"];
			$time_member_id = $this->userData["time"][$this->cart_no]['selected_time_member_id'];
			$time_timing_id = $this->userData["time"][$this->cart_no]['selected_time_timing_id'];

			$service_name_member_price_name = $this->db->getServiceMemberPriceNameAndServiceName(
				$time_member_id,
				$service_id
			);

			if ( $service_name_member_price_name["member_price"] == "0" ) {
				$service_name_member_price_name["member_price"] = $service_name_member_price_name["service_price"];
			}

			$custom_fields_cache_file_path = RBDOBOOKING_PLUGIN_DIR;
			$custom_fields_cache_file_path .= 'cache/includes/frontend/tpl/custom_fields/';
			$custom_fields_cache_file_path_vendor = $custom_fields_cache_file_path . 'custom_fields.'.$vendor_id.'.htm';
			$custom_fields_cache_file_path_vendor_service = $custom_fields_cache_file_path . 'custom_fields.'.$vendor_id.'.'.$service_id.'.htm';
			$custom_fields_html = '';

			$is_extra_template_available = $this->db->isVendorServiceOrOnlyVendorSpecificTemplateExists( $vendor_id, $service_id );

			if ( file_exists($custom_fields_cache_file_path_vendor_service) &&
			     $is_extra_template_available == 2 ) {//vendor and service specific custom fields
				 $custom_fields_html = file_get_contents( $custom_fields_cache_file_path_vendor_service );
			}
			else if ( file_exists($custom_fields_cache_file_path_vendor) &&
			          $is_extra_template_available == 1 ) {//vendor specific custom fields
				$custom_fields_html = file_get_contents( $custom_fields_cache_file_path_vendor );
			}
			else if ( $is_extra_template_available == 1 || $is_extra_template_available == 2 ) {
				$custom_fields = $this->db->getVendorOrServiceSpecificTemplateFields( $vendor_id, $service_id );
				if ( !is_null($custom_fields) ) {

					$custom_fields_html = $this->custom_fields_html_generator( $custom_fields );

					if ( $is_extra_template_available == 2 ) {
						// vendor service specific cache file
						/*file_put_contents(
							$custom_fields_cache_file_path_vendor_service,
							$custom_fields_html
						);*/
					}
					else if ( $is_extra_template_available == 1 ) {
						// vendor specific cache file
						/*file_put_contents(
							$custom_fields_cache_file_path_vendor,
							$custom_fields_html
						);*/
					}

				}
			}

			$start_time = strtotime($this->userData["time"][$this->cart_no]["start"]);

			$html = $this->get_view(
				"details_step",
				false,
				array(
					"date"          =>  date("F j, Y",$start_time),
					"time"          =>  date("g:i a", $start_time),
					"member"        =>  $service_name_member_price_name["member_full_name"],
					"price"         =>  $service_name_member_price_name["member_price"],
					"service"       =>  $service_name_member_price_name["service_name"],
					"currency"      =>  $this->db->getVendorSpecificSettingValueByName( "currency", $this->vendor_id ),
					"custom_fields" =>  $custom_fields_html
				)
			);

			$returned_data = array(
				"next_step" =>  "details",
				"html"      =>  $html
			);
			if ( isset($this->userData["details"]) ) {
				$returned_data["prev_step"] = "details";
				$returned_data["step_data"] = $this->userData["details"];
				$returned_data["step_data_already_exists"] = true;
			}

			die(wp_send_json_success( $returned_data ));
		}
	}

	public function get_payment_step_html() {

		$html = null;
		$vendor_id = $this->vendor_id;
		/*$payment_filePath = RBDOBOOKING_PLUGIN_DIR;
		$payment_filePath .= 'cache/includes/frontend/tpl/default/steps/payment/';
		$payment_filePath .= 'payment.'.$vendor_id.'.htm';*/

		/*if ( file_exists($payment_filePath && 1 == 0) ) {//explicit false
			$html = file_get_contents( $payment_filePath );
		}*/
		/*else {

		}*/

		$payment_methods = $this->db->getPaymentMethods();
		$order_items = array();
		$start_time_of_first_item = null;
		for ( $iteration = 0; $iteration <= $this->cart_no; $iteration++ ) {

			if ( isset($this->userData["time"][$iteration]) && isset($this->userData["service"][$iteration]) ) {
				if ($iteration == 0) {
					$start_time_of_first_item = $this->userData["time"][$iteration]["start"];
				}
				$service_name_member_price_name = $this->db->getServiceMemberPriceNameAndServiceName(
					$this->userData["time"][$iteration]["selected_time_member_id"],
					$this->userData["service"][$iteration]["service_id"]
				);
				if ( $service_name_member_price_name["member_price"] == "0" ) {
					$service_name_member_price_name["member_price"] = $service_name_member_price_name["service_price"];
				}
				array_push($order_items,$service_name_member_price_name);
			}

		}

		$order_view = $this->get_view(
			"order_view",
			false,
			array(
				"time"          =>  date("j M, g:i a",strtotime($start_time_of_first_item)),
				"order_items"   =>  $order_items,
                "include_tax"   =>  ($this->db->getVendorSpecificSettingValueByName("include_tax",$vendor_id) == "Enabled" ? true : false ),
                "currency"      =>  $this->db->getVendorSpecificSettingValueByName( "currency", $vendor_id ),
                'coupons'       =>  isset($this->userData['payment']['coupons']) ? $this->userData['payment']['coupons'] : []
			)
		);
		$html = $this->get_view(
			"payment_step",
			false,
			array(
				"payment_methods"   =>  $payment_methods,
				"order_view"        =>  $order_view
			)
		);

		$returned_data = array(
			"next_step" =>  "payment",
			"html"      =>  $html
		);

		die(wp_send_json_success( $returned_data ));
	}

	public function get_done_step_html() {

		$html = $this->get_view("done_step");
		$returned_data = array(
			"next_step" =>  "done",
			"html"      =>  $html
		);

		die(wp_send_json_success( $returned_data ));
	}

	private function custom_fields_html_generator( $custom_fields ) {
		$custom_fields_html = '';
		foreach ( $custom_fields as $custom_field ) {
			$required = "";
			$star = "";
			if ( $custom_field["is_required"] == "1" ) {
				$star = "*";
				$required .= 'required="required"';
			}
			if ( $custom_field["type"] == "text_field" ) {
				$custom_fields_html .= $this->get_view(
					"custom_fields.input",
					false,
					array(
						"id"        =>  $custom_field["type"]."_".$custom_field["template_id"]."_".$custom_field["template_field_id"],
						"value"     =>  $custom_field["value"],
						"required"  =>  $required,
						"star"      =>  $star
					)
				);
			}
			else if ( $custom_field["type"] == "dropdown" ) {
				$list_items = $this->db->getListItems( $custom_field["list_id"] );
				$items = '';
				foreach ( $list_items as $list_item ) {
					$items .= $this->get_view(
						"options",
						false,
						array(
							"id"    =>  $list_item["item_id"]."_".$list_item["list_id"],
							"name"  =>  $list_item["item"]
						)
					);
				}
				$custom_fields_html .= $this->get_view(
					"custom_fields.select",
					false,
					array(
						"id"                =>  $custom_field["type"]."_".$custom_field["template_id"]."_".$custom_field["template_field_id"],
						"value"             =>  $custom_field["value"],
						"select_options"    =>  $items,
						"required"          =>  $required,
						"star"              =>  $star
					)
				);
			}
			else if ( $custom_field["type"] == "checkbox_group"
			          || $custom_field["type"] == "radio_button_group") {
				$type = '';
				if ( $custom_field["type"] == "checkbox_group" ) {
					$type = "checkbox";
				}
				else if ( $custom_field["type"] == "radio_button_group" ) {
					$type = "radio";
				}
				$list_items = $this->db->getListItems( $custom_field["list_id"] );
				$items = '';
				foreach ( $list_items as $list_item ) {
					$items .= $this->get_view(
						"custom_fields.group.row",
						false,
						array(
							"id"    =>  $list_item["item_id"]."_".$list_item["list_id"],
							"value" =>  $list_item["item"],
							"type"  =>  $type,
							"name"  =>  $custom_field["type"]."_".$custom_field["template_id"]."_".$custom_field["list_id"]
						)
					);
				}
				$custom_fields_html .= $this->get_view(
					"custom_fields.group",
					false,
					array(
						"input_rows"    =>  $items,
						"value"         =>  $custom_field["value"],
						"field_type"    =>  "field_".$type
					)
				);
			}
			else if ( $custom_field["type"] == "aa" ) {
				$list_items = $this->db->getListItems( $custom_field["list_id"] );
				$items = '';
				foreach ( $list_items as $list_item ) {
					$items .= $this->get_view(
						"custom_fields.group.row",
						false,
						array(
							"id"    =>  $list_item["item_id"]."_".$list_item["list_id"],
							"value" =>  $list_item["item"],
							"type"  =>  "checkbox"
						)
					);
				}
			}
		}
		return $custom_fields_html;
	}

	private function add_time_to_booked_time_slots( $index, $customer_id, $creation_time ) {

        /*$start = date("Y-m-d H:i:s",strtotime($this->userData["time"][$index]["start"]));
        $end = date("Y-m-d H:i:s",strtotime($this->userData["time"][$index]["end"]));
        $timing_id = $this->userData["time"][$index]["selected_time_timing_id"];
        $member_id = $this->userData["time"][$index]["selected_time_member_id"];
        $service_id = $this->userData["service"][$index]["service_id"];

        $booked_slot = array(
            "service_id"    =>  $service_id,
            "member_id"     =>  $member_id,
            "vendor_id"     =>  $this->vendor_id,
            "customer_id"   =>  $customer_id,
            "timing_id"     =>  $timing_id,
            "date_start"    =>  $start,
            "date_end"      =>  $end,
            "is_booked"     =>  '1',
            "updated_at"    =>  $creation_time,
            "created_at"    =>  $creation_time
        );

        $booked_id_time_slot = $this->db->insert(
            "booked_time_slots",
            $booked_slot
        );

        return $booked_id_time_slot;*/

	}

	private function add_other_data_for_booked_time_slot( $id_time_slot, $creation_time ) {

		$other_data = array();

		foreach ( $this->userData["details"] as $index => $row ) {
			if ( $index != "vendor_id" && $index != "details-email" &&
			     $index != "details-name" && $index != "phone-name" ) {

				$other_data[$index] = $row;

			}
		}

		$slot_other_data = array(
			"id_time_slot"  =>  $id_time_slot,
			"data"          =>  serialize($other_data),
			"updated_at"    =>  $creation_time,
			"created_at"    =>  $creation_time
		);

		$id_slot_other_data = $this->db->insert(
			"booked_time_slots_other_data",
			$slot_other_data
		);

		return $id_time_slot;

	}

}

<?php
/**
 * @var array $payment_methods
 * @var string $order_view
 */
?>
<div class="col-12 payment-step">
    <div class="row">
        <div class="col-5 payment-column">
            <p><?php _e('Please tell us how you would like to pay','rbdobooking') ?></p>
            <div class="form-group">
		        <?php
		        foreach ( $payment_methods as $method ) {
			        ?>
                    <div class="radio">
                        <label>
                            <input type="radio" class="payment-method"
                                   name="payment-method"
                                   value="<?php echo $method["payment_method_id"] ?>" />
                            <span><?php _e('I will pay with','rbdobooking') ?> <?php echo $method["name"] ?></span>
                        </label>
                    </div>
			        <?php
		        }
		        ?>
            </div>
            <!-- Coupons part -->
            <?php
            do_action('rbdobooking_coupons_view','default');
            ?>
        </div>
        <?php echo $order_view; ?>
    </div>
</div>
<div class="col-12 cart-step">
	<div class="cart-text">
		<div><?php _e('Below you can find the services selected for booking.','rbdobooking'); ?></div>
		<div><?php _e('Click Book more, if you want to book more services.','rbdobooking'); ?></div>
	</div>
	<button class="btn btn-default btn-book-more btn-theme-color"
            type="button"><?php _e('Book more','rbdobooking') ?></button>
	<table class="table table-striped">
		<thead>
		<tr>
			<td><?php _e('Service','rbdobooking') ?></td>
			<td><?php _e('Date','rbdobooking') ?></td>
			<td><?php _e('Time','rbdobooking') ?></td>
			<td><?php _e('Employee','rbdobooking') ?></td>
			<td><?php _e('Price','rbdobooking') ?></td>
			<td></td>
		</tr>
		</thead>
		<tbody>
        <?php
        foreach ( $all_possible_bookings as $index => $booking ) {
            ?>
            <tr data-iteration="<?php echo $booking["iteration"] ?>">
                <td><?php echo $booking["service_name"]; ?></td>
                <td><?php echo $booking["date"]; ?></td>
                <td><?php echo $booking["time"]; ?></td>
                <td><?php echo $booking["member_name"]; ?></td>
                <td><?php echo $booking["currency"] . $booking["price"]; ?></td>
                <td>
                    <button class="btn btn-circle btn-edit-booking btn-theme-color"
                            title="<?php _e('Edit booking','rbdobooking') ?>"
                            type="button">
                        <span class="dashicons dashicons-edit"></span>
                    </button>
                    &nbsp;&nbsp;
                    <button class="btn btn-circle btn-del-booking btn-theme-color"
                            title="<?php _e('Delete booking','rbdobooking') ?>"
                            type="button">
                        <span class="dashicons dashicons-trash"></span>
                    </button>
                </td>
            </tr>
            <?php
        }
        ?>
		</tbody>
	</table>
</div>
<li class="{{step_class}}">
	<div class="number-and-name">
		<span>{{number}}. </span>
		<span>{{name}}</span>
	</div>
	<div class="highlight-part"></div>
</li>
<div class="col-4">
    <label for="<?php echo $id; ?>"
           class="control-label theme-color"
	        <?php echo $required; ?>
            ><?php echo $value; ?></label>
    <select id="<?php echo $id; ?>" class="form-control details-input"
            data-type="input"
            name="<?php echo $id; ?>">
	    <?php echo $select_options; ?>
    </select>
</div>
<?php
/**
 * @var $service
 * @var $member
 * @var $time
 * @var $currency
 * @var $price
 * @var $custom_fields
**/
?>
<div class="col-12 details-step">
    <div class="details-text">
        <?php _e('You selected to book','rbdobooking') ?>
        <span class="bold"> <?php echo $service; ?> </span> <?php _e('by','rbdobooking') ?>
        <span class="bold"> <?php echo $member; ?> </span>
        <?php _e('at','rbdobooking') ?> <span class="bold"> <?php echo $time; ?> </span> <?php _e('on','rbdobooking') ?>
        <span class="bold"> <?php echo $date; ?> </span>.
        <?php _e('Price for the service is','rbdobooking') ?> <span class="bold"><?php echo $currency.$price; ?></span>.
        <?php _e('Please provide your details in the form below to proceed with booking.','rbdobooking') ?>
    </div>
    <div class="details-form form-horizontal">
        <div class="row">
            <div class="col-4 form-group">
                <label class="control-label theme-color"><?php _e('Name','rbdobooking') ?></label>
                <input type="text" name="details-name" data-type="input"
                       class="form-control details-name details-input" />
            </div>
            <div class="col-4 form-group">
                <label class="control-label theme-color"><?php _e('Phone','rbdobooking') ?></label>
                <input type="text" name="details-phone" data-type="input"
                       class="form-control details-phone details-input" />
            </div>
            <div class="col-4 form-group">
                <label class="control-label theme-color"><?php _e('Email','rbdobooking') ?></label>
                <input type="text" name="details-email" data-type="input"
                       class="form-control details-email details-input" />
            </div>
        </div>
        <div class="row">
            <?php echo $custom_fields; ?>
        </div>
    </div>
</div>
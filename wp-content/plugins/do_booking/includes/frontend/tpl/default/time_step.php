<div class="col-12 time-step">
    <p><?php
        _e('Below you can find service time slots for service name by service is','rbdobooking');
        ?></p>
    <div class="row calendar-dates">
        <?php
        $single_day = '';
        foreach ( $calendar_time_slots as $day_row ) {
	        $day = date("Y-m-d",$day_row["timestamp"]);
	        $slots = '';
	        foreach ( $day_row["slots"] as $time_slot ) {
		        $start = date("H:i:s",$time_slot["start"]);
		        $end = date("H:i:s",$time_slot["end"]);
		        $start_in_hrs_secs = date("H:i",$time_slot["start"]);
		        $end_in_hrs_secs = date("H:i",$time_slot["end"]);
		        $slots .= $this->get_view(
			        "slot",
			        false,
			        array(
				        "start"         =>  $start_in_hrs_secs,
				        "end"           =>  $end_in_hrs_secs,
				        "start_format"  =>  $day . " " . $start,
				        "end_format"    =>  $day . " " . $end,
				        "timing_ids"     =>  $time_slot["timing_ids"],
                        "member_ids"    =>  $time_slot["member_ids"]
			        )
		        );
            }

	        $day = $this->get_view(
		        "day",
		        false,
		        array(
			        "day"           =>  $day,
			        "day_name"    => date("l",$day_row["timestamp"])
		        )
	        );

	        $single_day .= $this->get_view(
		        "single_day",
		        false,
		        array("day"=>$day,"slots"=>$slots)
	        );
        }
        echo $single_day;
        ?>
    </div>
</div>
<div class="time" data-start="<?php echo $start_format; ?>"
           data-timing_ids="<?php echo $timing_ids; ?>"
            data-member_ids="<?php echo $member_ids ?>"
           data-end="<?php echo $end_format; ?>">
    <span class="dashicons dashicons-update spin-icon spin-hide"></span>
    <!--<span class="time-marker"></span>-->
    <span><?php echo $start; ?></span>
    <span>-</span>
    <span><?php echo $end; ?></span>
</div>
<div id="rbdobooking-booking-form" class="rbdobooking-bootstrap-wrapper">

    <form action="" method="post">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-12"><?php _e('Booking','rbdobooking'); ?></div>
                </div>
            </div>
        </div>
        <div class="container">
        <div class="template-form template-<?php echo $template; ?>">
	        <?php echo $step_container; ?>
            <div class="row step-content">
	            <?php echo $step; ?>
            </div>
            <div class="row footer">
                <div class="col-12">
                    <div class="back-next-btn-row">
                        <div class="float-left">
                            <button class="btn btn-default btn-previous"
                                    type="button">
                                <span class="dashicons dashicons-update spin-icon spin-hide"></span>
                                <span><?php _e('Back','rbdobooking') ?></span>
                            </button>
                        </div>
                        <div class="float-right">
                            <button class="btn btn-default btn-next"
                                    type="button">
                                <span class="dashicons dashicons-update spin-icon spin-hide"></span>
						        <?php _e('Next','rbdobooking') ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
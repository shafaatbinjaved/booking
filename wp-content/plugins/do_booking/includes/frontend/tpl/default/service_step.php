<div class="col-12 form-horizontal service-step">
    <div class="default-other-color">
		<?php _e("Please select service:","rbdobooking"); ?>
    </div>
    <br>
    <div class="row">
        <div class="col-4 form-group">
            <label for="" class="control-label theme-color"><?php _e('Category','rbdobooking'); ?></label>
            <select class="form-control category-select read-option" name="category_id"
                    data-category_services="<?php echo $category_with_services; ?>"
                    data-category_members="<?php echo $category_with_members; ?>"
                    data-select_default="<?php _e("Select catgeory","rbdobooking") ?>">
                <option value=""><?php _e("Select catgeory","rbdobooking") ?></option>
				<?php echo $category_options; ?>
            </select>
        </div>
        <div class="col-4 form-group">
            <label for="" class="control-label theme-color"><?php _e('Service','rbdobooking'); ?></label>
            <select class="form-control service-select read-option" name="service_id"
                    data-service_members="<?php echo $service_with_members; ?>"
                    data-service_categories="<?php echo $service_with_categories; ?>"
                    data-select_default="<?php _e("Select service","rbdobooking") ?>">
                <option value=""><?php _e('Select service','rbdobooking') ?></option>
				<?php echo $service_options; ?>
            </select>
            <div class="rbdobooking-error"
                 data-error_msg="<?php _e('You have to select service in order to proceed further','rbdobooking'); ?>">
            </div>
        </div>
        <div class="col-4 form-group">
            <label for="" class="control-label theme-color"><?php _e('Employee','rbdobooking') ?></label>
            <select class="form-control member-select read-option" name="member_id"
                    data-select_default="<?php _e("Any","rbdobooking") ?>"
                    data-select_default_value="any">
                <option value="any"><?php _e("Any","rbdobooking") ?></option>
				<?php echo $member_options; ?>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <label for="" class="control-label default-other-color"><?php _e('available on or after','rbdobooking'); ?></label>
            <input type="text" name="date_available" class="form-control date-picker read-option" />
        </div>
        <div class="col-4">
            <ul class="days">
				<?php
				$days = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
				foreach ( $days as $index => $day ) {
					?>
                    <li class="single-day">
                        <div>
                            <div class="default-other-color"><?php echo $day ?></div>
                            <div class="day-select" data-day="<?php echo $index+1; ?>">
                                <span class="dashicons dashicons-yes day-include-uninclude day-include"></span>
                            </div>
                        </div>
                    </li>
					<?php
				}
				?>
            </ul>
        </div>
        <div class="col-3">
            <label for="" class="control-label default-other-color"><?php _e('Start from','rbdobooking') ?></label>
            <select class="form-control read-option" name="time-start">
				<?php echo $time_start_options; ?>
            </select>
        </div>
        <div class="col-3">
            <label for="" class="control-label default-other-color"><?php _e('Finish by','rbdobooking') ?></label>
            <select class="form-control read-option" name="time-end">
				<?php echo $time_end_options; ?>
            </select>
        </div>
    </div>
</div>
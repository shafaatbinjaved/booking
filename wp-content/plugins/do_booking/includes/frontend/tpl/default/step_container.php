<div class="row steps-container">
	<div class="col-12">
		<ul class="steps-list <?php echo $total_steps_class ?>">
            <?php
            $steps = '';
            foreach ( $all_steps as $step )
            {
                $steps .= '<li class="'.$step["step_class"].'">';
                $steps .= '<div class="number-and-name">';
                $steps .= '<span class="number">'.$step["number"].'. </span>';
                $steps .= '<span class="name">'.$step["name"].'</span>';
                $steps .= '</div>';
                $steps .= '<div class="highlight-part"></div>';
                $steps .= '</li>';
            }
            echo $steps;
            ?>
		</ul>
	</div>
</div>
<?php
/**
 * @var $time
 * @var $order_items
 * @var $include_tax
 * @var $currency
 * @var $tax_percent
 * @var array $coupons
*/
?>
<div class="col-7">
	<div class="order-view">
		<h3><?php _e('Your order','rbdobooking') ?></h3>
		<div class="order-time row">
            <div class="col-12 left-div">
                <div class="float-left"><?php _e('Time','rbdobooking') ?></div>
                <div class="float-right"><?php echo $time; ?></div>
                <div class="clear"></div>
            </div>
		</div>
		<div class="order-items">
			<?php
			$subtotal = 0.0;
			$only_tax = 0.0;
			$coupon_deduction = 0.0;
            $coupons_deduction_without_tax = 0.0;
            $coupons_deduction_only_tax = 0.0;
            //include_tax;
            foreach ( $order_items as $index => $order_item ) {
                ?>
                <div class="row order-item">
                    <div class="<?php echo ($include_tax ? "col-6" : "col-8") ?> product-staff-quantity">
                        <span class="product"><?php echo $order_item["service_name"] ?></span>
                        <span class="separator"><?php _e('with','rbdobooking') ?></span>
                        <span class="staff"><?php echo $order_item["member_full_name"] ?></span>
                        <span class="quantity">x 1</span>
                    </div>
                    <div class="<?php echo ($include_tax ? "col-2" : "col-4") ?> amount">
                        <span><?php
                            $item_val = floatval($order_item["member_price"]);
                            $subtotal += $item_val;
                            echo $currency . $item_val;
                            ?></span>
                    </div>
                    <?php
                    if ( $include_tax ) {
                        ?>
                        <div class="col-2 amount tax-percent">
                            <span><?php echo $order_item["tax_percent"].'%'; ?></span><br>
                            <span class="separator">(<?php _e('VAT','rbdobooking') ?>)</span>
                        </div>
                        <div class="col-2 amount tax-amount">
                        <span><?php
                            $item_val = floatval($order_item["tax_amount"]);
                            $only_tax += $item_val;
                            echo $currency . $order_item["member_price_with_tax"];
                            ?></span>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>
		</div>
		<div class="order-subtotal row">
            <div class="<?php echo ($include_tax ? "col-6" : "col-8") ?> left-div">
                <?php _e('Subtotal','rbdobooking') ?>
            </div>
            <div class="<?php echo ($include_tax ? "col-2" : "col-4") ?> amount right-div"><?php echo $currency . $subtotal; ?></div>
            <?php
            if ( $include_tax ) {
                ?>
                <div class="col-2"></div>
                <div class="col-2 amount right-div"><?php echo $currency; echo $subtotal+$only_tax; ?></div>
                <?php
            }
            ?>
		</div>
        <?php
        //..........
        do_action('rbdobooking_order_view_after_coupon_redeem', $subtotal, $only_tax, $coupons, $currency, 'default');
        //..........
        //..........
        $totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds = apply_filters(
            'rbdobooking_deduct_order_coupons',
            [
                'total_without_tax'  => $subtotal,
                'total_only_tax'  => $only_tax
            ],
            $coupons
        );
        $subtotal = $totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds['total_without_tax'];
        $only_tax = $totalWithoutTaxAndOnlyTaxAfterApplyingCouponAndCouponIds['total_only_tax'];
        //...........
        ?>
		<div class="order-total row">
            <div class="col-6 left-div font-weight-bold">
                <?php
                $subtotal = round($subtotal,2);
                $only_tax = round($only_tax,2);
                $total = round((float)$subtotal+$only_tax,2);//-$coupon_deduction;
                _e('Total','rbdobooking');
                ?>
            </div>
            <!--<div class="col-5 amount right-div"><?php /*echo $currency . $total */?></div>-->
            <div class="col-2 amount right-div"><?php echo $currency . $subtotal ?></div>
            <div class="col-2 amount right-div"><?php echo $currency . $only_tax ?></div>
            <div class="col-2 amount right-div"><?php echo $currency . $total ?></div>
		</div>
	</div>
</div>
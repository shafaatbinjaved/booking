<?php
/**
 * User: shafaatbinjaved
 * Date: 5/28/2018
 * Time: 10:36 PM
 */

require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-db.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/class-view.php';
require_once RBDOBOOKING_PLUGIN_DIR . 'includes/frontend/tpl/class-default.php';

class RBDoBooking_Frontend extends View {

	private $db;

	/**
	 * Primary class contructor
	 *
	 * @since 1.0.0
	*/
	public function __construct() {

		parent::__construct(plugin_dir_path( __FILE__ ) . 'tpl/');

		add_action('wp_enqueue_scripts',array($this,'frontend_scripts_and_styles'));
		// Register shortcode
		add_shortcode('rb_do_booking_form',array($this,'shortcode'));

		add_action('wp_ajax_nopriv_rbdobooking_form',array($this,'form_ajax_call'));
		add_action('wp_ajax_rbdobooking_form',array($this,'form_ajax_call'));

		$this->db = RBDoBooking_DB::instance();
	}

	function form_ajax_call() {
		check_ajax_referer('rbdobooking-front-form-nonce','security');

		if ( $_POST["type"] == "next-step" ) {
			$this->next_step_form( $_POST, "next" );
		}
		else if ( $_POST["type"] == "back-step" ) {
			$this->next_step_form( $_POST, "back" );
		}

		die();
	}

	private function next_step_form( $data, $direction ) {
		$vendor_id = $data["vendor_id"];
		$cart_enable_disabled = $this->db->getVendorSpecificSettingValueByName( "cart_enable", $vendor_id, 'cart');
		$vendor_steps_configuration_with_template_name = $this->db->getVendorStepConfigurationWithTemplateName( $cart_enable_disabled, $vendor_id );

		$template = $vendor_steps_configuration_with_template_name["template"];

		$steps = explode(",",$vendor_steps_configuration_with_template_name["steps"]);

		$next_step = array_search($data["current_step"],$steps);

		$template_class = null;

		if ( $template == "default" ) {
			$template_class = RBDoBooking_Frontend_Default::instance(
				$steps,
				$data["form_id"],
				$vendor_id
			);

			if ( $data["action_type"] == "submit" ) {
				$template_class->submit_step_content(
					$steps[$next_step],
					$data
				);
			}

			if ( isset($data["cart_number"]) ) {
				$template_class->setCartNo((int)$data["cart_number"]);
			}

			if ( $direction == "next" ) {
				$next_step++;
			}
			else if ( $direction == "back" ) {
				$next_step--;
			}
			$html = $template_class->prepare_step_content(
				$steps[$next_step]
			);
		}

	}

	/**
	 * Shortcode
	 *
	 * @since 1.0.0
	*/
	function shortcode( $atts ) {
		$a = shortcode_atts(array(
		), $atts );

		$vendor_id = null;
		if ( isset($atts['vendor_id']) ) {
			$vendor_id = $atts['vendor_id'];
		}
		else {
			$vendor_id = 1;
		}

		$html = '';

		$cart_enable_disabled = $this->db->getVendorSpecificSettingValueByName( "cart_enable", $vendor_id, 'cart');
		$vendor_steps_configuration_with_template_name = $this->db->getVendorStepConfigurationWithTemplateName( $cart_enable_disabled, $vendor_id );
		$template = '';

		if ( !is_null($vendor_steps_configuration_with_template_name) ) {

			$template = $vendor_steps_configuration_with_template_name["template"];

			$vendor_steps_configuration = $vendor_steps_configuration_with_template_name["steps"];
			$vendor_steps_configuration = explode(",",$vendor_steps_configuration);

			$template_class = null;

			if ( $template == "default" ) {
				$template_class = RBDoBooking_Frontend_Default::instance(
					$vendor_steps_configuration,
					"",
					$vendor_id
				);
				$html = $template_class->prepare_step_content(
					$vendor_steps_configuration[0]
				);
				$template_class->shortcode_scripts(
					$vendor_id,
					$vendor_steps_configuration[0]
				);
			}
		}
		return $html;
	}

	function frontend_scripts_and_styles() {
		//wp_register_script('jquery');
		wp_register_style(
			'rbdobooking-bootstrap',
			RBDOBOOKING_PLUGIN_URL . 'assets/css/bootstrap/bootstrap.css',
			array(),
			RBDOBOOKING_VERSION
		);
		wp_register_style(
			'rbdobooking-booking-form-style',
			RBDOBOOKING_PLUGIN_URL . 'assets/css/booking_form.css',
			array(),
			RBDOBOOKING_VERSION
		);
		wp_localize_script( 'jquery', 'RBDoBookingAjax', array(
			// URL to wp-admin/admin-ajax.php to process the request
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			// generate a nonce with a unique ID "myajax-post-comment-nonce"
			// so that you can check it later when an AJAX request is sent
			'security_rbdobooking_form' => wp_create_nonce( 'rbdobooking-front-form-nonce' )
		));
	}

}
new RBDoBooking_Frontend();